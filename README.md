# Ale House Rocks

[![Netlify Status](https://api.netlify.com/api/v1/badges/6ee87e02-ebd7-43a1-b71a-2d479683385d/deploy-status)](https://app.netlify.com/sites/ale-house-rock/deploys)

Beer reviews, hosted on Netlify, powered by 11ty.

Can be found at https://alehouse.rocks/

## Setup

Issues and MRs are welcome.

If you wish to run locally:

- `git clone`
- `npm i`
- `npm run watch` - this will spin up a server with auto refresh.
