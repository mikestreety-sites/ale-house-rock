---
title: Sori Brewing
permalink: brewery/sori-brewing/
location: 'Tallinn, Harjumaa Estonia'
style: Micro Brewery
website: 'https://www.soribrewing.com'
instagram: 'http://instagram.com/soribrewing'
twitter: 'https://twitter.com/soribrewing'
facebook: 'https://www.facebook.com/soribrewing'
untappd: 'https://untappd.com/SoriBrewingCo'
---

Serious beer for not so serious people. Sori Brewing is a talented team of creators in Tallinn, Estonia. Brewing in an old Soviet Military building, where the ambience feels as abnormal as the beer. Daring artisans treasuring quality, while playing with the impossible and constantly pushing boundaries. It's Serious Beer for not so Serious People. We are Sori - but not sorry.
