---
title: Ska Brewing
location: 'Durango, CO United States'
style: Regional Brewery
website: 'http://www.skabrewing.com'
instagram: 'http://instagram.com/skabrewing'
twitter: 'https://twitter.com/skabrewing'
facebook: 'https://www.facebook.com/skabrewing'
untappd: 'https://untappd.com/SkaBrewing'
permalink: brewery/ska-brewing/
---

Somewhere out in Colorado, in the year nineteen hundred and ninety-five, two guys named Dave and Bill learned that while they loved gulping down good beer, they weren’t yet old enough to buy it. They figured an answer to their quandary would appear if they drank enough and listened to enough thinking music, also known as Ska. On the second Skaturday of Skatember it hit them. If they brewed their own beer they’d have all the beer they could ever want. And while they were at it, why not brew the most magnificent suds ever quaffed in their neck of the woods…or any other neck for that matter.
