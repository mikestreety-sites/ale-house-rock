---
title: Krönleins Bryggeri
permalink: brewery/kronleins-bryggeri/
location: 'Halmstad, Hallands län Sweden'
style: Macro Brewery
website: 'https://www.kronleins.se'
instagram: 'http://instagram.com/kronleinsbryggeriab'
twitter: 'https://twitter.com/Kronleins'
facebook: 'https://www.facebook.com/KronleinsBryggeri'
untappd: 'https://untappd.com/KrnleinsBryggeri'
---

