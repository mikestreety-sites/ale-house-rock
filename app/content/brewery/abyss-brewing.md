---
title: ABYSS Brewing
permalink: brewery/abyss-brewing/
location: 'Lewes, East Sussex England'
style: Micro Brewery
website: 'https://abyssbrewing.co.uk'
instagram: 'http://instagram.com/abyssbrewing'
twitter: 'https://twitter.com/abyssbrewing'
facebook: 'https://www.facebook.com/abyssbrewing'
untappd: 'https://untappd.com/ABYSSBrewing'
---

ABYSS is a forward thinking Micro brewery born in the cellar of The Pelham Arms, Lewes, in 2016. From dark to light we brew modernbeers with BIG personalities. In 2020 we won the BREW LDN Accelerator Competition beating 19 other fledgling breweries to the prize. And to end the year we successfully completed a Crowdfunder to help build the ABYSS Brewery + Tap on the other side of Lewes, opening it's doors in October 2021. We believe in the power of beer to bring people together, we’ve always placed collaboration at the heart of what we do, whether brewing beer or working with the creative community and local charities. To date we’ve collaborated with our friends at Burning Sky, Partizan Brewing, Rivington Brewing Co, Villages, Hackney Brewery, Dig Brew, SALT Brewing, SMOD, ATOM Beers, Arundel Brewery and PLAY Brew Co, with many more to come. We curate awesome monthly and special events such as ABYSS EQUINOX, ABYSS Beer Cadets, Krazy Kraft Night, SUPERFUZZ Fest, ABYSS Cycle Club, ABYSS Pub Quiz, ABYSS Kayak Club, ABYSSMAS, and Camp ABYSS. Find out more on the events page or subscribe to our weekly newsletter.
