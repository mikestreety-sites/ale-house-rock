---
title: SingleCut Beersmiths
permalink: brewery/singlecut/
location: 'Astoria, NY United States'
style: Regional Brewery
website: 'http://www.SingleCut.com/'
instagram: 'http://instagram.com/SingleCutBeer'
twitter: 'https://twitter.com/SingleCutBeer'
facebook: 'https://www.facebook.com/SingleCutBeer/'
untappd: 'https://untappd.com/SingleCutBeersmiths'
---

Cranking out music obsessed award winning Lagers and IPAs since 2012. To all demanding a steadfast meticulousness towards everything that can fit into a pint glass - Welcome. Always Drink Loud.
