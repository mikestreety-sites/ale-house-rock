---
title: Good Things Brewing Co
permalink: brewery/good-things-brewing-company/
location: 'Sandhill Lane, East Sussex England'
style: Micro Brewery
website: 'http://www.goodthingsbrewing.co'
instagram: 'http://instagram.com/goodthingsbrewingco'
twitter: 'https://twitter.com/goodthingsbrco'
facebook: 'http://facebook.com/goodthingsbrewing'
untappd: 'https://untappd.com/GoodThingsBrewingCo'
---

We’re on a journey to make the craft of brewing the beers we love, better for the world we love. We are the first closed loop, fully sustainable brewery. We bore our own water, create our own power and turn our spent brewers grain in to flour which is used all over from pizza restaurants to bakeries. In July 2021 the brewery was tragically struck by lightning and the resulting fire burnt the site to the ground. The brewery relaunched in October 2022 as Allkin Brewing.
