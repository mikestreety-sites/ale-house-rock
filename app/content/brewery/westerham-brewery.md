---
title: Westerham Brewery Company Ltd.
permalink: brewery/westerham-brewery/
aliases:
  - westerham
  - westerham-brewery-company-ltd
location: 'Westerham, Kent England'
style: Micro Brewery
website: 'http://westerhambrewery.co.uk'
instagram: 'http://instagram.com/westerhambrew'
twitter: 'https://twitter.com/WesterhamBrew'
facebook: 'http://www.facebook.com/WesterhamBrew'
untappd: 'https://untappd.com/WesterhamBrew'
---

Established in 2004 by Robert Wicks, the Westerham Brewery Company is proud to bring brewing back to the historic town of Westerham. With a rich brewing heritage, Westerham can once again be proud of locally brewed, top quality real ales. Producing exceptional craft beer for the local market, we support the initiative to reduce food miles in the supply chain. Many ‘national brands’ are transported long distances and suffer from poor storage and handling. Westerham Brewery Company has revived many of the much-loved flavours of the old Black Eagle Brewery, which closed in 1965 following the catastrophic consolidation by the ‘Big Brewers’ in the 1950’s. WATER The local water, which percolates through the Lower Greensand Ridge to the South of Westerham, is excellent for brewing pale bitter beers for which Westerham was once famous. MALT We use only the finest English winter malting barley, with the main pale ale malt derived from the Maris Otter variety. Maris Otter provides a distinctive depth of flavour that we think justifies the premium cost of the malt. HOPS We are mad about hops, specifically Kent hops. Why be a small brewery in Kent and not use the wonderful hops grown by our valiant hop growers. 96% of the hops we use are grown in Kent and we use 9 different Kent hops in the Spirit of Kent. YEAST We have re-cultured the yeast from the Black Eagle Brewery. Westerham Brewery has acquired the sole rights to these yeast strains from Carlsberg UK.
