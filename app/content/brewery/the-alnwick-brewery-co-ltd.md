---
title: Alnwick Brewing Co Ltd
permalink: brewery/the-alnwick-brewery-co-ltd/
location: 'Alnwick, Northumberland England'
style: Micro Brewery
website: 'http://www.alnwickbrewery.co.uk'
instagram: 'http://instagram.com/Alnwick_brewery'
twitter: 'https://twitter.com/alnwickbrewery1'
facebook: 'https://www.facebook.com/alnwickbrewery/'
untappd: 'https://untappd.com/w/alnwick-brewing-co-ltd/5778'
---

Small, award winning micro brewery in Alnwick Northumberland. Part of the Harry Hotspur Holdings group which also includes Alnwick Rum and Lindisfarne Mead
