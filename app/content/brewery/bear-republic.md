---
title: Bear Republic Brewing Co.
permalink: brewery/bear-republic/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-94_b02d2_hd.jpeg'
location: 'Rohnert Park, CA United States'
style: Regional Brewery
website: 'https://bearrepublic.com'
instagram: 'http://instagram.com/bearrepublic'
twitter: 'https://twitter.com/bearrepublic'
facebook: 'https://www.facebook.com/bearrepublic'
untappd: 'https://untappd.com/bearrepublic'
---

Bear Republic beers are brewed and aged following our time tested and proven methods. We hand select only the choicest ingredients for each batch. The specialty grains and hops used by Bear Republic have been chosen for their unique characteristics. The recipes and their final formulations were developed over a period of several years. Those wonderful home brewing sessions and the lessons learned from trialand error have established the solid foundations for each recipe’s commercial equivalents. It’s our goal to continually develop new recipes using our time tested brewing methods. We work hard at fostering a sense of loyalty and tradition from eras gone by.
