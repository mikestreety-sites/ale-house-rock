---
title: Ards Brewing Co.
location: 'Greyabbey, County Down Northern Ireland'
style: Micro Brewery
twitter: 'https://twitter.com/ardsbrewing'
facebook: 'https://www.facebook.com/ArdsBrewing'
untappd: 'https://untappd.com/brewery/25009'
permalink: brewery/ards-brewing-co/
---

