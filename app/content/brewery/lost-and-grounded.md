---
title: Lost and Grounded Brewers
permalink: brewery/lost-and-grounded/
location: 'Bristol, Bristol City England'
style: Micro Brewery
website: 'https://www.lostandgrounded.co.uk'
instagram: 'http://instagram.com/lostandgroundedbrewers'
twitter: 'https://twitter.com/lostandgrounded'
facebook: 'https://www.facebook.com/lostandgrounded'
untappd: 'https://untappd.com/LostAndGroundedBrewers'
---

Lost (adjective): not knowing one’s whereabouts. Grounded (adjective): a person who is sensible and has a good understanding of what is really important in life. Lost and Grounded Brewers is a brewery in Bristol, UK. We are particularly fascinated by the precision of German brewing and the idiosyncratic nature of Belgian beers, and we chose the brewhouse of our dreams that could help us make these wonders of fermentation. Our Taproom is open every Friday (4pm-10pm) and Saturday (1pm-10pm), and some Sundays for events – keep track on our social media channels! We have ample tables for walk-ups, and also take bookings on our online store!
