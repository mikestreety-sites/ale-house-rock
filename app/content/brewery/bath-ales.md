---
title: Bath Ales
permalink: brewery/bath-ales/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-1464_0c8da.jpeg'
location: 'Bristol, City of Bristol England'
style: Micro Brewery
website: 'http://www.bathales.com'
instagram: 'http://instagram.com/bathales'
twitter: 'https://twitter.com/bathales'
facebook: 'http://www.facebook.com/bathales'
untappd: 'https://untappd.com/BathAles'
---

Bath Ales is the brewer of a distinctive range of premium, authentic and inventive beers. Fronted by flagship ale Gem, its four-vessel brewhouse has the capacity to produce more than 50,000 brewers’ barrels of beer –over 14.5 million pints. Hare Brewery, which opened its doors in 2018, is one of the West Country’s most sophisticated and technologically advanced breweries. It has doubled the available brewing capacity at Bath Ales. Bath Ales’ beers are available in supermarkets nationwide and in pubs across thecountry. Favourite beers include Gem amber ale, Lansdown IPA, Monterey California pale ale, Cubic pale ale and Wild Hare gluten-free pale ale.
