---
title: Sadler's Ales
permalink: brewery/sadlers-ales/
location: 'Staveley Mill Yard, England'
style: Micro Brewery
website: 'http://www.sadlersales.co.uk/'
instagram: 'http://instagram.com/sadlersbrewco'
twitter: 'https://twitter.com/sdpeakyblinder'
facebook: 'https://www.facebook.com/sadlersales.brewery/'
untappd: 'https://untappd.com/SadlersAles'
---

Founded in 1900 in the heart of the industrious Black Country, Sadler's is proud to produce the finest beers, brewed with experience and passion. Five generations of innovation and dedication have enabled us to brew beer that people truly love. Sadler’s was originally a family business thatbegan in Oldbury in 1861. Brewing later took place behind the Windsor Castle pub in Lye before being moved to the new brewery and bar, which opened in March 2015 and was extended in 2018. In 2019 Sadler’s was bought by Hawkshead Brewery and brewing ceased in Lye. Beers under the Sadlers brand are now brewed at Hawkshead’s site in Cumbria. Sadler’s two bars – the Taproom at Quinton and Brewer’s Social in Harborne – are not affected and continue to trade.
