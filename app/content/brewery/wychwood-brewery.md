---
title: Wychwood Brewery
permalink: brewery/wychwood-brewery/
location: 'Witney, Oxfordshire England'
style: Micro Brewery
website: 'https://hobgoblinbeer.co.uk'
instagram: 'http://instagram.com/hobgoblinbeer'
twitter: 'https://twitter.com/hobgoblinbeer'
facebook: 'https://www.facebook.com/hobgoblinbeer'
untappd: 'https://untappd.com/WychwoodBrewery'
---

Being different sits at the very heart of Hobgoblin's DNA, which naturally starts with our brewing ethos: challenging, experimental, fearless and always seeking the extraordinary. Head Brewer, Jon Tillson, and his eccentric but highly experienced team use entirely unique and highly secretive recipes to achieve the distinct colours and flavours found across the Hobgoblin and Wychwood range. It’s the ingrained knowledge, experience and intuition of our brewers that allows us to create our extraordinary range of different beers. There's nothing artificial about the Hobgoblin brand and there's certainly nothing artificial in our recipes.
