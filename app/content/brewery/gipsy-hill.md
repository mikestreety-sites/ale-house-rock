---
title: The Gipsy Hill Brewing Co.
permalink: brewery/gipsy-hill/
location: 'London, Greater London England'
style: Micro Brewery
website: 'https://www.gipsyhillbrew.com'
instagram: 'http://instagram.com/GipsyHillBrew'
twitter: 'https://twitter.com/GipsyHillBrew'
facebook: 'https://www.facebook.com/gipsyhillbrew'
untappd: 'https://untappd.com/GipsyHillBrewingCo'
---

Welcome to Gipsy Hill Brewery. Founded in 2014, we’re one of the largest independent breweries in London, on a mission to brew full-flavoured moreish brews, with nothing to hide and everything to give. Nestled at the base of Gipsy Hill in South London,a place brimming with generosity. We’ve been proudly employee-owned since 2021. Born out of family, cobbled together by two guys who wanted to make a stake in the brewing world. We are as considered with our actions as we are with our brewing. Concentrating on great taste and community. We choose the right route, not the easy. We won’t skimp on hops, flavour or giving back to those that matter. Giving everything we can, so you won’t end up feeling short changed. Brewed with more, never less.
