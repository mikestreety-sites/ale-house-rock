---
title: Left Handed Giant
permalink: brewery/left-handed-giant/
location: 'Bristol, City of Bristol England'
style: Micro Brewery
website: 'http://lefthandedgiant.com/'
instagram: 'http://instagram.com/Lefthandedgiantbrewing'
twitter: 'https://twitter.com/lhgbrewingco'
facebook: 'https://www.facebook.com/lefthandedgiant'
untappd: 'https://untappd.com/brewery/189129'
---

Brewing modern, progressive beer since 2015.
