---
title: Lowtide Brewing Co.
permalink: brewery/lowtide-brewing-co/
location: 'Bath, Bath and North East Somerset England'
style: Contract Brewery
website: 'https://www.lowtidebrewingco.com'
instagram: 'http://instagram.com/lowtidebrewingco'
untappd: 'https://untappd.com/Lowtide_Brewing_Co'
---

Back in the summer of 2019, we began our exploration of alcohol-free craft beers. Struggling to find a single shop where we could get all of the AF beers we wanted, we decided to open our own. We sourced all of the best AF craft beers we could find from around the world and sell them to UK customers via a single website. While running the ‘Crafty AF’ site, our quest to find the perfect AF brew often left us wanting more. We felt shortchanged; there wasn’t a brewery that emanated truly craft AF beer, and there were so many beer types left unexplored. This is what led to Lowtide Brewing Co. Combining stellar can artwork, a sense of crafty defiance and deliciously different AF beer, we seek to bring true craft beer to the world of AF.
