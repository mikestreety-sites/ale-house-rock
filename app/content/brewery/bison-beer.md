---
title: Bison Beer
permalink: brewery/bison-beer/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-216203_e784a.jpeg'
location: 'Brighton, Brighton and Hove England'
style: Micro Brewery
website: 'http://bisonbeer.co.uk/'
instagram: 'http://instagram.com/bisonbeer'
twitter: 'https://twitter.com/bisonbeer'
facebook: 'http://bisonbeercrafthouse'
untappd: 'https://untappd.com/BisonBeerCrafthouse'
---

