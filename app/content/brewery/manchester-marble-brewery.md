---
title: Marble Beers Ltd
permalink: brewery/manchester-marble-brewery/
location: 'Manchester, Greater Manchester England'
style: Micro Brewery
website: 'http://www.marblebeers.com'
instagram: 'http://instagram.com/marblebrewers'
twitter: 'https://twitter.com/marblebrewers'
facebook: 'https://www.facebook.com/marblebrewers/'
untappd: 'https://untappd.com/MarbleBeers'
---

Brewing up a storm since 1997, Marble Brewery is a microbrewery based in the heart of Manchester, UK. We specialise in innovative cask, keg, can and bottled beer, as well as revitalising historic British styles. At the helm is Head Brewer, Joe Ince, who is leading the brewery's new range of hop forward beers, barrel ageing programme, and exciting new events. The brewery was originally housed in the rear of The Marble Arch Inn, and comprised of a four-and-a-half-barrel plant installed by Brendan Dobbin. The brewhouse was situated behind glass observation windows, where curious drinkers could view their beer being created. The fermenters and conditioning tanks were situated in the cellar below. Due to increased production, the brewery expanded in 2009 to twelve-barrel capacity, seeing production up sticks and move to a railway arch a short distance away and creating Manchester’s first railway arch brewery. With no current plans to brew above 5000hl per year, we spread our production over seven fermenters, four bright beer tanks, one conditioning tank and a small but beautifully formed barrel aging program. As a small brewery with limited expansion we are proud to have sent staff onto; Blackjack Brewery, Buxton Brewery, Cloudwater, Dan’s Brewery, Legitimate Industries, Magic Rock, and Thornbridge.
