---
title: Hambleton Brewery
permalink: brewery/nick-staffords-hambleton-ales/
location: 'Melmerby, North Yorkshire England'
style: Micro Brewery
website: 'http://www.hambletonbrewery.co.uk/'
instagram: 'http://instagram.com/hambletonbrewery'
facebook: 'https://www.facebook.com/hambletonbrewery'
untappd: 'https://untappd.com/HambletonAlesCo/beer'
---

Hambleton Brewery was born in 1991, when Nick Stafford decided to start a brewery at the bottom of his in-laws’ garden. Armed with nothing more than a pair of wellies, some old steel tanks and a rusty Peugeot 205, Nick built his brewery with blood, sweat and tears. Why did he do this? Simply because he wanted to make great beer. Today’s demand has meant we have moved out of the garden, and we also have some slightly shinier tanks, but Nick’s original ambition still stands. We love making great beer. That’s what we do best. And everything we do at Hambleton Brewery revolves around this fact. So whether you are drinking one of our much loved cask, keg or bottled beers, or trying one of our monthly specials, you can be sure that you are drinking a beer of outstanding quality, brewed with real passion by people who just want one simple thing – to make great beer.
