---
title: Brasserie d'Achouffe
location: 'Houffalize, Région Wallonne Belgium'
style: Regional Brewery
website: 'https://chouffe.com'
instagram: 'http://instagram.com/chouffe_belgium'
facebook: 'https://www.facebook.com/chouffebelgium'
untappd: 'https://untappd.com/brasseriedachouffe'
permalink: brewery/brasserie-dachouffe/
---

Towards the end of the 1970s, Pierre Gobron and Christian Bauweraerts (two brother-in-laws) decided to create their own beer in their own brewery. With initially only a small amount of money available to them (200.000Bfr, less than 5.000€), they started out on what the fans of the brewery call the ‘Chouffe story’. At the beginning its founders thought of it as a hobby, but the brewery developed at such a rate that, one by one, they decided to devote themselves to the adventure full-time. The first ‘brassin’ (brewing mix) of LA CHOUFFE (49 litres) was finished on 27th August 1982. The elves of Achouffe were impatient to discover new countries, and their Dutch cousins were the first to give them a warm welcome. Even today, the Netherlands is the major destination of Achouffe Beers outside Belgium. Nowadays, more than 20 countries worldwide are supplied with our ‘nectar’ from the Ardennes forests. Coveted international awards have also been received year after year to reward their unique taste. Atthe end of the summer of 2006 the founders of the Brewery chose to entrust the future of their dear elves to the Duvel Moortgat Brewery. The group’s wish is to invest in Achouffe and to develop the full potential of the Brewery.
