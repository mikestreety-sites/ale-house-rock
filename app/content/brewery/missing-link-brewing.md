---
title: Missing Link Brewing
permalink: brewery/missing-link-brewing/
location: 'West Hoathly, West Sussex England'
style: Nano Brewery
website: 'http://www.missinglinkbrewing.com'
instagram: 'http://instagram.com/missinglinkbrewing'
twitter: 'https://twitter.com/misslinkbrewing'
facebook: 'https://www.facebook.com/missinglinkbrewing'
untappd: 'https://untappd.com/Missing_Link_Brewing'
---

Nestled in the depths of the Sussex woodland, we started out in 2016 as a missing link in the brewing industry - a place for aspiring new brewers to hone their craft on our state of the art unique to the UK brewing set-up. Four years later, with a beautiful taproom built on-site, we released our own brand - to, if we do say so ourselves, pretty damn positive acclaim. Our focus is balance; in the beers we produce, and with the environment we inhabit. This is Evolution.
