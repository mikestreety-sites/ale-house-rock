---
title: Brighton Bier
permalink: brewery/brighton-bier/
location: 'Brighton, East Sussex England'
style: Micro Brewery
website: 'http://www.brightonbier.com/'
instagram: 'http://instagram.com/brightonbier'
twitter: 'https://twitter.com/BrightonBier'
facebook: 'https://www.facebook.com/pages/Brighton-Bier-Co/233758013343716?fref=ts'
untappd: 'https://untappd.com/BrightonBier'
---

Brighton's original craft brewers. 100% independent. Est 2012. (The first production brewery in Brighton since 1973.) We primarily brew Pale Ales, IPA’s (West Coast and NEIPA), Porters and Stouts. And a rather good Pilsner! We specialise in building big flavours into beers with high drinkability at sessionable ABV’s. We don’t box tick styles just because they become fashionable. We believe that provenance and community are integral to the history of beer. We are therefore super focused on supplying our local area with the freshest world class beer. Our taproom is in Brighton. Because our brewery is in Brighton. 7x World Beer Awards and International Beer Challenge Gold medal winning brewery. We love cask beer and recognise its importance within UK beer culture. We respect the historic importance of German and Belgian brewers. We embrace the attitude, ingredients and flavour profiles of modern progressive beer styles and breweries from North America and beyond. We look forward to hosting you at our brewery taproom (Thur-Sat), or the Brighton Bierhaus (Edward Street) and Haus on the Hill (Southover Street).
