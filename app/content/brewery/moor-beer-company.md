---
title: Moor Beer Company
permalink: brewery/moor-beer-company/
location: 'Bristol, Bristol City England'
style: Micro Brewery
website: 'https://www.moorbeer.co.uk'
instagram: 'http://instagram.com/drinkmoorbeer'
twitter: 'https://twitter.com/drinkmoorbeer'
facebook: 'https://www.facebook.com/DrinkMoorBeer'
untappd: 'https://untappd.com/MoorBeerCompany'
---

Natural live beer since 2007. Moor pioneered the naturally hazy, unfined beer movement in 2007, firmly believing that adding isinglass finings (fish guts) to beer is unnecessary and harmful. Natural carbonation with live yeast is at the heart of everything we brew. It’s what makes our beers vegan friendly and what gives them such an enhanced flavour, aroma and mouthfeel that simply can’t be replicated. Live life and drink live!
