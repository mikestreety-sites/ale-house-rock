---
title: North
location: 'Leeds, West Yorkshire England'
style: Micro Brewery
aliases:
  - north
website: 'https://www.northbrewing.com'
instagram: 'http://instagram.com/northbrewco'
twitter: 'https://twitter.com/northbrewco'
facebook: 'https://www.facebook.com/northbrewingleeds'
untappd: 'https://untappd.com/NorthBrewingCo'
permalink: brewery/north-brewing-co/
---

North was founded in 2015 by John Gyngell and Christian Townsley, the pioneers behind legendary Leeds beer venue North Bar, which opened in 1997. Known as “the first craft beer bar in Britain”, North Bar influenced a new wave of modern beer bars and breweries across the country, including their own. In 2015 they turned their 10-year dream of making their own beer into a reality by opening a 15bbl brewery, featuring a 200-person capacity tap room, just half a mile from their original flagship bar. The core range and seasonal specials epitomise what they look for in beer: big juicy flavours with tonnes of hops; fun, playful and interesting flavour combinations; classics that demonstrate an uncompromising approach to quality.
