---
title: Duration Brewing
permalink: brewery/duration-brewing/
location: 'West Acre, Norfolk England'
style: Micro Brewery
website: 'https://durationbeer.com'
instagram: 'http://instagram.com/durationbeer'
twitter: 'https://twitter.com/durationbeer'
facebook: 'https://www.facebook.com/durationbeer'
untappd: 'https://untappd.com/DurationBeer'
---

Fresh Beers and Wile Ales brewed from nature with purpose. A destination farmhouse brewery in a medieval barn at Abbey Farm, West Acre Norfolk making beers that belong to showcase how beautifully brewing and agriculture can exist in tandem. Bates a South Carolinian brewer, former head brewer at Brew By Numbers has opened a high end facility with wood division in a medieval stone barn at West Acre Priory. Following two years of collaborative and nomadically brewed releases Duration is a modern brewery with a strong sense of history, place and time in it's offering of Everyday, Agricultural, Slow and Spon beers. Turtles All The Way Down - American Pale Sweeping Coast - Westie Happiness Runs - Westie Human Interface - Heller Bock Dripping Pitch West Coast IPA Remember When The Pub - West Coast IPA Shifting Baseline - Hoppy Pale Bet The Farm - Continental Pale Ebb & Flow - American Stout Small Doses - Lil Pilsner Deep Roots - Carrot Wit Strong Opinions, Held Lightly - IPA You End Up Where You Were - Table Pale Warm Hugs - Stout Lean Into Fear - New England IPA Ebb & Flow - American Stout Fortitude - Imperial Stout Task at Hand - IPA Bend Before You Break - Hoppy Pale Baubles of Vanity - Westie Doses - German Pilsner Concrete Realities - IPA Harvest Bier - Marzen Little Fanfare - Grisette Quiet Song - Wheat Beer Promise of Spring - Wheat Beer Petrichor - Beetroot IPA
