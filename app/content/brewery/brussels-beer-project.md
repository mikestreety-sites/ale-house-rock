---
title: Brussels Beer Project
permalink: brewery/brussels-beer-project/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-68062_274e4.jpeg'
location: 'Anderlecht, Brussels Belgium'
style: Regional Brewery
website: 'https://www.beerproject.be'
instagram: 'http://instagram.com/BrusselsBeerProject'
twitter: 'https://twitter.com/BeerProject_Bxl'
facebook: 'https://www.facebook.com/brusselsbeerproject'
untappd: 'https://untappd.com/BeerProject_Bxl'
---

Born in 2013, BBP represents an alternative view of Brussels: multicultural, contemporary & vibrant! Belgium is no dusty beer museum and BBP's on a mission to keep writing the beer story of today and tomorrow. Visit their 2 breweries in Brussels: - BBP Port Sud (beer garden in summer) - BBP Dansaert (lambic & mixed-ferm beers) Or one of our BBP Taprooms in Brussels, Paris or Tokyo.
