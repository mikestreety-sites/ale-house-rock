---
title: Anchor Brewing Company
permalink: brewery/anchor-brewing/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-3891_dbd73_hd.jpeg'
location: 'San Francisco, CA United States'
style: Macro Brewery
website: 'https://raiseanchor.anchorbrewing.com'
instagram: 'http://instagram.com/anchorbrewing'
twitter: 'https://twitter.com/AnchorBrewing'
facebook: 'https://www.facebook.com/anchorbrewing'
untappd: 'https://untappd.com/anchorbrewing'
---

Brewing hand-crafted beer in the heart of San Francisco since 1896.
