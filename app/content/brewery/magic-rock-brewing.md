---
title: Magic Rock Brewing
permalink: brewery/magic-rock-brewing/
location: 'Huddersfield, West Yorkshire England'
style: Regional Brewery
website: 'http://www.magicrockbrewing.com/'
instagram: 'http://instagram.com/magicrockbrewing'
twitter: 'https://twitter.com/MagicRockBrewCo'
facebook: 'http://www.facebook.com/pages/Magic-Rock-Brewing/137470926317005'
untappd: 'https://untappd.com/magicrockbrewing'
---

Founded in 2011, Magic Rock Brewing is a culmination of a lifelong passion for beer. Inspired by local brewing traditionand the vibrant US beer scene, Magic Rock Brewing draws inspiration from our local beer heritage and our peers in the global beer community to deliver a taste experience that is magically removed from the mundane. We make exceptional beer conjured with care and attention to detail; Beer that is flavourful, vibrant, satisfying and consistent; Beer that we love to drink ourselves and proudly serve to everyone. We package in keg, cask and can because we believe different dispense suits different experiences – so whether you’re outdoors, down at the pub, or at home – there is a Magic Rock beer for you. We understand that you don’t have time for bland, flavourless or inconsistent beer because neither do we. It’s time to sit back and Drink the Magic.
