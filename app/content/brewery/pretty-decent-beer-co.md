---
title: Pretty Decent Beer Co
location: 'London Borough of Waltham Forest, London England'
style: Micro Brewery
website: 'https://www.prettydecentbeer.co'
instagram: 'http://instagram.com/prettydecentbc'
twitter: 'https://twitter.com/prettydecentbc'
facebook: 'https://www.facebook.com/prettydecentbc'
untappd: 'https://untappd.com/PrettyDecentBeerCo'
permalink: brewery/pretty-decent-beer-co/
---

We brew modern, progressive, diverse, seasonal beer. Every beer sold includes a donation to causes driving change for good. Our OG spot - FG Taproom - is an inclusive neighbourhood bar where everyone feels welcome to sit down and enjoy themselves. We have 12 beers on tap, full wine list, and spirits from local legends Victory Gin. We also have gluten-free and no-alcohol beer, so something for everyone! Our second taproom - BHR Taproom - is still shiny new from Dec 2022, when we moved production to Unit 10 Uplands in E17, becoming the newest brewery to join the Blackhorse Beer Mile. BHR sits alongside; 20 taps, pouring the freshest beer straight from the brewery. We serve weekly special releases plus core favourites. Come say hi!
