---
title: Orkney Brewery
location: 'Stromness, Orkney Islands Scotland'
style: Micro Brewery
website: 'http://www.orkneybrewery.co.uk'
instagram: 'http://instagram.com/orkneybrewery123'
twitter: 'https://twitter.com/orkneybrewery'
facebook: 'https://www.facebook.com/orkneybrewery'
untappd: 'https://untappd.com/OrkneyBrewery'
permalink: brewery/orkney-brewery/
---

The Orkney Brewery was one of Scotland’s first independent micro–breweries and started brewing in 1988, in what was once an old schoolhouse in Quoyloo. We are a genuine island brewery that crafts a superb range of beers. North island values shape our resolute approach to brewing, resulting in world class, award-winning beers. We are honest, resilient, we work hard together and have an innate desire to perfect. We need to get it right first time, every time. We don’t believe in wasting resources and we are friends to the environment we dwell in. We brew iconic Scottish ales Dark Island, Red MacGregor, Dragonhead and Skull Splitter. In addition, we offer progressive pale ale Orkney Gold and the extraordinary Dark Island Reserve, aged in whisky oak casks. Norman Sinclair whose father had actually enjoyed his school years in the same building took over the tenure in 2006. He quickly developed a custom built brewhouse adjoining the schoolhouse and by doing so created the original classroom into a 5 Star visitor centre.
