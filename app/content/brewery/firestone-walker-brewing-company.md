---
title: Firestone Walker Brewing Company
permalink: brewery/firestone-walker-brewing-company/
location: 'Paso Robles, CA United States'
style: Regional Brewery
website: 'http://www.firestonebeer.com/'
instagram: 'http://instagram.com/firestonewalker'
twitter: 'https://twitter.com/FirestoneWalker'
facebook: 'https://www.facebook.com/firestone.walker/'
untappd: 'https://untappd.com/firestonewalker'
---

Founded by brothers-in-law Adam Firestone and David Walker in 1996, Firestone Walker Brewing Company is an innovative California beer company with three state-of-the-art brewing facilities. Firestone Walker’s main brewery in Paso Robles produces a diverse portfolio ranging from iconic pale ales to vintage barrel-aged beers. The Barrelworks facility in Buellton makes eccentric wild ales, while the Propagator pilot brewhouse in Venice specializes in R&D beers and limited local offerings.
