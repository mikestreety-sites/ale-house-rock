---
title: Glen Affric Brewery
permalink: brewery/glen-affric-brewery/
location: 'Birkenhead, Merseyside England'
style: Micro Brewery
website: 'http://glenaffricbrewery.com/'
instagram: 'http://instagram.com/Glenaffricbrew'
twitter: 'https://twitter.com/Glenaffricbrew'
facebook: 'https://www.facebook.com/GlenAffricBrew/'
untappd: 'https://untappd.com/GlenAffricBrewery'
---

Craft brewery in the Wirral. Co-founded by two brothers, Craig & Calum.
