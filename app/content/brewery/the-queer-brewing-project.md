---
title: The Queer Brewing Project
permalink: brewery/the-queer-brewing-project/
location: 'City of London, London England'
style: Contract Brewery
website: 'https://thequeerbrewingproject.com'
instagram: 'http://instagram.com/QueerBrewing'
twitter: 'https://twitter.com/QueerBrewing'
facebook: 'https://www.facebook.com/QueerBrewing'
untappd: 'https://untappd.com/TheQueerBrewingProject'
aliases:
  - 'queer-brewing'
---

Queer Brewing is a queer-owned brewery providing visibility and representation for LGBTQ+ people, using beer as a vehicle for social change. Our beers raise money for vital LGBTQ+ charities, as we work to make beer a little more approachable to those outside, and a little more, well, queer on the inside.
