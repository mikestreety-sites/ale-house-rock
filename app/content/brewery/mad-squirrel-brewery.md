---
title: Mad Squirrel Brewery
permalink: brewery/mad-squirrel-brewery/
location: 'Potten End, Hertfordshire England'
style: Micro Brewery
website: 'https://www.madsquirrelbrew.co.uk'
instagram: 'http://instagram.com/Madsquirrelbrew'
twitter: 'https://twitter.com/Madsquirrelbrew'
facebook: 'https://www.facebook.com/madsquirrelbrew'
untappd: 'https://untappd.com/MadSquirrelBrewery'
---

We are Mad Squirrel; a forward-thinking, contemporary brewery based just outside of London at the foot of the Chiltern Hills. From humble beginnings in 2010 we have continually grown and evolved, often in unexpected ways, but always with a boundless enthusiasm for quality, an inquisitive approach to flavour, and a restless desire to better ourselves. Our focus is on producing natural, unfiltered and unpasteurized beers that are creative and different, whilst also remaining commercial, enjoyable and sessionable. We never brew anything we wouldn't drink ourselves. Our brewery has recently been awarded SALSA+ accreditation, certifying outstanding production standards and hygiene practices, and guarantees a beer brewed to the highest calibre.
