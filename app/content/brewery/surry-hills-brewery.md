---
title: Surrey Hills Brewery
permalink: brewery/surry-hills-brewery/
location: 'Dorking, Surrey England'
style: Micro Brewery
website: 'http://www.surreyhills.co.uk/'
instagram: 'http://instagram.com/SurreyHillsBrew'
twitter: 'https://twitter.com/SurreyHillsBrew'
facebook: 'https://www.facebook.com/pages/Surrey-Hills-Brewery/188306917864031'
untappd: 'https://untappd.com/SurreyHillsBrewery'
---

Award-winning Independent Brewery based in the picturesque Surrey Hills. After starting out in the village of Shere, our current brewery is now to be found a few miles along the road at the Denbies Wine Estate just north of Dorking right next to Box Hill. Our range of ales has garnered a devoted following over the years and while most of our production goes into cask, from time to time we do bottle some of the output which never seems to take long to disappear from our shelves! Our flagship beer, Shere Drop, a hoppy and malty pale ale with subtle hints of grapefruit and lemon, was voted one of the Champion Beers of England in 2019.
