---
title: Belhaven Brewery
permalink: brewery/belhaven-brewery/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-98_76850_hd.jpeg'
location: 'Dunbar, East Lothian Scotland'
style: Regional Brewery
website: 'https://www.belhaven.co.uk'
instagram: 'http://instagram.com/belhavenbrews'
twitter: 'https://twitter.com/BelhavenBrews'
facebook: 'https://www.facebook.com/belhavenbrews'
untappd: 'https://untappd.com/BelhavenBrewery'
---

It is one of the oldest regional breweries in Scotland, with a commercial brewing history documented back to 1719, although it is probable that a brewery has existed on site since at least the middle ages. Belhaven Brewery was family owned until 1984 and since then has been subject to continual investment, and more recently the addition of a new Brewhouse in 2011. The portfolio of products brewed at Belhaven is diverse and includes cask ales such as Belhaven IPA and 80/- as well as keg products, including Belhaven’s flagship brand and Scotland’s number one ale brand, Belhaven Best. You’ll find Belhaven beers across the globe, exported as far away as Australia.
