---
title: GlassHouse Beer Co
permalink: brewery/glasshouse-beer-co/
location: 'Birmingham, West Midlands Combined Authority England'
style: Micro Brewery
website: 'https://www.glasshousebeer.co.uk'
instagram: 'http://instagram.com/glasshousebeerco'
twitter: 'https://twitter.com/GlassHouseBeers'
facebook: 'https://www.facebook.com/glasshousebeerco'
untappd: 'https://untappd.com/GlassHouseBeerCo'
---

Brewing the freshest beer possible with a focus on balance and drinkability since 2016.
