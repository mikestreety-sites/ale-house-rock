---
title: Adnams
permalink: brewery/adnams/
location: 'Southwold, Suffolk England'
style: Micro Brewery
website: 'http://adnams.co.uk/'
instagram: 'http://instagram.com/adnams'
twitter: 'https://twitter.com/adnams'
facebook: 'http://facebook.com/adnams'
untappd: 'https://untappd.com/adnams'
---

Behind the walls of our Victorian-looking brewery, we’ve got some of the most advanced and energy-efficient brewing equipment in Europe. We’re proud of our long history of brewing, and we are constantly experimenting with new, innovative techniques. We’ve developed a network of great friends all over the brewing world, and we’re keen to share our knowledge and learn from others too. Adnams brews a range of beers which reflect our heritage but also challenge conventions. Each one has its own unique and vibrant character that will appeal to drinkers who cherish individuality and seek out beers with personality and style. We use locally-grown East Anglian grains of malted barley, rye, wheat and oats wherever possible, and deploy a number of different hop varieties from Britain as well as across the world – all of which contribute to our beers’ individual characters.
