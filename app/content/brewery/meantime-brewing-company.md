---
title: Meantime Brewing Company
permalink: brewery/meantime-brewing-company/
aliases:
  - greenwich-brewing-company
location: 'City of London, London England'
style: Macro Brewery
website: 'http://www.meantimebrewing.com'
instagram: 'http://instagram.com/meantimebrewing'
twitter: 'https://twitter.com/MeantimeBrewing'
facebook: 'http://www.facebook.com/meantimebrewing'
untappd: 'https://untappd.com/MeantimeBrewingCompany'
---

Based in Greenwich and fiercely proud of its London roots, Meantime has won a worldwide reputation for beers of great quality and provenance. Meantime was the first British brewery to win any medals at the World Beer Cup in 2004 and has continued to do so since. Our mission is to produce great beers in a variety of styles full of taste and flavour, we also provide the knowledge and understanding necessary to appreciate the Craft Beer culture and the beers that come from it.
