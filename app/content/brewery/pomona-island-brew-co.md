---
title: Pomona Island Brew Co.
permalink: brewery/pomona-island-brew-co/
aliases:
  - pomonda-island
location: 'Salford, Greater Manchester England'
style: Micro Brewery
website: 'https://www.pomonaislandbrew.co.uk'
instagram: 'http://instagram.com/Pomonaislandbrew'
twitter: 'https://twitter.com/Pomonaisland'
facebook: 'https://www.facebook.com/Pomona.Island'
untappd: 'https://untappd.com/PomonaIslandBrewCo'
---

Award-winning brewery based in Salford. Formed in 2017 by Nick and Ryan, who started the Gas Lamp bar in Manchester, Gaz from Marble Brewery and James from Tempest Brewery, Pomona Island brewery is based in Salford, around the corner from Media City. Our beers range from sessionable pales and juicy IPAs to more traditional stouts and experimental sours. Our new pub, Northwestward Ho is coming to Manchester Summer 2023.
