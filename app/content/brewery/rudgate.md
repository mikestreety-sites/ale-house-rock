---
title: Rudgate Brewery
permalink: brewery/rudgate/
location: 'Tockwith, North Yorkshire England'
style: Micro Brewery
website: 'http://www.rudgatebrewery.co.uk/'
instagram: 'http://instagram.com/rudgatebrewery'
twitter: 'https://twitter.com/rudgatebrewery'
facebook: >-
  https://www.facebook.com/rudgatebrewery/?hc_ref=ARQxFHT8f092q43E1AUZAptBLPd9WO-n_0swzSiPEb_Zad1I-oYYtsI-tOu53v3K4xU&pnref=story
untappd: 'https://untappd.com/RudgateBrewery'
---

Rudgate Brewery was established in 1992, in the heart of Yorkshire - the Vale of York. The original brewery was in the former ammunition building of RAF Marston Moor Airfield, which was home to the Halifax bombers that helped defend the UK during World War 2. The name comes from the old Roman road of `Rudgate’ which runs through the airfield - the road that led the Vikings along our vale, defeating the Romans en route. This history is why our main main theme is Vikings! The brewery expanded into a modern new facility in 2010/11 on the same site. We are nationally recognised with several awards to our name, including CAMRA Overall Champion Beer of Britain 2009. All Marston Moor beers are brewed here after Rudgate took them over in 2004.
