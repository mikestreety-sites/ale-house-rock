---
title: Loch Lomond Brewery
permalink: brewery/loch-lomond-brewery/
location: 'Alexandria, West Dunbartonshire Scotland'
style: Micro Brewery
aliases:
  - loch-loman-brewery
website: 'http://www.lochlomondbrewery.com'
instagram: 'http://instagram.com/Lochlomondbrewery'
twitter: 'https://twitter.com/LochLomondBrew'
facebook: 'http://www.facebook.com/pages/Loch-Lomond-Brewery/192251900846207'
untappd: 'https://untappd.com/lochlomondbrewery'
---

Family owned and run brewery established in 2011 near the shores of Loch Lomond in Scotland making award winning beer in cask, bottle, keg and can. SIBA Cask Champion of Scotland 2015 SIBA Supreme Cask Champion of UK 2016 SIBA Cask and bottle Champion of Scotland 2017 CAMRA Champion Beer of Scotland 2017
