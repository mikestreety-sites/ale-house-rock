---
title: Elusive Brewing
location: 'Finchampstead, Wokingham England'
style: Micro Brewery
website: 'https://www.elusivebrewing.com'
instagram: 'http://instagram.com/elusivebrew'
twitter: 'https://twitter.com/ElusiveBrew'
facebook: 'https://www.facebook.com/elusivebrewing'
untappd: 'https://untappd.com/ElusiveBrewing'
permalink: brewery/elusive-brewing/
---

Elusive Brewing creates beer with one eye on tradition and the other on taking things to the next level. Founded by a home brewer who went on to win awards at national level, from our small brewery in Finchampstead, Berkshire we’d like to share our game with you. Insert coin to play. Tap Room open 2-8 Friday and 12-8 Saturday.
