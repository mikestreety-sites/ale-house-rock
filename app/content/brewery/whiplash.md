---
title: Whiplash
permalink: brewery/whiplash/
location: 'Dublin, County Dublin Ireland'
style: Micro Brewery
website: 'https://www.whiplashbeer.com'
instagram: 'http://instagram.com/whiplashbeer'
twitter: 'https://twitter.com/whiplashbeer'
facebook: 'https://www.facebook.com/whiplashbeer'
untappd: 'https://untappd.com/Whiplashbeer'
---

A weekend project that has since ruined our lives, now complete with Dublin brewery and tears. Brewing and canning on site with a team of 12, lashing out the beers we want to see more of.
