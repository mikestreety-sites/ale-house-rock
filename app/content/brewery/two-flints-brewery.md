---
title: Two Flints Brewery
location: 'Windsor, Berkshire England'
style: Micro Brewery
website: 'https://www.twoflintsbrewery.com'
instagram: 'http://instagram.com/twoflintsbrewery'
twitter: 'https://twitter.com/two_flints'
facebook: 'https://facebook.com/twoflintsbrewery'
untappd: 'https://untappd.com/brewery/532412'
permalink: brewery/two-flints-brewery/
---

Two Flints are a modern-focused, independent brewery nestled under a railway arch under Windsor Central Station. We believe in the balance between heritage and innovation, nuance and drinkability; revelling in the open-minded nature of modern, seasonal brewing and the sensory experiences that come with it. With this ethos, we offer a rotating beer list that balances our quest for hop-forward innovation with classic styles, taking inspiration from the brewers and beers that have come before us. Our mission is to bring the latest innovations in modern beer drawing from influences on our travels around the world. Our ultimate aim is to be one of the most innovative and exciting breweries in the UK.
