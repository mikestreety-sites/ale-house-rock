---
title: Farm Yard Brew Co
permalink: brewery/farm-yard-brew-co/
location: 'Cockerham, Lancashire England'
style: Micro Brewery
website: 'http://farmyardbrew.co.uk/'
instagram: 'http://instagram.com/farmyardbrewco'
twitter: 'https://twitter.com/farmyardbrewco'
facebook: 'https://www.facebook.com/farmyardbrewco'
untappd: 'https://untappd.com/FarmYardAles'
---

A 6th generation farmer decided to change his family farm's offering to something that fit into a pint glass and thus Farm Yard Ales was born! We're yet to find a style of beer we don't like so we brew everything from the weird & wonderful to the more modest and traditional. At the end of the day, we just want to make good beer for good folk, join us for a pint someday.
