---
title: Robinsons Brewery
permalink: brewery/robinsons-brewery/
location: 'Stockport, Greater Manchester England'
style: Regional Brewery
website: 'https://www.robinsonsbrewery.com'
instagram: 'http://instagram.com/robinsonsbrewery'
twitter: 'https://twitter.com/robbiesbrewery'
facebook: 'https://www.facebook.com/RobinsonsBrewery'
untappd: 'https://untappd.com/RobinsonsBrewery'
---

Based in the heart of Stockport for over 181-years, owning a collection of over 260 pubs, inns and hotels across the North West and North Wales, Robinsons is one of the most respected names in British brewing and innkeeping. A proud family of independent brewers, we operate one of the most advanced and sophisticated breweries in the UK (home to the largest Hopnik in the world) with a global reputation for producing traditional real ale. We apply decades of experience to create exciting new varieties and choices of flavours that always lead us towards the next Robinsons’ award-winning beer. The Unicorn Brewery rests over the foundations of the Unicorn pub, marking the beginning of a family business which continues to thrive 181 years later.
