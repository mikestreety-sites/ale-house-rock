---
title: DEYA Brewing Company
permalink: brewery/deya-brewing-company/
location: 'Cheltenham, Gloucestershire England'
style: Micro Brewery
website: 'https://deyabrewing.com'
instagram: 'http://instagram.com/deyabrewery'
twitter: 'https://twitter.com/deyabrewery'
facebook: 'https://www.facebook.com/DEYA-Brewing-Company-1591941271048230'
untappd: 'https://untappd.com/deyabrewing'
---

DEYA Brewing Company is an independent brewery in Cheltenham, UK. We focus on hoppy beers, lagers, traditional UK styles and mixed fermentation ales. We strive to make world class beer - we really do set out to brew the most delicious beer we possibly can and create a destination taproom focussing on on-site retail. Our on-site Taproom is atthe heart of our brewery and is open every week for fresh tins and delicious draft pours. We take a holistic approach to business, striving to be the best employer we possibly can be, while operating in a fair, sustainable and human driven way. We have come a long way in the short period of time since 2015, and each year looks set to be our busiest yet! Keep on Crocin’!
