---
title: The Kernel Brewery
location: 'London Borough of Southwark, London England'
style: Micro Brewery
aliases:
  - the-kernel
website: 'https://www.thekernelbrewery.com'
instagram: 'http://instagram.com/thekernelbrewery'
twitter: 'https://twitter.com/kernelbrewery'
facebook: 'https://www.facebook.com/pages/The-Kernel-Brewery/203846429748027'
untappd: 'https://untappd.com/TheKernelBrewery'
permalink: brewery/the-kernel-brewery/
---

The brewery springs from the need to have more good beer. Beer deserving of a certain attention. Beer that forces you to confront and consider what you are drinking. Upfront hops, lingering bitternesses, warming alcohols, bodies of malt. Lengths and depths of flavour. We make Pale Ales, IPA’s and old school London Porters and Stouts towards this end. Bottled alive, to give them time to grow. Our brews sometimes focus on the aromas, flavours and bitterness of hops; the complex warmth and layers of roasted malts; the clarity of cold fermentations; or the acidity, tannin, and texture contributed by mixed fermentations, by time and barrels, wild yeast and bacteria. All guided by the principles of simplicity and balance.
