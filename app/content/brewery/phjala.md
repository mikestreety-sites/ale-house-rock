---
title: Põhjala
permalink: brewery/phjala/
location: 'Tallinn, Harjumaa Estonia'
style: Micro Brewery
website: 'https://pohjalabeer.com'
instagram: 'http://instagram.com/pohjalabeer'
twitter: 'https://twitter.com/pohjalabeer'
facebook: 'https://www.facebook.com/pohjalabeer'
untappd: 'https://untappd.com/Pohjala'
---

Põhjala Brewery was the first in the new wave of Estonian microbreweries, starting as a gypsy brewery at the end of 2012. In April 2014, we started brewing in our own brewhouse in Tallinn's Nõmme district and at the end of 2018 moved to a new facility in Port Noblessner, featuring a 50 hl Rolec brewhouse and a tap room with Texas bbq-centered cuisine. We brew small batches of hand crafted ales, using the best malts, loads of mostly New World hops and a relentless passion for good beer. When creating our beers, we are inspired by Nordic nature, Estonian new cuisine and the work of our fellow small brewers.
