---
title: Eagle Brewery (formerly Charles Wells)
permalink: brewery/eagle-brewery/
aliases:
  - mcewans
  - youngs
  - charlie-wells
  - directors
location: 'Bedford, England'
style: Macro Brewery
website: 'http://eaglebrewery.co.uk'
instagram: 'http://instagram.com/theeaglebrewery'
twitter: 'https://twitter.com/theeaglebrewery'
facebook: 'https://www.facebook.com/theeaglebrewery'
untappd: 'https://untappd.com/w/eagle-brewery-formerly-charles-wells/1348'
---

The Eagle Brewery was previously a Charles Wells brewery site and is now part of Marston's wider business. The Eagle Brewery continues to brew many of the former Charles Wells brewery brands sold to Marston's.
