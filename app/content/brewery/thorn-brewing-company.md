---
title: Thorn Brewing Company
permalink: brewery/thorn-brewing-company/
location: 'San Diego, CA United States'
style: Micro Brewery
website: 'http://thorn.beer/'
instagram: 'http://instagram.com/thornbeer'
twitter: 'https://twitter.com/thornbeer'
facebook: 'https://www.facebook.com/thorn.beer/?ref=br_rs'
untappd: 'https://untappd.com/brewery/383311'
---

Thorn Brewing Co. began in North Park, San Diego where neighborhood folks pride themselves on walking or riding their bikes to get locally produced food, growing their own fruits and vegetables and, of course, brewing their own beer. In 2017, we opened Thorn Barrio Logan, in the historic neighborhood, filled with art, culture and amazing people. This location boasts a 30-barrel brewhouse, canning line, and tasting room from where we have been able to grow our distribution throughout Southern California as well as internationally. The most recent addition to the Thorn Tribe is Thorn Mission Hills. Nestled in the heart of Mission Hills, this tasting room is the perfect place to spend the afternoon hanging with friends enjoying a craft beer. Mission Hills, like our other locations, is both dog and family-friendly too. Independence in craft beer is incredibly important to us as a brewery as well as to the craft beer community as a whole. This tight-knit, craft community was founded on the principles of quality vs. quantity, passion vs. profit, and being a positive impact on the neighborhoods where we live and work.
