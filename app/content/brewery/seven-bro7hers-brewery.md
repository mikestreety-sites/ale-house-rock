---
title: Seven Bro7hers Brewery
permalink: brewery/seven-bro7hers-brewery/
alias:
  - seven-brothers
location: 'Salford, Greater Manchester England'
style: Micro Brewery
website: 'http://sevenbro7hers.com/'
instagram: 'http://instagram.com/sevenbro7hers'
twitter: 'https://twitter.com/Sevenbro7hers'
facebook: 'https://www.facebook.com/SEVENBRO7HERS'
untappd: 'https://untappd.com/SevenBro7hersBrewery'
---

We are seven actual brothers from Salford who love beer (we also have four sisters who love gin). It was our Dad who first got us into beer – he used to home brew when we were kids and so for us, there’s a lovely nostalgia in everything about beer. We brew our beer in Salford, where we also have our Tap Room – you can come over and visit any time! We also do brewery tours and tastings, as well as holding regular events at the Tap Room which are always extremely good fun.
