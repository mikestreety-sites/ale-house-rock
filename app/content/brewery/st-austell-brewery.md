---
title: St Austell Brewery
permalink: brewery/st-austell-brewery/
location: 'St. Austell, Cornwall England'
style: Regional Brewery
website: 'https://www.staustellbrewery.co.uk/'
instagram: 'http://instagram.com/st_austell_brewery'
twitter: 'https://twitter.com/staustellbrew'
facebook: 'http://www.facebook.com/staustellbrewery'
untappd: 'https://untappd.com/StAustellBrewery'
---

Independent, family-owned St Austell Brewery has been brewing beer in Cornwall since 1851. Fast forward 170 years and, while traditional brewing techniques remain, innovation, passion and craftsmanship continue to evolve. Most famous for its flagship pale ale, Tribute, St Austell Brewery’s range of award-winning beers – also including Proper Job IPA and korev lager - are available in pubs and supermarkets nationwide. St Austell Brewery also owns and operates over 180 pubs, inns, and hotels across the West Country - including managed houses and tenancies.
