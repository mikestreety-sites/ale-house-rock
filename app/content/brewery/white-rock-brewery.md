---
title: White Rock Brewery
permalink: brewery/white-rock-brewery/
location: 'St Sampsons, Guernsey Channel Islands'
style: Micro Brewery
website: 'http://www.whiterockbrewery.gg'
twitter: 'https://twitter.com/WhiteRockBeers'
facebook: 'https://www.facebook.com/WhiteRockBrewery'
untappd: 'https://untappd.com/WhiteRockBrewery'
---

Let us introduce ourselves. We are the White Rock Brewery, the first microbrewery in Guernsey making distinctive and tasty craft beer all year long. We are dedicated to using locally sourced ingredients to brew our inspired tasty beers.
