---
title: Beavertown
permalink: brewery/beavertown/
location: 'Tottenham Hale, London England'
style: Macro Brewery
aliases:
  - beaverton
website: 'https://beavertownbrewery.co.uk'
instagram: 'http://instagram.com/beavertownbeer'
twitter: 'https://twitter.com/BeavertownBeer'
facebook: 'https://www.facebook.com/beavertownbrewery'
untappd: 'https://untappd.com/BeavertownBeer'
---

In May 2014 we moved to our new 11,000sqft space in Tottenham Hale. We upgraded to a 30BBL (50HL) brew house.
