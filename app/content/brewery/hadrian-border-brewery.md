---
title: Hadrian Border Brewery
permalink: brewery/hadrian-border-brewery/
location: 'Newcastle upon Tyne, Tyne and Wear England'
style: Micro Brewery
website: 'http://www.hadrian-border-brewery.co.uk/shop'
instagram: 'http://instagram.com/HadrianBorder'
twitter: 'https://twitter.com/HadrianBorder'
facebook: 'https://www.facebook.com/HadrianBorder'
untappd: 'https://untappd.com/HadrianBorderBrewery'
---

Established in 1994 and based in Newcastle on the banks of the Tyne, Hadrian Border Brewery are experts in creating the finest locally brewed cask and bottled ales. Our time-served professional brewers are amongst the best in the business and you can taste their dedication to the craft of brewing in every sip of our crisp and clear beers. You’ll find we only ever use the finest ingredients and never compromise on quality. Try our beer and you’ll immediately understand why.
