---
title: 71 Brewing
location: 'Dundee, Dundee City Scotland'
style: Micro Brewery
website: 'http://www.71brewing.com'
instagram: 'http://instagram.com/71brewing'
twitter: 'https://twitter.com/71brewing'
facebook: 'https://www.facebook.com/71brewing/'
untappd: 'https://untappd.com/71Brewing'
permalink: brewery/71-brewing/
---

71 Brewing Co. are an independent brewery based in Dundee (Scotland), crafting crisp lagers, punchy pale ales and seasonal, special beers.
