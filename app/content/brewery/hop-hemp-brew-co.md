---
title: Hop & Hemp Brew Co
permalink: brewery/hop-hemp-brew-co/
location: England
style: Nano Brewery
website: 'http://www.hopandhempbrewery.com'
instagram: 'http://instagram.com/hopandhemp'
untappd: 'https://untappd.com/Hop_and_Hemp_Brew_Co'
---

A NEW KIND OF CRAFT BEER Hops and hemp are old friends. In fact, they're family. Perhaps that's why they go so well together. And now you can enjoy them combined, in our range of full flavoured craft brews, each with a twist of organic, hemp-derived CBD. They're the world's first low alcohol, low calorie, vegan beers to contain CBD without THC - the part that makes you 'high'. So you can unwind, and keep your cool.
