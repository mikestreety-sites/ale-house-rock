---
title: Nelson Brewery
permalink: brewery/nelson-brewery/
location: 'Chatham, Medway England'
style: Micro Brewery
website: 'http://www.nelsonbrewery.co.uk/'
twitter: 'https://twitter.com/nelsonbrewery'
facebook: 'https://www.facebook.com/nelson.brewery'
untappd: 'https://untappd.com/NelsonBrewery'
---

The brewery has been on this site in the Historic Dockyard, Chatham, producing craft ale for 25 years. The current Nelson Brewery established in 2006 brews using ingredients sourced from trusted suppliers, local and national. A traditional brewing process is used to provide the finest beer in cask or keg and bottle or can. Our wide range of ales includes some vegan-friendly products. We support many events locally and across the country and are always looking for ways to enhance our profile within the Dockyard and business community. Our ale is to be found across the nation, whether it be through the national supply to JD Wetherspoon or from other microbreweries that we undertake beer swaps with.
