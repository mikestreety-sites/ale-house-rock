---
title: Padstow Brewing Company
location: 'Padstow, Cornwall England'
style: Micro Brewery
website: 'http://www.padstowbrewing.co.uk'
instagram: 'http://instagram.com/padstowbrewingco'
twitter: 'https://twitter.com/padstowbrewing'
facebook: 'https://www.facebook.com/padstowbrewing/'
untappd: 'https://untappd.com/PadstowBrewingCompany'
permalink: brewery/padstow-brewing-company/
---

We are an award-winning microbrewery found on the rugged North Coast of Cornwall. Fresh, exciting, and fiercely independent! Our beers are genuinely hand-crafted and tested for quality at every stage of the process. Each recipe is unique, and driven by our desire to create a fantastic product, not to reduce costs or find cheaper ingredients. We buy malt from Warminster Maltings, which is the oldest floor malting house in the country, and we pay a premium for over 150 years of malting expertise because we think it makes the final product better. We work closely with our customers, delivering ourselves whenever possible, and building lasting relationships. We are a genuine, independent brewery, making beers we really love.
