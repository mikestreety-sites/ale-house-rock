---
title: LERVIG
permalink: brewery/lervig-aktiebryggeri/
aliases:
  - lervig
location: 'Stavanger, Rogaland Norway'
style: Regional Brewery
website: 'https://lervig.no'
instagram: 'http://instagram.com/LervigBeer'
twitter: 'https://twitter.com/LervigBeer'
facebook: 'https://www.facebook.com/LERVIGBEER/'
untappd: 'https://untappd.com/LervigAS'
---

Lervig is an independently owned and operated craft brewery located in Stavanger Norway. We produce a wide range of beers from easy-drinking pilsners and pale ales, barrel-aged barley wines and sour beers. We are constantly pushing the boundaries of brewing for both everyday people and craft beer lovers alike. Our goals are to brew the bestbeers in the world, we like to work outside our comfort zone as well as combine our creativity with the years of craft brewing experience that our brewers have. Everyday we work harder to deliver the quality our customers expect from Lervig. Today Lervig is focused on growth – we push the creative limits of brewing while making sure we have a healthy supply of refreshing beers to ship throughout Norway and beyond. Lervig’s people are what makes this brewery even more special; you’ll find an international team within its walls bringing a global perspective our brewing process, and working hard to make sure that Lervig keeps producing the beers that the people are looking for.
