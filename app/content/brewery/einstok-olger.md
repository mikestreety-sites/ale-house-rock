---
title: Einstök Ölgerð
location: 'Akureyri, Norðurland eystra Iceland'
style: Micro Brewery
website: 'https://einstokbeer.com'
instagram: 'http://instagram.com/einstok'
twitter: 'https://twitter.com/EinstokBeer'
facebook: 'https://www.facebook.com/Einstok'
untappd: 'https://untappd.com/einstokbeer'
permalink: brewery/einstok-olger/
---

The Einstök Brewery is located just 60 miles south of the Arctic circle in the fishing port of Akureyri, Iceland. There, the water flows from rain and prehistoric glaciers down the Hlíðarfjall Mountain and through ancient lava fields, delivering the purest water on Earth, and the perfect foundation for brewing deliciously refreshing craft ales and lagers.
