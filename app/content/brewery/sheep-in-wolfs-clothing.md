---
title: Sheep in Wolf's Clothing
location: Scotland
style: Micro Brewery
website: 'http://siwcbrewery.com'
instagram: 'http://instagram.com/siwcbrewery'
twitter: 'https://twitter.com/siwcbrewery'
facebook: 'https://www.facebook.com/SiWCbrewery/'
untappd: 'https://untappd.com/brewery/497909'
permalink: brewery/sheep-in-wolfs-clothing/
---

Sheep in Wolf’s Clothing are an internationally award-winning brewery focused on creating big beer experiences with less alcohol ............................................................................. inclusivity | community | sustainability
