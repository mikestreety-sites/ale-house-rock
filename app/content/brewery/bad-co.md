---
title: BAD Co. - Brewing & Distilling Company
permalink: brewery/bad-co/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-166712_2a06e.jpeg'
location: 'Dishforth, North Yorkshire England'
style: Micro Brewery
website: 'http://www.wearebad.co'
instagram: 'http://instagram.com/badcobrewinganddistilling'
twitter: 'https://twitter.com/WeAreBadCo'
facebook: 'https://www.facebook.com/BadCoBrewingAndDistilling'
untappd: 'https://untappd.com/BadCoBrewingandDistillingCompany'
---

Our beers are craft-brewed, with outstanding flavours and impact, inspired by the American approach to ale production and current British craft brewing renaissance. Our beers are distributed through bars, pubs and restaurants, as well as selected off-trade partners and wholesalers. We buck the trend and think big. Our beers make a statement. We are bold and expressive. BAD is fun. BAD is social. BAD is informal. BAD is exciting. BAD goes with music. BAD is a science and an art. BAD IS GOOD.
