---
title: Saint Mars Of The Desert
permalink: brewery/saint-mars-of-the-desert/
location: 'Sheffield, South Yorkshire England'
style: Micro Brewery
website: 'https://beerofsmod.co.uk'
instagram: 'http://instagram.com/beerofsmod'
twitter: 'https://twitter.com/beerofsmod'
facebook: 'https://www.facebook.com/stmarsbrewery'
untappd: 'https://untappd.com/BeerofSmod'
---

St Mars of the Desert is a small family-owned brewery in Sheffield. We have a 10 hectolitre brewery and a licensed taproom so you can taste our beers steps away from their birthplace. We have been brewing for over 25 years, in the USA and the UK, and our beers are made in the spirit of “great beers for great people”. That’s you! We brew using traditional techniques including a koelship, wood aging and spunding to offer naturally carbonated beers of purity for your enjoyment.
