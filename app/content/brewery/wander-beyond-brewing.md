---
title: Wander Beyond Brewing
permalink: brewery/wander-beyond-brewing/
location: 'Manchester, Greater Manchester England'
style: Micro Brewery
website: 'http://WanderBeyondBrewing.com'
instagram: 'http://instagram.com/WanderBeyondBrewing'
twitter: 'https://twitter.com/Wanderbeyond_'
facebook: 'http://facebook.com/WanderBeyondBrewing'
untappd: 'https://untappd.com/WanderBeyondBrewing'
---

We launched in December 2017 as a small team with big ambitions to make beers bursting with flavour. Inspired by the outdoors and our love for adventure, we began to develop a surreal and exciting world to reflect the flavour of our beers. Hops climb mountains, barrels dwell in caves and mischievous fruits roam free.
