---
title: Glamorgan Brewing Co
location: 'Llantrisant, Rhondda Cynon Taf,Rhondda Cynon Taff Wales'
style: Micro Brewery
website: 'http://glamorganbrewing.co.uk'
instagram: 'http://instagram.com/glamorganbrewing'
twitter: 'https://twitter.com/glambrewingco'
facebook: 'https://www.facebook.com/GlamorganBrewingCo/'
untappd: 'https://untappd.com/GlamorganBrewingCo'
permalink: brewery/glamorgan-brewing-co/
---

We are an award-winning, family-owned (3 generations) brewing company based in South Wales. Established in 1994, our aim is and has always been to bring better brews to the pubs, bars & clubs of Wales. We’re committed to quality at every point of the brewing process; from the raw ingredients we use to the designs on our pump clips and bottles - we want to brew the best beer that Wales has to offer for both on and off trade customers. With a team of 71 passionate, hardworking beer enthusiasts, we’ve continued to develop new recipes, invest in the brewery and spread the word about Glamorgan. After all… it’s where we’re from.
