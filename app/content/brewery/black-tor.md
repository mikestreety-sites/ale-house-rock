---
title: Black Tor Brewery
permalink: brewery/black-tor/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-143757_d5474.jpeg'
location: 'Christow, Devon England'
style: Micro Brewery
website: 'http://www.blacktorbrewery.co.uk'
instagram: 'http://instagram.com/black_tor_brewery'
twitter: 'https://twitter.com/BlackTorBrewery'
facebook: 'https://www.facebook.com/BlackTorBrewery'
untappd: 'https://untappd.com/BlackTorBrewery'
---

Independent brewery in Devon’s Dartmoor National Park producing a range of award-winning beers in cask and bottle since 2013.
