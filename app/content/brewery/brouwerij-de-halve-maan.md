---
title: Brouwerij De Halve Maan
location: 'Brugge, Vlaams Gewest Belgium'
style: Regional Brewery
website: 'https://www.halvemaan.be'
instagram: 'http://instagram.com/brugse_zot'
twitter: 'https://twitter.com/BrugseZotBruges'
facebook: 'https://www.facebook.com/brouwerijdehalvemaan'
untappd: 'https://untappd.com/BrouwerijDeHalveMaan'
permalink: brewery/brouwerij-de-halve-maan/
---

De Halve Maan Brewing Company is a family brewery in the heart of Bruges, Belgium. The brewery was restarted and refined by Veronique Maes in 2005. With unique beer recipe on the work, the brewery was renamed as ‘Brugse Zot’. Brugse Zot beer became the only beer that is personally brewed in the town center of Bruges. The Brugse Zot and Straffe Hendrik beers are two of the brewery’s finest types of beers. Straffe Hendrik beer is filled with bitter ale while the Brugse Zot is rich in fruity flavor along with different types of malts and hoppy aromas.
