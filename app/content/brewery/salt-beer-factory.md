---
title: SALT
permalink: brewery/salt-beer-factory/
location: 'Saltaire, England'
style: Micro Brewery
aliases:
  - salt
  - salt-beer-factory-n-pomonda-island
website: 'https://www.saltbeerfactory.co.uk'
instagram: 'http://instagram.com/saltbeerfactory'
twitter: 'https://twitter.com/saltbeerfactory'
facebook: 'https://www.facebook.com/SaltBeerFactory'
untappd: 'https://untappd.com/SaltBrewing'
---

Our name is SALT, after the pioneering industrialist who built the village we first called home. It was from here, we ventured out into the brewing world, equipped with only a single punchy vision – make great craft beer for everyone and do it our way. Tired of following tradition, we go where our brewing instincts take us, shamelessly combining heritage and modern brewery craft, crossing styles, exploring new ingredients and producing many award-winners along the way. When it comes to crafting beer and pursuing our own evolution, we’re pushing on at pace. We’re growing, and so is our selection. There’s now a whole world of great SALT craft beers just waiting for you to reach out and try. Don’t thank us, like the beer, it's a pleasure
