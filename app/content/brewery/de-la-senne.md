---
title: Brasserie de la Senne
permalink: brewery/de-la-senne/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-176_33206_hd.jpeg'
location: 'Brussels, Belgium'
style: Micro Brewery
website: 'https://www.brasseriedelasenne.be'
instagram: 'http://instagram.com/brasseriedelasenne'
twitter: 'https://twitter.com/BrasserieSenne'
facebook: 'https://www.facebook.com/Brasserie-de-la-Senne-111567682217219'
untappd: 'https://untappd.com/w/brasserie-de-la-senne/176'
---

The beers of Brasserie de la Senne are produced by two passionate brewers from Brussels: Yvan De Baets and Bernard Leboucq. They work in a small brewery honouring the traditional ways of brewing beer: unfiltered, unpasteurized, free of any additives and using only the finest raw materials of the highest quality. Their dedication to uncompromised quality is definitely one of the outstanding characteristics of the brewery. The beers, with their complex flavour and well-distinguished personalities, are true beers of character, made in Brussels.ShowLess
