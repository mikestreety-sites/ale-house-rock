---
title: Overtone Brewing Co
permalink: brewery/overtone-brewing-co/
location: 'Glasgow, Glasgow City Scotland'
style: Micro Brewery
website: 'https://www.overtonebrewing.com'
instagram: 'http://instagram.com/overtonebrewing'
twitter: 'https://twitter.com/Overtonebrewing'
facebook: 'https://facebook.com/Overtonebrewing'
untappd: 'https://untappd.com/brewery/400105'
---

Founded in Summer 2018, Overtone Brewing Co. are progressively making a difference in the Scottish Craft beer scene and are being recognised globally for their juicy NEIPAs
