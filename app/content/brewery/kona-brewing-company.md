---
title: Kona Brewing Hawaii
permalink: brewery/kona-brewing-company/
location: 'Kailua Kona, HI United States'
style: Regional Brewery
website: 'http://konabrewinghawaii.com/'
instagram: 'http://instagram.com/konabrewinghawaii'
facebook: 'https://www.facebook.com/Konapubandbrewery'
untappd: 'https://untappd.com/konabrewing'
---

Kona Brewing Company was started in the spring of 1994 by father and son team Cameron Healy and Spoon Khalsa, who had a dream to create fresh, local island brews made with spirit, passion and quality. It is a Hawaii-born and Hawaii-based craft brewery that prides itself on brewing the freshest beer of exceptional quality, closest to market. This helps to minimize its carbon footprint by reducing shipping of raw materials, finished beer and wasteful packaging materials. The brewery is headquartered where it began, in Kailua-Kona on Hawaii’s Big Island. For more information go to www.KonaBrewingHawaii.com
