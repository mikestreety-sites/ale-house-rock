---
title: Dark Star Brewing Co.
permalink: brewery/dark-star/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-1513_e5ce8_hd.jpeg'
location: 'Partridge Green, West Sussex England'
style: Micro Brewery
website: 'https://www.darkstarbrewing.co.uk'
instagram: 'http://instagram.com/darkstarbrewco'
twitter: 'https://twitter.com/darkstarbrewco'
facebook: 'https://www.facebook.com/darkstarbrewco'
untappd: 'https://untappd.com/DarkStarBrewingCo'
---

Dark Star Brewing Company was born in 1994 in the cellar of a Brighton pub. With a brew plant only marginally bigger than a home-brew kit, the characteristic style of hoppy beers was developed and tried out on the guinea pigs at the bar. The business was planned on the back of a beer mat and funded with money we didn’t have. 25 years on, we find ourselves in Partridge Green, just south west of Horsham, West Sussex, with a 45 barrel brewhouse. None of us wanted to leave the cellar, but apparently people liked drinking the same beers as us, so we needed to brew more. The brewery is named after a song by the band The Grateful Dead, although if you have a heard a different story, use that - never let the truth stand in the way of a good yarn.
