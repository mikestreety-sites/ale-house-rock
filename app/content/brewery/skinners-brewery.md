---
title: Skinner's Brewery
permalink: brewery/skinners-brewery/
location: 'Truro, Cornwall England'
style: Regional Brewery
website: 'http://www.skinnersbrewery.com/'
instagram: 'http://instagram.com/skinnersbrewery'
twitter: 'https://twitter.com/Skinnersbrewery'
facebook: 'https://www.facebook.com/pages/Skinners-Brewery/209864665734322'
untappd: 'https://untappd.com/SkinnersBrewingCo'
---

We’re a proud Cornish brewery, founded in 1997. Over the course of nearly 25 years Skinner’s has become one of Cornwall’s most recognisable and iconic brands, with Betty Stogs, Porthleven and Lushington’s flying the flag for great Cornish beer. We’ve charted our own course, danced to a different tune, and been the life and soul of the party for over two decades – always striving to give back to the community we know and love. The brewery went into administration in October 2022.
