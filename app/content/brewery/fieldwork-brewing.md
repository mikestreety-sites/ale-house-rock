---
title: Fieldwork® Brewing Company
permalink: brewery/fieldwork-brewing/
location: 'Berkeley, CA United States'
style: Regional Brewery
website: 'https://fieldworkbrewing.com'
instagram: 'http://instagram.com/FieldworkBrewingCo'
twitter: 'https://twitter.com/FieldworkBrewCo'
facebook: 'https://www.facebook.com/FieldworkBrewingCo'
untappd: 'https://untappd.com/FieldworkBrewingCompany'
---

Fieldwork® is a craft brewery founded in Berkeley, CA with a focus on exceptional, honest beer making.
