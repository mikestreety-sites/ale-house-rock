---
title: Little Monster Brewing Co
permalink: brewery/little-monster-brewing-company/
location: 'Chichester, West Sussex England'
style: Nano Brewery
website: 'https://littlemonsterbrew.com'
instagram: 'http://instagram.com/littlemonsterbrew'
twitter: 'https://twitter.com/littlemonbrew'
untappd: 'https://untappd.com/littlemonsterbrewco'
---

Globally inspired, locally crafted. Gypsy brewing fresh, modern, big flavoured beers. Collaborating and innovating. Always improving. Little Monster Brewing Company was founded by award-winning brewer Brenden Quinn who has had a passion for craft beer since travelling in Canada in 2000. After arriving back in the UK and not being able to find any similar beers, a mate suggested he should get a home brew kit to start making hisown. In 2018 his brewery was born, named after his daughter who he calls affectionately “my Little Monster”. The name came instantly to him, as he wanted to dedicate all that he did to his little girl and the inspiration she gave him. The name is fresh, modern and fun - exactly the approach to the beer he makes.
