---
title: 3 Sons Brewing Co.
location: 'Dania Beach, FL United States'
style: Micro Brewery
website: 'https://www.3sonsbrewingco.com'
instagram: 'http://instagram.com/3sonsbrewingco'
twitter: 'https://twitter.com/3sonsbrewingco'
facebook: 'https://www.facebook.com/3sonsbrewingco'
untappd: 'https://untappd.com/3SonsBrewingCo'
permalink: brewery/3-sons-brewing-co/
---

Unconventional ale for unconventional palates!
