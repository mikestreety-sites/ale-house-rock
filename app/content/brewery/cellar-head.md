---
title: Cellar Head Brewing Company
permalink: brewery/cellar-head/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-344944_7532b_hd.jpeg'
location: 'Flimwell, East Sussex England'
style: Micro Brewery
aliases:
  - cellar-head-only-with-love
website: 'http://www.cellarheadbrewing.com/'
instagram: 'http://instagram.com/cellarheadbrew'
twitter: 'https://twitter.com/cellarheadbrew'
facebook: 'https://www.facebook.com/cellarheadbrew/?ref=py_c'
untappd: 'https://untappd.com/CellarHeadBrewingCo'
---

Cellar Head Brewing Company is an award-winning independent microbrewery founded by Chris and Julia McKenzie in 2017. They were joined by David Berry, former brewer at The Old Dairy Brewery in Tenterden and Tonbridge Brewery in East Peckham, who was ready to don that Head Brewer cap and get the chance to brew his own recipes for the world to enjoy! We use traditional brewing methods to produce a varied range of modern beers focused on drinkability and quality using the finest of English grown malts and hops as well as selected high quality hops from around the world and our own strain of historic Sussex live yeast. David’s passion is English grown hops, especially lesser-known and long-forgotten varieties.
