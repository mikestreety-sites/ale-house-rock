---
title: Verdant Brewing Co
permalink: brewery/verdant-brewing-company/
aliases:
  - verdant-brewing-co
location: 'Penryn, Cornwall England'
style: Micro Brewery
website: 'https://verdantbrewing.co'
instagram: 'http://instagram.com/verdantbrew'
twitter: 'https://twitter.com/verdantbrew'
facebook: 'https://www.facebook.com/verdantbrewingco'
untappd: 'https://untappd.com/brewery/211325'
---

Verdant began life back in Autumn 2014 when homebrewers Adam and James teamed up with Rich to craft US-inspired beers that weren’t available locally. Since then the team has continued to grow alongside the demand for our beer! Over the past few years, we’ve become synonymous with hop-forward beer styles including pale ale, India Pale Ale (IPA) and Double IPA (DIPA). Our ethos revolves around brewing the flavour-packed beers we love to drink, and ensuring that beer lovers across the U.K. have access to delicious, quality-driven releases week after week.
