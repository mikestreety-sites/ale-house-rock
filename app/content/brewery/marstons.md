---
title: Marston's Brewery
permalink: brewery/marstons/
location: 'Burton-on-Trent, Staffordshire England'
style: Macro Brewery
aliases:
  - shipyard
website: 'https://www.marstonspubs.co.uk'
instagram: 'http://instagram.com/marstonsbrewery'
facebook: 'https://www.facebook.com/marstonsbrewery'
untappd: 'https://untappd.com/MarstonsBeerCo'
---

The local pub has always been at the heart of British community. To us, that’s important, and always will be. Pubs are at the heart of everything we do, and we’ve been running them and brewing beer for over 180 years. Now we’ve got over 14,000 people working for us, and many more in partnership, and they all share our passion. We are one of the country’s top pub businesses and a leading brewer of premium cask and bottled beers. We operate six breweries, producing over 60 of the country's best loved ales at our sites, with our original HQ still at Marston’s, Burton-on-Trent. We're proud of our heritage, and we work hard making sure that everything we do, from the beers, the pubs and the people, are always the best that they can possibly be.
