---
title: Firebird Brewing Co
permalink: brewery/firebird-brewing-company/
location: 'Rudgwick, West Sussex England'
style: Micro Brewery
website: 'http://www.firebirdbrewing.co.uk'
instagram: 'http://instagram.com/FirebirdBrewing'
twitter: 'https://twitter.com/FirebirdBrewing'
facebook: 'https://www.facebook.com/FirebirdBrewingCo'
untappd: 'https://untappd.com/FirebirdBrewingCo'
---

A craft micro-brewery designed to produce beers from around the world. Our range include UK cask beer, Czech Lager, Belgian Beers, Weissbier, Alt Beer etc
