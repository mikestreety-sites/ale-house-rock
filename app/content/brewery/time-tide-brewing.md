---
title: Time & Tide Brewing
location: 'Eastry, Kent England'
style: Micro Brewery
website: 'https://www.timeandtidebrewing.co.uk'
instagram: 'http://instagram.com/timeandtidebrewing'
twitter: 'https://twitter.com/TimeTideBrewing'
facebook: 'https://www.facebook.com/timeandtidebrewing'
untappd: 'https://untappd.com/brewery/108499'
permalink: brewery/time-tide-brewing/
---

A small, independent brewing based in Deal, Kent with a focus on quality and flavour. We're passionate about beer, and we produce modern juicy, hoppy beers, hazy IPA's, a range of lagers, dark beers, and the occasional sour. We also partner with the local community hop farm (Deal Hop Farm) to make a small, traditional range of modern hop forward cask products, also available in cans. Straight from our 4 degree cold store to you!
