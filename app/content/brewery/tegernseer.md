---
title: Herzoglich Bayerisches Brauhaus Tegernsee
permalink: brewery/tegernseer/
location: 'Tegernsee, Bayern Germany'
style: Regional Brewery
website: 'https://www.brauhaus-tegernsee.de'
instagram: 'http://instagram.com/brauhaus_tegernsee_official'
facebook: 'https://www.facebook.com/brauhaus.tegernsee.official'
untappd: 'https://untappd.com/w/herzoglich-bayerisches-brauhaus-tegernsee/5553'
---

