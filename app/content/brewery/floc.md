---
title: Floc.
permalink: brewery/floc/
location: 'Canterbury, Kent England'
style: Micro Brewery
website: 'https://www.flocbrewing.com/'
instagram: 'http://instagram.com/flocbrewing'
twitter: 'https://twitter.com/flocbrewing'
facebook: 'https://www.facebook.com/flocbrewing/'
untappd: 'https://untappd.com/FlocBrewing'
---

At Floc, we make hop-forward, delicious, flavourful beer, brewed with the finest ingredients; beer that makes people smile, liquid sunshine, to be enjoyed with friends, family or whoever makes you you. We wanted to create a community-driven brewery building, a culture that does well and does good, holding charity events to support the community in which we live. Whilst collaborating with artists on label artwork, our aim is to build strong relationships across the creative community which surrounds us. We will endeavour to grow slowly, create an environment of employment that educates and is enjoyable to work in and, like our beer, we will never be rushed.Our beer is unfiltered for the utmost flavour, always vegan and best enjoyed fresh.
