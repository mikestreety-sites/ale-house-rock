---
title: Tiny Rebel Brewing Co
permalink: brewery/tiny-rebel-brewing-co/
aliases:
  - tiny-rebel
location: 'Newport Docks, Newport Wales'
style: Regional Brewery
website: 'https://www.tinyrebel.co.uk'
instagram: 'http://instagram.com/tinyrebelbrewco'
twitter: 'https://twitter.com/tinyrebelbrewco'
facebook: 'https://www.facebook.com/tinyrebelbrewingco'
untappd: 'https://untappd.com/tinyrebel'
---

Good times with a hint of beer. Join the rebellion. Who doesn’t love trying something new? or finding THAT beer which will become your GO TO for years to come? If that’s you, we’ve got you covered! Tiny Rebel brews craft beers that will capture your imagination and defy your senses time and time again. When we’re not making awesome beer, we’re busy brewing up plans to better our people, community and planet.
