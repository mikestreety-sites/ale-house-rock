---
title: Arundel Brewery
permalink: brewery/arundel-brewery/
location: 'Arundel, West Sussex England'
style: Micro Brewery
website: 'http://www.arundelbrewery.co.uk/'
instagram: 'http://instagram.com/arundelbrewery'
twitter: 'https://twitter.com/ArundelBrewery'
facebook: 'https://www.facebook.com/pages/Arundel-Brewery/322185854545630'
untappd: 'https://untappd.com/ArundelBrewery'
---

We are an award-winning, independent brewery operating since 1992. We are run by a family of craft beer fanatics; our aim is to create the most delicious, flavoursome beers possible. We are very proud of the fact that five of our beers (Uptown, Dragonfly, Maple Stout, Blueberry Imperial and Battle of the Oats) are No1 in their categories in the South of England on Untappd. A new craft brewery growing out of a traditional caskbrewery is a rare thing. Inspired by the US breweries like Treehouse and Hill Farmstead, our focus is on freshness and using local ingredients; the aim to make ‘wow’ beers with maximum drinkability. In 2019 we opened our taproom, The Brewhouse Project, where you can try our beers as fresh as they come.
