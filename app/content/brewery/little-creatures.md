---
title: Little Creatures Brewing
permalink: brewery/little-creatures/
location: 'Fremantle, Western Australia Australia'
style: Macro Brewery
website: 'http://www.littlecreatures.com.au/'
instagram: 'http://instagram.com/littlecreaturesbrewing'
facebook: 'http://www.facebook.com/littlecreaturesbrewing'
untappd: 'https://untappd.com/LittleCreaturesBrewing'
---

It’s those little touches that make up Little Creatures. From how we chose our name – honouring the living “little creatures” (or, as most people would call it, yeast) in our beer… To our method of brewing the beer, using hops specially imported from the US… To doing little things thatmake a difference at the brewery, such as taking people on beer tours, having little chats with customers, or creating play areas for the little ones. Then there’s the little things we do to try and make a positive impact on the world around us. Add up all these little things and you can really feel – and taste – the difference.
