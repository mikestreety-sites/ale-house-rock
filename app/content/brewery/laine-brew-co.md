---
title: Laine Brew Co
permalink: brewery/laine-brew-co/
aliases:
  - north-laine
  - gin-brewery-x-north-laine
location: 'Brighton, Brighton and Hove England'
style: Micro Brewery
website: 'https://laine.shop'
instagram: 'http://instagram.com/lainebrewco'
twitter: 'https://twitter.com/lainebrewco'
facebook: 'https://www.facebook.com/LaineBrewCo'
untappd: 'https://untappd.com/LaineBrewCo'
---

Born from the inspirational pub and beer scene of Brighton, Laine Brew Co. brews beer imbued with a love of flavour, experimentation and good times. Our beers are unfiltered, unpasteurised and uncompromising – meaning more flavour, more love and just better beer. With a focus on consistent, sessionable beers and supported with locally-inspired designs, we create highly individual and authentic craft beer which is accessible to all. While our main brewery is in Sussex, we also have Brewpubs in North Laine Brewhouse, Brighton, and The People's Park Tavern, Hackney. This is where we brew the special beers, the limited edition wild, weird and wonderful beers that are all a little bit different but brewed just as lovingly as our core range!
