---
title: Camden Town Brewery
permalink: brewery/camden-town-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-10433_81dff.jpeg'
location: 'Enfield, London England'
style: Macro Brewery
website: 'http://www.camdentownbrewery.com'
instagram: 'http://instagram.com/camdentownbrewery'
twitter: 'https://twitter.com/CamdenBrewery'
facebook: 'http://www.facebook.com/camdenbrewery'
untappd: 'https://untappd.com/camdentownbrewery'
---

It started at the Horseshoe in Hampstead, North London, where Jasper Cuppaidge brewed beneath the pub. We moved up out of the cellar and down the road into Camden Town where we converted seven old Victorian railway arches and installed a beautiful modern brewery, made by Braukon in Germany. We developed our ideas into recipes and sold our first beers into London in the summer of 2010. When ordering beers for the Horseshoe we had to get the lager and wheat beer from Germany and the pale ales from America. Why wasn’t anyone making them in London? Why couldn’t we get great examples of those beers made around the corner from us? So we decided to be the one to do it; to make the beers we really wanted to drink and to do it in Camden Town. The brewery was going to be called Mac’s, after Jasper’s grandfather, but a brewery in New Zealand had already trademarked it. Jasper started typing in web addresses. Having lived in Camden for years, that was where he started and camdentownbrewery.com was free… We hada name! From 1910-60, Laurie McLaughlin, Jasper’s grandfather, ran McLaughlin’s Brewery (Mac’s) in Rockhampton, Australia. When he died, his daughter, Patricia, inherited the brewery and pub estate. Patricia was young when this happened and she passed on the management, and then later the ownership of McLaughlin’s, to Carlton & United. A few years later, for Patricia’s 50th, Jasper recreated Mac’s beer for his mum in the cellar of the Horseshoe and that’s when it all started.
