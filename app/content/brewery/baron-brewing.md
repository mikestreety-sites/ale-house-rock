---
title: Baron Brewing
permalink: brewery/baron-brewing/
location: 'Buntingford, Hertfordshire England'
style: Nano Brewery
website: 'https://baronbrewing.co.uk'
instagram: 'http://instagram.com/baron.brewing'
facebook: 'https://www.facebook.com/BaronBrewing'
untappd: 'https://untappd.com/Baron_Brewing'
---

Baron Brewing is a playground for beer, where no two batches are the same. Made for the fun and love of the process – the next beer will be the best beer. The brewery is based on a farm in Great Hormead, East Hertfordshire. The focus is to brew modern styles, particularly highly hopped ales and fresh lagers. The 1000L brewhouse allows for an ever-changing range of beers to be brewed. Each style will be pushed to its limits, with the evolution and development of new flavours.
