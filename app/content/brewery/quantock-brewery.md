---
title: Quantock Brewery
location: 'Taunton, Somerset England'
style: Micro Brewery
website: 'https://www.quantockbrewery.co.uk'
instagram: 'http://instagram.com/QuantockBrewery'
twitter: 'https://twitter.com/QuantockBrewery'
untappd: 'https://untappd.com/QuantockBrewery'
permalink: brewery/quantock-brewery/
---

Quantock Brewery was launched in 2007, an award winning craft microbrewery based in Bishops Lydeard in Somerset, with a comprehensive range including casks, kegs, cans and bottles. Quantocks range is brewed from the finest ingredients using both traditional and new craft brewing techniques to produce beers that are brimming with flavour and hop character - something that has made us one of Somerset’s leading awarding winning breweries. In February 2017 Quantock Ales was invigorated after massive investment, allowing our team and its new investors to continue brewing its much-loved traditional range of cask ales whist pushing forward to produce a new and exciting beers. This coming together of new and old brought about our modern range of craft hop lead style beers including our high in demand, multi award-winning QPA, national award-winning Titanium and rolling NEIPA.
