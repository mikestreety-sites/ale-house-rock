---
title: The White Hag Irish Brewing Company
permalink: brewery/the-white-hag/
location: 'Ballymote, County Sligo Ireland'
style: Micro Brewery
website: 'http://www.thewhitehag.com'
instagram: 'http://instagram.com/thewhitehag'
twitter: 'https://twitter.com/Thewhitehag'
facebook: 'http://www.facebook.com/thewhitehag'
untappd: 'https://untappd.com/TheWhiteHag'
---

We came together in late 2013 with the ambition of building an innovative and ambitious brewery here is Sligo. We've got an eclectic team, with several skill sets and backgrounds from across 2 continents combining to create The White Hag, Sligo's first brewery in over 100 years. We set out to make Interesting and innovative beers, with a nod towards the ancient and classic beer styles that were common in our hinterland for generations. While known for our award winning stout range and a stellar set of IPAs that make up most of our core range, our sour programme is emerging as probably our point of differentiation. The Púca sour range in particular has captured the imagination of the beer consumer who wants a tart, effervescent mixed fermentation sour beer. It's supported by our Heather sour, Apple sour and a few other special and collaboration sours we've made along the way. We also collaborate with many of our peers in the industry, and have made beers with breweries from across the world that have been truly ground breaking for us. We're also home to a unique crowd funded barrel-aging programme of over 40,000 litres of specially brewed beer on oak.
