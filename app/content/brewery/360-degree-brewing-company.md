---
title: 360° Brewing Company
permalink: brewery/360-degree-brewing-company/
aliases:
  - 360-brewing-company
location: 'Sheffield Park, East Sussex England'
style: Micro Brewery
website: 'http://www.360degreebrewing.com'
instagram: 'http://instagram.com/360brewco'
twitter: 'https://twitter.com/360brewco'
facebook: 'https://www.facebook.com/360degreebrewing'
untappd: 'https://untappd.com/360BrewCo'
---

We craft the finest beers; bold, pure and juicy. Endlessly drinkable, occasionally challenging, all made with passion and precision. Brewed in Sussex, England, with a 360° view.
