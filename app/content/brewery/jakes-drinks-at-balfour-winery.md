---
title: Jake’s Drinks at Balfour Winery
permalink: brewery/jakes-drinks-at-balfour-winery/
location: 'Staplehurst, Kent England'
style: Cidery
website: 'http://www.jakesdrinks.com'
instagram: 'http://instagram.com/JakesDrinks'
twitter: 'https://twitter.com/JakesDrinks'
facebook: 'http://www.facebook.com/JakesDrinks'
untappd: 'https://untappd.com/JakesDrinksBalfourWinery'
---

Jake's Drinks is a collection of beers and ciders from the winemakers at Balfour Winery, the home of Balfour Wines. As with our wines, the beers and ciders are made with the same skill and pioneering spirit. They combine the best local ingredients, a winemaker's touch and a vision for producing drinks of elegance and balance.
