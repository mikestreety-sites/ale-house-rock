---
title: FUERST WIACEK
permalink: brewery/fuerst-wiacek/
location: 'Berlin, Germany'
style: Micro Brewery
website: 'https://www.fuerstwiacek.com'
instagram: 'http://instagram.com/fuerstwiacekbrew'
twitter: 'https://twitter.com/fuerstwiacek'
facebook: 'https://www.facebook.com/fuerstwiacek'
untappd: 'https://untappd.com/fuerstwiacekbrew'
---

Georg Fuerst and Lukasz Wiacek burst into the Berlin craft beer scene in 2016 when they brewed Germany’s first New England IPA. They are driven by a constant need to learn and refine their brewing processes and have done so in part by collaborating with several of the world‘s most respected craft beer breweries. The opening of their new brewing facility in Berlin Siemensstadt provides the pair with more opportunities for experimentation and innovation; those who feel variety is the spice of life can expect to see more sour beers, stouts andforays into many other styles. All this while continuing to deliver the consistently sublime, juicy, hop-forward IPAs beer lovers have come to expect from FUERST WIACEK.
