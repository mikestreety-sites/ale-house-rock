---
title: Royal Swinkels Family Brewers
permalink: brewery/swinkels-family-brewers/
location: 'Lieshout, Noord-Brabant Netherlands'
style: Macro Brewery
website: 'https://www.swinkelsfamilybrewers.com'
facebook: 'https://www.facebook.com/swinkelsfamilybrewers'
untappd: 'https://untappd.com/SwinkelsFamilyBrewers'
---

Swinkels Family Brewers is the second oldest family-owned company in the Netherlands. Swinkels employs around 1000 people in total, nationally and internationally. Under the management of the Swinkels family, beer has been brewed in accordance with a unique family recipe for seven generations, at the Bavaria Brewery in the south of the Netherlands. This is how outstanding quality has been guaranteed since 1719. Pure ingredients are the basis for quality, which is why Swinkels is brewed using natural mineral water and barley that has been malted in-house, by Bavaria. Facts & figures •Largest independent brewery in the Netherlands. •100% family-owned •7th generation of the Swinkels family at the helm. •Annual production: more than 6 million hectolitres of beer •65% is exported to over 120 countries •Net turnover: more than € 400 million •1.000 employees worldwide •Bavaria uses natural mineral water from its own spring
