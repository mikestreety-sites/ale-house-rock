---
title: JW Lees and Co
permalink: brewery/jw-lees-and-co/
aliases:
  - marco-pierre-white
location: 'Middleton, Greater Manchester England'
style: Micro Brewery
website: 'https://www.jwlees.co.uk'
instagram: 'http://instagram.com/jwleesbrewery'
twitter: 'https://twitter.com/jwleesbrewery'
facebook: 'https://www.facebook.com/JWLeesBrewery'
untappd: 'https://untappd.com/JWLeesBrewery'
---

JW Lees is a family brewery company which was founded in 1828. We are based in Middleton in North Manchester and own JW Lees Brewery, JW Lees Pubs, The Alderley Edge Hotel, The Trearddur Bay Hotel and Willoughby’s Wine Merchants. JW Lees is a sixth-generation family business which employs just over 1,200 people, 141 at the brewery and site in Middleton Junction in North Manchester and 865 in its 35 managed pubs, inns and hotels, as well as letting a further 115 tied pubs to self-employed tenants. Cask beer is at the heart of JW Lees and we brew six cask ales as well as three lagers, three smooth beers and eight limited edition seasonal cask ales which are available throughout at different times of the year. We also have the sole UK distribution rights for Bohemia Regent Premium Lager from the Czech Republic. Willoughby’s is our wines and spirits company and we stock over 500 wines from all over the world.
