---
title: Paulaner Brauerei
permalink: brewery/paulaner/
location: 'Munich, Bayern Germany'
style: Macro Brewery
website: 'https://www.paulaner.com'
instagram: 'http://instagram.com/paulanermuenchen'
facebook: 'https://www.facebook.com/paulanermuenchen'
untappd: 'https://untappd.com/PaulanerGruppe'
---

Brewed in Munich, enjoyed all over the world! The Paulaner brewery has stood for the finest Munich art of brewing since 1634 for many years. But Paulaner beer specialties are also well-known and highly-valued outside the beergardens of Munich and the state borders of Germany. Nowadays our beers are enjoyed in more than 70 countries worldwide. It is important to know that each and every Paulaner beer, which is drunk anywhere in the world, was brewed in Munich in full accordance with the strictly applied Bavarian Purity Law. In this way, the Paulaner brewmasters, with their attention to detail and passion, guarantee that our high quality standards are maintained. Paulaner personifies Munich lifestyle and brings together beer fans and connoisseurs all over the world.
