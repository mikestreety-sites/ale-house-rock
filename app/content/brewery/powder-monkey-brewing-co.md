---
title: Powder Monkey Brewing Co
permalink: brewery/powder-monkey-brewing-co/
location: England
style: Micro Brewery
website: 'http://www.powdermonkeybrewing.com'
instagram: 'http://instagram.com/powdermonkeybrewingco'
twitter: 'https://twitter.com/PMBrewCo'
facebook: 'https://www.facebook.com/PMBrewCo'
untappd: 'https://untappd.com/Powder-Monkey-Brewing'
---

We are a brand new Brewery and Visitor centre, complete with Tap House & Kitchen, in Historic buildings with a waterside location in Gosport, Hampshire. The first two beers we have created are being brewed under our control whilst we build our brewery. The Hop & Hooker beers are named after England World Cup winning Hooker Steve Thompson MBE, who is part of our team.
