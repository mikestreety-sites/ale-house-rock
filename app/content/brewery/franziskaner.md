---
title: Spaten-Franziskaner-Löwenbräu-Gruppe
permalink: brewery/franziskaner/
location: 'Munich, Bayern Germany'
style: Macro Brewery
website: 'https://franziskaner-weissbier.de'
instagram: 'http://instagram.com/franziskaner.weissbier'
twitter: 'https://twitter.com/DrinkSpaten'
facebook: 'https://www.facebook.com/franziskaner.weissbier'
untappd: 'https://untappd.com/w/spaten-franziskaner-lowenbrau-gruppe/1176'
---

