---
title: Õllenaut
permalink: brewery/llenaut/
location: 'Saku, Harju maakond Estonia'
style: Micro Brewery
website: 'https://www.ollenaut.ee'
instagram: 'http://instagram.com/ollenaut'
facebook: 'https://www.facebook.com/ollenaut'
untappd: 'https://untappd.com/ollenaut'
---

Õllenaut story started in 2009 when our brewmaster Ilmar couldn’t find any interesting beers from the store and started home brewing. Within few years the demand for beer grew among friends. Ilmar teamed up with Urmas on 2012 and founded the brewery. First beers we released on 26. November on 2013. Õllenaut is the only brewery in Estonia, who is using local ingredients. We believe that every beer must have unique local character and flavor. Õllenaudi lugu sai alguse 2009. aastal kui pruumeister Ilmar ei leidnud poest ühtegi huvitavt õlut mida proovida ning hakkas kodus ise õlut pruulima. Mõne aastaga kasvas nõudlus sõprade ringis kiiresti ja nii lõid Ilmar ja Urmas 2012. aasta lõpus käed ning esimesed Õllenaudi õlled väljusid pruulikojast 2013. aasta 26. novembril. Õllenaut on käsitööpruulikoda, kes ainukese pruulikojana kasutab igapäevaselt ka kohalikku Eestimaist toorainet. Meile meeldib pruulida erilisi õllesid millel on tugev kohalik karakter.
