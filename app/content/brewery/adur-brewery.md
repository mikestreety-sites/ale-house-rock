---
title: Adur Brewery
permalink: brewery/adur-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-Adur_25669_7d05e.jpeg'
location: 'Shoreham-by-Sea, West Sussex England'
style: Micro Brewery
website: 'http://www.adurbrewery.co.uk/'
twitter: 'https://twitter.com/adur_brewery'
untappd: 'https://untappd.com/AdurBrewery'
---
