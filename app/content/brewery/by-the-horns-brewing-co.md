---
title: By The Horns Brewing Co.
permalink: brewery/by-the-horns-brewing-co/
location: 'Salfords, Surrey England'
style: Micro Brewery
website: 'http://www.bythehorns.co.uk/'
instagram: 'http://instagram.com/bythehornsbrew'
twitter: 'https://twitter.com/bythehornsbrew'
facebook: 'http://www.facebook.com/ByTheHornsBrew'
untappd: 'https://untappd.com/BytheHornsBrewingCo'
---

Brewing beer since 2011 Fully operational taproom open all week with great range of keg, can & cask beer
