---
title: Toast Ale
permalink: brewery/toast-ale/
aliases:
  - toast
location: 'City of London, England'
style: Contract Brewery
website: 'https://www.toastale.com'
instagram: 'http://instagram.com/toastale'
twitter: 'https://twitter.com/ToastAle'
facebook: 'https://www.facebook.com/toastale'
untappd: 'https://untappd.com/ToastAle'
---

Planet-saving craft beer, brewed with surplus fresh bread. All profits go to charities that aim to fix the global food system. By using surplus bread to replace virgin barley, we use less land, water and energy, and avoid carbon emissions. Over 3 million slices saved so far. By giving all profits to charity, not shareholders, we fund systemic change to fix the food system. By brewing quality beers and engaging people in conversation, we nudge positive action for the planet. Food production is the biggest contributor to climate change, but one third of all food is wasted. We’re here to change that. Raise a Toast. Save the world. Cheers. #HeresToChange
