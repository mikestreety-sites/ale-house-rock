---
title: Neon Raptor Brewing Co.
permalink: brewery/neon-raptor-brewing-co/
location: 'Nottingham, City of Nottingham England'
style: Micro Brewery
website: 'http://www.neonraptorbrewingco.com'
instagram: 'http://instagram.com/neonraptorbrew'
twitter: 'https://twitter.com/neonraptorbrew'
facebook: 'http://www.facebook.com/neonraptorbrew'
untappd: 'https://untappd.com/brewery/267674'
---

Fresh. Innovative. Neon. Nottingham.
