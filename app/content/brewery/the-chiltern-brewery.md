---
title: The Chiltern Brewery
location: 'Aylesbury, Buckinghamshire England'
style: Micro Brewery
website: 'http://www.chilternbrewery.co.uk/'
instagram: 'http://instagram.com/chilternbrewery'
twitter: 'https://twitter.com/chilternbrewery'
facebook: 'https://www.facebook.com/chilternbrewery/'
untappd: 'https://untappd.com/TheChilternBrewery'
permalink: brewery/the-chiltern-brewery/
---

Family run company brewing award-winning real ales with the best British ingredients since 1980
