---
title: Merakai Brewing Co.
permalink: brewery/merakai-brewing-co/
location: 'Easons Green, East Sussex England'
style: Micro Brewery
website: 'https://www.merakaibrewing.com'
instagram: 'http://instagram.com/merakai_brewing'
twitter: 'https://twitter.com/merakaibrewing'
facebook: 'https://www.facebook.com/MerakaiBrewing'
untappd: 'https://untappd.com/MerakaiBrewingCO'
---

Connecting people with passion, perspective and purpose. Our craft beer is created for everyone to discover + enjoy. #inclusion #ally #diversity
