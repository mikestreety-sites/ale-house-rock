---
title: Alpha Delta Brewing
permalink: brewery/alpha-delta-brewing/
location: 'Newburn, Tyne and Wear England'
style: Micro Brewery
website: 'https://www.alphadeltabrewing.com'
instagram: 'http://instagram.com/alphadeltabeer'
twitter: 'https://twitter.com/AlphaDeltaBeer'
facebook: 'https://www.facebook.com/AlphaDeltaBeer'
untappd: 'https://untappd.com/brewery/446842'
---

Alpha Delta Brewing was established in September 2019 in Newcastle Upon Tyne by Ross Holland. With over 5 years of brewing experience behind him, Ross decided to create something BIG. Now with 3 core beers and countless specials released since then the brewery has plans to expand even further, planning to release even more incredible big beers. Very exciting times.
