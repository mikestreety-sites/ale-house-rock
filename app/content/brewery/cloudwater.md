---
title: Cloudwater Brew Co.
permalink: brewery/cloudwater/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-189896_ce9fe_hd.jpeg'
location: 'Manchester, Greater Manchester England'
style: Micro Brewery
aliases:
  - cloudwater-brew-co
website: 'http://cloudwaterbrew.co'
instagram: 'http://instagram.com/cloudwaterbrew'
twitter: 'https://twitter.com/cloudwaterbrew'
facebook: 'https://www.facebook.com/cloudwaterbrew/'
untappd: 'https://untappd.com/CloudwaterBrewCo'
---

Specialists in modern seasonal beer.
