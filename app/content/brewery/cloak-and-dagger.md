---
title: Cloak and Dagger
permalink: brewery/cloak-and-dagger/
aliases:
  - cloak-dagger
location: 'West Worthing, England'
style: Micro Brewery
website: 'http://cloakanddaggerbrewing.com'
instagram: 'http://instagram.com/cloakanddaggerbrewing'
twitter: 'https://twitter.com/cloakdaggerbrew'
facebook: 'https://www.facebook.com/cloakanddaggerbrewing/'
untappd: 'https://untappd.com/CloakDagger'
---

Cloak + Dagger was founded by three friends from Brighton, Sussex, with an incredible passion for beer. Starting small, with big ideas, we're bringing our own style to the party. Something wicked this way comes.
