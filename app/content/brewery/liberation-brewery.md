---
title: Liberation Brewing Co
permalink: brewery/liberation-brewery/
location: 'Saviour, Jersey, JE2 7WF Channel Islands'
style: Micro Brewery
website: 'http://www.liberationgroup.com/brewery'
instagram: 'http://instagram.com/liberationbrewingco'
twitter: 'https://twitter.com/LibBrewery'
facebook: 'https://www.facebook.com/LiberationBrewingCo/'
untappd: 'https://untappd.com/thejerseybrewery'
---

Against the odds, for nearly 150 years we’ve continued to brew beer in the Channel Islands for the Channel Islands. We’ve weathered a lot over the years but we are still doing what we do best. Still making award-winning beer. With all the knowledge we’ve accrued over the years, the thing that continues to drive us is brewing great beer and providing great experiences for our customers. Pat and the brewery team take great pride in crafting unique and distinctive beers using the finest ingredients.
