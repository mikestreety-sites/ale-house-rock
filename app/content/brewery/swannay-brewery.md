---
title: Swannay Brewery
permalink: brewery/swannay-brewery/
location: 'Orkney, Highlands and Islands Scotland'
style: Micro Brewery
website: 'http://www.swannaybrewery.com'
instagram: 'http://instagram.com/swannaybrewery'
twitter: 'https://twitter.com/SwannayBrewery'
facebook: 'https://www.facebook.com/SwannayBrewery'
untappd: 'https://untappd.com/SwannayBrewery'
---

The brewery is located on the northwesterly tip of Orkney’s mainland, a wind-battered area known as Swannay. Brewing in a rustic farmstead we’re in a unique location: showered in spray from the Atlantic Ocean in the winter and surrounded by fertile farmland in the summer. Once a dairy farm producing farmhouse cheese, our cluster of farm buildings is as much a labour of love as our beers. A mixture of cow sheds, barns and coldstores that used to house beasts, cheesemakers and tonnes of maturing cheese; the whole siteis connected by our sheltered courtyard that you enter under the archway - the shape of which now forms part of our logo.
