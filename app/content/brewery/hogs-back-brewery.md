---
title: Hogs Back Brewery
permalink: brewery/hogs-back-brewery/
location: 'Tongham, Surrey England'
style: Micro Brewery
website: 'https://hogsback.co.uk'
instagram: 'http://instagram.com/hogsbackbrewery'
twitter: 'https://twitter.com/hogsbackbrewery'
facebook: 'https://www.facebook.com/hogsbackbrewery'
untappd: 'https://untappd.com/HogsBackBrewery'
---

Surrey's award-winning brewer of fine English Ales and Lagers
