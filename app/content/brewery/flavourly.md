---
title: Flavourly
permalink: brewery/flavourly/
location: 'Edinburgh, City of Edinburgh Scotland'
style: Contract Brewery
website: 'http://www.flavourly.com/'
instagram: 'http://instagram.com/flavourlyhq'
twitter: 'https://twitter.com/Flavourly'
facebook: 'http://www.facebook.com/flavourly'
untappd: 'https://untappd.com/Flavourly'
---

Flavourly is a monthly craft beer club. In addition to supplying a variety of craft beers from the UK and around the world, Flavourly also include beers brewed to their own recipes.
