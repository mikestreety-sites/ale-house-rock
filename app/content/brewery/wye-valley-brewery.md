---
title: Wye Valley Brewery
permalink: brewery/wye-valley-brewery/
location: 'Stoke Lacy, Herefordshire England'
style: Micro Brewery
website: 'https://www.wyevalleybrewery.co.uk'
instagram: 'http://instagram.com/WyeValleyBrewery'
twitter: 'https://twitter.com/WyeValleyBrew'
facebook: 'https://www.facebook.com/wyevalleybrewery'
untappd: 'https://untappd.com/WyeValleyBrewery'
---

We're a family-run brewer of fine cask & bottle-conditioned beers. If you like real ales, real pubs & the company of real people, you're our kind of person! Since 1985, we have been producing the very best cask, keg and bottled beers, using the finest ingredients, while at the same time supporting the Great British institution that is fondly known as the 'pub'. Our reputation for consistent excellence in product quality and customer service is the reason our beers are served in more than 1,200 pubs and bars throughout the West Midlands and South Wales. We've gone from a humbling beginning of producing approximately 10 brewers' barrels a week to an average of 800 barrels per week - that's more than 12 million pints per year.
