---
title: People Like Us
permalink: brewery/people-like-us/
location: 'København, Region Hovedstaden Denmark'
style: Micro Brewery
website: 'https://peoplelikeus.dk'
instagram: 'http://instagram.com/peoplelikeusdk'
facebook: 'https://www.facebook.com/peoplelikeus.dk'
untappd: 'https://untappd.com/PeopleLikeUs'
---

People Like Us is a craft brewery run by people from socially marginalized groups. We are a sustainable business and we are proof that tolerant and inclusive workplaces mean business. We’re not a charity. We do not hire autists to do good. We employ unique talents to make good business - and make a difference. People Like Us do not have to waste time doing what we do not do well. We just stick with what we are great at. We are all good at something, and with our unique skills combined, we make great beer. People Like Us revolutionize the way we think about work. Please like us!
