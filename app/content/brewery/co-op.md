---
title: Co-operative Group
permalink: brewery/co-op/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-15933_76a39.jpeg'
location: 'Manchester, Greater Manchester England'
style: Bar / Restaurant / Store
website: 'https://www.coop.co.uk'
instagram: 'http://instagram.com/coopuk'
twitter: 'https://twitter.com/coopuk'
facebook: 'https://twitter.com/coopuk?lang=en'
untappd: 'https://untappd.com/w/co-operative-group/15933'
---

In 1844, the Rochdale pioneers had an idea that changed the world. They believed in a different way of doing business - that would be owned by its members and work for the common good. They believed that when people worked together they were stronger; principles are more valuable than profits. In WW1, The Co-op were the first to introduce rationing to make sure everyone got their share. It was the startof a national movement that people joined with pride. A movement that never stopped trying to change the way things were done. A movement that was destined to be different. The customers own the Co-op, therefore it is customers who get the benefits of co-operative trade. We’re the UK’s fifth biggest food retailer with more than 2,500 stores.
