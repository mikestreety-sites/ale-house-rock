---
title: RedWillow Brewery
permalink: brewery/redwillow-brewery/
location: 'Macclesfield, Cheshire East England'
style: Micro Brewery
website: 'https://www.redwillowbrewery.com'
instagram: 'http://instagram.com/redwillowbrewery'
twitter: 'https://twitter.com/redwillowbrew'
facebook: 'https://www.facebook.com/redwillowbrewery'
untappd: 'https://untappd.com/RedwillowBrewery'
---

RedWillow, a modern brewery. Traditional Cask & Modern Ales: Love and respect for the great British cask pint started the brewery and honed its reputation. That devotion remains just as strong today complemented by an array of more hop-forward styles completes our love of ale. Lagers: Developing from a brewing necessity into a passion, our appreciation of bottom-fermented beers has slowly matured over time. Inspired by Bohemia, Bavaria and beyond we now craft a range of quality beers, naturally carbonated with a minimum of 6 weeks lagering time. Mixed & Wild Fermentation, Brewing & Blending: A natural evolution for the brewery, an idea inoculated from our own interests and knowledge developed into a program of carefully considered releases of low-intervention beers and blends from foeders and barrels.
