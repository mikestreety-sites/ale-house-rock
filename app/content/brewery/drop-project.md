---
title: Drop Project
permalink: brewery/drop-project/
location: 'The Willow Centre, London England'
style: Micro Brewery
aliases:
  - exale-brewing-x-drop-project
website: 'https://www.drop-project.co.uk'
instagram: 'http://instagram.com/dropprojectbrew'
twitter: 'https://twitter.com/dropprojectbrew'
facebook: 'https://www.facebook.com/dropprojectbrew'
untappd: 'https://untappd.com/Drop_Project'
---

Creating the freshest, premium flavoursome beers with consistent high-quality results. Pushing creative boundaries and creating the beers that both inspire our passions for the industry and our lifestyles. We don’t cut corners and you can taste it. We love the outdoors and enjoying nature to the max, whether we are in the sea or on the mountains. With an Eco conscious mindset, we are attempting to create as little a negative impact on the world as possible when brewing. We currently operate on a Solar site. Spent grain is recycled into local agricultural as animal feed. Spent yeast and hops are in turn recycled into compost directly on site. We have a one tree one brew policy, where we have committed to planting a tree for every batch brewed. We are by no means perfect, but we are trying… This also doesn’t stop with our beer production. All our Merch is produced exclusively from organic cotton. It is all EarthPositive, which is manufactured at facilities powered by green renewable energy, from low-impact raw materials and a member of the Fair Wear Foundation who protect workers rights and conditions. This amounts to an overall carbon footprint reduction of 90% compared to commonplace practices. We have ambitions for further carbon offsetting programs, so watch this space for those, and look to support the local community wherever possible.
