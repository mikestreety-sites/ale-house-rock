---
title: Lyme Regis Brewery
permalink: brewery/lyme-regis-brewery/
location: 'Lyme Regis, Dorset England'
style: Micro Brewery
website: 'https://lymeregisbrewery.com/'
instagram: 'http://instagram.com/lymeregisbrewery'
twitter: 'https://twitter.com/lymeregisbrew'
facebook: 'https://www.facebook.com/lymeregisbrewery'
untappd: 'https://untappd.com/LymeRegisBrewery'
---

Lyme Regis Brewery have been crafting award-winning artisan beers on the Jurassic Coast since 2010. The brand, founded in 2010, has established itself as a reputable micro-brewery here in our little corner of the country. A new team have taken the helm of the micro brewery in 2018. Supported by our crew of family and friends, we look to continue to craft great beer and share it with friends and visitors alike. We aim to build on the business created by our founders, and evolve the product offering to include a new line of beers. We're passionate about three things. Our "rule of thirds"... Purity - producing natural craft beers, free from the processes and additives used in mass production. Sustainability - where possible we want to turn our production into a sustainable process. Already we use natural spring water, log fired heating for our hot water, and our tap is based in a water mill that generates green energy from the local river. Fun - we're not in this solely for profit. We want to create beers thatbring friends and families together. We're also passionate about our local community, organisations and charity. Pure. Sustainable. Fun. We look forward to sharing a beer with you. Adam, Dan, Elliott + Wes
