---
title: Cölner Hofbräu Früh
permalink: brewery/fruh/
location: 'Cologne, Nordrhein-Westfalen Germany'
style: Regional Brewery
website: 'https://www.frueh.de'
instagram: 'http://instagram.com/frueh_koelsch'
facebook: 'https://www.facebook.com/FruehKoelsch'
untappd: 'https://untappd.com/w/colner-hofbrau-fruh/386'
---

