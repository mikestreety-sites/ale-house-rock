---
title: Stieglbrauerei zu Salzburg
permalink: brewery/steigl/
location: 'Salzburg, Austria'
style: Micro Brewery
website: 'https://www.stiegl.at'
instagram: 'http://instagram.com/stieglbrauerei'
twitter: 'https://twitter.com/stieglbrauerei'
facebook: 'https://www.facebook.com/stieglbrauerei'
untappd: 'https://untappd.com/stieglbrauerei'
---

