---
title: Escapist Brew Co
location: 'Chichester, West Sussex England'
style: Brew Pub
website: 'http://www.theescapistchichester.co.uk'
instagram: 'http://instagram.com/escapistbrewco'
facebook: 'https://www.facebook.com/theescapistchichester'
untappd: 'https://untappd.com/EscapistBrewCo'
permalink: brewery/escapist-brew-co/
---

Brewing bangin', hop forward craft beers in the West Sussex countryside. Currently serving predominantly our craft beer bar, The Escapist in Chichester. Come and check us out if you're on the south coast.
