---
title: Castle Rock Brewery
permalink: brewery/castle-rock-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-1460_8a115.jpeg'
location: 'Nottingham, City of Nottingham England'
style: Micro Brewery
website: 'https://www.castlerockbrewery.co.uk'
instagram: 'http://instagram.com/CRBrewery'
twitter: 'https://twitter.com/CRBrewery'
facebook: 'https://www.facebook.com/CastleRockBrewery'
untappd: 'https://untappd.com/CastleRockBrewery'
---

Castle Rock is a brewery based in Nottingham. Our approach to brewing is open minded but meticulous, and we try and cater to many different tastes. Over the years we've developed a reputation for well-made, consistent cask beer although we do experiment with different formats and try our hand at a huge range of styles. We've got both barrel ageing and lambic projects on the go as well as a 160L pilot brewery which helps us both experiment with new ingredients and processes but also develop our knowledge in specific styles.
