---
title: Oakham Ales
permalink: brewery/oakham-ales/
location: 'Peterborough, City of Peterborough England'
style: Regional Brewery
website: 'https://www.oakhamales.com'
instagram: 'http://instagram.com/oakhamales'
twitter: 'https://twitter.com/OakhamAles'
facebook: 'https://www.facebook.com/OakhamAles'
untappd: 'https://untappd.com/OakhamAles'
---

Oakham Ales was established in 1993 in Oakham (Rutland) - the first beer brewed was Jeffrey Hudson Bitter (JHB). In 1998 the brewery relocated to new premises in Peterborough, a 35 barrel plant being installed in the new Brewery Tap brew pub. Rapidly increasing demand meant that capacity at the Brewery Tap site was reached and in October 2006 a new 75 barrel brewery was opened in the Woodston area of Peterborough. In 2009 on another hop sourcing visit to the USA, John Bryan, our Brewing Director, discovered an exceptional new hop variety – Citra. He instantly fell in love with it and couldn’t wait to brew with it. He rushed it to the UK, ensuring that Oakham Ales were the first brewery to brew a beer with 100% Citra hops. Citra is now our best selling beer. Volume has doubled in the last few years with particularly rapid growth in bottled beer production and Oakham Ales today brews over 25000 brewers barrels a year (43000 HL) of beer per year. That’s more than 7m pints! We are now going through ambitious expansion works that will enable us to diversify more into producing exciting keg beers.
