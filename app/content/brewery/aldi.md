---
title: ALDI Stores UK
permalink: brewery/aldi/
aliases:
  - aldi-stores-uk
  - 10-4-brewing
  - freak-aleworks
  - harpers-brewing-company
  - rheinbacher
  - the-buckhorn-brewery
  - the-hop-foundry
location: 'Atherstone, Warwickshire England'
style: Bar / Restaurant / Store
website: 'https://www.aldi.co.uk/'
twitter: 'https://twitter.com/AldiUK'
facebook: 'https://www.facebook.com/AldiUK'
untappd: 'https://untappd.com/w/aldi-stores-uk/24518'
---

We're one of the world's fastest-growing retailers. Our corporate responsibility plan is driven by 5 principles - community, people, customers, suppliers and environment. It’s about making sure that what you buy has been grown, caught or made with ultimate care for the environment and that workers are treated fairly. It’s also about our incredible suppliers, being fairer to farmers, raising money for great causes, inspiring kids to eat fresh, sourcing sustainable foods and reducing waste. Ultimately, it’s about making sure that, with our customers, we feel 'Everyday Amazing.'
