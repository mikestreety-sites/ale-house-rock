---
title: Hammerton Brewery
permalink: brewery/hammerton/
location: 'Islington, London England'
style: Micro Brewery
website: 'https://www.hammertonbrewery.co.uk'
instagram: 'http://instagram.com/HammertonBrew'
twitter: 'https://twitter.com/hammertonbrew'
facebook: 'https://www.facebook.com/hammertonbrewery'
untappd: 'https://untappd.com/Hammerton'
---

Hammerton Brewery is a microbrewery based in Islington brewing small batch beers with the aim to bring great tasting beer back to the borough and also re-establish the Hammerton Brewery name in London. Hammerton Brewery originally began brewing in London in 1868. Sadly it ceased to brew in the late 1950s and was later demolished. In 2014, a member of the Hammerton family decided to resurrect the family name in brewing. It also brings brewing back to Islington, a London borough with a great beer makingheritage. Hammerton Beers today are based both on modern and traditional brewing techniques. The team are lovers of all things beer and our beers are influenced not just by English beers but the great beers found across the globe.
