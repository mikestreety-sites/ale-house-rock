---
title: Basqueland Brewing
location: 'Hernani, Euskadi Spain'
style: Micro Brewery
website: 'https://basquebeer.com'
instagram: 'http://instagram.com/basquelandbrew'
facebook: 'https://www.facebook.com/BasquelandBrewing'
untappd: 'https://untappd.com/brewery/114728'
permalink: brewery/basqueland-brewing/
---

Award-winning independent craft brewery, Basqueland, has been brewing beer for the Basque Country and beyond since 2015. Our aim is to create beers that match the Basque culinary experience. We produce fresh, hop-forward IPAs in both East and West Coast styles, crushable lagers, sharp and fruity sours, and bold stouts. We constantly develop new recipes for limited production runs and strive to push the boundaries with each brew. You can find our creations at specialty bars, restaurants, and shops throughout Europe. Visit our TapRoom in Hernani or our tavern, Basqueland Izakaia, in San Sebastian. And remember to #AskForBasque!
