---
title: Harvey's Brewery
permalink: brewery/harveys-brewery/
location: 'Lewes, East Sussex England'
style: Micro Brewery
website: 'http://www.harveys.org.uk'
instagram: 'http://instagram.com/harveysbrewery'
twitter: 'https://twitter.com/Harveys1790'
facebook: 'https://www.facebook.com/HarveysBreweryLewes/'
untappd: 'https://untappd.com/Harveys'
---

Harvey's is the oldest independent Brewery in Sussex. Founded by John Harvey in 1790 it still remains a privately owned family business, with the eighth generation currently working there. We pride ourselves on the craft of our brewing, environmental credentials, local distribution, local charitable activities and loyal staff. Widely known for our Best Bitter, our beers and pubs can be found in and around the South East. As a Regional Brewer we tend not distribute further than 60 miles from the brewery premises, however our beers can be purchased via our online shop shop.harveys.org.uk and at our Brewery Shop in Lewes.
