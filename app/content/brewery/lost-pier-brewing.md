---
title: Lost Pier Brewing
permalink: brewery/lost-pier-brewing/
aliases:
  - lost-pier
location: 'Hove, Brighton and Hove England'
style: Nano Brewery
website: 'http://www.lostpier.com/'
instagram: 'http://instagram.com/Lostpier'
twitter: 'https://twitter.com/Lostpierbrewing'
facebook: 'https://www.facebook.com/LostPierBrewing/'
untappd: 'https://untappd.com/Lost_Pier_Brewing'
---

Nomadic brewer based in Brighton, Brewed in Sussex, producing hop driven balanced beers which are vegan-friendly.
