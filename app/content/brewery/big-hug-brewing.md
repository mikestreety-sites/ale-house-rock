---
title: Big Hug Brewing
permalink: brewery/big-hug-brewing/
location: 'City of London, London England'
style: Micro Brewery
website: 'http://www.bighugbrewing.com/'
instagram: 'http://instagram.com/bighugbrewing'
twitter: 'https://twitter.com/BigHugBrewing'
facebook: 'https://www.facebook.com/bighugbrewing'
untappd: 'https://untappd.com/BigHugBrewing'
---

Hobo brewers. London based. Brighton home. Beer for the masses. Brewing out of Portobello Brewery in West London, The Great Yorkshire Brewery and various East Sussex. We came together with a passion to create the beers that we want to drink but to also be a part of the movement. To help drive craft beer forward, make a difference along the way and to take great, flavoursome beer to the masses
