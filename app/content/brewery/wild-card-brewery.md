---
title: Wild Card Brewery
permalink: brewery/wild-card-brewery/
location: 'Walthamstow, London England'
style: Micro Brewery
website: 'https://www.wildcardbrewery.co.uk'
instagram: 'http://instagram.com/wildcardbrewery'
twitter: 'https://twitter.com/wildcardbrewery'
facebook: 'https://www.facebook.com/wildcardbrewery'
untappd: 'https://untappd.com/WildCardBrewery'
---

Wild Card Brewery was founded in 2012 at the Ravenswood Industrial Estate, Walthamstow. With support from the local community, our shed became one of Walthamstow's best loved bars, hosting beer festivals, tap takeovers and live music. Meanwhile, in the brewery, Jaega was working away, tweaking old recipes and coming up with new ones. By 2017, she had 2 award winning beers under her belt and was struggling to make enough beer to keep up with demand - we were going to need a bigger brewery. In2018 the brewery moved over to Lockwood Way, where we have doubled capacity. Wild Card Brewery now has two tap rooms in Walthamstow, and our beer is available in pubs, bars, restaurants and supermarkets across the UK.
