---
title: Timothy Taylor's Brewery
location: 'Keighley, West Yorkshire England'
style: Regional Brewery
website: 'https://www.timothytaylor.co.uk/'
instagram: 'http://instagram.com/timothytaylorsbrewery'
twitter: 'https://twitter.com/TimothyTaylors'
facebook: 'https://www.facebook.com/TimothyTaylors/'
untappd: 'https://untappd.com/TimothyTaylors'
permalink: brewery/timothy-taylors-brewery/
aliases:
	- timothy-taylor
---

Timothy Taylor established the brewery in the centre of Keighley in 1858, moving to The Knowle Spring, our present site, in 1863.    The principle of not accepting second best was laid down and remains with us to the present day. This means that the very finest ingredients are used to brew the best possible beers, with Landlord winning many awards.    Mission: Taking extra time, care and pride in traditional, hands-on brewing, making no compromise when it comes to ingredients.    All for that taste of Taylor's.
