---
title: Vibrant Forest Brewery
location: 'Hardley, Hampshire England'
style: Micro Brewery
website: 'https://www.vibrantforest.co.uk'
instagram: 'http://instagram.com/vibrantforest'
twitter: 'https://twitter.com/Vibrant_Forest'
facebook: 'https://www.facebook.com/vibrantforest'
untappd: 'https://untappd.com/VibrantForestBrewery'
permalink: brewery/vibrant-forest-brewery/
---

Vibrant Forest is a microbrewery on the edge of the New Forest. Our passion is to develop, experiment and brew richly flavoured beer full of exceptional character. To brew modern beers packed with flavour. Every style and every beer we create is designed to bring forth a grand adventure in flavour. Whether it be from cask, keg, can or bottle, we want our beers to excite the palate and stimulate the taste buds. Everything we brew is unfiltered, unpasteurised and suitable for vegans. Our taproom is an opportunity for us to showcase our beers in our sunny yard while giving you the chance to buy a selection of cans and draft beer to take away with you.
