---
title: Bestens Brewery
location: 'Haywards Heath, West Sussex England'
style: Nano Brewery
website: 'http://www.bestensbrewery.co.uk'
instagram: 'http://instagram.com/bestensbrewery'
twitter: 'https://twitter.com/bestensbrewery'
facebook: 'https://www.facebook.com/Bestens-Brewery-299752627081677/'
untappd: 'https://untappd.com/BestensBrewery'
permalink: brewery/bestens-brewery/
---

Brewing fresh, naturally hazy, unfined and unfiltered beers bursting with flavour. Our micropub, The Tap, is in the centre of Haywards Heath. Here you'll find a showcase of our beers as well as an ever changing roster of phenomenal guest breweries on keg & cask. All beers produced are vegan. A percentage of profits from all beer sales are donated to our community fund, this fund supports local charities & initiatives.
