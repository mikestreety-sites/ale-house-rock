---
title: Klosterbrauerei Andechs
permalink: brewery/klosterbrauerei-andechs/
location: 'Andechs, Bayern Germany'
style: Micro Brewery
website: 'https://www.andechs.de'
instagram: 'http://instagram.com/kloster.andechs'
facebook: 'https://www.facebook.com/kloster.andechs'
untappd: 'https://untappd.com/w/klosterbrauerei-andechs/5758'
---

