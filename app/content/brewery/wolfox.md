---
title: Wolfox
permalink: brewery/wolfox/
location: 'Brighton, England'
style: Contract Brewery
website: 'https://www.wolfox.co.uk/'
untappd: 'https://untappd.com/w/wolfox/473312'
---

