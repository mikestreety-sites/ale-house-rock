---
title: Dartmoor Brewery
permalink: brewery/dartmoor-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-5304_e3dc6.jpeg'
location: 'Princetown, Devon England'
style: Micro Brewery
website: 'http://www.dartmoorbrewery.co.uk/'
twitter: 'https://twitter.com/DartmoorBrewery'
facebook: 'https://www.facebook.com/groups/JAASing/'
untappd: 'https://untappd.com/w/dartmoor-brewery/5304'
---

We use only the finest ingredients in our traditiional craft ales. The finest English malts, carefully selected hops, our own strain of yeast, and clear, pure Dartmoor Water. As England's highest brewery, at 1465ft above sea level, we are also well placed to enjoy the benefits of clean, fresh Dartmoor, and stunning Dartmoor views in every direction - both of which, we think, add a certain touch of magic to our brews. Dartmoor Brewery was established in Princetown in 1994 by award-winning brewer Simon Loveless and local businessman in humble premises behind the Prince of Wales pub. Our first beer, Jail Ale (brewed within 800 metres of the infamous Dartmoor Prison) is a consistent award-winner to this day, and has been joined by a full range of superb craft ales including Legend, Dartmoor IPA and Dartmoor Best. Today we have a state-of-the-art purpose-built brewery complex, but still based very firmly in Princetown, our birthplace.
