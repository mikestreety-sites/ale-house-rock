---
title: Hairy Dog Brewery
permalink: brewery/hairy-dog-brewery/
location: 'Wivelsfield, East Sussex England'
style: Micro Brewery
website: 'https://hairydogbrewery.beer/'
instagram: 'http://instagram.com/hairydogbrewery/'
twitter: 'https://twitter.com/HairyDogBrewery'
untappd: 'https://untappd.com/HairyDogBrewery'
---

Hairy Dog Brewery, the heart of Sussex craft beer. We brew at More House Farm overlooking the South Downs National Park and our beers reflect our surroundings. Naturally our beer range includes the classics as well an ever growing range of new and exciting brews encompassing the passion and considerable expertise of head brewer Russell Lee. Take a look at his profile on the team page to learn why he is our very own beer hero. We use ingredients from Sussex wherever possible and believe wholeheartedly in sustainability. We think this gives our beer a unique position amongst the many craft breweries in this beautiful county of ours. Our beer also tastes absolutely fantastic. From the bright and crisp Pure Bred pilsner lager with its distinctly summery taste to our Far Fetched Pale Ale with its European and American hop taste, Russell’s brilliance shines brightly within each one.
