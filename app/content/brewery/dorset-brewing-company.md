---
title: Dorset Brewing Company
permalink: brewery/dorset-brewing-company/
image: >-
  https://assets.untappd.com/site/brewery_logos/brewery-dorsetbrewingcompany_8453.jpeg
location: 'Dorchester, Dorset England'
style: Micro Brewery
website: 'https://www.dbcales.com'
twitter: 'https://twitter.com/dbcales'
facebook: 'https://www.facebook.com/DorsetBrewingCompany'
untappd: 'https://untappd.com/DorsetBrewingCompany'
---

Dorset Brewing Company was founded in 1996, restoring an 800-year-old brewing tradition to the Dorset port of Weymouth in the heart of the Jurassic Coast. In 2008 DBC took over the running of Dorchester's brewpub Tom Brown's and the production of its ales. Since then, DBC has grown in size and stature, acquiring the Jurassic Brewhouse in December 2010 to accommodate a larger plant and thus satisfying the increasing demand for its award-winning core range and innovative limited edition specials. Our beers are available throughout the UK and are delivered locally by thebrewery to customers in the south-west.
