---
title: Box Steam Brewery
permalink: brewery/steam-box-brewery/
location: 'Holt, Wiltshire England'
style: Micro Brewery
website: 'http://boxsteambrewery.com'
instagram: 'http://instagram.com/boxsteambrewery'
twitter: 'https://twitter.com/BoxBrew'
facebook: 'http://facebook.com/BoxBrew'
untappd: 'https://untappd.com/BoxSteamBrewery'
---

Founded in 2004, we’re a family-owned, independent brewery now based in the beautiful Wiltshire village of Holt, near Bradford-on-Avon. Celebrating 15 years of producing regional beer, we've launched a new identity for 2019, for our growing range of cask, keg and bottled ales – all of which take inspiration from the engineering triumphs of Isambard Kingdom Brunel. We're committed to producing distinctive, contemporary ales using time honoured methods - and will never compromise on quality and taste. By using steam infusion,we ensure a high standard of naturally flavoursome results, which continue to attract industry acclaim year on year.
