---
title: Maltus Faber
permalink: brewery/maltus-faber/
location: 'Genoa, Liguria Italy'
style: Micro Brewery
website: 'http://www.maltusfaber.com'
facebook: 'http://www.facebook.com/pages/Maltus-Faber/219707594762112'
untappd: 'https://untappd.com/w/maltus-faber/30880'
---

