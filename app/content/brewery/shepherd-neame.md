---
title: Shepherd Neame
permalink: brewery/shepherd-neame/
aliases:
  - bear-island
  - the-faversham-steam-brewery
location: 'Faversham, Kent England'
style: Regional Brewery
website: 'https://www.shepherdneame.co.uk'
instagram: 'http://instagram.com/ShepherdNeame'
twitter: 'https://twitter.com/ShepherdNeame'
facebook: 'https://www.facebook.com/shepherdneame'
untappd: 'https://untappd.com/brewery/1132'
---

Britain’s oldest brewer Shepherd Neame has been based in the market town of Faversham, Kent for over 300 years. Perhaps best known for great British classic ales such as Spitfire, which carries the Royal Warrant, its diverse portfolio includes the Bear Island Collection and the Whitstable Bay Collection. The award-winning Kentish brewer also produces international beers under licence including American best-seller Samuel Adams Boston Lager, and works with Boon Rawd brewery to distribute Singha, Thailand’s first premium lager. The independent family business boasts an award-winning visitor centre and more than 300 pubs and hotels throughout London and the South East, from the historic heart of the City to the Kent coastline. www.shepherdneame.co.uk
