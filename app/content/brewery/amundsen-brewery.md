---
title: Amundsen Brewery
permalink: brewery/amundsen-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-16909_e3daa.jpeg'
location: 'Hauketo, Oslo Norway'
style: Micro Brewery
website: 'https://www.amundsenbrewery.com'
instagram: 'http://instagram.com/amundsenbrewery'
twitter: 'https://twitter.com/amundsenbrewery'
facebook: 'https://www.facebook.com/amundsenbryggeri'
untappd: 'https://untappd.com/AmundsenBryggeri'
---

Amundsen Bryggeri is an Oslo-based brewery focused on producing craft beers of the highest quality for the non-conformists out there. Norways fastest growing brewery. 2020 - 11000hl 2019 - 9500hl 2018 - 7000hl 2017 - 4500hl 2016 - 1500hl
