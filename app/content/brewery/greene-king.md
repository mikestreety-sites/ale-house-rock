---
title: Greene King
permalink: brewery/greene-king/
aliases:
  - morland
location: 'Bury St Edmunds, Suffolk England'
style: Macro Brewery
website: 'http://www.greeneking.co.uk/'
twitter: 'https://twitter.com/greeneking'
facebook: 'https://www.facebook.com/GreeneKingIPA1799/'
untappd: 'https://untappd.com/GreeneKing'
---

For more than 200 years, we’ve been brewing quality cask ales at the Westgate Brewery in Bury St Edmunds. It’s exciting to think that we’ve had a brewery on our site since 1700. But even more impressive is that brewing in the town can be traced back as far as the Domesday book in 1086. We built a "new" brewhouse at Westgate Brewery in 1938 and we’re still using the building to brew all of our beers today. It’s designed so that gravity transfers the ingredients from one floor to the next for each stage of thebrewing process. We’re really proud of our heritage, it’s taught us a lot. We’re also proud that we are as passionate about our beers today as our brewing ancestors were before us. We know it’s this innate passion in our people that means we brew quality beers day in, day out.
