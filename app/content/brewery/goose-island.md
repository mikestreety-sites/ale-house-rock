---
title: Goose Island Beer Co.
permalink: brewery/goose-island/
aliases:
  - goose-island-beer-co
location: 'Chicago, IL United States'
style: Macro Brewery
website: 'http://www.gooseisland.com/'
instagram: 'http://instagram.com/gooseisland'
twitter: 'https://twitter.com/GooseIsland'
facebook: 'https://facebook.com/Goose-Island-157863547303/'
untappd: 'https://untappd.com/gooseisland'
---

Our famous beer began with a trip across Europe, when Goose Island founder (and unabashed beer lover) John Hall took a tour across the continent. Pint by pint, he savored the styles and selections of brews in every region, and thought to himself, “America deserves some damn fine beer like this, too.” Craft brewing wasn’t widely known at the time, but upon return from his European sojourn, John set out to change all that. He settled down in his hometown of Chicago—a city perfect for craft beer, with rapidly evolving tastes and the largest system of fresh water on the planet. And then he got to brewing. First he made some stellar beer. Then he invited his consumers in to watch his process at the brewery, bringing them behind the scenes every step of the way. The result was a new fascination with craft brewing, and beer that not only catered to people’s tastes, but challenged them as well. That was back in 1988, and we haven’t slowed down since. By 1995, John’s beer had become so popular that he decided to open a larger brewery, along with a bottling plant to keep up with demand. Today, what was once one man’s pint-filled dream has become the Goose Island empire you know and love..
