---
title: Thornbridge Brewery
permalink: brewery/thornbridge/
location: 'Bakewell, Derbyshire England'
style: Regional Brewery
aliases:
  - thornbridge-collaboration
website: 'https://thornbridgebrewery.co.uk'
instagram: 'http://instagram.com/thornbridge'
twitter: 'https://twitter.com/thornbridge'
facebook: 'https://www.facebook.com/thornbridgebrewery'
untappd: 'https://untappd.com/thornbridgebrewery'
---

We brewed our first beer in early 2005 after the establishment of a 10 barrel brewery in the grounds of Thornbridge Hall. Our initial focus was on a range of cask beers that used traditional recipes but provided a modern twist through the use of a wide range of hops, malts and the innovation and passion of the brewing team. Now, with over 350 industry awards and a state of the art brewery and bottling in Bakewell, we're proud to be one of the leading craft breweries in the UK. Our beer now appears worldwide; initially it was only to Italy, but now over 35 countries worldwide, including our recent addition of Brazil, sell our brilliant beer. The increase in trade both at home and abroad has led to four expansions between 2009 and today.
