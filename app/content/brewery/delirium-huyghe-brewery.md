---
title: Delirium - Huyghe Brewery
location: 'Melle, Vlaams Gewest Belgium'
style: Regional Brewery
website: 'https://www.brouwerijhuyghe.be'
instagram: 'http://instagram.com/delirium_brewery'
twitter: 'https://twitter.com/deliriumbrewery'
facebook: 'https://www.facebook.com/Deliriumbrewery'
untappd: 'https://untappd.com/BrouwerijHuyghe'
permalink: brewery/delirium-huyghe-brewery/
---

Huyghe Brewery is one of Belgium’s renowned brewers that focuses on speciality Beers and Premium Brands. Huyghe Brewery is the proud owner of the Delirium brand, Averbode abbey beer, La guillotine Gold Blond multigrain, Mongozo gluten free beer and many more.     We focus on quality products and we strongly believe in renewable energy and corporate social responsibility. Which led us to be the Most Sustainable Belgium Family Brewer.     Today, Delirium is served and enjoyed all over the world. And we believe that the Pink Elephant will keep on growing!
