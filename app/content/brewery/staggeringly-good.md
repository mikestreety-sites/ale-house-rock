---
title: Staggeringly Good
permalink: brewery/staggeringly-good/
location: 'Portsmouth, England'
style: Micro Brewery
website: 'https://www.staggeringlygood.com'
instagram: 'http://instagram.com/staggeringlygoodbeer'
twitter: 'https://twitter.com/StaggeringBeer'
facebook: 'https://www.facebook.com/staggeringlygood'
untappd: 'https://untappd.com/StaggeringlyGood'
---

From the moment they started they knew they weren’t cool. They didn’t have beards and attempts at growing them were humiliating and embarrassing for them and their families. So they set aside their facial wax and combs and focused on what they knew best… brewing quality beer (and dinosaur puns)! Hatched in 2015 by three clean-shaven friends with an obsession forbeer and dinosaurs, we have quickly become known as one of the UK’s highest-rated breweries. Yes it’s all got a little out of control since we first said “Hey shall we get together on Sunday and brew some beer?” Now there are grown up things like bills, but don’t worry we’re not going to let that get in the way of us having a lot of fun. Catch you at the tap room soon dino-riders!
