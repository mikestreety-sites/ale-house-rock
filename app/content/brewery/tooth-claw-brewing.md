---
title: Tooth & Claw Brewing
permalink: brewery/tooth-claw-brewing/
location: 'Hartlepool, England'
style: Micro Brewery
website: 'http://www.toothandclawbrew.com'
instagram: 'http://instagram.com/toothclawbrew'
twitter: 'https://twitter.com/ToothClawBrew'
facebook: 'https://www.facebook.com/ToothClawBrew/'
untappd: 'https://untappd.com/ToothClawBrewing'
---

Tooth & Claw is a collective of Camerons employees dedicated to producing a range of innovative beer styles. Made up of passionate craft beer fans & brewing experts, we have creative freedom to produce a selection of awesome beers. Using our small pilot brewery we aim to brew over 40 different gyles a year, created by the various members of the Tooth & Claw team. We package only a selection of these as limited edition runs so everyone can share our passion.
