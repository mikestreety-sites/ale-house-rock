---
title: Asda
permalink: brewery/asda-smart-price/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-28691_ff8af.jpeg'
location: 'Leeds, West Yorkshire England'
style: Bar / Restaurant / Store
website: 'http://www.asda.com/'
twitter: 'https://twitter.com/asda'
untappd: 'https://untappd.com/w/asda/28691'
---

Asda Stores Ltd is a British supermarket chain, headquartered in Leeds. The company was founded in 1949 when the Asquith family merged their retail business with the Associated Dairies Company of Yorkshire. We expanded into southern England during the 70s-80s, and acquired Allied Carpets, 61 large Gateway Supermarkets and other businesses such as MFI Group. It sold these during the 90s to concentrate on the supermarkets. It was listed on the London Stock Exchange until 1999 when it was acquired by Walmart for £6.7 billion. Asda was the second-largest supermarket chain in the UK between 2003 and 2014 by market share, at which point we fell into third place. Since April 2019, we have been in second-place behind Tesco and ahead of Sainsbury's.
