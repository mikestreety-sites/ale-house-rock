---
title: Theakston
permalink: brewery/theakston/
location: 'Masham, North Yorkshire England'
style: Regional Brewery
website: 'https://www.theakstons.co.uk'
instagram: 'http://instagram.com/theakstonbrewery'
twitter: 'https://twitter.com/theakston1827'
facebook: 'https://www.facebook.com/Theakston1827'
untappd: 'https://untappd.com/Peculier'
---

T&R Theakston Ltd is an independent, family brewing company founded in 1827 located in the Yorkshire Dales market town of Masham, North Yorkshire. The company is controlled and run by direct descendents of the founder RobertTheakston, great-great grandsons, Simon, Nick, Tim and Edward Theakston. Theakstons is one of Britain’s few remaining traditional family brewing companies. The company is a member of the British Beer and Pub Association , the Independent Family Brewers of Britain and a supporter of the Government’s Drinkaware campaign, CAMRA and Pub is the Hub. The company takes pride in its track record as craft brewing innovators; brewing a wide range of cask, brewery conditioned and bottled ales for the UK domestic and several overseas markets. Among the beers produced is Theakston Old Peculier, a beer for which the company is renowned. The 19th century brewery is of a traditional ‘tower’ construction, containing many of the original features, thus upholding the great traditions of the English top-fermented brewing method
