---
title: Loud Shirt Brewing Co
permalink: brewery/loud-shirt-brewing-company/
aliases:
  - loud-shirt-brewing-co
location: 'Brighton, Brighton and Hove England'
style: Micro Brewery
website: 'http://loudshirtbeer.co.uk/'
instagram: 'http://instagram.com/loudshirtbeer'
twitter: 'https://twitter.com/loudshirtbeer'
facebook: 'https://www.facebook.com/loudshirtbeer'
untappd: 'https://untappd.com/LoudShirtBrewingCo'
---

The Loud Shirt Brewing Co. is a Brighton based microbrewery crafting American style ales and lagers. The head brewer, taking note from his long history from the US, has brought his recipes to the UK in an effort to bring some of his own flavour to the market.
