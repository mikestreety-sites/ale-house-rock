---
title: Beak Brewery
permalink: brewery/beak-brewery/
untappd: 'https://untappd.com/TheBeakBrewery'
location: 'Lewes, East Sussex England'
style: Micro Brewery
aliases:
  - beak-brewery-x-northern-monk
  - verdant-brewing-company-x-beak-brewery
  - beak
website: 'https://beakbrewery.com'
instagram: 'http://instagram.com/TheBeakBrewery'
twitter: 'https://twitter.com/TheBeakBrewery'
facebook: 'https://www.facebook.com/thebeakbrewery'
---

Conceived as a nomadic brewery, The Beak has spent the last several years releasing small batches of unfiltered, seasonally-inspired beer with the help of friends at some the UK’s greatest breweries. We’ve now laid roots in the historic brewing town of Lewes where we’ve built a neighbourhood brewery and taproom with a mixed fermentation project. A 10 minute walk from Cliffe High Street, it has 15 keg lines devoted to brewery-fresh beer, alongside a guest beer fridge and a natural, bio-dynamic wine list curated by the youngest Master of Wine in the UK. Alongside world class drinks, we offer an ever-changing roster of street food every Friday, Saturday and Sunday.
