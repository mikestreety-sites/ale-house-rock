---
title: Silver Rocket Brewing
permalink: brewery/silver-rocket-brewing/
location: 'Hassocks, West Sussex England'
style: Nano Brewery
website: 'http://www.silverrocketbrewing.co.uk'
instagram: 'http://instagram.com/silverrocketbrewing'
facebook: 'https://www.facebook.com/silverrocketbrewing/'
untappd: 'https://untappd.com/SilverRocketBrewing'
---

A small brewery in Hassocks, West Sussex, run by two friends who like craft beer and Sonic Youth.
