---
title: Brew By Numbers
permalink: brewery/brew-by-numbers/
location: 'London, Greater London England'
style: Micro Brewery
website: 'https://bbno.co'
instagram: 'http://instagram.com/brewbynumbers'
twitter: 'https://twitter.com/BrewByNumbers'
facebook: 'https://www.facebook.com/brewbynumbers'
untappd: 'https://untappd.com/brewery/50834'
---

Please don't rate if it's out of date! Brew By Numbers has released over 450 different beers across an expansive range of styles. We are best-known for our diversity: hoppy, modern saisons; mixed fermentation farmhouse ales; hazy, vibrant dark coffee beers & rich stouts; highly-hopped pales & IPAs. Our beers are described as bold, balanced and drinkable, regardless of style/strength, and we are now ranked in the Top 100 Breweries in the world by RateBeer. As well as helping lead the way for hazy East Coast pale ales in the UK, BBNo is also seen as a pioneer on issues such as the importance of freshness, beer cold storage, and are a big influence on taproom culture in London. We are an exploratory brewery with a commitment to learning, thoroughness and excellence in all we do.
