---
title: Black Sheep
permalink: brewery/black-sheep-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-134_fac2e.jpeg'
location: 'Masham, North Yorkshire England'
style: Regional Brewery
website: 'https://www.blacksheepbrewery.com'
instagram: 'http://instagram.com/blacksheepbrewery'
twitter: 'https://twitter.com/BlackSheepBeer'
facebook: 'https://www.facebook.com/BlackSheepBrewery'
untappd: 'https://untappd.com/blacksheepbrewery'
aliases:
  - black-sheep
---

Located in Masham, North Yorkshire, Black Sheep is a brewery full of soul, with a rich history and an innovative future. Our story is one that has been told for years. When Paul Theakston decided to establish his own independent brewery in his home town of Masham in 1992, no one could have predicted the journey that was to unfold. As a brewing pioneer with true Yorkshire grit and determination, he forged Black Sheep Brewery, where quality, consistency and independence come first. As our name suggests, we are Black Sheep. We behave differently, not afraid to follow what we believe in.
