---
title: Rivington Brewing Co
permalink: brewery/rivington-brewing-co/
location: 'Rivington, Lancashire England'
style: Micro Brewery
website: 'https://www.rivingtonbrewing.co.uk'
instagram: 'http://instagram.com/rivingtonbrewco'
twitter: 'https://twitter.com/rivingtonbrewco'
facebook: 'https://www.facebook.com/pages/Rivington-Brew-Co/298614664295174'
untappd: 'https://untappd.com/brewery/211176'
---

We are a modern craft brewery and tap room founded in 2014 in Rivington, focusing on innovative modern styles while drawing inspiration from the past. Our inspiration comes from the world over, but our focus is very much local. We concentrate on making the best beer in styles we love, for the people in our local communities. Our passion for the North West is paid back tenfold. We have local landlords who buy whatever we make, no matter the style - from barleywine to grisette - we’re really lucky that so many places support us because they like our beers, and their customers love to try anything local. The people we look up to are all the brewers who are never content with the beers that they’re making - you can always be better.
