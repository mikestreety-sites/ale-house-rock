---
title: Ringwood Brewery
permalink: brewery/ringwood-brewery/
location: 'Ringwood, Hampshire England'
style: Micro Brewery
website: 'https://www.ringwoodbrewery.co.uk'
untappd: 'https://untappd.com/RingwoodBrewery'
---

Ringwood Brewery was founded by Peter Austin in 1978, a man who is revered as "the father of British Micro-brewing". Ringwood Brewery's first brewhouse was in a former bakery in the old station yard brewing Best Bitter and Fortyniner for a handful of local customers. 1979 saw the production of Old Thumper, which has since become the flagship brew. In 1986, having outgrown the Minty's yard premises, the brewery moved to its present site, ironically the location of the old Tunks' Brewery which ceased trading in 1821. Today the brewery is able to produce circa 40,000 barrels of its renowned and distinctive beers. In addition to Best, Fortyniner, and Old Thumper, we now brew the blonde Boondoggle and the deliciously warming XXXX Porter.
