---
title: Lymestone Brewery
permalink: brewery/lymestone-brewery/
location: 'Stone, Staffordshire England'
style: Micro Brewery
website: 'http://www.lymestonebrewery.co.uk'
twitter: 'https://twitter.com/LymestoneBrewer'
facebook: 'http://www.facebook.com/lymestone.brewery'
untappd: 'https://untappd.com/w/lymestone-brewery/4059'
---

