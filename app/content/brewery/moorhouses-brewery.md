---
title: Moorhouse's Brewery
location: 'Burnley, Lancashire England'
style: Regional Brewery
website: 'http://www.moorhouses.co.uk'
instagram: 'http://instagram.com/moorhousesbrew'
twitter: 'https://twitter.com/moorhousesbrew'
facebook: 'http://www.facebook.com/moorhousesbrew'
untappd: 'https://untappd.com/moorhouses'
permalink: brewery/moorhouses-brewery/
---

In it's mighty midst, sits Moorhouse's Brewery, famous for its mystical beers all brewed in the shadow of Pendle Hill. #MysticalBeers
