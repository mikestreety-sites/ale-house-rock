---
title: Wookey Brewing Co.
permalink: brewery/wookey-brewing-co/
location: 'Wookey Hole, Somerset England'
style: Micro Brewery
website: 'https://www.wookeyale.co.uk/craftbeerstore'
instagram: 'http://instagram.com/WookeyBrewingCo'
twitter: 'https://twitter.com/Wookeyale'
facebook: 'https://www.facebook.com/wookeyale/'
untappd: 'https://untappd.com/WookeyBrewingCo'
---

Deep beneath the Mendip Hills lies an underworld of secret caverns and half-glimpsed creatures. Cavers emerge wide eyed with tales of supernatural symbols and witches' marks. Home to the Wookey Brewing Co. whose magically crafted small batch brews contain super-natural ingredients of their own.
