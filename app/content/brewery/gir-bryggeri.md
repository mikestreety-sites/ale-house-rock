---
title: Ægir Bryggeri
permalink: brewery/gir-bryggeri/
location: 'Flåm, Vestland Norway'
style: Micro Brewery
website: 'https://www.aegirbryggeri.no'
instagram: 'http://instagram.com/aegir.bryggeri'
twitter: 'https://twitter.com/aegirbryggeri'
facebook: 'https://www.facebook.com/%C3%86gir-Bryggeri-132619153452123'
untappd: 'https://untappd.com/AEgirBryggeri'
---

The story about Ægir started already in the late 1980`s– and on the other side of the Atlantic Ocean– when Evan Lewis started with homebrewing in Upstate New York. It´s been said that Evan smuggled bottles of his brew through the back door of his high school for the principal to try, and instead of criticizing he offered constructive feedback about how the beers could be improved.
