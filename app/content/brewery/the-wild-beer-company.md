---
title: The Wild Beer Co
permalink: brewery/the-wild-beer-company/
location: 'Evercreech, Somerset England'
style: Micro Brewery
website: 'https://www.wildbeerco.com'
instagram: 'http://instagram.com/wildbeerco'
twitter: 'https://twitter.com/WildBeerCo'
facebook: 'https://www.facebook.com/WildBeerCo'
untappd: 'https://untappd.com/TheWildBeerCo'
---

Wild Beer, Wild Yeast, Wild Hops, Wild ingredients, Wild Ideas, Wild Ambitions... We believe our beers are more memorable and flavourful because we make them using nature, science and a little Somerset magic. We are a true farmhouse brewery - surrounded by nature never fails to inspire our way of making beer. Our use of alternative fermentations, unorthodox yeasts, seasonally-foraged and unusual ingredients helps us deliver remarkable flavour by nature. If we are honest, barrel ageing and blending excites us the most. We have 600 barrels in our library, in their previous lives they contained whisky, sherry, rum, port, tequila and brandy. Time in these old barrels reveals complex layers of flavours, which simply cannot be rushed.
