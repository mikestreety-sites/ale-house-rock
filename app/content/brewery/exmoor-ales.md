---
title: Exmoor Ales
permalink: brewery/exmoor-ales/
location: 'Taunton, Somerset England'
style: Micro Brewery
website: 'https://www.exmoorales.co.uk'
instagram: 'http://instagram.com/Exmoorales'
twitter: 'https://twitter.com/Exmoorales'
facebook: 'https://www.facebook.com/ExmoorAles'
untappd: 'https://untappd.com/ExmoorAles'
---

Founded in 1979 in Wiveliscombe, Exmoor Ales was one of the pioneer microbreweries to stand against the onslaught of keg ale flooding the market from the big brewers. Concentrating on crafting ales of character and flavour, we gained a following, helping lead to the cask ale revival of the early eighties. The following year, with only our 13th brew, our 3.8% session bitter simply known as Exmoor Ale was named Champion Best Bitter in the 1980 Great British Beer Festival. Our reputation for producing innovative ale was further confirmed when we brewed Exmoor Gold in 1986, the first ever single malt Golden Ale. Lambasted by critics at the time, it found favour amongst the public and this style is now the most popular with ale drinkers.
