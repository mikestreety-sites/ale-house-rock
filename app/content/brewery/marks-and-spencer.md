---
title: Marks & Spencer
permalink: brewery/marks-and-spencer/
aliases:
  - marks-spencer
location: 'City of London, London England'
style: Bar / Restaurant / Store
website: 'https://www.marksandspencer.com'
instagram: 'http://instagram.com/marksandspencer'
twitter: 'https://twitter.com/marksandspencer'
facebook: 'https://www.facebook.com/MarksandSpencer'
untappd: 'https://untappd.com/w/marks-spencer/242395'
---

Marks and Spencer Group plc (commonly abbreviated as M&S) is a major British multinational retailer, with headquarters in London, that specialises in selling clothing, home and food products, mostly of its own label. It is listed on the London Stock Exchange and is a constituent of the FTSE 250 Index, having previously been in the FTSE 100 Index from its creation until 2019. M&S was founded in 1884 by Michael Marks and Thomas Spencer in Leeds. M&S currently has 959 stores across the UK, including 615 that only sell food products, and through its television advertising asserts the exclusive nature and luxury of its food and beverages; it also offers an online food delivery service through Ocado retail.
