---
title: Hepworth & Co
permalink: brewery/hepworth-co-brewers/
location: 'Billingshurst, West Sussex England'
style: Micro Brewery
website: 'https://hepworthbrewery.co.uk'
instagram: 'http://instagram.com/hepworthbrewery'
twitter: 'https://twitter.com/hepworthsbeer'
facebook: 'https://www.facebook.com/Hepworth-Co-Brewers-Ltd-175433872511671'
untappd: 'https://untappd.com/jubelpeachbeer'
---

Hepworths, based at a new site near Pulborough as of 2016 (no longer in Horsham), West Sussex, are award winning brewers of Craft Beer, contemporary beers made with the honed skills of craftsmen. Acclaimed as Supreme Champion in 2012, our success is attributable to the high quality of our ingredients, the traditional methods we employ and the long experience that results in the perfect pint.
