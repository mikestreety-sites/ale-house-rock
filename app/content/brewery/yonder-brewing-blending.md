---
title: Yonder Brewing
permalink: brewery/yonder-brewing-blending/
aliases:
  - yonder-brewing
location: 'Binegar, Somerset England'
style: Micro Brewery
website: 'http://www.brewyonder.co.uk'
instagram: 'http://instagram.com/brewyonder'
twitter: 'https://twitter.com/brewyonder'
facebook: 'https://www.facebook.com/brewyonder/'
untappd: 'https://untappd.com/brewery/391964'
---

Yonder is a modern farmhouse brerwery based in Somerset, UK. We brew beers inspired by a shared passion for flavour, fermentation, and fun.
