---
title: Modest Beer
location: 'Antrim, Antrim and Newtownabbey Northern Ireland'
style: Nano Brewery
website: 'https://www.modestbeer.co.uk/'
instagram: 'http://instagram.com/modestbeer'
twitter: 'https://twitter.com/ModestBeer'
facebook: 'https://www.facebook.com/modestbeer/'
untappd: 'https://untappd.com/ModestBeer'
permalink: brewery/modest-beer/
---

We're a little independent brewery crafting small batch beers for discerning drinkers. Originally from Holywood (with one L), County Down but now brewed and canned in Randalstown.
