---
title: UnBarred Brewery
permalink: brewery/unbarred-brewery/
aliases:
  - unbarred
  - unbarred-x-bison-beer
location: 'Brighton, Brighton and Hove England'
style: Micro Brewery
website: 'http://www.unbarredbrewery.com'
instagram: 'http://instagram.com/unbarredbrewery'
twitter: 'https://twitter.com/unbarredbrewery'
facebook: 'https://www.facebook.com/UnBarredBrewery/?ref=bookmarks'
untappd: 'https://untappd.com/UnBarredBrewery'
---

Made of Brighton | We Are UnBarred
