---
title: Newbarns Brewery
permalink: brewery/newbarns-brewery/
location: 'Leith, City of Edinburgh Scotland'
style: Micro Brewery
website: 'https://www.newbarnsbrewery.com'
instagram: 'http://instagram.com/newbarnsbrewery'
twitter: 'https://twitter.com/NewbarnsBrewery'
facebook: 'https://www.facebook.com/newbarnsbrewery'
untappd: 'https://untappd.com/w/newbarns-brewery/467016'
---

