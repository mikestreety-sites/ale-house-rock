---
title: BIRRIFICIO AGRICOLO BALADIN - Baladin Indipendente Italian Farm Brewery
permalink: brewery/birrificio-agricolo-baladin-baladin-indipendente-italian-farm-brewery/
location: 'Piozzo, Piemonte Italy'
style: Micro Brewery
website: 'http://www.baladin.it/'
instagram: 'http://instagram.com/birrabaladin'
twitter: 'https://twitter.com/BirraBaladin'
facebook: 'https://www.facebook.com/baladin'
untappd: 'https://untappd.com/birrabaladin'
---

ITALIANO - Il birrificio agricolo Baladin nasce come brewpub nel 1996 a Piozzo, un piccolo paese che si affaccia sulle Langhe, in Piemonte, ad opera del mastro birraio Teo Musso. Da subito Teo decide di mettere in bottiglia le sue birre di forte ispirazione belga. L’idea ispiratrice di Teo è stata da subito la volontà di voler abbinare birra e cibo. Ha pub in tutta Italia che contribuiscono alla diffusione della cultura della birra artigianale. Nel 2016 è stato inaugurato il nuovo birrificio,dotato di moderne tecnologie con lo scopo diaccrescere la qualità della birra e rispettare la materia prima frutto del lavoro nei campi. È ospitato all’interno del Baladin Open Garden, un luogoimmerso nel verde attorno ad una cascina storica, pensatoper entrare a contatto con la filosofia Baladin. ENGLISH –Baladin,indipendent farm brewery, was founded as a brewpub in 1996 in Piozzo, a small village, in Piedmont, by the brewmaster Teo Musso. Immediately Teo decided to bottle his beers with a strong belgian inspiration. The Teo’s idea was pair beer and food. Baladin has many pubs in Italy that contribute to spread craft beer culture. In 2016 the new brewery was inaugurated, equipped with modern technologies with the aim of increasing the quality of beer and respecting the raw material resulting from work in the fields. It is inside the Baladin Open Garden, a place surrounded by country near a historic farmhouse, thought to get in touch with Baladin philosophy.
