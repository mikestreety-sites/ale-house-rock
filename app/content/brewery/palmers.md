---
title: Palmers Brewery
permalink: brewery/palmers/
location: 'Bridport, Dorset England'
style: Micro Brewery
website: 'http://palmersbrewery.com/'
twitter: 'https://twitter.com/palmersbrewery'
facebook: 'http://www.facebook.com/PalmersBrewery'
untappd: 'https://untappd.com/palmersbrewery'
---

Palmers: Brewing finest ales since 1794 In 1794, Dorset rope and net makers the Gundry family built a brewery on the banks of the River Brit in Bridport. In the late 19th century, two Palmer brothers – John Cleeves and Robert Henry – bought the operation and its pubs and gave it their names: JC & RH Palmer. Today, their great grandsons John and Cleeves Palmer work in the company and Palmers Brewery remains among the best of small independent brewers. It's not a microbrewery, it's a family owned regional.
