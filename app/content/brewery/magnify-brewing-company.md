---
title: Magnify Brewing Company
location: 'Fairfield, NJ United States'
style: Micro Brewery
website: 'http://magnifybrewing.com'
instagram: 'http://instagram.com/magnifybrewing'
twitter: 'https://twitter.com/MagnifyBrewing'
facebook: 'https://www.facebook.com/magnifybrewing'
untappd: 'https://untappd.com/MagnifyBrewingCompany'
permalink: brewery/magnify-brewing-company/
aliases:
  - 'magnify-brewing'
---
