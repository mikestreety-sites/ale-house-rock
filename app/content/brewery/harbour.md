---
title: Harbour Brewing Co
permalink: brewery/harbour/
location: 'Lanivet, Cornwall England'
style: Micro Brewery
website: 'http://Www.harbourbrewing.com'
instagram: 'http://instagram.com/HarbourBrewing'
twitter: 'https://twitter.com/Harbourbrewing'
facebook: 'https://www.facebook.com/Harbourbrewing/'
untappd: 'https://untappd.com/harbourbrewing'
---

Harbour Brewing Company is a small craft brewery based in North Cornwall. We are committed to making beers that are contemporary and deliver an uncompromising taste experience. We use pure Cornish spring water sourced on the hillside next to the brewery, and only the finest raw materials. We believe this is the only way to deliver a premium quality product. We apply a progressive and innovative approach in both beer style and brewing technique, whist honoring traditional and proven methods. Using this approach we aim to produce a range of full flavoured, balanced and creative beers.
