---
title: Salopian Brewery
permalink: brewery/salopian-brewery/
aliases:
  - sopian-brewery
location: 'Shrewsbury, Shropshire England'
style: Micro Brewery
website: 'https://www.salopianbrewery.co.uk'
instagram: 'http://instagram.com/salopianbrewery'
twitter: 'https://twitter.com/SalopianBrewery'
facebook: 'https://www.facebook.com/SalopianBrewery'
untappd: 'https://untappd.com/salopian-brewery'
---

Quietly brewing beautiful beers since 1995.
