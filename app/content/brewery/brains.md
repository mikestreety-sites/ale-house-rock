---
title: Brains
permalink: brewery/brains/
location: 'Cardiff, Wales'
style: Regional Brewery
website: 'http://www.sabrain.com'
instagram: 'http://instagram.com/brainsbrewery'
twitter: 'https://twitter.com/brainsbrewery'
facebook: 'http://www.facebook.com/brainsbrewery'
untappd: 'https://untappd.com/brainsbrewery'
---

Brains is Wales’ most famous drink. It is the toast of a nation. Still owned by the descendants of the founders that took over a fledgling brewery in a small stone building behind a Cardiff pub in 1882, you can now find the Brains name above the door of over 270 pubs, bars and hotels across Wales and the West of England. Brewed at The Cardiff Brewery, Brains' award-winning beers are now widely available across Wales and can increasingly be found in supermarkets and pubs throughout the rest of Britain.
