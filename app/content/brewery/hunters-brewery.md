---
title: Hunter's Brewery
permalink: brewery/hunters-brewery/
location: 'Ipplepen, Devon England'
style: Micro Brewery
website: 'http://www.huntersbrewery.com'
twitter: 'https://twitter.com/huntersbrewery'
facebook: 'https://www.facebook.com/huntersbrewery'
untappd: 'https://untappd.com/HuntersBrewery'
---

Hunter’s is a family run brewery that’s been crafting top quality ales in the heart of the Devon’s countryside since 2008. Our mission is to create the UK’s most flavoursome ales using only the finest local ingredients.
