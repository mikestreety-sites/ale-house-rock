---
title: Birra Peroni
location: 'Roma, Lazio Italy'
style: Macro Brewery
website: 'https://peroni.it'
instagram: 'http://instagram.com/birra_peroni'
twitter: 'https://twitter.com/Birra_Peroni'
facebook: 'https://www.facebook.com/birraperoni'
untappd: 'https://untappd.com/w/birra-peroni/1921'
permalink: brewery/birra-peroni/
---

