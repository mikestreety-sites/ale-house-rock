---
title: Schlenkerla ("Heller-Bräu" Trum)
permalink: brewery/heller/
location: 'Bamberg, Bayern Germany'
style: Micro Brewery
website: 'https://www.schlenkerla.de'
instagram: 'http://instagram.com/Schlenkerla1405'
twitter: 'https://twitter.com/Schlenkerla1405'
facebook: 'https://www.facebook.com/Schlenkerla'
untappd: 'https://untappd.com/Schlenkerla-Brewery'
---

Schlenkerla, the historic smoked beer brewery. In the middle of the old part of Bamberg, directly beneath the mighty cathedral, one can find the smoked beer brewery Schlenkerla. First mentioned in 1405 as "house of the blue lion" and now run by the Trum family in the 6th generation, it is the fountain of Aecht Schlenkerla Rauchbier (Original Schlenkerla Smokebeer), Bamberg's Specialty for centuries. The Original Schlenkerla Smokebeer is here at the brewery tavern still being tapped directly from the wooden barrel according to old tradition.
