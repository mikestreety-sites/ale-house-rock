---
title: Boss Brewing Company
permalink: brewery/boss-brewing/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-196837_f92bc.jpeg'
location: 'Swansea, Wales'
style: Micro Brewery
website: 'http://www.bossbrewing.co.uk'
instagram: 'http://instagram.com/bossbrewingco'
twitter: 'https://twitter.com/bossbrewingco'
facebook: 'https://www.facebook.com/BossBrewingCo/'
untappd: 'https://untappd.com/BossBrewingCompany'
---

We are Boss Brewing, a multi-award-winning brewery from Swansea, South Wales. Our main boss is a woman; we’re one of the few female owned and led breweries in the UK. Welcome to our comic book universe where edginess and grit meets fun and mischief and where all of the characters you see are based on real heroes and villains that make up our brew team.
