---
title: Caffle Brewery
permalink: brewery/caffle-brewery/
location: 'Narberth, Pembrokeshire Wales'
style: Micro Brewery
website: 'http://www.cafflebrewery.co.uk'
twitter: 'https://twitter.com/cafflebrewery'
facebook: 'https://www.facebook.com/cafflebrewery'
untappd: 'https://untappd.com/CaffleBrewery'
---

Caffle microbrewery is in a building with a varied past. The school was built in 1845, extended in 1873 and in our opinion the most important pupil was Chris' grandmother who attended until she was 14 when she left to go into service in 1925. The school closed in 1977 and was given a new lease of life in 1988 by Chris and Sharon who converted into an outdoor pursuits center. With just over two months solid effort to create a brewery, beer store, dry store and small social space it is the start of a new adventure at the school making cask and bottle conditioned beer.ShowLess
