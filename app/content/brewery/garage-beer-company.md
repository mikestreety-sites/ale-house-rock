---
title: Garage Beer Co.
permalink: brewery/garage-beer-company/
location: 'Barcelona, Catalunya Spain'
style: Micro Brewery
website: 'https://www.garagebeer.co'
instagram: 'http://instagram.com/garagebeerco'
twitter: 'https://twitter.com/garagebeerco'
facebook: 'https://www.facebook.com/garagebeerco'
untappd: 'https://untappd.com/GarageBeerCo'
---

We started as a brewpub in 2015 in the Eixample neighborhood of Barcelona. In 2017 we opened our main brewery where most of our beers are brewed, though we still use the brewpub for our mixed fermentation beers. You can find us every day at Consell de Cent 261 and Calvell 45, Barcelona, where we'll receive you with open arms and fresh beers!
