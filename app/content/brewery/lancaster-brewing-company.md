---
title: Lancaster Brewing Company
permalink: brewery/lancaster-brewing-company/
location: 'Lancaster, PA United States'
style: Micro Brewery
website: 'http://www.lancasterbrewing.com/'
instagram: 'http://instagram.com/lancasterbrewingcompany'
twitter: 'https://twitter.com/lancasterbrew'
facebook: 'http://www.facebook.com/pages/Lancaster-Brewing-Company/88334416519?ref=mf'
untappd: 'https://untappd.com/LBCLANCASTER'
---

Blessed with fresh air, pure water and the finest, non-irrigated farmland in the world, Lancaster is a place where a hand-shake still means something, and where true craftsmanship and self-reliance remain part of everyday life. We unite an obsessive commitment to quality ingredients with creativity, skill and experience, to brew consistently great and approachable ales and lagers. We're proud to brew alongside scores of artisan craftspeople, farmers working more than 5,000 locally owned farms and our fiercely independent Amish neighbors.
