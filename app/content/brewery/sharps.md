---
title: Sharp's Brewery
permalink: brewery/sharps/
location: 'Rock, Cornwall England'
style: Macro Brewery
website: 'https://www.sharpsbrewery.co.uk'
instagram: 'http://instagram.com/SharpsBrewery'
twitter: 'https://twitter.com/SharpsBrewery'
facebook: 'https://www.facebook.com/sharpsbreweryuk'
untappd: 'https://untappd.com/SharpsBrewery'
---

We create exceptional quality beer on the North Cornish coast, including the UK's #1 cask beer, Doom Bar. Sharp's Brewery was founded in 1994 in Rock, Cornwall. The Brewery's unique position on Cornwall's Atlantic coast is a major influence. Our main brand Doom Bar is named after an infamous sandbank at the mouth of the Camel Estuary.
