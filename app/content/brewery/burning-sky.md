---
title: Burning Sky Brewery
permalink: brewery/burning-sky/
aliases:
  - burning-sky-brewery
location: 'Firle, East Sussex England'
style: Micro Brewery
website: 'https://www.burningskybeer.com'
instagram: 'http://instagram.com/burningskybeer'
twitter: 'https://twitter.com/burningskybeer'
facebook: 'https://www.facebook.com/burningskybrewery'
untappd: 'https://untappd.com/w/burning-sky-brewery/86117'
---

Burning Sky Brewery was conceived and set up by Mark as a long term project and my last brewery build (hopefully). Tom was onboard from an early stage, helping with trial brews. Paula joined when we had some beer to sell & does the sensible stuff. Here, nestled at the base of the South Downs in an old Sussex Barn that has been refurbished to a high standard, we can let our imaginations run wild. The landscape and provenance of our location was a major factor in the brewery’s set up. The crux of the brewery is to brew the beers that excite us, without compromise in terms of ingredient costs and time – that all too often overlooked ingredient in beer. Whether it’s a fresh hoppy pale, or a oak aged beer – they are all considered and cared for beers that we want to drink. We have started work on an extensive ageing program – not tempted by short cuts, we are far more interested in tradition and the slow-working yeasts that bring complexities and depths of flavour to the beers. It is not envisaged thatthe full extent of Burning Sky will be apparent for another 2 or 3 years, when these slow beers are ready and we can fully embark on our blending schedule. Currently we have the ability to be ageing up to 14,500 litres of beer in a mixture of 225 litre oak barriques & 2500 litre oak foudres. The character of each barrel and beer will develop over time leading us on an exciting journey, where the beer guides us and we work with it, learning from it.
