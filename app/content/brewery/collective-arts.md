---
title: Collective Arts Brewing
permalink: brewery/collective-arts/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-76841_24c1b_hd.jpeg'
location: 'Hamilton, ON Canada'
style: Regional Brewery
website: 'https://collectiveartsbrewing.com'
instagram: 'http://instagram.com/collectivebrew'
twitter: 'https://twitter.com/CollectiveBrew'
facebook: 'https://www.facebook.com/collectiveartsbrewing'
untappd: 'https://untappd.com/CollectiveArts'
---

Collective Arts Brewing was started in 2013, by Matt Johnston & Bob Russell. Collective Arts fuses the creativity of craft beverages with the inspired talents of artists from around the world. We feature limited-edition works of art on our labels, and we work to make sure the liquid on the inside is as diverse and creative as the artists we profile. We are a grassroots beer company fusing the craft of brewing with the inspired talents of emerging and seasoned artists, musicians, photographers and film makers. Our brewery is dedicated to promoting artists and raising the creative consciousness through the sociability of craft beer.
