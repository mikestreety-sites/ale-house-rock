---
title: Laverstoke Park Farm
permalink: brewery/laverstoke-park-farm/
aliases:
  - gourmet-burger-kitchen
location: 'Overton, Nr Basingstoke, Hants England'
style: Micro Brewery
website: 'http://www.laverstokepark.co.uk'
twitter: 'https://twitter.com/laverstoke'
facebook: 'http://www.facebook.com/pages/Laverstoke-Park-Farm/102023581179'
untappd: 'https://untappd.com/MrLaverstoke'
---

We have collected and grown old traditional hop varieties and selected the best combination of these for their aroma, and balanced them with additional organically grown hops, in this, our first beer. Although we farm under both biodynamic and organic standards, we go much further in our quest to produce the very best hops and barley. Our micro-biology lab continuously tests the soils, adding compost and compost teas so that the biology in the soil has the right proportions of beneficial bacteria and fungi for the hops and barley to be at their and most flavoursome. This creates the deep flavours of the beer.
