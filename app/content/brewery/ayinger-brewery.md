---
title: Ayinger Privatbrauerei
permalink: brewery/ayinger-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-10915_40d60.jpeg'
location: 'Aying, Bayern Germany'
style: Regional Brewery
website: 'https://www.ayinger.de'
instagram: 'http://instagram.com/ayinger_privatbrauerei'
facebook: 'https://www.facebook.com/AyingerPrivatbrauerei'
untappd: 'https://untappd.com/AyingerBrewery'
---

Beer in Bavaria has had an exceptionally high value since time immemorial. It is not just the proverbial “liquid bread” alone, but rather also the social “cement” of our culture, in which values such as conviviality, community, honesty, reliability and solidarity with the homeland play a major role. The deep roots in our region, continuously deepened over generations, form the nutrient base of the Ayinger Brewery. The quality of our beer, consistently awarded numerous prizes for its excellence, is consequently not just based on the select ingredients of our region and the capacities of our brewing art. Equally important is the spirit, the mindset, the very values with which we have performed our craft for over 130 years. Strong financial, social and cultural commitment for the well-being of all concerned is therefore firmly incorporated into the production of our Ayinger beers – in the company, in the community and in the entire region. Beyond the realm of the world-famous Ayinger beer specialties,these crucial components have also contributed to creating a unique brand name philosophy: “Aying, a Complete Work of Art” as the brand was once described with perfect accuracy by a specialist journal. Maintaining this within the boundaries of genuine beer culture and preserving it for generations to come is our daily assignment. Sincerely, The Inselkammer Family
