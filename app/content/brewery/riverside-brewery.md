---
title: Riverside Brewery
permalink: brewery/riverside-brewery/
location: 'Upper Beeding, West Sussex England'
style: Micro Brewery
website: 'http://www.riversidebreweryltd.co.uk/'
twitter: 'https://twitter.com/Riverside_brew'
facebook: 'https://www.facebook.com/riversidebreweryltd/?fref=ts'
untappd: 'https://untappd.com/RiversideBreweryltd'
---

The brewery was founded by Keith Kempton, Mike Rice and Roger Paxton. The brewery is equipped with a 5bbl kit. Not to be confused with the Riverside Brewery in Lincolnshire, which is entirely unrelated (and which closed in 2014).
