---
title: Holdens Brewery
permalink: brewery/holdens/
location: 'Dudley, West Midlands Combined Authority England'
style: Micro Brewery
website: 'https://www.holdensbrewery.co.uk'
instagram: 'http://instagram.com/holdensbreweryltd'
twitter: 'https://twitter.com/holdensbrewery'
facebook: 'https://www.facebook.com/holdensbrewery'
untappd: 'https://untappd.com/HoldensBrewery'
---

Holden's Brewery Pure Black Country
