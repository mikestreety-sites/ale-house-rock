---
title: Innis & Gunn
permalink: brewery/innis-gunn/
location: 'Edinburgh, Scotland'
style: Regional Brewery
website: 'https://www.innisandgunn.com'
instagram: 'http://instagram.com/innisandgunn'
twitter: 'https://twitter.com/InnisandGunn'
facebook: 'https://www.facebook.com/InnisAndGunn'
untappd: 'https://untappd.com/InnisandGunnUK'
---

Spearheading the barrel-ageing movement in Scotland, The Original bourbon barrel-aged beer is the one that started Innis & Gunn in 2003. We grew to be one of Scotland’s most successful international craft brewers, exporting to over 35 countries. We are the No.2 craft beer in the UK off-trade, No.1 imported craft beer in Canada, and in the top 3 imported beers in Sweden and the US. Winning over fifty awards, including Gold in the Scottish Beer Awards and prestigious Monde Selection.
