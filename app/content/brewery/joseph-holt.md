---
title: Joseph Holt
permalink: brewery/joseph-holt/
location: 'Manchester, Greater Manchester England'
style: Regional Brewery
website: 'https://www.joseph-holt.com'
instagram: 'http://instagram.com/josephholt'
twitter: 'https://twitter.com/JosephHolt1849'
facebook: 'https://www.facebook.com/josephholtbrewery'
untappd: 'https://untappd.com/JosephHolt'
---

Joseph Holt has been brewing beer since 1849. We use our expertise & experience to bring you the best cask, keg, lager & selected bottles across the UK.
