---
title: Boojum And Snark
permalink: brewery/boojum-and-snark/
location: 'Sandown, England'
style: Brew Pub
website: 'https://www.boojumandsnark.co.uk'
instagram: 'http://instagram.com/boojumandsnarkiw'
facebook: 'https://www.facebook.com/boojumandsnarkIOW/'
untappd: 'https://untappd.com/w/boojum-and-snark/474096'
---

Boojum&Snark is an innovative new regeneration space on Sandown High Street that features a brew pub, taproom, gallery and retail space. Boojum&Snark is an innovative new regeneration space on Sandown High Street that features a brew pub, taproom, gallery and retail space The vision of two life-long friends, Julie Jones-Evans and Tracy Mikich, it’s the first entirely craft beer taproom on the Island. During the year there’s a programme of exhibitions and events featuring contemporary portrait photography, hidden meanings and narratives that resonate with the town’s cultural heritage.
