---
title: Bryggeriet Vestfyen
permalink: brewery/brygget-med-stolthed-re/
image: >-
  https://assets.untappd.com/site/brewery_logos/brewery-BryggerietVestfyen_3420.jpeg
location: Denmark
style: Regional Brewery
website: 'https://bryggeriet-vestfyen.dk'
instagram: 'http://instagram.com/bryggeriet_vestfyen'
facebook: 'https://www.facebook.com/BryggerietVestfyen'
untappd: 'https://untappd.com/BryggerietVestfyen'
---

A/S Bryggeriet Vestfyen is a proud and independent Danish brewery of beer since 1885. For most of these years, our beer carried the name Vestfyen (West Funen, named after the geographical place), although our portfolio also comprises a wide range of other tasty beers, such as Willemoes, Frejdahl and the original Danish soda, Jolly Cola. Though our name seemingly denote a small and local brewery, we are actually Denmark's fourth largest brewery. Our production has been steadily increasing since 1885. Today we produce beer and soft drinks, marketed in a variety of bottles and cans, for own brands as well as private labels. Similarly we contract manufacture for other breweries. Oh - and we are the second largest brewer of craft beers in Denmark. In 2015, we decided to completely eliminate the consumption of environmentally damaging fuel oil from our production of beer and soft drinks. Today, our sole energy source is CO2 neutral biofuel. Here, we can proudly tell that we are first movers within the Danish brewery industry. We hope, you will enjoy our wide variety of beers. There should be a taste, aroma, sense, colour (or whatever criteria you choose a beer for) - for everyone. Please feel free to contact us for any questions. CHEERS! (SKÅL!)
