---
title: Sierra Nevada Brewing Co.
location: 'Chico, CA United States'
style: Regional Brewery
website: 'https://sierranevada.com'
instagram: 'http://instagram.com/sierranevada'
twitter: 'https://twitter.com/SierraNevada'
facebook: 'https://www.facebook.com/sierranevadabeer'
untappd: 'https://untappd.com/SierraNevada'
permalink: brewery/sierra-nevada-brewing-co/
aliases:
  - sierra-nevada
---

In 1980, Ken Grossman built a small brewery in the city of Chico, California. To this day, premium ingredients and time-honored brewing techniques make Sierra Nevada ales and lagers truly exceptional beers.
