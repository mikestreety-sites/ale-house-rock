---
title: Lakedown Brewing Co.
permalink: brewery/lakedown-brewing-co/
location: 'Burwash, East Sussex England'
style: Micro Brewery
website: 'https://lakedownbrewing.com'
instagram: 'http://instagram.com/lakedownbrewingco'
facebook: 'https://www.facebook.com/LakedownBrewingCo'
untappd: 'https://untappd.com/LakedownBrewingCo'
---

Lakedown Brewing Company is a family run brewery in rural East Sussex. We brew modern and traditional beers in can, bottle, keg and cask.
