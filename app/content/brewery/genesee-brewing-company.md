---
title: Genesee Brewing Company
permalink: brewery/genesee-brewing-company/
location: 'Rochester, NY United States'
style: Macro Brewery
website: 'http://www.geneseebeer.com'
instagram: 'http://instagram.com/geneseebrewery'
twitter: 'https://twitter.com/geneseebrewery'
facebook: 'http://www.facebook.com/geneseebrewery'
untappd: 'https://untappd.com/GeneseeBrewingCompany'
---

