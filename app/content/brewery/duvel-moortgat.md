---
title: Duvel Moortgat
permalink: brewery/duvel-moortgat/
location: 'Puurs, Vlaanderen Belgium'
style: Regional Brewery
website: 'https://www.duvel.com'
instagram: 'http://instagram.com/duvel_belgium'
facebook: 'https://www.facebook.com/duvelbelgium'
untappd: 'https://untappd.com/duvel'
---

It all began when Jan-Léonard Moortgat and his wife founded the Moortgat brewery farm in 1871. Around the turn of the century, Moortgat was one of the over 3,000 breweries operating in Belgium. Jan-Leonard experimented by trial and error, and his top-fermented beers were soon greatly appreciated in the brewery's home town of Puurs and far beyond. Before long, the Brussels bourgeoisie was also won over by his beers. Business was booming and Jan-Leonard's two sons, Albert and Victor, joined the company. There was a clear division of labour: Albert became the brewer, Victor was responsible for delivering the beer to Brussels by horse and dray. In 1923 the production of Duvel began with just a few crates. Today, Duvel is enjoyed literally all around the world (in over 50 countries) by countless beer lovers. The beer is still brewed with profound respect for the original recipe and the time it needs to mature.
