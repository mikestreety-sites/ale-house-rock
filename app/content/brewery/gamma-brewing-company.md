---
title: Gamma Brewing Company
location: 'Herlev Municipality, Region Hovedstaden Denmark'
style: Micro Brewery
website: 'http://gammabrewing.com'
instagram: 'http://instagram.com/gammabrewing'
facebook: 'https://www.facebook.com/gammabrewingco'
untappd: 'https://untappd.com/brewery/193525'
permalink: brewery/gamma-brewing-company/
---

Gamma Brewing Co. is a microbrewery located in Herlev, outside Copenhagen, focusing primarily on hop-forward beers. Visit us: HERLEV The Brewery Tasting Room & Shop in Herlev are open every Friday from 12.00-21.00. AARHUS Showroom & Can Shop in Aarhus are open every Friday from 12.00-18.00.
