---
title: BrewDog
permalink: brewery/brewdog/
location: 'Ellon, Aberdeenshire Scotland'
style: Regional Brewery
aliases:
  - brewdog-vs
website: 'http://www.brewdog.com'
instagram: 'http://instagram.com/brewdogofficial'
twitter: 'https://twitter.com/BrewDog'
facebook: 'http://www.facebook.com/pages/BrewDog/21251598643'
untappd: 'https://untappd.com/brewdogbrewery'
---

MARTIN AND I (JAMES) WERE BORED OF THE INDUSTRIALLY BREWED LAGERS AND STUFFY ALES THAT DOMINATED THE UK BEER MARKET. We decided the best way to fix this undesirable predicament was to brew our own. Consequently in April 2007 BrewDog was born. Both only 24 at the time, we leased a building in Fraserburgh, got some scary bank loans, spent all our money on stainless steel and started making some hardcore craft beers. We brewed tiny batches, filled bottles by hand and sold our beers at local markets and out of the back of our beat up old van. Our biggest mission when we set up BrewDog was to make other people as passionate about great craft beer as we are. And that is still our biggest mission today.
