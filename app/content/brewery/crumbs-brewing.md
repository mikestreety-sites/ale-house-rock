---
title: Crumbs Brewing
location: 'Reigate, England'
style: Micro Brewery
website: 'http://www.crumbsbrewing.co.uk'
instagram: 'http://instagram.com/crumbsbrewing'
twitter: 'https://twitter.com/CrumbsBrewing'
facebook: 'https://www.facebook.com/crumbsbrewing/'
untappd: 'https://untappd.com/CrumbsBrewing'
permalink: brewery/crumbs-brewing/
---

Crumbs Brewing make beer using left over artisan bread from Chalk Hills bakery in Reigate. We're doing our bit for the growing problem of food waste and making unique, local beers that do justice to such a delicious ingredient.    Crumbs Brewing, bread differently....
