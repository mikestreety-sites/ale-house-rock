---
title: Howling Hops
permalink: brewery/howling-hops/
location: 'Hackney, London England'
style: Micro Brewery
website: 'https://www.howlinghops.co.uk'
instagram: 'http://instagram.com/howlinghops'
twitter: 'https://twitter.com/howlinghops'
facebook: 'https://www.facebook.com/howlinghops'
untappd: 'https://untappd.com/HowlingHops2015'
---

We are a microbrewery and tank bar in the heart of Hackney Wick, brewing an ever-changing range of bold, characterful, uncompromising and generously-hopped beers with bags of flavour. Born in 2011, brewing in the basement of Hackney's first brewpub, The Cock Tavern, we had one simple brief; to ‘make some interesting beer,’ and that we did. We brewed over 100 different beers in the ‘brew cave’ over two years, never brewing the same beer twice, before upgrading to the UK's first Tank Bar. We used serving tanksin the basement of the Cock Tavern and connected them to the taps upstairs - it delivers superfresh beer that requires fewer kegs. We took it one step further at the Tank Bar: 10 tanks with the taps pouring beer directly from them.
