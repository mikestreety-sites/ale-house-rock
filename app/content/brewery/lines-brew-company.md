---
title: Lines Brew Co.
permalink: brewery/lines-brew-co/
location: 'Usk, Monmouthshire Wales'
style: Micro Brewery
aliases:
  - lines-brew-company
website: 'http://www.linesbrewco.com/'
instagram: 'http://instagram.com/linesbrewing'
twitter: 'https://twitter.com/linesbrewing'
facebook: 'https://www.facebook.com/linesbrewing'
untappd: 'https://untappd.com/w/lines-brew-co/300963'
---

Blenders of eco-minded and unfined vegan beer. Pizza and craft beer delivered to your home! BrewPub @ 37a Bridge Street, Usk NP15 1BQ
