---
title: Purity Brewing Co.
permalink: brewery/purity-brewing-company/
location: 'Alcester, Warwickshire England'
style: Regional Brewery
website: 'https://puritybrewing.com'
instagram: 'http://instagram.com/puritybrewingco'
twitter: 'https://twitter.com/PurityBrewingCo'
facebook: 'https://www.facebook.com/puritybrewingco'
untappd: 'https://untappd.com/puritybrewingco'
---

Purity Brewing Company is an award winning craft brewery established in 2005. When we set out the mission was simple: brew great beer without prejudice, with a conscience and with a consistency and an attention to detail, which is second to none. Based on a working farm in the heart of the beautiful Warwickshire countryside, our brewery practices are designed to be environmentally friendly and in some cases in the case of our wetland system, environmentally enriching.
