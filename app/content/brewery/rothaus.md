---
title: Badische Staatsbrauerei Rothaus
permalink: brewery/rothaus/
location: 'Grafenhausen, Baden-Württemberg Germany'
style: Micro Brewery
website: 'https://www.rothaus.de'
instagram: 'http://instagram.com/badischestaatsbrauereirothaus'
facebook: 'https://www.facebook.com/BrauereiRothaus'
untappd: 'https://untappd.com/w/badische-staatsbrauerei-rothaus/1077'
---

