---
title: Brixton Brewery
permalink: brewery/brixton-brewery/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-85722_a4d50_hd.jpeg'
location: 'Brixton, London England'
style: Regional Brewery
website: 'https://brixtonbrewery.com'
instagram: 'http://instagram.com/BrixtonBrewery'
twitter: 'https://twitter.com/BrixtonBrewery'
facebook: 'https://www.facebook.com/BrixtonBrewery'
untappd: 'https://untappd.com/BrixtonBrewery'
---

Brixton Brewery was started in 2013 by two local couples, and we’ve been brewing in this melting-pot part of south London ever since. Everything we do is supercharged with the buzz and electricity of our home and its openness to new people and ideas. Our locally crafted beers are modern classics with a Brixton twist – telling the story of Brixton in their names, vibrant designs, and flavours. From the ultra-refreshing Coldharbour Lager to our juicy Atlantic American Pale Ale, our range of bold, modern, and perfectly balanced brews offer something for everyone to enjoy.
