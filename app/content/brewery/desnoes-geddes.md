---
title: Desnoes & Geddes
location: 'Kingston, St. Andrew Parish Jamaica'
style: Macro Brewery
website: 'http://www.redstripebeer.com/'
twitter: 'https://twitter.com/redstripe'
facebook: 'http://www.facebook.com/redstripe'
untappd: 'https://untappd.com/w/desnoes-geddes/442'
permalink: brewery/desnoes-geddes/
---

Desnoes and Geddes Limited (D&G) is a Jamaican brewer and beverage producer. It was formed in 1918 by Eugene Peter Desnoes and Thomas Hargreaves Geddes, who combined their two shops into one business. In October 2015, Dutch brewer Heineken acquired Diageo's stake taking its stake to 73.3%.
