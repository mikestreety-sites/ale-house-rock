---
title: Utopian Brewing Ltd
location: 'Crediton, Devon England'
style: Micro Brewery
website: 'https://www.utopianbrewing.com'
instagram: 'http://instagram.com/TeamUtopian'
twitter: 'https://twitter.com/TeamUtopian'
facebook: 'https://www.facebook.com/teamutopian'
untappd: 'https://untappd.com/Utopian_Brewing'
permalink: brewery/utopian-brewing-ltd/
aliases:
	- utopia-brewing
---

At Utopian Brewing our mission is simple: to celebrate incredible British lager. From our independent brewery in picturesque Devon, we brew in a traditional way but with our own interpretations of classic styles, using only 100% British grown ingredients. Against this kaleidoscope of wonderful home grown ingredients and producers it was an easy decision for us to significantly reduce our food miles by choosing to brew only with 100% home grown ingredients. Allied to this, the UK is the only country to have strongly focused on building natural disease resistance, making our hops both environmentally friendly and more appealing. Very few British hops are irrigated, making them some of the most environmentally sustainable in the world.
