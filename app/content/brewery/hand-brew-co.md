---
title: Hand Brew Co
permalink: brewery/hand-brew-co/
location: 'Brighton, Brighton and Hove England'
style: Micro Brewery
website: 'http://www.handbrewco.com'
instagram: 'http://instagram.com/HandBrewCo'
twitter: 'https://twitter.com/HandBrewCo'
facebook: 'http://www.facebook.com/handbrewco'
untappd: 'https://untappd.com/HandBrewCo'
---

An independent brewing company with a brewpub in Brighton and brewery and taproom in Worthing, UK, creating high quality, accessible beers for every palate. In late 2020 we commissioned our brand new, high quality, steam heated brewery in Worthing. It features a 2000 litre brew kit, four 4000 litre fermenters, an automated canning line and large 5oC cold store. We are also joined by Cloak and Dagger Brewing, renting space on the site, whose beers are also featured in our taproom. Our taproom is where you can find the freshest Hand Brew Co and Cloak and Dagger beers to drink in or takeaway with 12 draft lines, toad table and a welcoming atmosphere. We’re currently building the bar and operating a pop up taproom and shop for the time being.
