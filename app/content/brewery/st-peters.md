---
title: St. Peter’s Brewery Co.
permalink: brewery/st-peters/
location: 'Bungay, Suffolk England'
style: Micro Brewery
website: 'https://www.stpetersbrewery.co.uk'
instagram: 'http://instagram.com/stpetersbrewery'
twitter: 'https://twitter.com/StPetersBrewery'
facebook: 'https://www.facebook.com/StPetersBreweryOfficialPage'
untappd: 'https://untappd.com/StPetersBreweryCo'
---

The brewery was built in 1996 and is housed in an attractive range of traditional former agricultural buildings adjacent to St. Peter’s Hall. Siting the brewery at St. Peter’s was ideal because of the excellent water quality from our own deep bore-hole. Locally malted barley is used, together with Kentish hops, to produce a range of classical English cask-conditioned ales. In addition thecompany produces a range of superb bottled beers. We brew ‘traditional’ beers (bitters, mild etc.) as well as some more unusual beers such as honey porter and fruit beer. (We are replicating what was common practice up to the Nineteenth Century to add fruits and honey to beers to create special seasonal brews).
