---
title: Brasserie Goudale - Les Brasseurs de Gayant
permalink: brewery/brasserie-goudale-les-brasseurs-de-gayant/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-1439_333a8_hd.jpeg'
location: 'Arques, Hauts-de-France France'
style: Macro Brewery
website: 'https://www.brasserie-goudale.com'
instagram: 'http://instagram.com/brasserie.goudale'
facebook: 'https://www.facebook.com/GoudaleBrasserie'
untappd: 'https://untappd.com/w/brasserie-goudale-les-brasseurs-de-gayant/1439'
---

