---
title: Vocation Brewery
permalink: brewery/vocation-brewery/
aliases:
  - vocation
  - vocation-x
location: 'Hebden Bridge, West Yorkshire England'
style: Micro Brewery
website: 'http://www.vocationbrewery.com'
instagram: 'http://instagram.com/vocationbrewery'
twitter: 'https://twitter.com/vocationbrewery'
facebook: 'http://www.facebook.com/vocationbrewery'
untappd: 'https://untappd.com/Vocation'
---

A brewery is just another factory making a product. It’s the people and their passion that make this our Vocation. Those that put in the long hours and hard graft for their craft, to bring better beer to more people. Beer that sets a benchmark and pushes the limits of what it’s supposed to be. Beer to feel proud of. Not just for ourselves but for those where a Vocation is that well deserved reward. For following their calling, chasing their ambition, or simply debriefing on a Friday – same time, usual pub. It’s our Vocation and it's theirs as well.
