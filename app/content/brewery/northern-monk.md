---
title: Northern Monk
permalink: brewery/northern-monk/
location: 'Leeds, West Yorkshire England'
style: Regional Brewery
aliases:
  - norther-monk
website: 'https://northernmonk.com'
instagram: 'http://instagram.com/northernmonk'
twitter: 'https://twitter.com/NMBCo'
facebook: 'https://www.facebook.com/NORTHERNMONKBREWCO'
untappd: 'https://untappd.com/NorthernMonk'
---

We are an independent brewery based in Holbeck, Leeds. Inspired by our northern surroundings and history of monastic brewing practiced across the region for thousands of years, we commit ourselves to creating the highest quality beers; combining the best of traditional monastic brewing values with a progressive approach to ingredients and techniques. We regularly collaborate with national and international breweries, businesses and charities to help strengthen the north for positive change, and to continually diversify our own offering, such as through our Patrons Projects. Today, in the north, we carry a torch for all the beer that was, and all that beer can be. We’re on a quest to create the best beer we can, giving back where we can.
