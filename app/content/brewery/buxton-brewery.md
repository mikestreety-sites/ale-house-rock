---
title: Buxton Brewery
permalink: brewery/buxton-brewery/
location: 'Buxton, Derbyshire England'
style: Micro Brewery
website: 'https://www.buxtonbrewery.co.uk'
instagram: 'http://instagram.com/BuxtonBrewery'
twitter: 'https://twitter.com/BuxtonBrewery'
facebook: 'https://www.facebook.com/BuxtonBreweryCompany'
untappd: 'https://untappd.com/BuxtonBrewery'
---

Buxton Brewery’s first ever brew was mashed-in on Jan 1st 2009. The brewhouse was Geoff’s garage and the batch was 40litres. For 12 months, Buxton cuckoo-brewed at a local microbrewery before, in 2010, brewing on a 800litre plant began. Fast-forward 9 years and the team is now in a custom designed brewhouse, producing 3500 litres per brew. We brew over 30 beers of all kinds, with many special editions already brewed with more to come. Our beers are designed to delight the senses and enthral the drinker. Some are big and strong, full of powerful hops and malts, others are lighter, and have subtle nuances of flavour and aroma. All are intended to be enjoyed as a lovingly hand-made product, nurtured by a team of dedicated beer lovers.
