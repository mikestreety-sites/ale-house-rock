---
title: London Beer Factory
permalink: brewery/london-beer-factory/
location: 'West Norwood, London England'
style: Micro Brewery
website: 'http://www.thelondonbeerfactory.com/'
instagram: 'http://instagram.com/londonbeerfactory'
twitter: 'https://twitter.com/ldnbeerfactory'
facebook: 'https://www.facebook.com/londonbeerfactory'
untappd: 'https://untappd.com/TheLondonBeerFactory'
---

London Beer Factory is an open exploration of beer, a journey into its shared moments and enjoyment. Modern and independent, crafting progressive beer since 2014 from our home in South London.
