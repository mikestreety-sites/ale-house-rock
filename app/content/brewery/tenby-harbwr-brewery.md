---
title: Tenby Harbwr Brewery
location: 'Tenby, Pembrokeshire Wales'
style: Micro Brewery
website: 'http://harbwr.wales'
instagram: 'http://instagram.com/Harbwr'
twitter: 'https://twitter.com/harbwraletenby'
facebook: 'https://www.facebook.com/Harbwr-Tenby-Harbour-Brewery-781233185331213/'
untappd: 'https://untappd.com/HarbwrBrewery'
permalink: brewery/tenby-harbwr-brewery/
---

HARBWR craft beer is currently available at the Buccaneer Inn and the Hope & Anchor Inn. The Brewery’s developing range of beers is also available in bottles for customers to take home with them, as well as being sold in local artisan restaurants, cafes & pubs. Cask ales often found in the Cardiff area.
