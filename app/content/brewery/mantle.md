---
title: Mantle
location: 'Cardigan, Ceredigion Wales'
style: Micro Brewery
website: 'http://www.mantlebrewery.com'
twitter: 'https://twitter.com/MantleBrewery'
facebook: 'https://www.facebook.com/pages/Mantle-Brewery-ltd/358393000848511'
untappd: 'https://untappd.com/MantleBrewery'
permalink: brewery/mantle/
---

Shop Opening Hours Monday to Friday: 9am to 5pm. Sat: 10:30 - 12:30. Sun: Closed Bragdy Mantle / Mantle Brewery Ltd is a family-run micro brewery proudly producing quality Welsh Real Ales. Our distinctive ales are crafted on the border of Ceredigion and Pembrokeshire in the picturesque market town of Cardigan. Our craft brewery is a modern facility, and by utilising traditional techniques and natural ingredients we brew a range of quality ales that remind you what beer should taste like.
