---
title: Brouwerij De Ranke
location: 'Mouscron, Région Wallonne Belgium'
style: Micro Brewery
website: 'https://www.deranke.be'
instagram: 'http://instagram.com/brouwerijderanke'
twitter: 'https://twitter.com/bierenderanke'
facebook: 'https://www.facebook.com/brouwerij.deranke'
untappd: 'https://untappd.com/BierenDeRanke'
permalink: brewery/brouwerij-de-ranke/
---

Why start a new brewery in a country like Belgium where breweries are everywhere already? In the early 80’s we –“beer lovers”- were concerned about the mass-disappearance of many small and medium-sized breweries in Belgium. Considering that they were the breweries capable of making the best beers. It was also alarming that during that period lots of outstanding, bitter beers were sweetened to meet the demands of mass-consumption. Assuming that people didn’t like the bitter taste anymore. The reality was that new techniques allowed using hop-extracts rather than hop-flowers, which allowed for cheaper production. Also, the industrial manufacturers believed that the taste of beer should be neutral in order to reach the masses (combined with mass-marketing). These developments in the beer industry led to us undertaking action to prove that quality can only be guaranteed with the best raw materials and with respect for traditional methods.
