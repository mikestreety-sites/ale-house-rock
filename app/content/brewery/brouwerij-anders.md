---
title: Brouwerij Anders!
permalink: brewery/brouwerij-anders/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-27467_bd61e.jpeg'
location: 'Halen, Vlaanderen Belgium'
style: Regional Brewery
website: 'https://www.brouwerijanders.be'
instagram: 'http://instagram.com/brouwerijanders'
twitter: 'https://twitter.com/bieranders'
facebook: 'https://www.facebook.com/brouwerijanders'
untappd: 'https://untappd.com/BrouwerijAnders'
---

BREWED TO BE DIFFERENT Custom craft beer brewing is our passion. "Beautiful beers which would otherwise not have seen the light of day, now have a chance to tingle taste buds of beer lovers all over the world."
