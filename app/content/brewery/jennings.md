---
title: Jennings Brewery
permalink: brewery/jennings-brewery/
aliases:
  - jennings
location: 'Cockermouth, Cumbria England'
style: Macro Brewery
website: 'http://www.jenningsbrewery.co.uk'
twitter: 'https://twitter.com/JenningsBrewery'
facebook: 'https://www.facebook.com/JenningsBrewery'
untappd: 'https://untappd.com/JenningsBreweryCockermouth'
---

Established as a family concern back in 1828, the original Jennings brewery was located in the village of Lorton, approximately 2 miles from the Brewery’s current location. In 1874, having outgrown the site, the brewery moved to the historic market town of Cockermouth, situated on the edge of the Lake District National Park. Nestled between the rivers Cocker and Derwent at the foot of the famous Cockermouth Castle, the site is perfect with its abundant supply of pure Lakeland water which is drawn from the brewery’s own well. A true craft brewery, the team at Jennings have continued to brew award winner beers which have helped make Jennings synonymous with the Lake District and the 16 million people who visit the area each year.
