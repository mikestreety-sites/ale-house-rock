---
title: To Øl
permalink: brewery/to-l/
location: 'Svinninge, Region Sjælland Denmark'
style: Micro Brewery
website: 'https://www.toolbeer.dk'
instagram: 'http://instagram.com/toolbeer'
twitter: 'https://twitter.com/toolbeer'
facebook: 'https://www.facebook.com/toolbeer'
untappd: 'https://untappd.com/Toolbeer'
---

As of 2019, we bought a 25,000 sqm facility out in the west coast of Sjælland and named it To Øl City. This place will be our craft beverage hub: a hub where we can have total autonomy over our brewing and other exciting projects; it's also where other talented producers from all over the world can come and utilise the space and resources available. It is about unleashing the potential of our craft beverage industries, and demanding the products be made accessible to everyone.
