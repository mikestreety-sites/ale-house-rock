---
title: Fierce Beer
permalink: brewery/fierce-beer/
location: 'Aberdeen, Aberdeen City Scotland'
style: Micro Brewery
website: 'https://www.fiercebeer.com'
instagram: 'http://instagram.com/fiercebeer'
twitter: 'https://twitter.com/fiercebeer'
facebook: 'https://www.facebook.com/fiercebeerco'
untappd: 'https://untappd.com/FierceHQ'
---

Multi-award-winning brewery based in Aberdeen, Scotland​
