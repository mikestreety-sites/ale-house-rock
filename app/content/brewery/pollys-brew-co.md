---
title: Polly's Brew Co.
location: 'Mold, Flintshire Wales'
style: Micro Brewery
website: 'https://pollysbrew.co'
instagram: 'http://instagram.com/pollysbrewco'
twitter: 'https://twitter.com/pollysbrewco'
facebook: 'https://www.facebook.com/pollysbrewco'
untappd: 'https://untappd.com/brewery/373345'
permalink: brewery/pollys-brew-co/
aliases:
  - pollys
---

We love to brew fresh, vibrant beers in small batches on Polly’s old farm in Mold, North Wales.
