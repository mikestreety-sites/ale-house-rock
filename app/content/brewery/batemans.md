---
title: Batemans
permalink: brewery/batemans/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-8151_712db_hd.jpeg'
location: England
style: Regional Brewery
website: 'https://www.bateman.co.uk/our-beers/'
instagram: 'http://instagram.com/batemansbrewery'
twitter: 'https://twitter.com/batemansbrewery'
facebook: 'https://www.facebook.com/batemansbrewery'
untappd: 'https://untappd.com/Batemans'
---

Batemans Brewery is a family brewery in Wainfleet Lincs UK, with the fourth generation Jaclyn and Stuart now at the helm, and the fifth training to eventually join the business. The Brewery was established in 1874. The beer is brewed by instinct, not by numbers using craft know-how and the best ingredients. Beer that pushes the boundaries. Beer with real flavour and character. Beer that brings people together. The celebrated Triple XB (XXXB) – a smooth yet distinctively flavoured English tawny beer – was first unveiled in 1978 by the then head brewer, Ken Dixon. It fast became a best-seller and big favourite amongst beer drinkers. In 1986, it was voted CAMRA’s Beer of the Year at the GBBF; and has been named CAMRA Premium Beer of the Year four times since. Along side XXXB 3 other core beers are brewed XB, Yella Belly Gold and Salem Porter. As well as cask range there are a variety of bottles which can be found in supermarkets, especially locally and on the Brewery’s online shop. The seasonal beer range celebrates monthly specials traditions, originally brewed for the seasons and inspired by great icons of British heritage. The most famous seasonal beer is Christmas Rosey Nosey. Batemans also have an adventurous streak which focuses on small batch production and a bit different from their norm. This beer evolution is driven through Salem Brew Co. With a new brewer having recently joined there are bound to be some more exciting
