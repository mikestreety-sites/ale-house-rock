---
title: Burnt Mill Brewery
permalink: brewery/burnt-mill-brewery/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-350204_2c2b0_hd.jpeg'
location: 'Ipswich, Suffolk England'
style: Micro Brewery
website: 'https://burnt-mill-brewery.myshopify.com/'
instagram: 'http://instagram.com/burntmillbrewery'
twitter: 'https://twitter.com/burntmillbeer'
facebook: 'https://www.facebook.com/Burnt-Mill-Brewery-1787125541610662/'
untappd: 'https://untappd.com/BurntMillBrewery'
---

Burnt Mill is a new brewing project focused on producing fresh, unfiltered beers from our brewhouse on a Suffolk farm.
