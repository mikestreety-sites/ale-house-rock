---
title: Track Brewing Company
permalink: brewery/track-brewing-company/
aliases:
  - track
location: 'Manchester, Greater Manchester England'
style: Micro Brewery
website: 'https://trackbrewing.co'
instagram: 'http://instagram.com/trackbrewingco'
twitter: 'https://twitter.com/trackbrewco'
facebook: 'https://www.facebook.com/Trackbrewingco'
untappd: 'https://untappd.com/trackbrewingco'
---

In 2008 Sam set off on what would become a two year odyssey cycling around the world. The first part of that trip took him from the East to the West Coast of the United States, it was on this leg that Sam started coming across small Taprooms and Breweries producing beer that left a lasting impression. Beer became more than a way to end the day. It became a way to reflect, connect and share. Local breweries were the lifeblood of communities large & small, places for discussion, inspiration & exploration. 1 bike, 5 continents, 25 countries, 35,000 miles. The start of an idea! After returning from that trip, Sam threw himself into the beer world, gaining experience in London before heading back to his home in the North West and settling on a 2000 Sq/ft Arch underneath Manchester's Piccadilly Station. Track was born. After years of working away in our little arch, we finally found a location to take the next step and realise the dream we'd had since our inception. To have a Production Facility & Taproom all under one roof. In the summer of 2021 we moved up the road to our new, all in one facility on Piccadilly trading Estate, our new home. A lot has happened since the start, more people have joined, the brewing capacity has increased but the heart of it remains the same. To produce beers to take you on your own journey.
