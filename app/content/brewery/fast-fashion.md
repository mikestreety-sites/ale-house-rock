---
title: Fast Fashion
location: 'Seattle, WA United States'
style: Micro Brewery
website: 'https://themasonryseattle.com/fast-fashion'
instagram: 'http://instagram.com/fastfashionbeer'
untappd: 'https://untappd.com/brewery/479889'
permalink: brewery/fast-fashion/
---

A hop focused brewery in Seattle, WA with a tasting room at The Masonry Fremont and a tasting room coming soon to Lower Queen Anne.
