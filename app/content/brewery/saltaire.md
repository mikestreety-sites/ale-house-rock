---
title: Saltaire Brewery
permalink: brewery/saltaire/
location: 'Shipley, West Yorkshire England'
style: Micro Brewery
website: 'http://www.saltairebrewery.co.uk'
instagram: 'http://instagram.com/saltairebrewery'
twitter: 'https://twitter.com/SaltaireBrewery'
facebook: 'http://www.facebook.com/saltairebrewery'
untappd: 'https://untappd.com/SaltaireBrewery'
---

BEER. DONE. RIGHT. At Saltaire we’re passionate about how we make our beer – it’s the only way we know. We brew with pride and stay true to ourselves. We don’t compromise on quality and we don’t cut corners. Truly independent, we’ve been making beer this way since 2006. Brewed our way, it’s honest beer with real character.
