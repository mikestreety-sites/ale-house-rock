---
title: Brasserie St-Feuillien
permalink: brewery/st-feuillien/
location: 'Le Rœulx, Wallonie Belgium'
style: Micro Brewery
website: 'http://www.st-feuillien.com'
instagram: 'http://instagram.com/st.feuillien'
twitter: 'https://twitter.com/st_feuillien'
facebook: 'https://www.facebook.com/stfeuillien'
untappd: 'https://untappd.com/w/brasserie-st-feuillien/295'
---

Brasserie St-Feuillien was founded in 1873 by Stéphanie Friart. In 1920, production was transfered to more modern buildings, which are still home to the majority of the current production facilities today. The construction of these buildings dates back to 1893. They have thus been the subject of continual renovations over the past few years, designed to harmoniously integrate modern équipment and 19th century industrial heritage. Now in its 5th generation of brewing tradition, the Friart family continues to turn out a wide range of quality beers, such as St-Feuillien and Grisette.
