---
title: Badger Beers (Hall & Woodhouse)
permalink: brewery/badger/
aliases:
  - badger-beers-hall-woodhouse
location: 'Blandford St Mary, Dorset England'
style: Regional Brewery
website: 'https://www.badgerbeers.com'
instagram: 'http://instagram.com/badgerbeers'
twitter: 'https://twitter.com/Badgerales'
facebook: 'https://www.facebook.com/BadgerBeers'
untappd: 'https://untappd.com/brewery/1752'
---

It was deep in Dorset’s rolling hills and patchwork fields, in 1777, that local farmer Charles Hall brewed our first ale. A union of the Hall & Woodhouse families soon followed, in both marriage & profession. The story of Badger had begun. Today, seven generations and two-and-a-half centuries later, we remain proudly independent, brewing characterful ales and sharing tales steeped in the flavours of the landscape we call home.
