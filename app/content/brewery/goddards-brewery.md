---
title: Goddards Brewery
location: 'Ryde, Isle of Wight England'
style: Micro Brewery
website: 'http://www.goddardsbrewery.com'
instagram: 'http://instagram.com/goddardsbrewery'
twitter: 'https://twitter.com/GoddardsBrewery'
facebook: 'https://www.facebook.com/goddards.brewery'
untappd: 'https://untappd.com/brewery/12708'
permalink: brewery/goddards-brewery/
---

Having been comprehensively ‘stuffed’ by Lloyd’s (the insurance market) Anthony Goddard and his wife Alix had to sell up their home and vineyard business at Barton Manor in Whippingham on the Isle of Wight. In 1991, having picked themselves up and dusted themselves off, they relocated to a caravan at the then derelict Barnsley Farm in Ryde, on the Island, where they began renovations.    Feeling unemployable in a more conventional role, the next idea was to start a new business - and having developed a taste for making alcohol, brewing seemed a logical step...
