---
title: Jack's Abby Craft Lagers
permalink: brewery/jacks-abbey/
location: 'Framingham, MA United States'
style: Regional Brewery
website: 'https://jacksabby.com'
instagram: 'http://instagram.com/jacksabbycraftlagers'
facebook: 'https://www.facebook.com/jacksabbycraftlagers'
untappd: 'https://untappd.com/JacksAbbyBrewing'
---

Jack's Abby is a family-owned craft brewery in Framingham, MA, brewing craft lagers with traditional German standards and American innovation.
