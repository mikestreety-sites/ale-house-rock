---
title: Other Half Brewing Co.
permalink: brewery/other-half-brewing-co/
location: 'Brooklyn, NY United States'
style: Regional Brewery
website: 'https://otherhalfbrewing.com'
instagram: 'http://instagram.com/OtherHalfNYC'
twitter: 'https://twitter.com/OtherHalfNYC'
facebook: 'https://www.facebook.com/otherhalfnyc'
untappd: 'https://untappd.com/brewery/94785'
---

In 2014, Sam Richardson, Matt Monahan and Andrew Burman founded Other Half Brewing Company, a local brewery in New York City with a simple mission: to create beers that they wantedto drink from a company that they wanted to be a part of. Their vision was to build a passionate team that brewed great beers in the state of New York—done so with effort and thoughtfulness—to represent the “Other Half” of the industry. Today, Other Half craft beers in their Brooklyn brewery and are dedicated to collaborating with breweries both in New York as well as across the world in an effort to constantly move the industry forward while elevating the craft. The Other Half team believes that local breweries play an important role in their communities which is why they partner with these other likeminded brewers and brands in local nabes across the country and the world—but always return to their home state of New York and their Brooklyn taproom.
