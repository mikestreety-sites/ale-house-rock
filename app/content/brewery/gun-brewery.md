---
title: Gun Brewery
permalink: brewery/gun-brewery/
aliases:
  - gin-brewery
location: 'Heathfield, East Sussex England'
style: Micro Brewery
website: 'https://www.gunbrewery.co.uk/'
twitter: 'https://twitter.com/GunBrewery'
untappd: 'https://untappd.com/GunBrewery'
---

Over the past few years, there has been a quiet revolution in the way people drink and make beer. Small brewers working with enthusiasm and great ingredients and without the constraints of large corporations are producing delicious beers once again. We are part of that movement. Our aim is simple; Inspired by brewing styles both old and new, to make great beer, using the best ingredients. Most of our beer is vegan and a growing number are also gluten free.
