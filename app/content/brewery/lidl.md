---
title: Lidl GB
permalink: brewery/lidl/
aliases:
  - down-south-brewing
  - hatherwood-craft-beer-company
  - perlenbacher
  - lidl-gb
location: 'Wimbledon, London England'
style: Bar / Restaurant / Store
website: 'https://www.lidl.co.uk'
instagram: 'http://instagram.com/lidlgb'
twitter: 'https://twitter.com/LidlGB'
facebook: 'https://www.facebook.com/lidlgb/'
untappd: 'https://untappd.com/w/lidl-gb/126407'
---

The first Lidl GB store opened its doors in 1994 and 25 years later we now have over 800 stores and 13 regional distribution centres across Great Britain, employing over 22,000 staff. We take pride in providing our customers with the highest quality products at the lowest possible prices, and work closely with our suppliers to make this possible. We’re also passionate about sourcing locally where possible, and approximately two thirds of our products are sourced from British suppliers. The Lidl brand was founded in Germany and has grown a great deal to become one of Europe's leading food retailers. Our own beer brand is called the 'Hatherwood Craft Beer Company.’ We collaborate withmany breweries and all come under this banner.
