---
title: Dynamite Valley Brewing Co.
permalink: brewery/dynamite-valley-brewing-company/
location: 'Truro, Cornwall England'
style: Micro Brewery
website: 'http://www.dynamitevalley.com'
instagram: 'http://instagram.com/dynamitevalleybrewco'
twitter: 'https://twitter.com/dynamitevalley'
facebook: 'https://www.facebook.com/dynamitevalley/'
untappd: 'https://untappd.com/DynamiteValley'
---

