---
title: Stannary Brewing Company
permalink: brewery/stannary-brewing-company/
location: 'Tavistock, Devon England'
style: Micro Brewery
website: 'http://www.stannarybrewing.shop'
instagram: 'http://instagram.com/stannarybrewing'
twitter: 'https://twitter.com/stannarybrewing'
facebook: 'https://www.facebook.com/stannarybrewing'
untappd: 'https://untappd.com/StannaryBrewingCompany'
---

Tavistock's Own Brewery,,,,, producers of full flavoured, modern beers in the heart of West Devon since 2016. Eebria Trade page: https://www.eebriatrade.com/producer/show/905
