---
title: Long Man Brewery
permalink: brewery/long-man-brewery/
location: 'Litlington, East Sussex England'
style: Micro Brewery
website: 'http://www.longmanbrewery.com'
instagram: 'http://instagram.com/LongManBrewery'
twitter: 'https://twitter.com/longmanbrewery'
facebook: 'https://www.facebook.com/longmanbrewery'
untappd: 'https://untappd.com/LongManBrewery'
---

Here at Long Man we define craft as handmade, locally sourced, and sustainable, and we believe this is evident in every pint of our award-winning beers. Brewing since 2012, when Duncan Ellis said goodbye to his dairy herds at Church Farm and turned the site into a brewery. Visit our new brewery shop and tasting room, all on site at Church Farm. A range of cask ales are available to try and take away in cask, bottle, polypin and mini cask. Clothing, merchandise and gift options also available. Starting in the wonderfully restored flint walled brewery shop & tasting room, our brewery tour and beer tasting experience will provide you with an insight into the history of Church Farm and agriculture in the beautiful Cuckmere Valley.
