---
title: Bini Brew Co
location: 'Ilkley, West Yorkshire England'
style: Nano Brewery
website: 'http://binibrew.co'
instagram: 'http://instagram.com/binibrew'
twitter: 'https://twitter.com/binibrew'
facebook: 'http://facebook.com/binibrew/'
untappd: 'https://untappd.com/brewery/478961'
permalink: brewery/bini-brew-co/
---

We are Bini Brew Co, Ilkley’s newest brewery and pop-up taproom right in the heart of the town. We craft small-batch modern craft beers in keg and cans, with a wide range of hoppy pale ales, juicy IPAs, lagers, sours and DIPAs.
