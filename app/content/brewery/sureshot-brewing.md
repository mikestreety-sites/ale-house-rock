---
title: Sureshot Brewing
location: 'Manchester, Greater Manchester England'
style: Micro Brewery
website: 'http://sureshotbrew.com'
instagram: 'http://instagram.com/sureshotbrew'
twitter: 'https://twitter.com/sureshotbrew'
facebook: 'https://www.facebook.com/sureshotbrew'
untappd: 'https://untappd.com/SureshotBrewing'
permalink: brewery/sureshot-brewing/
---

Brewing beer to be enjoyed. High quality & small batch. From the beating belly of Manchester.
