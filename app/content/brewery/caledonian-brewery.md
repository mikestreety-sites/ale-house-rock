---
title: Caledonian Brewing Co.
permalink: brewery/caledonian-brewery/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-2071_7939f_hd.jpeg'
location: 'Edinburgh, City of Edinburgh,Midlothian Scotland'
style: Macro Brewery
website: 'https://www.caledonianbeer.com'
instagram: 'http://instagram.com/caledonianbrewery'
twitter: 'https://twitter.com/caledonianbeer'
facebook: 'https://www.facebook.com/caledonianbeer'
untappd: 'https://untappd.com/caledonianbeer'
---

We are proud custodians of a Victorian brewery in Scotland's capital. The Caledonian Brewery was built in 1869, drawing on abundant supplies of local barley and mineral-rich well water. The brew house, blessed with Victorian ingenuity, is still right at the heart of our brewing process. Our direct-fired open coppers are believed to be the last remaining in Europe of this size and age in commercial use. With nearly a century and a half of brewing experience, we are continuously trying out new ideas to make sure that our beers remain as relevant for the tastes of today's drinkers as they were for generations before.
