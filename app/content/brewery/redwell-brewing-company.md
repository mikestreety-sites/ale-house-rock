---
title: Redwell Brewing Co.
permalink: brewery/redwell-brewing-company/
location: 'Norwich, Norfolk England'
style: Micro Brewery
website: 'http://www.redwellbrewing.com'
instagram: 'http://instagram.com/redwellbrewing'
twitter: 'https://twitter.com/BrewingRedwell'
facebook: 'https://www.facebook.com/redwellbeer'
untappd: 'https://untappd.com/redwellbrewing'
---

Redwell is run by a small and very passionate team, who warmly welcome you to the brewery. Our goal? To create fantastic local beer for local people to enjoy; whether that’s here at our taproom or in one of the many establishments that we supply across East Anglia. We firmly believe that great beers are for everyone; which is why our full range is certified vegan and gluten-free. We brew in small batches right here at the brewery, using locally farmed Norfolk malt and a variety of exciting hops from around the world. Here at the taproom you can enjoy fresh beer straight from the source, alongside food from our resident foodies, The Redwell Vault Pizzeria. We also host a variety of pop-up food options on Sundays.
