---
title: BritHop Brew Co
location: 'Belvedere, England'
style: Micro Brewery
website: 'https://www.brithopbeer.com/'
twitter: 'https://twitter.com/Brithopbrewco'
facebook: 'https://www.facebook.com/Brithopbrewingco/'
untappd: 'https://untappd.com/brewery/397808'
permalink: brewery/brithop-brew-co/
---

If like us, you're bang into your 90's Indie tunes & great craft beer then, you've come to the right place. We blend our beer styles along with our favourites tunes & fashion from the classic 'Britpop' era. Whether you are Blur or Oasis, there's something for everyone.  Cheers our kid!
