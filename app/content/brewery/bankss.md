---
title: Banks's Beer
permalink: brewery/bankss/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-BankssUK_11798.jpeg'
location: 'Wolverhampton, West Midlands England'
style: Regional Brewery
website: 'https://www.bankssbeer.co.uk'
instagram: 'http://instagram.com/bankssbeer'
twitter: 'https://twitter.com/BankssBeer'
untappd: 'https://untappd.com/BankssBeer'
---

The people of Wolverhampton are renowned for their witty, straight-talking approach to life, telling it exactly how it is. As the beer of Wolverhampton, we have adopted that no-nonsense approach. No frills, just great beer. Standing tall above the Wolverhampton city skyline with its iconic chimney, Bank’s Brewery has been brewing great beers since 1875. The famous Victorian Park Brewery has remained faithful to its longstanding principal that to brew the best beers, you must use the best natural ingredients. The brewery today is a hive of activity, with a team of dedicated brewers making sure those principals remain at the heart of the brewery.
