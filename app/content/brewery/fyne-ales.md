---
title: Fyne Ales
permalink: brewery/fyne-ales/
location: 'Cairndow, Argyll and Bute Scotland'
style: Micro Brewery
website: 'https://www.fyneales.com'
instagram: 'http://instagram.com/FyneAles'
twitter: 'https://twitter.com/Fyneales'
facebook: 'https://www.facebook.com/FyneAles'
untappd: 'https://untappd.com/FyneAles'
---

We are Fyne Ales. We are a family-owned Scottish farm brewery and we've been creating modern, ambitious and progressive beers since 2001.
