---
title: Wimbledon Brewery
permalink: brewery/wimbledon-brewery/
location: 'Wimbledon, London England'
style: Brew Pub
website: 'http://www.wimbledonbrewery.com/'
instagram: 'http://instagram.com/Wimbledonbrewery'
twitter: 'https://twitter.com/WimbledonBrew'
facebook: 'https://www.facebook.com/wimbledonbrewery'
untappd: 'https://untappd.com/Wimbledon_Brewery'
---

The Wimbledon Brewery is the home of South West Londons finest Beers and Ales. Founded in 2015 on the site of the original brewery dating back to 1832, we are custodians of the heritage and traditions of beer brewing, whilst also inspiring the new generation of beer drinkers and brewers. We have our own Tap Room available on site.
