---
title: Allkin Brewing Company
location: 'Tunbridge Wells, Kent England'
style: Micro Brewery
website: 'https://allkinbrewing.com'
instagram: 'http://instagram.com/allkinbrewing'
twitter: 'https://twitter.com/AllkinBrewing'
facebook: 'https://www.facebook.com/allkinbrewing'
untappd: 'https://untappd.com/brewery/529956'
permalink: brewery/allkin-brewing-company/
aliases:
	- allkin-brewing
---
