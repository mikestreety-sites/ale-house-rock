---
title: Phantom Brewing Co.
permalink: brewery/phantom-brewing-co/
location: 'Reading, Berkshire England'
style: Micro Brewery
website: 'https://www.phantombrew.com'
instagram: 'http://instagram.com/phantombrewco'
twitter: 'https://twitter.com/PhantomBrewCo'
facebook: 'https://www.facebook.com/phantombrewco'
untappd: 'https://untappd.com/PhantomBrewingCo'
---

Phantom Brewing Co. is a craft beer brewery based in Reading, Berkshire. With the main goal of producing the freshest beer, made with the highest quality ingredients, we have worked hard to provide a welcoming community hub in our taproom in Reading that welcomes any person from any background! After the third lockdown in early 2021, we opened our second bar in the adjacent warehouse unit, and extended our opening hours to include Sunday afternoons. Our two bars in adjacent units, Neon & The Haunt, are open for walk-ins every weekend. We look forward to welcoming you to Phantom! Keep an eye on our social feeds for news of upcoming street food vendors, events and new beers.
