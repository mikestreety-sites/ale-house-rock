---
title: Finback Brewery
location: 'Queens, NY United States'
style: Micro Brewery
website: 'https://www.finbackbrewery.com'
instagram: 'http://instagram.com/finbackbrewery'
twitter: 'https://twitter.com/finbackbrewery'
facebook: 'https://www.facebook.com/FinbackBrewery'
untappd: 'https://untappd.com/Finback'
permalink: brewery/finback-brewery/
---

Brewed with Care in Queens, NYC
