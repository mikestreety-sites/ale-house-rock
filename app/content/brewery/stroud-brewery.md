---
title: Stroud Brewery
permalink: brewery/stroud-brewery/
location: 'Thrupp, Stroud, Gloucestershire England'
style: Micro Brewery
website: 'https://stroudbrewery.co.uk'
instagram: 'http://instagram.com/stroudbrewery'
twitter: 'https://twitter.com/stroudbrewery'
facebook: 'https://www.facebook.com/stroudbrewery'
untappd: 'https://untappd.com/StroudBrewery'
---

Stroud Brewery established in 2006, initially a passion project for our founder Greg Pilley; an adventurer, environmentalist and beer lover. Today we are a thriving enterprise and our tap room has become one of Stroud’s cherished local landmarks. We make great tasting, ethical and organic beer. Our mission is to make outstanding organic beer without damaging the planet and bring peopletogether to inspire positive change. We believe the only way to be truly sustainable is to invest in responsible organic farming. If we don’t look after our soils through organic farming they won’t be healthy enough to feed us in 60 years. We also work hard to lower our carbon and water footprints and implement circular economy principles.
