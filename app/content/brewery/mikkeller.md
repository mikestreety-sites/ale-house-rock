---
title: Mikkeller
permalink: brewery/mikkeller/
location: 'Copenhagen, Region Hovedstaden Denmark'
style: Micro Brewery
website: 'http://www.mikkeller.com'
instagram: 'http://instagram.com/mikkellerbeer'
twitter: 'https://twitter.com/MikkellerBeer'
facebook: 'http://www.facebook.com/mikkeller'
untappd: 'https://untappd.com/brewery/2813'
---

In 2006 he was a math and physics teacher that started experimenting with hops, malt and yeast back home in his kitchen in Copenhagen. Today Mikkel Borg Bjergsø exports his micro brewed beer to 40 different countries and is internationally acclaimed as one of the most innovative and cutting edge brewers in the world.
