---
title: Brakspear Brewing Company
permalink: brewery/brakspear/
image: >-
  https://assets.untappd.com/site/brewery_logos/brewery-WHBrakspearSons_1353.jpeg
location: 'Witney, Oxfordshire England'
style: Micro Brewery
website: 'http://www.brakspear-beers.co.uk'
twitter: 'https://twitter.com/Oxfordshirebeer'
facebook: 'http://www.facebook.com/pages/Brakspear-Beer/122175544521430'
untappd: 'https://untappd.com/BrakspearBrewingCompany'
---

Following the closure of the Brakspear Brewery in Henley on Thames, Wychwood Brewery in Witney set up the Brakspear Brewing Company and took on the brewing of Brakspear Beers from its site in Witney, Oxfordshire.
