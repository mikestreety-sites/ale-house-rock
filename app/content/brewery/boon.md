---
title: Brouwerij Boon
permalink: brewery/boon/
image: 'https://assets.untappd.com/site/brewery_logos_hd/brewery-268_73ba2_hd.jpeg'
location: 'Lembeek, Vlaanderen Belgium'
style: Regional Brewery
website: 'https://www.boon.be'
instagram: 'http://instagram.com/brouwerij_boon'
twitter: 'https://twitter.com/BrouwerijBoon'
facebook: 'https://www.facebook.com/BrouwerijBoon'
untappd: 'https://untappd.com/BrouwerijBoon'
---

Since 1975, Boon Brewery has been owned and run by Frank Boon, one of the pioneers of the authentic lambic and gueuze revival. But the first signs of the brewery date back to 1680, as a farm-brewery and distillery in the village of Lembeek. In 1860 Louis Paul bought the brewery to brew only lambic and faro. From 1875, he began bottling gueuze lambic. In 1898, Pierre Troch bought the brewery. But after the economic crisis of 1927, it came into the hands of Joseph De Vits. His son, Rene is renowned for the production of soft and fine gueuze lambic. As Rene De Vits had no children, he sold the brewery to Frank Boon.
