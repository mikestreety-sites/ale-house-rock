---
title: Full Circle Brew Co
location: 'Newcastle upon Tyne, Tyne and Wear England'
style: Micro Brewery
website: 'https://fullcirclebrew.co.uk'
instagram: 'http://instagram.com/fullcirclebrew'
twitter: 'https://twitter.com/fullcirclebrew'
facebook: 'https://www.facebook.com/fullcirclebrew'
untappd: 'https://untappd.com/FullCircleBrewCo'
permalink: brewery/full-circle-brew-co/
---

Based in Hoults Yard, within easy reach of Newcastle city centre, in the exciting area of Byker/Walker/Ouseburn. Immerse yourself in the brewery environment from the comfort of our glass-fronted taproom, and choose from up to 20 different cask and keg products from our core range, seasonals, and collaborations, along with guest taps from some of our best friends in the craft brewing scene.
