---
title: 'The Seed: A Living Beer Project'
location: 'Atlantic City, NJ United States'
style: Micro Brewery
website: 'http://theseedbeer.com'
instagram: 'http://instagram.com/theseedbeer'
facebook: 'https://www.facebook.com/TheSeedBeer'
untappd: 'https://untappd.com/brewery/386673'
permalink: brewery/the-seed-a-living-beer-project/
---

