---
title: Brewgooder
location: 'Glasgow, Glasgow City,Dunbartonshire Scotland'
style: Contract Brewery
website: 'http://www.brewgooder.com'
instagram: 'http://instagram.com/brewgooder'
twitter: 'https://twitter.com/brewgoodr'
facebook: 'http://facebook.com/brewgooder'
untappd: 'https://untappd.com/Brewgooder'
permalink: brewery/brewgooder/
---

Brewgooder was founded to harness the power of beer to make a positive difference in the world. Since 2016, every single can and pint of Brewgooder enjoyed has made waves by helping to fund the completion of people and community projects undertaken through the Brewgooder Foundation. Our drinkers have reached and made a positive difference to the lives of over 200,000 people worldwide, with projects spanning vital areas such as clean water & sanitation, hunger, inequality and climate action. To discover the impact made by our beer, scan the QR code found on our cans.
