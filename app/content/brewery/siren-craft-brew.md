---
title: Siren Craft Brew
permalink: brewery/siren-craft-brew/
location: 'Wokingham, Berkshire England'
style: Micro Brewery
website: 'http://www.sirencraftbrew.com'
instagram: 'http://instagram.com/sirencraftbrew'
twitter: 'https://twitter.com/sirencraftbrew'
facebook: 'http://www.facebook.com/sirencraftbrew'
untappd: 'https://untappd.com/SirenCraftBrew'
---

Our desire is to discover ways to excite people’s curiosity and share our knowledge of our craft. We believe all beer drinkers should be able to enjoy craft beer as wine drinkers enjoy the finest wines. Our sirens exist to the share our message and open people’s minds.
