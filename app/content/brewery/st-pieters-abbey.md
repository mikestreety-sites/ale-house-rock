---
title: Brouwerij Palm
permalink: brewery/st-pieters-abbey/
location: 'Steenhuffel, Vlaanderen Belgium'
style: Macro Brewery
website: 'https://www.palm.be'
twitter: 'https://twitter.com/palmbelgie'
facebook: 'https://www.facebook.com/PalmBelgie'
untappd: 'https://untappd.com/brouwerijpalm'
---

De geschiedenis van brouwerij PALM gaat terug tot 1686. Tegenover de kerk in Steenhuffel, baat Theodoor Cornet, rentmeester van Kasteel Diepensteyn, een herberg uit. Hij brouwt er bier en stookt er jenever voor zijn gasten. Dankzij de comeback van de traditionele streekbiercultuur in België, als gevolg van de studentenrevolte en de flowerpowerbeweging in 1968, wordt Alfred Van Roy de hoofdrolspeler en sterkste groeier met zijn Brabantse amberbier ‘Spéciale PALM’. In de volksmond wordt Brouwerij DE HOORN steeds vaker de brouwerij van de PALM genoemd. In 1974 verandert ze dan ook haar naam in Brouwerij PALM.
