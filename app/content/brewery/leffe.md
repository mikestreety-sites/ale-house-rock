---
title: Abbaye de Leffe
permalink: brewery/leffe/
location: 'Leuven, Vlaanderen Belgium'
style: Macro Brewery
website: 'https://leffe.com'
instagram: 'http://instagram.com/leffe_belgium'
twitter: 'https://twitter.com/Leffe'
facebook: 'https://www.facebook.com/Leffe'
untappd: 'https://untappd.com/w/abbaye-de-leffe/5'
---

Leffe, a brewing tradition since 1240 Founded in 1152, Notre-Dame de Leffe was an abbey of Premonstratensian canons, i.e. monks living in a community characterised by its hospitality. The Leffe abbey in 1740, in its heyday: in the foreground we can see the mill with its half-timbered gable, using the water of the Leffe river. In the background, facing each other, are two enemy fortresses: the Montorgueil tower and the castle of Crevecoeur. A lay master brewer worked for the abbey and made a Leffe beer that was so delicious that the parishioners preferred to drink a Leffe on Sundays rather than go to church. The abbot had to take forceful action. The abbey and the brewery were closed during the French Revolution and seemed to be nothing but a distant memory, until the abbey was re-established in 1929. In 1952 abbot Nys and Albert Lootvoet decided to once again take up the brewing tradition of Leffe with its well-guarded recipe and offer a range of delicious Leffe beers. In the meantime, AB-InBev has taken up the torch and has made a commitment to honour the tradition of the Leffe beer, which has been brewed according to the same recipe since 1240.
