---
title: Red Rock Brewery
permalink: brewery/red-rock-brewery/
location: 'Bishopsteignton, Devon England'
style: Regional Brewery
website: 'http://www.redrockbrewery.co.uk/'
instagram: 'http://instagram.com/redrockbreweryuk'
twitter: 'https://twitter.com/redrockbrewery1'
facebook: 'https://www.facebook.com/RedRockBrewery'
untappd: 'https://untappd.com/RedRockBrewery'
---

Established in 2006, we are an award winning family run brewery based in South West England. Our extensive range of delicious English beers use not only the finest natural ingredients but also pure spring water from the South Devon farm where we are based. We take great pride in our artisan approach to producing innovative and best quality ales and that remains at the heart of what we do. Choose from our traditional, craft and slow beer ranges – all with a unique character and flavour of their own.
