---
title: Modern Times Beer
permalink: brewery/modern-times-beer/
location: 'San Diego, CA United States'
style: Regional Brewery
website: 'https://www.moderntimesbeer.com'
instagram: 'http://instagram.com/ModernTimesBeer'
twitter: 'https://twitter.com/ModernTimesBeer'
facebook: 'https://www.facebook.com/ModernTimesIsYourFriend'
untappd: 'https://untappd.com/ModernTimesBeer'
---

Modern Times is an intrepid cadre of brewers, coffee roasters, culinary wizards, creative powerhouses, and beer-slingers that began as a 30bbl production brewery and tasting room in the Point Loma neighborhood of San Diego in 2013. Since its inception, the Modern Times multiverse has expanded to include a robust and ever-growing barrel program, & tasting room locations.
