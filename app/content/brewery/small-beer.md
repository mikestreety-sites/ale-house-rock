---
title: Small Beer Brew Co
permalink: brewery/small-beer/
location: 'South Bermondsey, London England'
style: Micro Brewery
website: 'https://theoriginalsmallbeer.com/'
instagram: 'http://instagram.com/originalsmallbeer'
twitter: 'https://twitter.com/origsmallbeer'
facebook: 'https://www.facebook.com/originalsmallbeer/'
untappd: 'https://untappd.com/Small-Beer-Brew-Co'
---

Small beer is for big thinkers. We are reinvigorating a lost tradition for the people. For us, big thinking means pioneering new brewing. We have built a bespoke brewing kit to exclusively produce lower alcohol beer up to 2.8% - the world's first dedicated small beer brewery. In a world where drinking water could be fatal, small beer was enjoyed in households, workplaces andeven schools across the country for it's hydrating and nutritional properties. A staple of British life in the 1700’s, it was traditionally always brewed between 0.5-2.8%. With provision of clean drinking water in the 19th century, small beer passed and the art of its creation was lost. We specialise solely in the production of this historic beer so rich in history!
