---
title: Hiver Beers
permalink: brewery/hiver/
location: 'City of London, London England'
style: Contract Brewery
website: 'http://www.hiverbeers.com/'
instagram: 'http://instagram.com/hiverbeers'
twitter: 'https://twitter.com/HiverBeers'
facebook: 'https://www.facebook.com/hiverbeers'
untappd: 'https://untappd.com/w/hiver-beers/413455'
---

We've been as committed to bees as we have been to beer since we started brewing in 2013. We know that without bees we’d all be in a difficult place environmentally, so we made it our cause to champion these gems of nature. We’re helping independent British beekeepers by buying their honey to nurture urban and rural hives, creating colonies that can do the heavy lifting of pollinating the food we eat. We use the honey in our brewing, helping us to deliver this crisp, light-tasting beer with a subtle honey aroma. We also don’t pasteurise our beer,which means you always get the freshest, most natural beer. And, just like the bees, we tread lightly in all areas of our business, so there’s plenty of reasons to feel good about choosing Hiver.
