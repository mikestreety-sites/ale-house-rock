---
title: Briarbank Brewing Company
permalink: brewery/briarbank-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-63665_81148.jpeg'
location: 'Ipswich, Suffolk England'
style: Nano Brewery
website: 'http://www.briarbank.org'
instagram: 'http://instagram.com/briarbankbrewery'
twitter: 'https://twitter.com/briarbankbrew'
facebook: 'https://www.facebook.com/BriarbankBrewery'
untappd: 'https://untappd.com/BriarbankBrewingCompany'
---

When you’re looking for a great beer experience in Suffolk, look no further than the Briarbank Brewing Company. Set next to the historic waterfront in the heart of Ipswich, our award-winning, independent microbrewery and bar was established in May 2013, and we’ve been creating beers ever since. We offer a variety of ales, milds, stouts and a continental style lager. While some are regular favourites, at Briarbank we keepthings fresh with seasonal beers, variations on old favourites and more. We’ve won two golds and one silver for the East Region in the SIBA Independent Keg Beer Awards 2020, and two golds in the 2020 SIBA Digital Beer Awards. There's always something brewing to keep the drinkers happy!
