---
title: Tenby Brewing Co
permalink: brewery/tenby-brewing-co/
location: 'Tenby, Pembrokeshire Wales'
style: Micro Brewery
website: 'https://www.tenbybrewingco.com'
instagram: 'http://instagram.com/Tenbybrewingco'
twitter: 'https://twitter.com/Tenbybrewingco'
facebook: 'https://www.facebook.com/Tenbybrewingco'
untappd: 'https://untappd.com/TenbyBrewingCompany'
---

- Juicy brews since 2015 - Order fresh cans here>> www.tenbybrewingco.com Or get in touch for trade supply>>([email&#160;protected]) - Independent beer; brewed by the beach.
