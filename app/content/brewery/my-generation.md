---
title: My Generation Beer Co.
permalink: brewery/my-generation/
location: England
style: Contract Brewery
website: 'http://mygenerationbeer.co.uk/'
twitter: 'https://twitter.com/mygenbeer'
facebook: 'https://www.facebook.com/mygenerationbeer'
untappd: 'https://untappd.com/w/my-generation-beer-co/189696'
---

Beers are brewed under contract by Black Sheep.
