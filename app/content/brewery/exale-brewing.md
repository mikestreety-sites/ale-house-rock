---
title: Exale Brewing
permalink: brewery/exale-brewing/
location: 'Walthamstow, England'
style: Micro Brewery
website: 'https://exalebrew.co.uk'
instagram: 'http://instagram.com/exalebrewing'
twitter: 'https://twitter.com/exalebrewing'
facebook: 'https://www.facebook.com/ExaleBrewingTaproom'
untappd: 'https://untappd.com/ExaleBrewing'
---

