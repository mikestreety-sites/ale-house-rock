---
title: Double-Barrelled Brewery
location: 'Tilehurst, Reading England'
style: Micro Brewery
website: 'https://doublebarrelled.co.uk'
instagram: 'http://instagram.com/doublebarrelledbrewery'
twitter: 'https://twitter.com/dbbrewery'
facebook: 'https://www.facebook.com/DoubleBarrelledBrewery'
untappd: 'https://untappd.com/DoubleBarrelledBrewery'
permalink: brewery/double-barrelled-brewery/
---

Double-Barrelled are an independent, modern brewery based in Reading, Berkshire. We specialise in small batch beers, brewed and packaged by a passionate team with an unwavering dedication to quality and a love for great beer. We brew a wide range of styles; Barrel-aged Imperial Stouts to juicy Pale Ales, big hoppy IPAs and zingy sours. Nearly all our beers are vegan and we take pride in sourcing the best ingredients to create our beers, grain to glass. We strive to be known for our dedication to quality, consistency and to do it with passion, care & kindness.
