---
title: Guinness Open Gate Brewery
permalink: brewery/open-gate-brewery/
location: 'Halethorpe, MD United States'
style: Macro Brewery
website: 'https://www.guinnessbrewerybaltimore.com'
instagram: 'http://instagram.com/guinnessbrewerybalt'
facebook: 'https://www.facebook.com/GuinnessBreweryBalt'
untappd: 'https://untappd.com/GuinnessOGBBalt'
---

The first Guinness brewery on American soil for over 60 years, the Guinness Open Gate Brewery is the home of Guinness Blonde Lager and the birthplace of new Guinness beers. Proudly brewing in Maryland.
