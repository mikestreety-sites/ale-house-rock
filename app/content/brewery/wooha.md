---
title: WooHa Brewing Company
permalink: brewery/wooha/
location: 'Kinloss, Moray Scotland'
style: Micro Brewery
website: 'http://www.woohabrewing.com'
instagram: 'http://instagram.com/wooha_brewing'
twitter: 'https://twitter.com/woohabrewing'
facebook: 'http://www.facebook.com/WoohaBrewingCompany'
untappd: 'https://untappd.com/WooHaBrewingCompany'
---

Using expertly blended malts, balanced flavours and the cool, clear waters of Scotland, WooHa Brewing Company has strong ideas about how they brew our beers - and how they want their customers to feel about drinking them. All WooHa beers are bottle, cask and KeyKeg conditioned to preserve mouthfeel and hop oils. Complex and moreish, this is Scotland…bottled.
