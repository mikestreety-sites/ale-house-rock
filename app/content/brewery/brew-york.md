---
title: Brew York
permalink: brewery/brew-york/
location: 'York, England'
style: Micro Brewery
website: 'https://brewyork.co.uk'
instagram: 'http://instagram.com/brewyorkbeer'
twitter: 'https://twitter.com/brewyorkbeer'
facebook: 'https://www.facebook.com/BrewYorkBeer'
untappd: 'https://untappd.com/BrewYork'
---

Located in York’s bustling city centre, just off Walmgate, Brew York is a showpiece 20Hl craft brewery alongside its tap room and beer hall. In early 2021 main production expanded to a twin 50Hl state of the art brewhouse on the outskirts of York. Featuring 40 taps the Beer Hall can boast the largest and widest selection of beers, lagers and ciders in York and its surrounding area. With its own purpose built kitchen we can deliver some of the best street food in the area alongside your beer. In addition to the awesome Beer Hall we have a smaller Tap Room located just next door within the brewery building itself. You can sit within touching distance of the fermenting tanks, even the brewers themselves! Come down and enjoy our beer whilst overlooking our latest brews fermenting away, or enjoy a fresh pint in our beer garden opposite the impressive Rowntree Wharf. We have 10 cask beers on rotation as well as 10 keg beer lines including cider and lager lines. A fantastic place to take in a brew and the view, as with the Beer Hall we are fully dog and family friendly. Open Tues - Sat 12pm - 11pm Sun 12pm - 9pm Food available until 2 hours before close.
