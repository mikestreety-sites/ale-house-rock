---
title: PLAY BREW CO̠
permalink: brewery/play-brew-co/
location: 'Middlesbrough, England'
style: Micro Brewery
website: 'https://www.playbrewco.com'
instagram: 'http://instagram.com/playbrewco'
twitter: 'https://twitter.com/playbrewco'
facebook: 'https://www.facebook.com/playbrewco'
untappd: 'https://untappd.com/PlayBrewCo'
---

WE BRING YOU CRAFT BEER WITH A NOSTALGIC TWIST, BORN FROM INFLUENTIAL CULTURE OF THE 80'S & 90'S.
