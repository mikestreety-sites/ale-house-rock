---
title: Allendale Brewery
permalink: brewery/allendale-brewery/
image: 'https://assets.untappd.com/site/brewery_logos/brewery-8263_a277c.jpeg'
location: 'Hexham, Northumberland England'
style: Micro Brewery
website: 'http://www.allendalebrewery.com/beers'
facebook: 'https://www.facebook.com/allendale.brewery'
untappd: 'https://untappd.com/AllendaleBrewery'
---

Based in Northumberland, near the borders of Cumbria and Durham, brewing quality ales since 2006. The brewery is a state of the art 20 barrel plant run out of a historic lead smelting mill. During the industrial revolution the mill would have formed the centre of the mining industry in the area. This heritage is reflected in the identities of our beers.
