---
title: First Chop Brewing Arm
permalink: brewery/first-chop-brewing-arm/
location: 'Salford, Greater Manchester England'
style: Micro Brewery
website: 'http://www.firstchopbrewingarm.com'
instagram: 'http://instagram.com/firstchop'
twitter: 'https://twitter.com/FirstChopAle'
facebook: 'https://www.facebook.com/firstchopbrewingarm'
untappd: 'https://untappd.com/FirstChopBrewingArm'
---

Brewing without prejudice since 2013, our passion for bringing people together over a pint is at the core of everything we do, that is why you will find all our beer is gluten free and vegan friendly. We have a simple vision and that is to remain loyal to our cause, give back to our people and planet, all whilst becoming the most inclusive craft beer brand you will find on the shelves.
