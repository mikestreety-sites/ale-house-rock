---
title: Slater's Ales
permalink: brewery/slaters-handcrafted-ales/
location: 'Stafford, Staffordshire England'
style: Micro Brewery
website: 'http://www.slatersales.co.uk'
twitter: 'https://twitter.com/slatersales'
facebook: 'https://www.facebook.com/pages/Slaters-Ales/356019043660'
untappd: 'https://untappd.com/SlatersAles'
---

Ged and Moyra Slater started Slater’s back in 1995, when it was originally called the Eccleshall Brewery and based behind the George Hotel in the town of the same name. Business boomed and our beers became popular throughout the Midlands. In 2006, the brewery moved to our current home in Stafford and also became known as Slater’s Ales. The 30-barrel brewery still remains in family hands, though Ged’s son Andrew is now in charge. During the summer of 2019,the brewery was very excited to install a hop cannon, which as its name might suggest, is a bit of brewing kit that enables more hops to be added to Slater’s beers. We also host an annual Oktoberfest with music, a hog roast and plenty of beer with fancy dress encouraged.
