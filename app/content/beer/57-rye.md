---json
{
    "title": "Shipyard Rye Pale Ale",
    "number": "57",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BXlPyoflVSX/",
    "date": "2017-08-09",
    "permalink": "beer/shipyard-rye-pale-ale-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "An apparently pale ale which is a tropical and citrus beer. It&#39;s getting better with every sip. An odd one, but well &quot;needed&quot;"
}
---