---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1398440025",
  "title": "Harvest Beer - Munich Is Thataway",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-07-13",
  "style": "Pale Ale - English",
  "abv": "4%",
  "breweries": [
    "brewery/modest-beer/",
    "brewery/ards-brewing-co/"
  ],
  "number": 947,
  "permalink": "beer/harvest-beer-munich-is-thataway-modest-beer-ards-brewing-co/",
  "review": "I'm not quite sure what I was expecting from a \"harvest beer\". This was pleasant and drinkable with a taste of biscuits. I wouldn't say no to another but won't be hunting it down."
}
---

