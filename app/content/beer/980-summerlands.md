---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1424540358",
  "title": "Summerlands",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-10-11",
  "style": "IPA - Session",
  "abv": "3.4%",
  "breweries": [
    "brewery/vibrant-forest-brewery/"
  ],
  "number": 980,
  "permalink": "beer/summerlands-vibrant-forest-brewery/",
  "review": "I appreciate this was a session ale which, by nature, are more muted, but this felt like it was lacking something. I don't think I would session this."
}
---
