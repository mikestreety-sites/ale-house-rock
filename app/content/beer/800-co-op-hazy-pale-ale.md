---json
{
    "title": "Co-op Hazy Pale Ale",
    "number": "800",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1273171145",
    "serving": "Can",
    "purchased": "shop/co-op-food/",
    "date": "2023-05-11",
    "permalink": "beer/co-op-hazy-pale-ale-toast-ale/",
    "breweries": [
        "brewery/toast-ale/"
    ],
    "tags": [
        "hazy"
    ],
    "review": "A mediocre &quot;hazy&quot; beer which I&#39;ve bought a few times, due to price and accessibility. It fills a hole, and quashes a desire, but nothing more."
}
---