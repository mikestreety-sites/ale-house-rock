---json
{
    "title": "Sleeping Lemons Export",
    "number": "325",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CAJPOSWJbow/",
    "date": "2020-05-13",
    "permalink": "beer/sleeping-lemons-export-the-wild-beer-company/",
    "breweries": [
        "brewery/the-wild-beer-company/"
    ],
    "tags": [],
    "review": "My Wild Beer delivery turned up yesterday and thought I would give them a crack. Bought this as it was one of @wildbeerco&#39;s most popular beers (well, this is the stronger Export version at 6%), but I struggled to see why. I believe this taste and cider-like balance is bordering on the &quot;sour&quot; trend which is popular at the moment, as it was rather lemony in flavour. Definitely a summer &quot;garden party&quot; beer - maybe one to be enjoyed while wearing a trilby and deck shoes. Not for me I&#39;m afraid"
}
---