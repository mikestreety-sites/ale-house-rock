---json
{
    "title": "Ghost Ship 0.5%",
    "number": "289",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B9bU-dvpFJC/",
    "date": "2020-03-07",
    "permalink": "beer/ghost-ship-05-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "I was designated driver last night, so on the non-alcoholic stuff. This had a very metallic tang (possibly impaired by the Tanglefoot that preceded this). It got better with each sip and accompanied the curry I was eating ok, but I won&#39;t be buying anymore of these. 🏅 5/10"
}
---