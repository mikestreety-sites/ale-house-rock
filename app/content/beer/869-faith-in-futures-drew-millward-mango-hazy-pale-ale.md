---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1329298950",
  "title": "FAITH IN FUTURES // DREW MILLWARD // MANGO HAZY PALE ALE",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/co-op-food/",
  "date": "2023-11-07",
  "style": "Pale Ale - New England / Hazy",
  "abv": "5.4%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 869,
  "permalink": "beer/faith-in-futures-drew-millward-mango-hazy-pale-ale-northern-monk/",
  "review": "Beer on a Tuesday to celebrate the boys birthday. A bit too mango in this one for me to really enjoy it, but it went down well with a curry. It was light, but the fruity flavour was a bit overpowering."
}
---

