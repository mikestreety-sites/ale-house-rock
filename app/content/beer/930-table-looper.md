---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1383239425",
  "title": "Table Looper",
  "serving": "Can",
  "rating": 7.5,
  "date": "2024-05-23",
  "style": "Table Beer",
  "abv": "3.8%",
  "breweries": [
    "brewery/full-circle-brew-co/",
    "brewery/sheep-in-wolfs-clothing/"
  ],
  "number": 930,
  "permalink": "beer/table-looper-full-circle-brew-co-sheep-in-wolfs-clothing/",
  "review": "A nice IPA"
}
---
