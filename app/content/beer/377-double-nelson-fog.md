---json
{
    "title": "Double Nelson Fog",
    "number": "377",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CHApFIfFtwX/",
    "date": "2020-10-31",
    "permalink": "beer/double-nelson-fog-burnt-mill-brewery/",
    "breweries": [
        "brewery/burnt-mill-brewery/"
    ],
    "tags": [],
    "review": "Another Suffolk beer from holiday, this stonking double IPA was spot on for an evening sip. I&#39;ve become a big fan of double IPAs after my tickle with Fourpure and this one from @burntmillbrewery ticks every box."
}
---