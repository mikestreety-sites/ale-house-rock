---json
{
    "title": "Drift Wood",
    "number": "213",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/Bvuay9sF9n4/",
    "date": "2019-04-01",
    "permalink": "beer/drift-wood-red-rock-brewery/",
    "breweries": [
        "brewery/red-rock-brewery/"
    ],
    "tags": [],
    "review": "A completely average beer. Not disappointing, nor exciting. Would not recommend nor dissuade someone from drinking this beer"
}
---