---json
{
    "title": "Running with Sceptres",
    "number": "648",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CdQlImvKD1A/",
    "date": "2022-05-07",
    "permalink": "beer/running-with-sceptres-lost-and-grounded/",
    "breweries": [
        "brewery/lost-and-grounded/"
    ],
    "tags": [],
    "review": "This India Pale Lager was confused as a beer and has the worse bits of both an IPA and lager. It was hoppy and fizzy and just a bit bland with a lager-y aftertaste."
}
---