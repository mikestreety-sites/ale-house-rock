---json
{
    "title": "Butty Bach",
    "number": "59",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BYC-EPLluJc/",
    "serving": "Bottle",
    "date": "2017-08-21",
    "permalink": "beer/butty-bach-wye-valley-brewery/",
    "breweries": [
        "brewery/wye-valley-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Despite a rude sounding name (which is an attribute I often avoid in beers), this pint went down a lot quicker and easier than anticipated. A good pint (P.s. Take note of @laurzstreet in the background drinking wine off the table... #classiclaurz)"
}
---