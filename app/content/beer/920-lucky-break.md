---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1376064118",
  "title": "Lucky Break",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-28",
  "style": "IPA - New England / Hazy",
  "abv": "6.7%",
  "breweries": [
    "brewery/brewdog/"
  ],
  "number": 920,
  "permalink": "beer/lucky-break-brewdog/",
  "review": "After the success of the last Brewdog, I had high hopes for this. Alas, we are back to standard Brewdog beers. It was \"ok\" but I won't be buying one of these again. Bland and uninspiring but not disgusting"
}
---

