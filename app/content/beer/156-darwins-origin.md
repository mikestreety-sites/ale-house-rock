---json
{
    "title": "Darwin’s Origin",
    "number": "156",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/Bnt_7AKFRYD/",
    "date": "2018-09-14",
    "permalink": "beer/darwins-origin-sopian-brewery/",
    "breweries": [
        "brewery/salopian-brewery/"
    ],
    "tags": [],
    "review": "Forgot to take a photo straight away. Another birthday beer, but one I’m not too keen on. Drinkable but wouldn’t have another."
}
---