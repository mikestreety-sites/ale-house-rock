---json
{
    "title": "Wainwright The Golden Ale",
    "number": "132",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bli5WGaFsBJ/",
    "date": "2018-07-22",
    "permalink": "beer/wainwright-the-golden-ale-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [],
    "review": "Clean, crisp, refreshing and a great post-stag beer. Would happily have a few of these"
}
---