---json
{
    "title": "Curtain",
    "number": "752",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1234423368",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-12-27",
    "permalink": "beer/curtain-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "A lovely flavoursome DIPA, which tastes like a lot of beak stuff, but it&#39;s always good."
}
---