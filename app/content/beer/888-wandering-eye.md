---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1320073483",
  "title": "Wandering Eye",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-06",
  "style": "Pale Ale - New Zealand",
  "abv": "5.5%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 888,
  "permalink": "beer/wandering-eye-overtone-brewing-co/",
  "review": "This was lush. It was all kinds of juicy and felt like it packed a punch much stronger than its 5.5% suggests. I'm going to keep an eye out for more NZIPA, because if they are all this good, they will be killer."
}
---

