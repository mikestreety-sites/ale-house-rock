---json
{
    "title": "Magic Lager",
    "number": "694",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/Cg5L1KkKp_E/",
    "date": "2022-08-05",
    "permalink": "beer/magic-lager-magic-rock-brewing/",
    "breweries": [
        "brewery/magic-rock-brewing/"
    ],
    "tags": [],
    "review": "This has the beer type/caption of &quot;No Tricks&quot; and they were right. It&#39;s lager was unexciting, lacking character and very plain. I drank it and felt refreshed like I&#39;d had a glass of water - not really what you drink beer for (but at least I wasn&#39;t disgusted by it)"
}
---