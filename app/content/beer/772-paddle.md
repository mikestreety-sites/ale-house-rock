---json
{
    "title": "Paddle",
    "number": "772",
    "rating": "6.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1251570687",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-02-26",
    "permalink": "beer/paddle-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A fairly run-of-the-mill Pale Ale. Certainly had the light juiciness which was bordering on a NEIPA. I wouldn&#39;t turn this one down, but cant say I would hunt one down again."
}
---