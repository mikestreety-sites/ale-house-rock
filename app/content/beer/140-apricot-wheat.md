---json
{
    "title": "Apricot Wheat",
    "number": "140",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BmHKvBsFSSw/",
    "date": "2018-08-05",
    "permalink": "beer/apricot-wheat-wiens-brewing/",
    "breweries": [
        "brewery/wiens-brewing/"
    ],
    "tags": [],
    "review": "Yet another beer from the same place as the last couple. They seem to be getting better. Can’t taste the apricot, so just a sweet wheat beer."
}
---