---json
{
    "title": "Lancaster Bomber",
    "number": "103",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BhPdwIKlao3/",
    "date": "2018-04-06",
    "permalink": "beer/lancaster-bomber-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Smells like an old man pub and tastes like it belongs there. Not in a bad way! Just an average beer"
}
---