---json
{
    "title": "APA Actually",
    "number": "767",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1247609943",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-02-12",
    "permalink": "beer/apa-actually-lost-pier-brewing/",
    "breweries": [
        "brewery/lost-pier-brewing/"
    ],
    "tags": [
        "apa"
    ],
    "review": "I was expecting a light, dull beer but it was nothing of the sort. Full bodied with a malty flavour, I wouldn&#39;t want too many of these but hit the spot"
}
---