---json
{
    "title": "Jail Ale",
    "number": "581",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CUslwY0KTwI/",
    "date": "2021-10-06",
    "permalink": "beer/jail-ale-dartmoor-brewery/",
    "breweries": [
        "brewery/dartmoor-brewery/"
    ],
    "tags": [],
    "review": "This beer was described on the bottle as having a &quot;rich Moorish aftertaste&quot;. I&#39;m not sure if that was a pun or not, but it was damn tasty and the malts were lovely, leaving me wanting more after each sip. Pleasant holiday after dinner pint."
}
---