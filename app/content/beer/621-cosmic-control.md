---json
{
    "title": "Cosmic Control",
    "number": "621",
    "rating": "10",
    "canonical": "https://www.instagram.com/p/CaLWMJHqOnT/",
    "date": "2022-02-19",
    "permalink": "beer/cosmic-control-deya-brewing-company-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/deya-brewing-company/"
    ],
    "tags": [
        "dipa",
        "ipa"
    ],
    "review": "This DIPA was incredible. When I saw on Instagram that @deyabrewery and @thebeakbrewery were collaborating I was excited. I didn&#39;t even realise it was in shops until I walked into @palate.bottleshop today and saw the last can. It was a perfect DIPA - fruity and smooth but with a hint of bitterness to remind you you&#39;re drinking a beer &amp; not a fruit juice. Despite going for the bigger cans from Deya, I still didn&#39;t want this 8% marvel to end."
}
---