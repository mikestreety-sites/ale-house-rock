---json
{
    "title": "Seagull King",
    "number": "775",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1253494690",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2023-03-05",
    "permalink": "beer/seagull-king-azvex-brewing-company/",
    "breweries": [
        "brewery/azvex-brewing-company/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "This was as thick and juicy as it looks. Definitely a slow sipper, but was full of flavour. Highly recommended as a beer if you ever see one."
}
---