---json
{
    "title": "Tony's Hopolonely",
    "number": "525",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CQ4g8GipmgC/",
    "date": "2021-07-04",
    "permalink": "beer/tonys-hopolonely-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "The last of the Brewdog and Friends beers I had from the free first box. A &quot;raspberry and white chocolate milkshake IPA&quot;. I could taste the raspberries, but the white chocolate gave it a sweet and fake taste. Not one for me"
}
---