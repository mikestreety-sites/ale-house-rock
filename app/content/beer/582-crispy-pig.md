---json
{
    "title": "Crispy Pig",
    "number": "582",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CUvOFc1qw2E/",
    "date": "2021-10-07",
    "permalink": "beer/crispy-pig-hunters-brewery/",
    "breweries": [
        "brewery/hunters-brewery/"
    ],
    "tags": [],
    "review": "On opening the bottle, I could a good whiff of apple, although this smell quickly dissipated. After that novelty, the remaining beer was average."
}
---