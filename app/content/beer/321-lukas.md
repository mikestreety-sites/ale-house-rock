---json
{
    "title": "Lukas",
    "number": "321",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B_8KSEsJXZy/",
    "date": "2020-05-08",
    "permalink": "beer/lukas-thornbridge/",
    "breweries": [
        "brewery/thornbridge/"
    ],
    "tags": [
        "lager"
    ],
    "review": "After my last Thornbridge experience (Tart, 2/10) I was hesitant about this one. After a day in the garden, this helles style lager was perfect. Nice taste for a lager and a lovely looking can. Once the hard work of the day wore off, this turned out to be a better lager than most, but lacking anything significant. Would grab a couple of these for a BBQ but nothing more"
}
---