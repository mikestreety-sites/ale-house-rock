---json
{
    "title": "Keltoi",
    "number": "643",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CcsblLlqRi3/",
    "date": "2022-04-23",
    "permalink": "beer/keltoi-the-white-hag/",
    "breweries": [
        "brewery/the-white-hag/"
    ],
    "tags": [],
    "review": "This was one of the most blad beers I&#39;ve had. So very tasteless, which made it easy to drink I suppose, just disappointing. Serves me right for trying to have a beer on a Wednesday."
}
---