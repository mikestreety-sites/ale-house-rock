---json
{
    "title": "Forty Niner",
    "number": "149",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/Bm4V676F6nr/",
    "date": "2018-08-24",
    "permalink": "beer/forty-niner-ringwood-brewery/",
    "breweries": [
        "brewery/ringwood-brewery/"
    ],
    "tags": [],
    "review": "Not intentional that this @ringwoodbrewery beer ended up as 149! This beer really made me realise how much I love Ringwood beers. Consistently excellent taste, they are fast approaching as one of my favourite breweries. This is a cracking bitter and it is hard to criticise (and yes that is an @ale_house_rock stamp on the side of the bottle)"
}
---