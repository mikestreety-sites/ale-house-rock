---json
{
    "title": "Imperial Bueno Shake",
    "number": "566",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CTsfaC4KPcT/",
    "date": "2021-09-11",
    "permalink": "beer/imperial-bueno-shake-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [],
    "review": "This. Was. Sickly. A super thick, sweet stout which took me a long while to drink. Slow sips over a good hour or more and was a very filling beverage. Definitely a drink to end the evening and you wouldn&#39;t want any food with it. I&#39;m not sure I would have it again. Picked up from the @unbarredbrewery tap room I visited last week.."
}
---