---json
{
    "title": "Tannen Zapfle",
    "number": "683",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CgAbQX8K8Am/",
    "date": "2022-07-14",
    "permalink": "beer/tannen-zapfle-rothaus/",
    "breweries": [
        "brewery/rothaus/"
    ],
    "tags": [],
    "review": "An apparent pilsner-style beer from german. Not as good as the last one, left a funny taste in my mouth, but was pleasant enough to drink cold after a hot summers day."
}
---