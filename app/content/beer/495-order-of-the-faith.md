---json
{
    "title": "Order of the Faith",
    "number": "495",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CPNa1zqFefu/",
    "date": "2021-05-23",
    "permalink": "beer/order-of-the-faith-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "A collaboration between @northernmonk and @alphadeltabeerand part of the &quot;Twist on Faith&quot; series. A classic hazy DIPA which was fruity and smooth, easy drinking and a delight."
}
---