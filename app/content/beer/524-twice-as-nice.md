---json
{
    "title": "Twice as Nice",
    "number": "524",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CQ3IalLlH-N/",
    "date": "2021-07-03",
    "permalink": "beer/twice-as-nice-the-hop-foundry/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "A double IPA from the Aldi brewery? Couldn&#39;t turn that down. A bit tastier than the usual offering from the supermarket, but it felt like it was a DIPA just to make a stronger beer, rather than making a better beer. Beers from Aldi have this special ability to get slightly worse as you drink them. The first sip always tastes better than the last."
}
---