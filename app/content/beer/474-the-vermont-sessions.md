---json
{
    "title": "The Vermont Sessions",
    "number": "474",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CN-0nmSlGT3/",
    "date": "2021-04-22",
    "permalink": "beer/the-vermont-sessions-brewdog-northern-monk/",
    "breweries": [
        "brewery/brewdog/",
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "Brewdog and Northern Monk - can&#39;t go wrong, eh? Opened this with anticipation but when I took the first sip of this, I was convinced it wasn&#39;t that good. However,  as the drink went on, it got better and better to the point where I didn&#39;t want it to end. Smooth drinking and great taste. Remnants of a DIPA without being as strong. Picked this one up in Tesco."
}
---