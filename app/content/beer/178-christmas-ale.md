---json
{
    "title": "Christmas Ale",
    "number": "178",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bq4r334gG4f/",
    "date": "2018-12-02",
    "permalink": "beer/christmas-ale-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "it’s December so I was able to crack open this beer - accompanied by some co-op Brie and cranberry tree crisps. Nice beer, not as Christmassy tasting as the one from Harvey’s but went down pleasantly. Lasted the whole evening."
}
---