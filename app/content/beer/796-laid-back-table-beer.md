---json
{
    "title": "Laid Back - Table Beer",
    "number": "796",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1272959277",
    "serving": "Can",
    "date": "2023-05-10",
    "permalink": "beer/laid-back-table-beer-tenby-brewing-co/",
    "breweries": [
        "brewery/tenby-brewing-co/"
    ],
    "tags": [
        "tablebeer"
    ],
    "review": "Table beers aren&#39;t my thing really, so this landed a bit flat. It&#39;s not very strong, so it was lacking some punch (and flavour, really)"
}
---