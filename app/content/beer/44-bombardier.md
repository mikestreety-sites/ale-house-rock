---json
{
    "title": "Bombardier",
    "number": "44",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BTPFjiDlev3/",
    "serving": "Can",
    "date": "2017-04-23",
    "permalink": "beer/bombardier-eagle-brewery/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "(Brewery previously was Wells) Shamefully out the can for this one and for the I apologise. A dark, tasty malty beer, but average in the beer world. Good for tiding over until payday"
}
---