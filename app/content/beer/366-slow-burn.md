---json
{
    "title": "Slow Burn",
    "number": "366",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CEC6FjmpBeR/",
    "date": "2020-08-18",
    "permalink": "beer/slow-burn-pollys-brew-co/",
    "breweries": [
        "brewery/pollys-brew-co/"
    ],
    "tags": [],
    "review": "I feel like I was deceived with this beer as nowhere on the can does it say it was a sour. I only found out when I googled and discovered it was a tweak from a previous sour brew. It tasted like fizzy orange and lemon squash. Managed to finish the whole drink (maybe egged on by the novelty of drinking out a new glass) but I definitely won&#39;t be buying this again."
}
---
