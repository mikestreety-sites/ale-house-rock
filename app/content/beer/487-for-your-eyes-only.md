---json
{
    "title": "For Your Eyes Only",
    "number": "487",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/COsvxmzlwnL/",
    "date": "2021-05-10",
    "permalink": "beer/for-your-eyes-only-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "This was a tasty DDH IPA which went down without incident. Nothing groundbreaking but a pleasurable drink."
}
---