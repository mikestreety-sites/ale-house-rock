---json
{
    "title": "Pale Ale",
    "number": "191",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Br-6QKwAdws/",
    "date": "2018-12-29",
    "permalink": "beer/pale-ale-my-generation/",
    "breweries": [
        "brewery/my-generation/"
    ],
    "tags": [
        "ipa",
        "paleale",
        "goldenale"
    ],
    "review": "Not heard of “My Generation” brewery before. Although a pale ale, the flavour of the hops were strong. I’m fond of this beer, and would happily drink this all evening"
}
---