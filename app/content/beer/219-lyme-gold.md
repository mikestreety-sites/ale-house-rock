---json
{
    "title": "Lyme Gold",
    "number": "219",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BwaDRcWl_34/",
    "date": "2019-04-18",
    "permalink": "beer/lyme-gold-lyme-regis-brewery/",
    "breweries": [
        "brewery/lyme-regis-brewery/"
    ],
    "tags": [],
    "review": "This beer had a crisp tang on first sip which immidiately put me off but the proceeding sips managed to clamber back some sort of respect. Still not enough to have me bowled over but better than some beers I&#39;ve had"
}
---