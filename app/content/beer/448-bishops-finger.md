---json
{
    "title": "Bishops Finger",
    "number": "448",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CMS4Ie9Fh1c/",
    "date": "2021-03-11",
    "permalink": "beer/bishops-finger-shepherd-neame/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [],
    "review": "I can&#39;t believe I&#39;m 448 beers and over 4 years into this review malarkey and I&#39;ve only just got round to posting a Bishops Finger. No, it&#39;s not from some 2 person hipster garage and yes, it is available in every kind of shop with an alcohol license. But, as I was drinking this this evening, I was reminded about how good a beer it is. Full bodied, morish and a good old classic ale. I&#39;d not bought one in a while as, for some reason, my memory of it was not favourable, but it&#39;s a very tasty solid pint."
}
---