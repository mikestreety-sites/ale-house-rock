---json
{
    "title": "Curiosity",
    "number": "798",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1273169407",
    "serving": "Can",
    "date": "2023-05-11",
    "permalink": "beer/curiosity-wookey-brewing-co/",
    "breweries": [
        "brewery/wookey-brewing-co/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "An unexpectedly tasty DIPA. I didn&#39;t have the highest of hopes for this (I can&#39;t tell you why), but it went down rather well"
}
---