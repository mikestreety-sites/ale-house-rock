---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1386651429",
  "title": "Tones",
  "serving": "Can",
  "rating": 9.5,
  "purchased": "shop/beak-brewery-taproom/",
  "date": "2024-06-02",
  "style": "IPA - Imperial / Double",
  "abv": "8%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 936,
  "permalink": "beer/tones-beak/",
  "review": "Hot diggidy damn. This DIPA was incredible and I could have drunk many more of these. There was a hint of bitterness to take away the feeling of drinking fruit juice. The 8% is barely noticeable (dangerously so). Knocking a small score off due to cosT"
}
---
