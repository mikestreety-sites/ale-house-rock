---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1345842878",
  "title": "AUNT BESSIE'S // STICKY TOFFEE PUDDING & CUSTARD PORTER (2023)",
  "serving": "Can",
  "rating": 2,
  "purchased": "shop/asda/",
  "date": "2024-01-01",
  "style": "Porter - Other",
  "abv": "4.5%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 885,
  "permalink": "beer/aunt-bessies-sticky-toffee-pudding-custard-porter-2023-northern-monk/",
  "review": "This really wasn't enjoyable. It tasted very watery and the flavour wasn't enjoyable. I managed about half the glass before I realised I was just drinking it for the sake of it."
}
---

