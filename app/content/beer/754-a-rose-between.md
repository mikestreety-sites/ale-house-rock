---json
{
    "title": "A Rose Between",
    "number": "754",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1235546482",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-12-30",
    "permalink": "beer/a-rose-between-farm-yard-brew-co-thorn-brewing-company/",
    "breweries": [
        "brewery/farm-yard-brew-co/",
        "brewery/thorn-brewing-company/"
    ],
    "tags": [
        "westcoastipa"
    ],
    "review": "I could appreciate the quality of this, but the flavours weren&#39;t for me really. This hasn&#39;t put me off the brewery, but has made me question the style."
}
---