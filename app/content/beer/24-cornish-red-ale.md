---json
{
    "title": "Cornish Red Ale",
    "number": "24",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BOVDv_JgS6Y/",
    "serving": "Bottle",
    "purchased": "shop/marks-and-spencer/",
    "date": "2016-12-22",
    "permalink": "beer/cornish-red-ale-st-austell-brewery-marks-and-spencer/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [
        "redale"
    ],
    "review": "Bottle conditioned red beer. Tasty, bit watery but good flavours"
}
---