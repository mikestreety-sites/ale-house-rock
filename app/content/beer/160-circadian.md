---json
{
    "title": "Circadian",
    "number": "160",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BoFPlIjFHM9/",
    "date": "2018-09-23",
    "permalink": "beer/circadian-ringwood-brewery/",
    "breweries": [
        "brewery/ringwood-brewery/"
    ],
    "tags": [],
    "review": "Another smashing beer from @ringwoodbrewery. As far as I am concerned they can do no wrong"
}
---