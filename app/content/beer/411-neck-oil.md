---json
{
    "title": "Neck Oil",
    "number": "411",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CJ56ztFlIUC/",
    "date": "2021-01-11",
    "permalink": "beer/neck-oil-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [],
    "review": "If someone was &quot;getting into beers&quot; and asked for a gateway hipster craft beer, I would highly recommend this. Very easy drinking, mellow flavours and doesn&#39;t really taste like &quot;beer&quot; per se. Nice enough, not as good as Gamma Ray though"
}
---