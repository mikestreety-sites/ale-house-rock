---json
{
    "title": "Wolf Rock",
    "number": "177",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bq0bYGQgHW1/",
    "date": "2018-11-30",
    "permalink": "beer/wolf-rock-sharps/",
    "breweries": [
        "brewery/sharps/"
    ],
    "tags": [],
    "review": "@sharpsbrewery is another brewery that struggles to make a bad beer. Wonderful red IPA. Went down very easily."
}
---