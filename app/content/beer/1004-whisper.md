---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1449071608",
  "title": "Whisper",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/beer-no-evil/",
  "date": "2025-01-10",
  "style": "Pale Ale - New England / Hazy",
  "abv": "5%",
  "breweries": [
    "brewery/floc/"
  ],
  "number": 1004,
  "permalink": "beer/whisper-floc/",
  "review": "A nice, light, easy drinking pale ale. Enough flavour to keep you interested but nothing overwhelming."
}
---

