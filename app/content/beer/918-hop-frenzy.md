---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1373153600",
  "title": "Hop Frenzy",
  "serving": "Can",
  "rating": 10,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-18",
  "style": "IPA - New England / Hazy",
  "abv": "5.8%",
  "breweries": [
    "brewery/brewdog/"
  ],
  "number": 918,
  "permalink": "beer/hop-frenzy-brewdog/",
  "review": "Brewdog have an average of 5.6/10 on my website, but I can't help but pick up new beers when I see them and I was so glad I did with this. It's light, it's juicy and it's hazy but with a bitter tang to remind you you're drinking beer. My new favourite"
}
---

