---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1439555421",
  "title": "Screened Out of Heaven",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-12-07",
  "style": "IPA - New England / Hazy",
  "abv": "6%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 996,
  "permalink": "beer/screened-out-of-heaven-sureshot-brewing/",
  "review": "An underwhelming NEIPA, really. It didn't bowl me over and, although it was reminiscent of a lovely, fluffy, Sureshot beer this slightly missed the mark. It's hard to say exactly what was missing, but something was."
}
---

