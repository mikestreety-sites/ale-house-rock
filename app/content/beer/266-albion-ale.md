---json
{
    "title": "Albion Ale",
    "number": "266",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B6rHTn5pwlC/",
    "date": "2019-12-29",
    "permalink": "beer/albion-ale-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Can you tell I got a @harveysbrewery selection of beers for Christmas? A big standard bitter,  reminiscent of those odd breweries you get in Aldi. Not very remarkable"
}
---