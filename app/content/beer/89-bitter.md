---json
{
    "title": "Bitter",
    "number": "89",
    "rating": "1",
    "canonical": "https://www.instagram.com/p/BdYGd0fF06b/",
    "date": "2017-12-31",
    "permalink": "beer/bitter-asda-smart-price/",
    "breweries": [
        "brewery/asda-smart-price/"
    ],
    "tags": [],
    "review": "Always wanted to try one of these @asda beers. 4 for 90p. Didn’t have high hopes and it was exactly as I expected. Soapy, watery nothingness. Had to pour it away."
}
---