---json
{
    "title": "Kaleidoscope",
    "number": "597",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CYFUCL5q0O7/",
    "date": "2021-12-29",
    "permalink": "beer/kaleidoscope-wiper-and-true/",
    "breweries": [
        "brewery/wiper-and-true/"
    ],
    "tags": [],
    "review": "A plain pale ale which was lacking any kind of flavour. It wasn&#39;t &quot;bad&quot;, just didn&#39;t really taste of much. It&#39;s a shame, as I had high hopes for this one"
}
---