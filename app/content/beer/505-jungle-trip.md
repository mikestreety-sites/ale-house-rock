---json
{
    "title": "Jungle Trip",
    "number": "505",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CPy7bq5FPcU/",
    "date": "2021-06-06",
    "permalink": "beer/jungle-trip-london-beer-factory/",
    "breweries": [
        "brewery/london-beer-factory/"
    ],
    "tags": [
        "neipa",
        "ipa"
    ],
    "review": "I didn&#39;t realise I was such a fan of New England IPAs until @marclittlemore asked for some recommendations and all the NEIPAs I had recently drunk scored highly. I grabbed this one when I saw it on the Tesco shelves and was not disappointed. It was smooth easy drinking with plenty of flavour and definitely one I will buy again. @londonbeerfactory"
}
---