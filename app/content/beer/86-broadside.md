---json
{
    "title": "Broadside",
    "number": "86",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BdDapegFs0d/",
    "date": "2017-12-23",
    "permalink": "beer/broadside-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "This 6.3% beer commemorates the battle of Solebay in 1672 apparently. If the battle was anything like the beer, it was a dark, strong, chocolatey fight which was slow and heavy. Certainly not one to be repeated! I feel full up after a few sips already!"
}
---