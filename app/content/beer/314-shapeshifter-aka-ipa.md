---json
{
    "title": "Shapeshifter (A.K.A. IPA)",
    "number": "314",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B_f9xYupyuM/",
    "date": "2020-04-27",
    "permalink": "beer/shapeshifter-aka-ipa-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "I seem to be on a string of IPAs at the moment, but when they are this good I am absolutely fine with that.  This one was an amazing way to end the weekend and was perfect served fridge cold. The taste was sensational and was well worth the wait. @fourpure have smashed it out the park with this one and I will be ordering more of these come payday 😚👌"
}
---