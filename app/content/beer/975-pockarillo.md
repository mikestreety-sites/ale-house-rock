---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1422864082",
  "title": "Pockarillo",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/padstow-tasting-rooms/",
  "date": "2024-10-05",
  "style": "IPA - American",
  "abv": "5.6%",
  "breweries": [
    "brewery/padstow-brewing-company/"
  ],
  "number": 975,
  "permalink": "beer/pockarillo-padstow-brewing-company/",
  "review": "This was a quintessential West Coast IPA. Clean and crisp.wotb plenty of hops."
}
---
