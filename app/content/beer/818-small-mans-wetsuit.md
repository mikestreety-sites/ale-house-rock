---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1288934326",
  "title": "Small Man's Wetsuit",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/sureshot-brewing/",
  "date": "2023-06-30",
  "style": "Pale Ale - New England / Hazy",
  "abv": "3.9%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 818,
  "permalink": "beer/small-mans-wetsuit-sureshot-brewing/",
  "review": "Sureshot need to stop making absolute bangers, otherwise I'll run out of points to give. This was another absolutely superb Pale Ale and went very well with a curry (and was perfectly good on its own, too)."
}
---
