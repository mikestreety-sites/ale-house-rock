---json
{
    "title": "Silk Spring Cowboy",
    "number": "811",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1282886340",
    "serving": "Can",
    "purchased": "shop/sureshot-brewing/",
    "date": "2023-06-10",
    "permalink": "beer/silk-spring-cowboy-left-handed-giant-sureshot-brewing/",
    "breweries": [
        "brewery/left-handed-giant/",
        "brewery/sureshot-brewing/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A no-frills, straightforward Pale Ale. Not astounding, but was nice after a warm commute on the train."
}
---