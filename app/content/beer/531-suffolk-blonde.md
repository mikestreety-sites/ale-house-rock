---json
{
    "title": "Suffolk Blonde",
    "number": "531",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CRMfKyNpqp9/",
    "date": "2021-07-11",
    "permalink": "beer/suffolk-blonde-st-peters/",
    "breweries": [
        "brewery/st-peters/"
    ],
    "tags": [],
    "review": "A wheat beer brewed especially for Sainsbury&#39;s. Could take or leave this one and always struggle to write reviews for &quot;middle of the road&quot; beers."
}
---