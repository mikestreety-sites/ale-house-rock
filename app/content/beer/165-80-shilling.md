---json
{
    "title": "80 Shilling",
    "number": "165",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bo2Z6JJlrEQ/",
    "date": "2018-10-12",
    "permalink": "beer/80-shilling-belhaven-brewery/",
    "breweries": [
        "brewery/belhaven-brewery/"
    ],
    "tags": [],
    "review": "Smooth &amp; tasty. Everything you want in a beer."
}
---