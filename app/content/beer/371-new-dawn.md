---json
{
    "title": "New Dawn",
    "number": "371",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CF2PfLQlLIU/",
    "date": "2020-10-02",
    "permalink": "beer/new-dawn-shepherd-neame/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [],
    "review": "Good to be back on the bottled beers again after a stint of cans. Drank last week but I can still remember the fruity lightness this beer carried. Plenty of carbonation, so more of a summer tipple, but pleasingly tasty"
}
---