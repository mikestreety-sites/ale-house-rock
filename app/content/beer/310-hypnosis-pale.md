---json
{
    "title": "Hypnosis Pale",
    "number": "310",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B_LXG-mJR0I/",
    "date": "2020-04-19",
    "permalink": "beer/hypnosis-pale-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [
        "bitter",
        "paleale"
    ],
    "review": "Sublime beer from @loudshirtbeer - their best yet! I seem to be having a string of surprisingly tasty beer. With Pale in the name, I wasn&#39;t holding out much hope but this pint is top drawer. 🏅9/10"
}
---