---json
{
    "title": "Öl Mörk Lager",
    "number": "236",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B2pT7g-l3dZ/",
    "date": "2019-09-20",
    "permalink": "beer/ol-mork-lager-kronleins-bryggeri/",
    "breweries": [
        "brewery/kronleins-bryggeri/"
    ],
    "tags": [],
    "review": "Dark lager beer from IKEA. This is incredible, and saying that about a lager and something from IKEA is rare. Great taste and very easy to drink (and only £1.79!). Shame I only picked up 2, I could drink a whole case"
}
---