---json
{
    "title": "Anytime IPA",
    "number": "466",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CNbE2qWlolJ/",
    "date": "2021-04-08",
    "permalink": "beer/anytime-ipa-meantime-brewing-company/",
    "breweries": [
        "brewery/meantime-brewing-company/"
    ],
    "tags": [],
    "review": "Instead of chocolate this year, Ma and Pa bought me beer for Easter. I always feel extra disappointment when I&#39;m not a fan of beer someone has bought me, but this one was a bit flat with its flavour. It was a light IPA that ticked all the boxes except the taste. Went down easy enough while chatting with friends on zoom but highly forgettable."
}
---