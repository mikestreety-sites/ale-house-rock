---json
{
    "title": "Devonshire Pale Ale",
    "number": "579",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CUow_TMK-_d/",
    "date": "2021-10-05",
    "permalink": "beer/devonshire-pale-ale-black-tor/",
    "breweries": [
        "brewery/black-tor/"
    ],
    "tags": [],
    "review": "Been enjoying getting back into the bottled bitters recently, and this is no exception. Enjoyable golden pale ale."
}
---