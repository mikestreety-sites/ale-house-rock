---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1383059028",
  "title": "Passing Thoughts",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-05-22",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/verdant-brewing-company/",
    "brewery/beak-brewery/"
  ],
  "number": 929,
  "permalink": "beer/passing-thoughts-verdant-brewing-company-beak/",
  "review": "It seems when Beak collaborates with anyone, it makes an outstanding beer. Verdant are powerhouses and, with beak, made this incredible IPA. Packed full of flavour and spot on at 6.5%. I was definitely disappointed when I finished this one"
}
---
