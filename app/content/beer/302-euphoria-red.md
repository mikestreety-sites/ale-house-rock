---json
{
    "title": "Euphoria Red",
    "number": "302",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B-0OPu8pPud/",
    "date": "2020-04-10",
    "permalink": "beer/euphoria-red-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [],
    "review": "Next up from @loudshirtbeer is this tasty, well balanced red ale. A lovely beer that got better with every sip. Not too heavy either (which was perfect after a good Friday pottering!)"
}
---