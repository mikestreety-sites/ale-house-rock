---json
{
    "title": "New England IPA",
    "number": "381",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CHTGMOxlZtr/",
    "date": "2020-11-07",
    "permalink": "beer/new-england-ipa-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "I was starting to give up on Adams canned beer, but then this beauty came along. Crisp, full of flavour and lovely a pleasure to drink. Was disappointed when this one ended! Last of the Adams small cans and a cracker to finish on."
}
---