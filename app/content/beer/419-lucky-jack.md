---json
{
    "title": "Lucky Jack",
    "number": "419",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CKH0tpKFJRI/",
    "date": "2021-01-16",
    "permalink": "beer/lucky-jack-lervig-aktiebryggeri/",
    "breweries": [
        "brewery/lervig-aktiebryggeri/"
    ],
    "tags": [],
    "review": "A simple IPA which neither offends nor compliments the palate. Easy drinking, without a desire to drink more. Could a beer be any more mediocre?"
}
---