---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1407868186",
  "title": "SELFSAME",
  "serving": "Can",
  "rating": 8.5,
  "purchased": "shop/beak-brewery-taproom/",
  "date": "2024-08-11",
  "style": "IPA - New Zealand",
  "abv": "6.5%",
  "breweries": [
    "brewery/finback-brewery/",
    "brewery/beak-brewery/"
  ],
  "number": 957,
  "permalink": "beer/selfsame-finback-brewery-beak/",
  "review": "One of the most expensive beers I've ever bought (when considering the ml-to-pound ratio). It was a classic Beak IPA, fruity and full but not sure it was worth the £10 price tag."
}
---
