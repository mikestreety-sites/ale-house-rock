---json
{
    "title": "Soft Focus",
    "number": "742",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1224970791",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-11-27",
    "permalink": "beer/soft-focus-baron-brewing/",
    "breweries": [
        "brewery/baron-brewing/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A light, hazy IPA that was a good evening drink, quite expensive, but another quality beer from this emerging (certainly for me) brewery. Will be keeping an eye out for more of their beers - especially as they are 500ml!"
}
---