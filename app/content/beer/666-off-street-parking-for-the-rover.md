---json
{
    "title": "Off Street Parking For the Rover",
    "number": "666",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CeTPloJqWMZ/",
    "date": "2022-06-02",
    "permalink": "beer/off-street-parking-for-the-rover-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "Not quite the big punchy beer I&#39;m used to from Deya, but then again this is a pale ale. Tasty enough but wouldn&#39;t buy it again."
}
---