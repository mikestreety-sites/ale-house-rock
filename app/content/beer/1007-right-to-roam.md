---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1450223270",
  "title": "Right To Roam",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/brewser-online-store/",
  "date": "2025-01-16",
  "style": "Bitter - Best",
  "abv": "4.2%",
  "breweries": [
    "brewery/buxton-brewery/"
  ],
  "number": 1007,
  "permalink": "beer/right-to-roam-buxton-brewery/",
  "review": "This was a very underwhelming bitter and was definitely better once it had got to room temperature."
}
---

