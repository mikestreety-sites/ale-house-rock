---json
{
    "title": "Debauched Orgy",
    "number": "771",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1250626739",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-02-24",
    "permalink": "beer/debauched-orgy-cloak-and-dagger/",
    "breweries": [
        "brewery/cloak-and-dagger/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "I was a little disappointed by this NEIPA. It was lacking in taste and had a bit of an odd tang for an aftertaste."
}
---