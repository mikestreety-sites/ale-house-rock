---json
{
    "title": "Lava",
    "number": "399",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CJO1CqMFTzv/",
    "date": "2020-12-25",
    "permalink": "beer/lava-exale-brewing-drop-project/",
    "breweries": [
        "brewery/drop-project/",
        "brewery/exale-brewing/"
    ],
    "tags": [],
    "review": "A coffee bitter, was a lot lighter when pouring than expected. I&#39;m more used to a stout when it comes to coffee beer. Not an overly strong coffee flavour, which is nice, a pleasant Christmas evening sipper. Not sure I would buy it again, though.  Shout out again to Sister Ale House for the donation."
}
---