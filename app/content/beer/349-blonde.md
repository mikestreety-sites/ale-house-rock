---json
{
    "title": "Blonde",
    "number": "349",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CCG1yy5JeP4/",
    "date": "2020-07-01",
    "permalink": "beer/blonde-saltaire/",
    "breweries": [
        "brewery/saltaire/"
    ],
    "tags": [],
    "review": "This is very much a #latergram. This light, crisp beer was perfect on one of those hot summer days we had last week. Would very much have one of these again"
}
---