---json
{
    "title": "Gatto",
    "number": "668",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CeWbZtNqjbl/",
    "date": "2022-06-03",
    "permalink": "beer/gatto-beak-brewery-north-brewing-co/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/north-brewing-co/"
    ],
    "tags": [],
    "review": "This beer was a &quot;Imperial Triple Fruited Sour Cherry Stout&quot;. It was... Interesting. I wouldn&#39;t buy it again and I wouldn&#39;t say I &quot;enjoyed&quot; it as such. But, I did drink it all and I wasn&#39;t horrified - nor did I feel the need to pull a face each sip. The 10% did hit me when I got up from the sofa."
}
---