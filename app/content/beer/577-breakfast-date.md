---json
{
    "title": "Breakfast Date",
    "number": "577",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CUS_N4MqxfN/",
    "date": "2021-09-26",
    "permalink": "beer/breakfast-date-buxton-brewery/",
    "breweries": [
        "brewery/buxton-brewery/"
    ],
    "tags": [],
    "review": "A sickly sweet, thick stout which, except the first sip, I can&#39;t say I enjoyed that much. The first taste was a novelty and gave me false hope for things to come. I finished it, but mainly out of respect for my wife who bought it for me. Talking of which - she was so repulsed by the smell on my breath, she left the room."
}
---