---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1456572612",
  "title": "Cavern",
  "serving": "Can",
  "rating": 3,
  "purchased": "shop/brewser-online-store/",
  "date": "2025-02-14",
  "style": "Gluten-Free",
  "abv": "4.2%",
  "breweries": [
    "brewery/buxton-brewery/"
  ],
  "number": 1016,
  "permalink": "beer/cavern-buxton-brewery/",
  "review": "I feel like lagers are getting better, as are gluten free beers, but this one missed the mark. Struggled to find any flavour with this at all."
}
---

