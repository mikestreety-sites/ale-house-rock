---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1303365423",
  "title": "Another Day Done",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/beer-no-evil/",
  "date": "2023-08-11",
  "style": "Pale Ale - American",
  "abv": "4.4%",
  "breweries": [
    "brewery/duration-brewing/"
  ],
  "number": 831,
  "permalink": "beer/another-day-done-duration-brewing/",
  "review": "A good enough pale ale, but a little bit flat. I would have another, if offered, but there was something missing from this one."
}
---

