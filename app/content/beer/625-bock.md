---json
{
    "title": "Bock",
    "number": "625",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Capxa34q6Kw/",
    "date": "2022-03-03",
    "permalink": "beer/bock-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [],
    "review": "A dark coffee lager. I had this past weekend and have been struggling to work out what I thought about it. I don&#39;t think I was that impressed - it was all the worst bits of a stout and lager but still drinkable. It confuses me."
}
---