---json
{
    "title": "Blonde",
    "number": "98",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BfMIoJCl69G/",
    "date": "2018-02-14",
    "permalink": "beer/blonde-leffe/",
    "breweries": [
        "brewery/leffe/"
    ],
    "tags": [],
    "review": "I’m back! After moving house and not having internet, I took a break from posting. But I’m back. Blonde beer that is easy to drink. Wouldn’t buy it but it got left at my house. Quaffable"
}
---