---json
{
    "title": "Alnwick IPA",
    "number": "511",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CQJk6dMJKHf/",
    "date": "2021-06-15",
    "permalink": "beer/alnwick-ipa-the-alnwick-brewery-co-ltd/",
    "breweries": [
        "brewery/the-alnwick-brewery-co-ltd/"
    ],
    "tags": [],
    "review": "Sorry for the lack off-brand photo, I&#39;m visiting Mum and Dad Ale House after their holiday (and drinking their beer). Not very impressed with this one, had a wholesome smell but an odd aftertaste (and taste in general). Not to keen on this one"
}
---