---json
{
    "title": "Basecamp (A.K.A. Lager)",
    "number": "312",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B_Z7niMpbFa/",
    "date": "2020-04-25",
    "permalink": "beer/basecamp-aka-lager-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [],
    "review": "For a lager, this had excellent taste. Refreshing, light and full of flavour. I feel like I&#39;m going to have to order some more @fourpure beers after this."
}
---