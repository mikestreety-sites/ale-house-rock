---json
{
    "title": "More Oats",
    "number": "535",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CRexoYuJlgm/",
    "date": "2021-07-18",
    "permalink": "beer/more-oats-verdant-brewing-company-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/verdant-brewing-company/"
    ],
    "tags": [],
    "review": "@thebeakbrewery teamed up with @verdantbrew to make two IPAs using the same hops and technique, but one with oats and another with wheat. This was the first one. Clean and smooth but something not quite hitting the spot for me."
}
---