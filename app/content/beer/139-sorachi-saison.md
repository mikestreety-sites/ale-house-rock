---json
{
    "title": "Sorachi Saison",
    "number": "139",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BmEjn_uFZGR/",
    "date": "2018-08-04",
    "permalink": "beer/sorachi-saison-gir-bryggeri/",
    "breweries": [
        "brewery/gir-bryggeri/"
    ],
    "tags": [],
    "review": "Finally, a craft beer with a bit of taste. Drunk after bottling 40 bottles of home brew, this was refreshing. When I said a bit of taste, I meant a bit. Might actually consider having this again if there was nothing better on offer"
}
---