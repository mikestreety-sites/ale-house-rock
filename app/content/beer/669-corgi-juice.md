---json
{
    "title": "Corgi Juice",
    "number": "669",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CeYspN1KrMn/",
    "date": "2022-06-04",
    "permalink": "beer/corgi-juice-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "If there is one thing Brewdog do well, it&#39;s capitalising on events and you can&#39;t have a &quot;platty jubes&quot; without them making some sort of beer. Unimpressive, as a beer, and reminded me of the pale ales and the like you get from Aldi. Nothing to dislike about it, just not the most flavoursome. I can imagine it would have gone well with a BBQ"
}
---