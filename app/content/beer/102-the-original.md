---json
{
    "title": "The Original",
    "number": "102",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BguAeVwlHt8/",
    "date": "2018-03-24",
    "permalink": "beer/the-original-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [],
    "review": "Picked this up in Lidl, haven’t had one for years. These are beautifully, oaky, scotchy and easily drinkable. Brewed in barrels of spirits and dangerously high in alcohol. Went down far too quickly"
}
---