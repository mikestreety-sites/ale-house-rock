---json
{
    "title": "Pilsner",
    "number": "354",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CCPLUMmpY7W/",
    "serving": "Can",
    "date": "2020-07-04",
    "permalink": "beer/pilsner-seven-bro7hers-brewery/",
    "breweries": [
        "brewery/seven-bro7hers-brewery/"
    ],
    "tags": [
        "pilsner"
    ],
    "review": "Sorry for all the new beer spam, @beer52hq delivered this week. This, hands down, is one of the best lagers (pilsners) I&#39;ve ever had and would easily pick it over many of the ales I have previously drunk. Crisp, flavourful and absolutely smashed the role of being an accompaniment to a dinner of homemade chilli cheese and chips. Need to find where I can buy some more @sevenbro7hers beer"
}
---