---json
{
    "title": "Budding Pale Ale",
    "number": "189",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Br0NQARgsBH/",
    "date": "2018-12-25",
    "permalink": "beer/budding-pale-ale-stroud-brewery/",
    "breweries": [
        "brewery/stroud-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Not as nice as the first one but still pleasant"
}
---