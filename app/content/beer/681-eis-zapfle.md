---json
{
    "title": "Eis Zapfle",
    "number": "681",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CfyWux8qRE9/",
    "date": "2022-07-09",
    "permalink": "beer/eis-zapfle-rothaus/",
    "breweries": [
        "brewery/rothaus/"
    ],
    "tags": [],
    "review": "After the first sip of this, I exclaimed about it being very &quot;European&quot;. If you&#39;ve had one of those big tankards of beer on the continent, you&#39;d know what I mean. I would not be disappointed if I was served this in the sun, in a different country. It&#39;s not really designed for being drunk in the evening on the sofa in England though..."
}
---