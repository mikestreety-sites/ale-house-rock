---json
{
    "title": "Crazy Horse",
    "number": "570",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CT-LMLvKWQh/",
    "date": "2021-09-18",
    "permalink": "beer/crazy-horse-hepworth-co-brewers/",
    "breweries": [
        "brewery/hepworth-co-brewers/"
    ],
    "tags": [],
    "review": "A light, simple IPA. Perfect as a desk beer (a colleague picked up a case of these) and one I could drink of an evening, but not the most complex in flavours."
}
---