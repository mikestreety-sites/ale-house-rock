---json
{
    "title": "Früh Kölsch",
    "number": "417",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CKFVwxQFtqM/",
    "date": "2021-01-15",
    "permalink": "beer/kolsch-fruh/",
    "breweries": [
        "brewery/fruh/"
    ],
    "tags": [],
    "review": "Very lager-like. Light and refreshing after the slightly heavier IIPA before. Managed to drink this one pretty quickly, so can&#39;t have been too offensive. Apparently true Kölsch beers are like champagne in that it can only get the name if it is brewed in the region of Cologne"
}
---