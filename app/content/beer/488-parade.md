---json
{
    "title": "Parade",
    "number": "488",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CO3qB9UF5Gs/",
    "date": "2021-05-14",
    "permalink": "beer/parade-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "I&#39;ve swiftly become a big fan of @thebeakbrewery beers (need to head over there for a pint soon) and this one was no exception. Picked up from @palate.bottleshop a couple of weeks back, this IPA was smooth and fruity and very easy drinking. Had to pace myself as I found I was drinking it extremely quickly..."
}
---