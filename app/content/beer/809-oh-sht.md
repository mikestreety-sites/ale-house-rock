---json
{
    "title": "Oh Sh*t",
    "number": "809",
    "rating": "2.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1281221461",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-06-04",
    "permalink": "beer/oh-sht-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [
        "sour"
    ],
    "review": "I grabbed this without looking as it had similar artwork to the NEIPA I had the other day. I did not realise it was a sour and I don&#39;t really like sours. It tasted like an alcoholic squash - half of it went to the wife who doesn&#39;t even drink beer!"
}
---