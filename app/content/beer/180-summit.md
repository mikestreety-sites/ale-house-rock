---json
{
    "title": "Summit",
    "number": "180",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BrFIT3wA1QW/",
    "date": "2018-12-07",
    "permalink": "beer/summit-allendale-brewery/",
    "breweries": [
        "brewery/allendale-brewery/"
    ],
    "tags": [],
    "review": "Light, crisp, tasty. Drank this without questioning its flavour."
}
---