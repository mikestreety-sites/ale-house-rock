---json
{
    "title": "42|DDH Pale Ale",
    "number": "499",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CPgasomlc7Q/",
    "date": "2021-05-30",
    "permalink": "beer/42ddh-pale-ale-brew-by-numbers/",
    "breweries": [
        "brewery/brew-by-numbers/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Crisp, light and fresh Pale Ale with the added pizzazz of being double dry hopped. This might be the taste of cold beer after a long hot day but this was damn refreshing. This little number was part of the @brewdogofficial and friends box I got for free as a trial. Smashing work @brewbynumbers"
}
---