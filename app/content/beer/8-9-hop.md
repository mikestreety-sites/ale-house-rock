---json
{
    "title": "9 Hop",
    "number": "8",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BNuik5XgaTr/",
    "serving": "Bottle",
    "purchased": "shop/marks-and-spencer/",
    "date": "2016-12-07",
    "permalink": "beer/9-hop-westerham-marks-and-spencer/",
    "breweries": [
        "brewery/marks-and-spencer/",
        "brewery/westerham-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Hmm, 9 Hop is not as good as the 31 Hop from the other day. Although more hoppy (so not as tasty). I can manage it. Wouldn&#39;t have I again."
}
---