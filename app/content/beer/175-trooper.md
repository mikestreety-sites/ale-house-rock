---json
{
    "title": "Trooper",
    "number": "175",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BqNV2C7AgEK/",
    "date": "2018-11-15",
    "permalink": "beer/trooper-robinsons-brewery/",
    "breweries": [
        "brewery/robinsons-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Drank this with not much thought. Just tasted like a bog standard bitter. Would drink again but wouldn’t hunt it down"
}
---