---json
{
    "title": "New World",
    "number": "346",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CBmJlFcJ7Qk/",
    "date": "2020-06-19",
    "permalink": "beer/new-world-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "Really loving @northernmonk at the moment. Absolute sterling lineup of beers. This one was classic craft IPA - hoppy and dry."
}
---