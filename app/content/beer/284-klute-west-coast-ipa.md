---json
{
    "title": "Klute West Coast IPA",
    "number": "284",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B84qXa_pyc1/",
    "date": "2020-02-22",
    "permalink": "beer/klute-west-coast-ipa-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "The first half of this beer tasted good, married with a Thai curry they went together very well. However, in the cold hard light of post-dinner drinking, I realised how bland tasting this was. What a rollercoaster of emotions"
}
---