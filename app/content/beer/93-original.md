---json
{
    "title": "The Rev. James Original",
    "number": "93",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Bd0tIi6FYuB/",
    "date": "2018-01-11",
    "permalink": "beer/original-the-rev-james/",
    "breweries": [
        "brewery/brains/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Apparently I would be rewarded with the finish of this beer. I was mildly satisfied. Drinkable but nothing special."
}
---