---json
{
    "title": "Wild Hop",
    "number": "295",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B-RiTTfp5xL/",
    "date": "2020-03-28",
    "permalink": "beer/wild-hop-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Finally got a Harvey&#39;s glass to accompany all those @harveysbrewery beers I&#39;ve been having. Lovely light beer, easy to drink. Would have been better in a beer garden with friends but, alas, current circumstances do not allow for that."
}
---