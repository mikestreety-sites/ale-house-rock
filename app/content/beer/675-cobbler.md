---json
{
    "title": "Cobbler",
    "number": "675",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Ce15oUdKicO/",
    "date": "2022-06-15",
    "permalink": "beer/cobbler-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "A little bit underwhelmed by this one, to be honest. It was lacking the thick fruity flavour I&#39;ve come to love from Beak. It wasn&#39;t bad, just not as outstanding as their DIPAs previously have been"
}
---