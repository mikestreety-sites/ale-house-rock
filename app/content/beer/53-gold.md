---json
{
    "title": "Gold",
    "number": "53",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BWS77N5FUtT/",
    "serving": "Bottle",
    "date": "2017-07-08",
    "permalink": "beer/gold-butcombe-brewing-company/",
    "breweries": [
        "brewery/butcombe-brewing-company/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "This was so tasty I forgot to take a picture! Crisp, light, wonderful"
}
---