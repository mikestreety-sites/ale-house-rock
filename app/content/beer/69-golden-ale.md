---json
{
    "title": "Golden Ale",
    "number": "69",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BZ_6G8FFYAp/",
    "date": "2017-10-08",
    "permalink": "beer/golden-ale-co-op/",
    "breweries": [
        "brewery/co-op/"
    ],
    "tags": [],
    "review": "an easy drinking beer from co-op. Apparently perfect for a summer garden, but an autumnal kitchen is just fine too. A little too fizzy for me"
}
---