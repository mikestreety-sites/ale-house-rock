---json
{
    "title": "EPA",
    "number": "66",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BY1MDuDF6zr/",
    "date": "2017-09-09",
    "permalink": "beer/epa-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Excuse the glass (and the courgette). A light beer, missing a lot of flavour, really. Ok, I suppose"
}
---