---json
{
    "title": "DDH Pale",
    "number": "521",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CQih197pKFS/",
    "date": "2021-06-25",
    "permalink": "beer/ddh-pale-cloudwater/",
    "breweries": [
        "brewery/cloudwater/"
    ],
    "tags": [],
    "review": "Finally got round to picking up some @cloudwaterbrew beer from Tesco. This DDH Pale hit the spot. Smooth and tasty, with the feeling you&#39;re drinking something a lot more than 5%. Has that zing without making you wobbly."
}
---