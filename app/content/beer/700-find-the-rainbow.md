---json
{
    "title": "Find the Rainbow",
    "number": "700",
    "rating": "9.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1192455594",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-08-17",
    "permalink": "beer/find-the-rainbow-alpha-delta-brewing-merakai-brewing-co/",
    "breweries": [
        "brewery/alpha-delta-brewing/",
        "brewery/merakai-brewing-co/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "What an absolute banger to start in Untappd quest. Smooth and juicy and so easy to drink. Spot on at just under 6%, this NEIPA is one of the best I&#39;ve had."
}
---