---json
{
    "title": "Like Clockwork",
    "number": "759",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1239409617",
    "serving": "Can",
    "date": "2023-01-13",
    "permalink": "beer/like-clockwork-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A pale ale which went down very well this evening. Would have a few pints of these in a pub quite happily"
}
---