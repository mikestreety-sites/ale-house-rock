---json
{
    "title": "Willemoes Stout",
    "number": "41",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BS6vWWoF3RZ/",
    "serving": "Bottle",
    "date": "2017-04-15",
    "permalink": "beer/willemoes-brygget-med-stolthed-re/",
    "breweries": [
        "brewery/brygget-med-stolthed-re/"
    ],
    "tags": [
        "stout"
    ],
    "review": "I&#39;ve had a few beers while I&#39;ve been in Denmark 🇩🇰 but this is the only one I&#39;ve bought &quot;home&quot; for an evening in (on that note &quot;Ale 16&quot; is very popular over here and great 👍). Back to this beer, it&#39;s got a bit of a head as I was dancing with it, but it is very tasty and would recommend it if you find yourself in this fine country"
}
---