---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1291619069",
  "title": "Stedsegrøn / Evergreen",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/asda/",
  "date": "2023-07-07",
  "style": "IPA - Session",
  "abv": "3.5%",
  "breweries": [
    "brewery/mikkeller/"
  ],
  "number": 820,
  "permalink": "beer/stedsegrn-evergreen-mikkeller/",
  "review": "A passable, drinkable, run-of-the-mill Hazy from a big supermarket. Has this a couple of times and doesn't disappoint. It fills the criteria of \"give me a cheap half-decent beer\""
}
---
