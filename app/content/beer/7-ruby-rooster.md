---json
{
    "title": "Ruby Rooster",
    "number": "7",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BNtM75sA3kq/",
    "serving": "Bottle",
    "purchased": "shop/lidl/",
    "date": "2016-12-07",
    "permalink": "beer/ruby-rooster-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "rubyale"
    ],
    "review": "More drinkable than the colour implies. A malty tasty beer."
}
---