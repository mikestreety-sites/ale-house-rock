---json
{
    "title": "Black IPA",
    "number": "188",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Br0MU-sAhIf/",
    "date": "2018-12-25",
    "permalink": "beer/black-ipa-greene-king/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [
        "blackipa"
    ],
    "review": "I’m slowly becoming a fan of black IPA beers as they’re becoming more and more popular. Very tasty beers"
}
---