---json
{
    "title": "Steyning Stinker",
    "number": "443",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CL-LZjhlF2T/",
    "date": "2021-03-03",
    "permalink": "beer/steyning-stinker-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "This smoked beer wasn&#39;t for me. It took me two-thirds of a pint to get to a point where I was actually &quot;enjoying&quot;. This is only my second smoked beer, but the thicker, syrupy texture of the past one suited the smoked taste better"
}
---