---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1451709338",
  "title": "Testing One Two",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/beer-no-evil/",
  "date": "2025-01-23",
  "style": "Pale Ale - American",
  "abv": "4.6%",
  "breweries": [
    "brewery/escapist-brew-co/"
  ],
  "number": 1009,
  "permalink": "beer/testing-one-two-escapist-brew-co/",
  "review": "A lovely light, crisp pale ale which I would drink all evening."
}
---

