---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1417058439",
  "title": "The Future Is Now",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-09-13",
  "style": "IPA - New England / Hazy",
  "abv": "6.9%",
  "breweries": [
    "brewery/quantock-brewery/"
  ],
  "number": 968,
  "permalink": "beer/the-future-is-now-quantock-brewery/",
  "review": "A fairly good NEIPA. This one as nice, but I felt it lacked some of the real depth and fruitiness I've come to expect from the style."
}
---

