---json
{
    "title": "London Pale Ale",
    "number": "83",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BbVTtC8Foq5/",
    "date": "2017-11-10",
    "permalink": "beer/london-pale-ale-meantime-brewing-company/",
    "breweries": [
        "brewery/meantime-brewing-company/"
    ],
    "tags": [],
    "review": "a lighter, hoppy beer. Not the nicest but will suffice as a Light post-dinner sipple"
}
---