---json
{
    "title": "Hop & Hooker Amber Ale",
    "number": "781",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1259575591",
    "serving": "Can",
    "date": "2023-03-26",
    "permalink": "beer/hop-hooker-amber-ale-powder-monkey-brewing-co/",
    "breweries": [
        "brewery/powder-monkey-brewing-co/"
    ],
    "tags": [
        "amberale"
    ],
    "review": "Tasted like a classic bitter &amp; took me back to my roots. Earthly and full bodied. Got this in a &quot;beer and book&quot; box - not one I would have chosen to buy of it was on the shelf as a collaboration with a rugby player isn&#39;t what I am for"
}
---