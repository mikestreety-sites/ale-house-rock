---json
{
    "title": "Patronus",
    "number": "110",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bi-IpBPlNxf/",
    "date": "2018-05-19",
    "permalink": "beer/patronus-perlenbacher/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "weissbeer"
    ],
    "review": "This is not a Harry Potter beer but instead a Weissbier. Probably my favourite wheat beer so far - it’s slightly fruity and lovely after a hard days decorating. Sun illuminated but golden nonetheless."
}
---