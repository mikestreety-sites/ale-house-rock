---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1397931044",
  "title": "HOWLER NZ PILSNER",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-07-11",
  "style": "Pilsner - New Zealand",
  "abv": "5%",
  "breweries": [
    "brewery/missing-link-brewing/"
  ],
  "number": 945,
  "permalink": "beer/howler-nz-pilsner-missing-link-brewing/",
  "review": "The last of my Missing Link Brewser box and one to watch with the football. It was a light, thirst quencher, but had a bit of a dry aftertaste which I wasn't the biggest fan of. The football result helped though."
}
---

