---json
{
    "title": "Marylou",
    "number": "671",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CemKTxUqWjZ/",
    "date": "2022-06-09",
    "permalink": "beer/marylou-verdant-brewing-company/",
    "breweries": [
        "brewery/verdant-brewing-company/"
    ],
    "tags": [],
    "review": "I wouldn&#39;t normally pick a pale ale, as they tend to be light, flavourless and a bit boring, however, this &quot;Pales&quot; box from @thefuss.club has forced me to try some proper ones. This pale from @verdantbrew is absolutely fantastic. I had to stop myself from drinking it all in one go. It tasted like it should have been more than the 5% that it is. It had a morish light-but-fruity flavour. Definitely one of the best pales I&#39;ve had."
}
---