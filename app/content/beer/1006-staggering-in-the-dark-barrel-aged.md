---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1450222633",
  "title": "Staggering In the Dark (Barrel Aged)",
  "serving": "Bottle",
  "rating": 7,
  "purchased": "shop/hand-brew-co-brewery-tap-room/",
  "date": "2025-01-16",
  "style": "Stout - Imperial / Double",
  "abv": "10.2%",
  "breweries": [
    "brewery/hand-brew-co/"
  ],
  "number": 1006,
  "permalink": "beer/staggering-in-the-dark-barrel-aged-hand-brew-co/",
  "review": "This was like nothing I've had before. It was thick and sweet and like a meal in itself. A break away from the normal place as I took it to my sisters to share - there was no way I'd manage a bottle. It was tasty, but I'm not sure I'd have it again."
}
---

