---json
{
    "title": "Mosaic Pale Ale",
    "number": "380",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CHQ_xVxJ0xu/",
    "date": "2020-11-06",
    "permalink": "beer/mosaic-pale-ale-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "Another disappointing can from Adams, picked up while in their hometown of Southwold a couple of weeks ago. Once again, a forgettable taste which is such a shame. A smidge better than the last."
}
---