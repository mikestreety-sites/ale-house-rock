---json
{
    "title": "A New Level",
    "number": "574",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CUN8_cdKqi3/",
    "date": "2021-09-24",
    "permalink": "beer/a-new-level-cloudwater/",
    "breweries": [
        "brewery/cloudwater/"
    ],
    "tags": [],
    "review": "A smooth, easy-drinkin&#39; juicy DIPA."
}
---