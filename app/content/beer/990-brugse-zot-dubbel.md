---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1434199412",
  "title": "Brugse Zot Dubbel",
  "serving": "Bottle",
  "rating": 8,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-11-16",
  "style": "Belgian Dubbel",
  "abv": "7.5%",
  "breweries": [
    "brewery/brouwerij-de-halve-maan/"
  ],
  "number": 990,
  "permalink": "beer/brugse-zot-dubbel-brouwerij-de-halve-maan/",
  "review": "I've not had many Belgian beers, so it's hard to know where this falls on the scale. It was smooth and tasty with a bit of a kick and I'd definitely have it again."
}
---
