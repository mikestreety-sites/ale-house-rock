---json
{
    "title": "Black",
    "number": "355",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CCRpH1apbdd/",
    "date": "2020-07-05",
    "permalink": "beer/black-boss-brewing/",
    "breweries": [
        "brewery/boss-brewing/"
    ],
    "tags": [],
    "review": "This &quot;seriously silky stout&quot; was anything but. Had a bit of an odd fizz to it and was not a scratch on the porter from the other night. The bottle design also leaves a lot to be desired. Overall, not that impressed."
}
---