---json
{
    "title": "Drunk Monk",
    "number": "101",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/BgCX2LbHqvI/",
    "date": "2018-03-07",
    "permalink": "beer/drunk-monk-saltdean-brewery/",
    "breweries": [
        "brewery/saltdean-brewery/"
    ],
    "tags": [
        "stout"
    ],
    "review": "This “home brew” from the Saltdean Brewery was exchanged for some bike brake pads, which seems like a good deal. This is their stout and it is glorious. It’s got all the right flavours and smells without being too heavy. It’s chocolatey and coffee-y and tasty. Definitely a slow burner but would happily have this again and again"
}
---