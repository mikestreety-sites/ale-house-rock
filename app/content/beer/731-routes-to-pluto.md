---json
{
    "title": "Routes To Pluto",
    "number": "731",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1216703486",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-10-30",
    "permalink": "beer/routes-to-pluto-cloak-and-dagger/",
    "breweries": [
        "brewery/cloak-and-dagger/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "A clean crisp NEIPA with a cool can to boot"
}
---