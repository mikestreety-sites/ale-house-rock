---json
{
    "title": "Jukebox",
    "number": "690",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CgnQyX3K1r0/",
    "date": "2022-07-29",
    "permalink": "beer/jukebox-hand-brew-co/",
    "breweries": [
        "brewery/hand-brew-co/"
    ],
    "tags": [],
    "review": "Dang, this was tasty. Local brewery @handbrewco have produced this wonderfully full-bodied DDH IPA. Immediately drinkable, it was smooth, fruity and perfect at 6%"
}
---