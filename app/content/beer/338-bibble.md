---json
{
    "title": "Bibble",
    "number": "338",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CA5QTP-J8YL/",
    "date": "2020-06-01",
    "permalink": "beer/bibble-the-wild-beer-company/",
    "breweries": [
        "brewery/the-wild-beer-company/"
    ],
    "tags": [],
    "review": "Now this. This is a @wildbeerco beer I can get behind. Lovely balance of flavour - refreshing and hoppy without being too overbearing. This is definitely one I would buy again if I saw it in the shop. Great way to end a somewhat interesting fling with Wild Beer"
}
---