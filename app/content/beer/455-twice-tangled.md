---json
{
    "title": "Twice Tangled",
    "number": "455",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CMsqrempWzE/",
    "date": "2021-03-21",
    "permalink": "beer/twice-tangled-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "With my recent frivolity into Double IPAs, this somewhat piqued my interest when I saw it on the Tesco website. Not only is it going back to my traditional beer-tasting routes with a classic ale, it has the modern twist of my recent flirtations. I&#39;ve had this double hopped version of Badger&#39;s Tanglefoot in the fridge for a couple of weeks - was saving it as an accompaniment to a roast dinner (to which it was a great match). There was some of the DIPA characteristics there, but not enough for my liking. It was more of an intense flavour of a beer which didn&#39;t need it. Nice enough, but I think I&#39;ll stick to the classic Tanglefoot if I&#39;m feeling that way inclined."
}
---