---json
{
    "title": "Softly Spoken Magic Spells",
    "number": "416",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CKFOfPaFrhQ/",
    "date": "2021-01-15",
    "permalink": "beer/softly-spoken-magic-spells-singlecut/",
    "breweries": [
        "brewery/singlecut/"
    ],
    "tags": [],
    "review": "Beer 2 of the Zoom tasting. A lot easier on the taste-buds &amp; a lot easier to drink (dangerous at 8.6%). The orange comes through in both taste and smell but isn&#39;t overpowering. Bordering on the enjoyment of a DIPA - would pick another one of these up if I saw it."
}
---