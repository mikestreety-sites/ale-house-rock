---json
{
    "title": "T.E.A.",
    "number": "92",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bdiu1ZuFdBv/",
    "date": "2018-01-04",
    "permalink": "beer/tea-hogs-back-brewery/",
    "breweries": [
        "brewery/hogs-back-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A good old fashioned winter evening ale. Great for sitting indoors in front of the TV. Lovely."
}
---