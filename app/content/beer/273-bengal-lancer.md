---json
{
    "title": "Bengal Lancer",
    "number": "273",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B7rastqpHHd/",
    "date": "2020-01-23",
    "permalink": "beer/bengal-lancer-fullers/",
    "breweries": [
        "brewery/fullers/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "This was a cracking pint of IPA. Bottle conditioned, it reminded me a lot of a Young&#39;s Beer. I found this tucked away in my local co-op, dusty and neglected. One of the best Fuller&#39;s beers I&#39;ve had"
}
---