---json
{
    "title": "Annapurna",
    "number": "677",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Ce9MJ-xqSY9/",
    "date": "2022-06-18",
    "permalink": "beer/annapurna-glasshouse-beer-co/",
    "breweries": [
        "brewery/glasshouse-beer-co/"
    ],
    "tags": [],
    "review": "Another full-bodied pale. Juicy and refreshing, a great beer to end a sunny Friday."
}
---