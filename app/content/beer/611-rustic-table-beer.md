---json
{
    "title": "Rustic Table Beer",
    "number": "611",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CZquotfKYlz/",
    "date": "2022-02-07",
    "permalink": "beer/rustic-table-beer-burning-sky/",
    "breweries": [
        "brewery/burning-sky/"
    ],
    "tags": [],
    "review": "I was not a fan of this at all. It was hard to pinpoint exactly what it was that I didn&#39;t enjoy but I think it was too hoppy and light for my tastes. Managed to finish it though, which is something."
}
---