---json
{
    "title": "Kew Gardens Golden Ale",
    "number": "261",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B5yfiSwpI9m/",
    "date": "2019-12-07",
    "permalink": "beer/kew-gardens-golden-ale-twickenham-fine-ales/",
    "breweries": [
        "brewery/twickenham-fine-ales/"
    ],
    "tags": [],
    "review": "Seem to be on a bit of a run of new beers at the moment, must be the run up to Christmas (started this account 3 years ago!). This light crisp ale from local &quot;Kew gardens brewery&quot; was easy to drink and could taste the hint of honey from local bees"
}
---