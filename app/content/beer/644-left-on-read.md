---json
{
    "title": "Left on Read",
    "number": "644",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Ccsb2oXKK2w/",
    "date": "2022-04-23",
    "permalink": "beer/left-on-read-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "Yet another cracking DIPA from @deyabrewery. Once again proving they can do no wrong. Smooth, fruity, 8% and damn tasty."
}
---