---json
{
    "title": "Both Barrels - Oak Aged Broadside",
    "number": "256",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B5V9DujFQj2/",
    "date": "2019-11-26",
    "permalink": "beer/both-barrels-oak-aged-broadside-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "Wow, what a punch this beer has. Brewed in rum barrels, this beer took a 6.3% bottle of Broadside and ramped it up to 9%! It tasted like it had a full shot of rum in it, which warmed the cockles with every sip but took away the lovely taste of the broadside beer. Worth a try, but wouldn&#39;t rush to have it again"
}
---