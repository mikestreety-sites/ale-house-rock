---json
{
    "title": "Ruddles Best",
    "number": "169",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BplotbBgICY/",
    "date": "2018-10-31",
    "permalink": "beer/ruddles-best-greene-king/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [],
    "review": "If this was meant to be their best, I would hate to try their worst. Weirdly watery and severely lacking any sort of aftertaste"
}
---