---json
{
    "title": "Mallow Laser Quest",
    "number": "457",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CM5WDqVpzr6/",
    "date": "2021-03-26",
    "permalink": "beer/mallow-laser-quest-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "This was an unusual one to get used to and not really one to have with dinner (more of a post-dinner beer, really). A consistency that reminded me of my pineapple juice and lemonade days (with no fizz) combined with a slight after-taste of the mallow. Very sweet and the first beer (in over 450) that both my wife and I liked. Can&#39;t say I would buy this again, but enjoyed it nonetheless."
}
---