---json
{
    "title": "Gamma Ray",
    "number": "322",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B_-zQlrp1Lm/",
    "date": "2020-05-09",
    "permalink": "beer/gamma-ray-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [],
    "review": "What a beautifully delightful beer. Was surprised about the excellent taste on this one as I was expecting Brewdog levels, but wow, it blew me away. Nice and light, full of flavour with a warm afterglow of &quot;not a care in the world&quot;. Will definitely be buying more of these."
}
---