---json
{
    "title": "Southwold Winter IPA",
    "number": "71",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BaPXhYgloiS/",
    "date": "2017-10-14",
    "permalink": "beer/southwold-winter-ipa-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "Brewed by Adnams for Marks &amp; Spencer. Tasting it I can see why they don’t sell it under the “Adnams” name. It’s certainly not up to scratch and doesn’t compare to their other beers (was drinking a Ghost ship just yesterday). Very floral with an odd aftertaste. Wouldn’t buy it again."
}
---