---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1454739644",
  "title": "Hatchet",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/brewser-online-store/",
  "date": "2025-02-06",
  "style": "IPA - Session",
  "abv": "3.4%",
  "breweries": [
    "brewery/buxton-brewery/"
  ],
  "number": 1015,
  "permalink": "beer/hatchet-buxton-brewery/",
  "review": "A little underwhelming as an IPA (or a Pale Ale, as the session cancels out the India bit). Drinkable enough and could session it, but won't be rushing to buy another. Something wasn't right with the hope/aftertaste but couldn't put my finger on it"
}
---

