---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1381051536",
  "title": "King Crush",
  "serving": "Can",
  "rating": 4,
  "purchased": "shop/sainsburys/",
  "date": "2024-05-15",
  "style": "IPA - Imperial / Double Milkshake",
  "abv": "8.4%",
  "breweries": [
    "brewery/brewdog/"
  ],
  "number": 926,
  "permalink": "beer/king-crush-brewdog/",
  "review": "Sticky and sweet is not really what you want from a beer. This wasn't as bad as I was expecting it to be, but I won't be buying it again. The 8.4% seemed more like a flex then a necessity"
}
---

