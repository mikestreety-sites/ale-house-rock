---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1319232412",
  "title": "Chur Cuz",
  "serving": "Can",
  "rating": 8.5,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-02",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 853,
  "permalink": "beer/chur-cuz-overtone-brewing-co/",
  "review": "This was really good. Reminiscent of a Sureshot or Beak DIPA. Thick, juicy and full of flavour. Definitely not a session beer!"
}
---

