---json
{
    "title": "Heart & Soul",
    "number": "726",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1211823516",
    "serving": "Can",
    "purchased": "shop/asda/",
    "date": "2022-10-15",
    "permalink": "beer/heart-soul-vocation-brewery/",
    "breweries": [
        "brewery/vocation-brewery/"
    ],
    "tags": [
        "sessionipa"
    ],
    "review": "Every now and then I thought this session IPA was going to be ok, but then I left it for a bit and got an odd aftertaste. Not for me, this one"
}
---