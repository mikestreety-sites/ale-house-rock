---json
{
    "title": "Half Pipe",
    "number": "514",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CQRe2p5JmDh/",
    "date": "2021-06-18",
    "permalink": "beer/half-pipe-aldi/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "A session ale with slightly more flavour. Exactly what you would expect from an Aldi beer. Middle of the road. Suited to a BBQ or park where you&#39;re after a refreshing drink which will go with anything."
}
---