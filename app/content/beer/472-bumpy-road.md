---json
{
    "title": "Bumpy Road",
    "number": "472",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CN2hu7yFePc/",
    "date": "2021-04-19",
    "permalink": "beer/bumpy-road-thornbridge-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/thornbridge/"
    ],
    "tags": [
        "dipa",
        "ipa"
    ],
    "review": "Damn, @thebeakbrewery don&#39;t struggle to make tasty IPAs do they?! This DIPA from them and @thornbridge smashes it out the park. Full bodied and full of flavour. Thoroughly enjoyed this whole can. If I had one cristisim, then the alcohol taste was a bit too present, but it wouldn&#39;t stop me buying this 8% monster again."
}
---