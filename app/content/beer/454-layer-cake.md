---json
{
    "title": "Layer cake",
    "number": "454",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CMqKGX7pU3-/",
    "date": "2021-03-20",
    "permalink": "beer/layer-cake-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "This is a dessert stout if ever I had one. Marshmallow and chocolate and you can taste it all. Reminds me of a beer flavoured marshmallow rather than the other way round. A thick stout which is definitely best enjoyed over an evening. Probably not with a big heavy dinner as it&#39;s going to fill you up. You can&#39;t really taste the 7% kick until you go to standup. I did enjoy it though, went very well with film night"
}
---