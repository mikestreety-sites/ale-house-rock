---json
{
    "title": "It's Not Terry's It's Ours",
    "number": "607",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CZXoCTYq86R/",
    "date": "2022-01-30",
    "permalink": "beer/its-not-terrys-its-ours-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [],
    "review": "This was a lovely tasting stout, but a bit disappointing. It was marketed as a &quot;Chocolate Orange Stout&quot; however, it lacked any sort of orange taste. Smooth and easy to drink, but not what it said on the tin."
}
---