---json
{
    "title": "Wharf IPA",
    "number": "269",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B635G18lIWM/",
    "date": "2020-01-03",
    "permalink": "beer/wharf-ipa-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "I tried to get back into the habit of posting beers as I drink them, as it seemed to be slipping of late. I couldn&#39;t do it with this one as I drank it too damn quickly. Fantastic IPA - one of the best I&#39;ve ever had. More please!"
}
---