---json
{
    "title": "Banana Bread Beer",
    "number": "99",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BfO8R5YF9Gj/",
    "date": "2018-02-15",
    "permalink": "beer/banana-bread-beer-eagle-brewery/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [],
    "review": "(Brewery previously was Wells) Bought for me by @robynphillippa. An odd little beer. It tastes like you are taking a sip of beer and then eating one of those foam bananas after every mouthful. Subtle banana taste but mainly beer. Good, and would have it again, but wouldn’t rush to buy it"
}
---