---json
{
    "title": "Unite Tribute IPA",
    "number": "342",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CBL2YPpJMI-/",
    "date": "2020-06-08",
    "permalink": "beer/unite-tribute-ipa-wild-card-brewery/",
    "breweries": [
        "brewery/wild-card-brewery/"
    ],
    "tags": [],
    "review": "I drank this IPA without incident. Was ok, went down well and was a little bit disappointed when it finished. I wouldn&#39;t reach for it again but wouldn&#39;t turn it down if I was offered one"
}
---