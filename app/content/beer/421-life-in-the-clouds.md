---json
{
    "title": "Life in the clouds",
    "number": "421",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CKUv9oYlZK0/",
    "date": "2021-01-21",
    "permalink": "beer/life-in-the-clouds-collective-arts/",
    "breweries": [
        "brewery/collective-arts/"
    ],
    "tags": [],
    "review": "A double dry hopped IPA to see in my Thursday evening. First sip was a bit of a disappointment but as it went on, it got a lot better - so much so I didn&#39;t want to finish it! There is a bit of a strong hoppy aftertaste, which knocks it down a point but still a pretty solid pint."
}
---