---json
{
    "title": "Milk Stout",
    "number": "190",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/Br99h-HAUvq/",
    "date": "2018-12-29",
    "permalink": "beer/milk-stout-black-sheep-brewery/",
    "breweries": [
        "brewery/black-sheep-brewery/"
    ],
    "tags": [
        "stout"
    ],
    "review": "My final one from Christmas day (a very much #latergram, I know). Quite a strong heavy beer - not one for me. Had quite a strong coffee taste and, despite loving coffee, it wasn’t to my liking"
}
---