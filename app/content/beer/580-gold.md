---json
{
    "title": "Gold",
    "number": "580",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CUoxXlaKs6U/",
    "date": "2021-10-05",
    "permalink": "beer/gold-exmoor-ales/",
    "breweries": [
        "brewery/exmoor-ales/"
    ],
    "tags": [],
    "review": "The first attempt at buying one of these ended up with my wife being ID&#39;d and subsequently denied being able to buy it, as she had left her purse in the car. Eventually managed to pick one up myself from different shop. Light,  very carbonated and very easy drinking. Not the most complex of flavours, but enjoyable."
}
---