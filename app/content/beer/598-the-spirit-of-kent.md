---json
{
    "title": "Spirit of Kent",
    "number": "598",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CYGiq0qKena/",
    "date": "2021-12-30",
    "permalink": "beer/spirit-of-kent-westerham-brewery/",
    "breweries": [
        "brewery/westerham-brewery/"
    ],
    "tags": [],
    "review": "A drinkable but uninspiring IPA from Kent. A bit better than the previous one, but not one I would buy again. For those counting, this is number 599! The big is 600 next 😁"
}
---