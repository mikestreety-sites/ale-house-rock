---json
{
    "title": "Goodwood Sussex Ale",
    "number": "18",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BOF02VTA9I4/",
    "serving": "Bottle",
    "date": "2016-12-16",
    "permalink": "beer/goodwood-sussex-ale-hepworth-co-brewers/",
    "breweries": [
        "brewery/hepworth-co-brewers/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A light, fizzy, hoppy ale. Not really a fan, but drinkable."
}
---