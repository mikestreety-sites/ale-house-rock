---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1382127920",
  "title": "Wingman",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/sainsburys/",
  "date": "2024-05-18",
  "style": "IPA - Session",
  "abv": "4.3%",
  "breweries": [
    "brewery/brewdog/"
  ],
  "number": 928,
  "permalink": "beer/wingman-brewdog/",
  "review": "A run-of-the-mill mass-produced session IPA. Nothing groundbreaking but not offensive"
}
---
