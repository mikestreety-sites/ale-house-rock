---json
{
    "title": "Scaramanga",
    "number": "400",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CJRaBE9Fdhy/",
    "date": "2020-12-26",
    "permalink": "beer/scaramanga-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "Back on the @gunbrewery beer and this one is a slow grower. After the first few sips I was sure it was going to be a 3, but the more I drank the slightly better it got. Was expecting it to be more of a sour but was just sharp and citrus-y"
}
---