---json
{
    "title": "Willo",
    "number": "631",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/Ca-wiWsq07s/",
    "date": "2022-03-11",
    "permalink": "beer/willo-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "Seem to be on a roll with lovely beers at the moment. Another absolute corker from @thebeakbrewery which was picked up at @palate.bottleshop. Just generally a great beer."
}
---