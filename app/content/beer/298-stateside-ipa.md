---json
{
    "title": "The Buckhorn Brewery Stateside IPA",
    "number": "298",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/B-iPC5xJz6X/",
    "date": "2020-04-03",
    "permalink": "beer/stateside-ipa-the-buckhorn-brewery/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "This can stay stateside to be honest. This was the kind of beer you would expect from Aldi for £1.50. A slight ambience of taste with a watery finish."
}
---