---json
{
    "title": "Electric Boogaloo",
    "number": "164",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BokRKO7l8ya/",
    "date": "2018-10-05",
    "permalink": "beer/electric-boogaloo-flavourly/",
    "breweries": [
        "brewery/flavourly/"
    ],
    "tags": [
        "lager"
    ],
    "review": "Another beer from @flavourlyhq and although not strictly an ale, I thought I would give it ago. It’s a mid-range beer - better than a lot of lagers but nowhere near as good as an ale. Has flavour but a bit bland for me."
}
---