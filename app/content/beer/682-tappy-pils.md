---json
{
    "title": "Tappy Pils",
    "number": "682",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Cf2PXdlKgLU/",
    "date": "2022-07-10",
    "permalink": "beer/tappy-oils-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [
        "pilsner"
    ],
    "review": "I was asked the other day how I keep my reviews subjective - the answer is that I don&#39;t. Most of my reviews are purely based on how I feel at that moment, although there aren&#39;t many scores I would change having had a second or third of the same beer. This is the beer you want after (or during) a hot summers day (believe me, I&#39;ve been there). It&#39;s lovely and light and refreshing - the right amount of flavour you would expect from a pilsner. I don&#39;t know if it was just because I&#39;d got back from a seafront bike ride, or becuase it is from @deyabrewery (and lord knows I am biased towards Deya) but this hit the nail on the head. Right in that moment, it would have got a 10, but on reflection it was &quot;just&quot; a pilsner (but a good one)"
}
---