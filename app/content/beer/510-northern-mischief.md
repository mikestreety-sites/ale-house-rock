---json
{
    "title": "Northern Mischief",
    "number": "510",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CQERsptlRHp/",
    "date": "2021-06-13",
    "permalink": "beer/northern-mischief-wooha/",
    "breweries": [
        "brewery/wooha/"
    ],
    "tags": [],
    "review": "A friend came round to play board games and bought a couple of @woohabrewing beers with him. This bottle conditioned lager was a crisp little number. Very lively (hence the big head) but light and refreshing. As with any lager, there was no notably &quot;big&quot; flavour."
}
---