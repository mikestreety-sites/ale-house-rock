---json
{
    "title": "Pool",
    "number": "517",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CQUBqw9pIUv/",
    "date": "2021-06-19",
    "permalink": "beer/pool-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I think I want to drink @thebeakbrewery beers for the rest of time. This pale was so damn good and, at 5%, I could drink a good few of an evening. Packed full of flavours with a bitter tang that takes me back to my ale days. I was wholeheartedly disappointed when I finished this one"
}
---