---json
{
    "title": "Jeffrey Hudson Bitter JHB",
    "number": "183",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BrVs4MHg8Tn/",
    "date": "2018-12-13",
    "permalink": "beer/jeffrey-hudson-bitter-jhb-oakham-ales/",
    "breweries": [
        "brewery/oakham-ales/"
    ],
    "tags": [],
    "review": "What a Rod Stewart of a beer. Not horrible but nothing there to like. Plain, simple with a hint of taste. Middle of the road"
}
---