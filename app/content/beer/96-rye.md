---json
{
    "title": "The Rev. James Rye",
    "number": "96",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bd8fo7KFUOy/",
    "date": "2018-01-14",
    "permalink": "beer/rye-the-rev-james/",
    "breweries": [
        "brewery/brains/"
    ],
    "tags": [],
    "review": "This full bodied ale was a perfect slow sipping beer. Lovely flavours without being too heavy"
}
---