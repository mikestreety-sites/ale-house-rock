---json
{
    "title": "New Zealand Ultra Pale",
    "number": "359",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CCtt1xSJwPM/",
    "date": "2020-07-16",
    "permalink": "beer/new-zealand-ultra-pale-first-chop-brewing-arm/",
    "breweries": [
        "brewery/first-chop-brewing-arm/"
    ],
    "tags": [],
    "review": "Such a non-descript beer. Reminded me a lot of the Octopus Energy craft lager, just slightly worse. It was so bland I&#39;m struggling to write anything about it. Yet to find a @flavourlyhq beer which is top drawer"
}
---