---json
{
    "title": "Kobolo",
    "number": "386",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CHnGlqyFGiY/",
    "date": "2020-11-15",
    "permalink": "beer/kobolo-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "Last of the Suffolk beers, this @adnams beer is one of taste. Opened after the disaster of the previous beer, this lager (yes lager) was a refreshing change. Light, fizzy and flavoursome"
}
---