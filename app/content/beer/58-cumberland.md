---json
{
    "title": "Cumberland",
    "number": "58",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BX59MxWFQR1/",
    "date": "2017-08-17",
    "permalink": "beer/cumberland-jennings/",
    "breweries": [
        "brewery/jennings-brewery/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "just a bog standard beer. Good staple pint"
}
---