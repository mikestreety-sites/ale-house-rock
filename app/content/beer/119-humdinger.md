---json
{
    "title": "Humdinger",
    "number": "119",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BkX9Gh8F1_N/",
    "date": "2018-06-23",
    "permalink": "beer/humdinger-joseph-holt/",
    "breweries": [
        "brewery/joseph-holt/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Well this is a humdinger of beer. Lovely and crisp with a honey aftertaste. Tasty."
}
---