---json
{
    "title": "Wonky Donkey",
    "number": "105",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BhXFgbQlvsI/",
    "date": "2018-04-09",
    "permalink": "beer/wonky-donkey-white-rock-brewery/",
    "breweries": [
        "brewery/white-rock-brewery/"
    ],
    "tags": [],
    "review": "This oddly-named beer has an odd logo and an odd taste. Brewed here in Guernsey, it’s drinkable, but I’m glad I’m only having half a pint. 4.5/10 from me, and a 6 from @geoffst."
}
---