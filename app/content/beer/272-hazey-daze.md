---json
{
    "title": "Hazey Daze",
    "number": "272",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B7T0EHCpnla/",
    "date": "2020-01-14",
    "permalink": "beer/hazey-daze-london-beer-factory/",
    "breweries": [
        "brewery/london-beer-factory/"
    ],
    "tags": [],
    "review": "Fruity, but classic hipster IPA. Nothing exciting"
}
---