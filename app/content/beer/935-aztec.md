---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1380452364",
  "title": "Aztec",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sainsburys/",
  "date": "2024-05-12",
  "style": "IPA - American",
  "abv": "6.2%",
  "breweries": [
    "brewery/mad-squirrel-brewery/"
  ],
  "number": 935,
  "permalink": "beer/aztec-mad-squirrel-brewery/",
  "review": "A surprisingly smooth beer from a supermarket. Nice subtle flavours and an all-to-easy-to-drink beer (as you can tell from the fact I couldn't stop to take the photo!)"
}
---

