---json
{
    "title": "King Slayer",
    "number": "370",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CFjTQY8FsJS/",
    "date": "2020-09-25",
    "permalink": "beer/king-slayer-buxton-brewery/",
    "breweries": [
        "brewery/buxton-brewery/"
    ],
    "tags": [],
    "review": "After a months hiatus from posting new beers (don&#39;t worry, I was still drinking beer - I just fancied a few weeks of drinking some old favourites without having to think of scores), I thought I would get back on the wagon and post this beauty. I&#39;ve had a few @buxtonbrewery beers in the past and some have been a bit meh, but this one is incredible. Smooth, very drinkable and a drinking 8%. Reminded me of a couple of Fourpure beers. Excellent stuff Buxton!"
}
---