---json
{
    "title": "Uptown",
    "number": "533",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CRZ69AvJkuW/",
    "date": "2021-07-16",
    "permalink": "beer/uptown-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [],
    "review": "Picked this NEIPA up from @palate.bottleshop. A bit too alcoholic tasting, if you can picture such a thing. I also had to drink some water during as it was quite dry. The taste was there though, so was enjoyable enough."
}
---