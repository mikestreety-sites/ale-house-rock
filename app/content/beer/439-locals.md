---json
{
    "title": "Locals",
    "number": "439",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CLwJAdAFibe/",
    "date": "2021-02-26",
    "permalink": "beer/locals-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Yet another incredible small brewery IPA. Not too far from me in Lewes, this refreshing, very drinkable beer hit all the spots. I was disappointed when I finished it. I&#39;d not heard about @thebeakbrewery until just over a week ago when their beers start flooding my feed, so when I was in @palate.bottleshop last week I picked one up on a whim, and what an absolute corker it turned out to be."
}
---