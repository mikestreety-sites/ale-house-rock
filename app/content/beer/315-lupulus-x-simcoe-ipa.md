---json
{
    "title": "Lupulus X Simcoe IPA",
    "number": "315",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B_icTAgJfR9/",
    "date": "2020-04-28",
    "permalink": "beer/lupulus-x-simcoe-ipa-buxton-brewery/",
    "breweries": [
        "brewery/buxton-brewery/"
    ],
    "tags": [],
    "review": "My first foray into @buxtonbrewery  and what a refreshing beer it was too. Had to stop myself from drinking this in one - despite the hazy appearance, it was a light, crisp IPA. Looking forward to the other Buxton beers in my fridge!"
}
---