---json
{
    "title": "NEIPA",
    "number": "662",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CeJ1WOsqUN3/",
    "date": "2022-05-29",
    "permalink": "beer/neipa-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [],
    "review": "New England IPAs are such good, juicy, easy drinking beers and this one is no exception. @unbarredbrewery know how to make a good beer and so their NEIPA is top notch. Can design from local legend Cassette Lord - who has decorated a few green phone boxes in Brighton and is definitely worth a Google!"
}
---