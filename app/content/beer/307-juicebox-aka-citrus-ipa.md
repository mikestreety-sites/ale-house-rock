---json
{
    "title": "Juicebox (A.K.A. Citrus IPA)",
    "number": "307",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B_HcIUPJ-Tc/",
    "date": "2020-04-18",
    "permalink": "beer/juicebox-aka-citrus-ipa-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [],
    "review": "I had written this off as previous citrus/fruit beers had left a lot to be desired. This, however, was one of the best beers of this type I have ever had. I was blown away by the lush taste and the very subtle citrus flavour. Good job @fourpure."
}
---