---json
{
    "title": "Talking Head",
    "number": "587",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CVqk7BWKK-0/",
    "date": "2021-10-30",
    "permalink": "beer/talking-head-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "One of their better beers, but still not ground-breaking. A drink to do something with, a entry-level beer, rather than a beer to sit and enjoy on its own."
}
---