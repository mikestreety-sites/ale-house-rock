---json
{
    "title": "Dank Marvin",
    "number": "624",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Caeei_kqGSR/",
    "date": "2022-02-27",
    "permalink": "beer/dank-marvin-abyss-brewing/",
    "breweries": [
        "brewery/abyss-brewing/"
    ],
    "tags": [],
    "review": "A little underwhelming as a beer goes. It was ok, but not astounding."
}
---