---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1422864657",
  "title": "Get On Astro!",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-10-05",
  "style": "IPA - New England / Hazy",
  "abv": "6.4%",
  "breweries": [
    "brewery/quantock-brewery/"
  ],
  "number": 977,
  "permalink": "beer/get-on-astro-quantock-brewery/",
  "review": "A nice enough NEIPA, but I wasn't blown away by it. It was lacking both the punch of fruit and the thickness I would expect from the style."
}
---
