---json
{
    "title": "Hop Commotion",
    "number": "238",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B22BOFiFO8E/",
    "date": "2019-09-25",
    "permalink": "beer/hop-commotion-jennings/",
    "breweries": [
        "brewery/jennings-brewery/"
    ],
    "tags": [],
    "review": "This beer stank of strawberries so wasn&#39;t holding out much hope for it. A surprisingly pleasant taste. Subtle fruity notes with a hoppy body. Would drink again, I think"
}
---