---json
{
    "title": "Golden Ale",
    "number": "374",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CGYMnm-lESE/",
    "date": "2020-10-15",
    "permalink": "beer/golden-ale-harpers-brewing-company/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "I was starting to think I should just post all the beers I buy from Aldi as a 7 and be done with it, but this one bucked the trend. I&#39;ve never had a beer that started off bad and got worse with each sip. An ambience which was remeniscent of the CBD beer I had before. Managed to choke down the last few mouthfuls. This is definitely not a beer I will be buying again."
}
---