---json
{
    "title": "Easy Times IPA",
    "number": "235",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/B2UzSRKFmDA/",
    "date": "2019-09-12",
    "permalink": "beer/easy-times-ipa-hop-hemp-brew-co/",
    "breweries": [
        "brewery/hop-hemp-brew-co/"
    ],
    "tags": [],
    "review": "Forgot to write this last week. Had this freebie from a sample at work and I wish I hadn&#39;t. 0% alcohol with 8mg of CBD. It had the chemical taste of a non-alcoholic beer with the aftertaste of someone smoking marijuana walking past and coughing outside. Terrible taste but somehow I did manage to finish it"
}
---