---json
{
    "title": "Clwb Tropica",
    "number": "723",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1210888492",
    "serving": "Can",
    "purchased": "shop/sainsburys/",
    "date": "2022-10-13",
    "permalink": "beer/clwb-tropica-tiny-rebel-brewing-co/",
    "breweries": [
        "brewery/tiny-rebel-brewing-co/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A drinkable tropical IPA. Thought it would be higher after the first couple of sip, but as the flavour settled it became a bit more average."
}
---