---json
{
    "title": "In The Grip",
    "number": "721",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1208464227",
    "serving": "Bottle",
    "date": "2022-10-06",
    "permalink": "beer/in-the-grip-caffle-brewery/",
    "breweries": [
        "brewery/caffle-brewery/"
    ],
    "tags": [
        "redale"
    ],
    "review": "Depp red ale which was enjoyable enough. Wouldn&#39;t rush back to another one but it wasn&#39;t disappointing."
}
---