---json
{
    "title": "Joker IPA",
    "number": "568",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CT0Zd79K0v2/",
    "date": "2021-09-14",
    "permalink": "beer/joker-ipa-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "Just a standard, mass-produced, flat flavoured IPA. Not disgusting, but not distinctive"
}
---