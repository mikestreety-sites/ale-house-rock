---json
{
    "title": "Dry Hopped Lager",
    "number": "296",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B-heGh4JWoj/",
    "date": "2020-04-03",
    "permalink": "beer/dry-hopped-lager-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "A good tasting lager. Would hammer these at a BBQ or on a summer&#39;s day, even enjoyed it during a lockdown in my lounge! Good one @adnams"
}
---