---json
{
    "title": "Cheeky",
    "number": "47",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BUPi8YCFmRH/",
    "serving": "Bottle",
    "purchased": "shop/seven-cellars/",
    "date": "2017-05-18",
    "permalink": "beer/cheeky-pressure-drop/",
    "breweries": [
        "brewery/pressure-drop/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "another craft beer, this one bottle conditioned. Slight better than the other day but still wouldn&#39;t buy another"
}
---