---json
{
    "title": "Shake Some Action",
    "number": "534",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CRcXzBjJuqV/",
    "date": "2021-07-17",
    "permalink": "beer/shake-some-action-burning-sky/",
    "breweries": [
        "brewery/burning-sky/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Well this was a surprise. A lovely smooth hazy IPA (was expecting a clear, hoppy one). Tasty flavours from start to finish and one I would certainly have again from @burningskybeer. Bought from @palate.bottleshop, this beer was perfect fridge-cold after a hot day. My only regret is I only bought one!"
}
---