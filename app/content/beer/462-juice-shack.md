---json
{
    "title": "Juice Shack",
    "number": "462",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CNNlObIJkQ0/",
    "date": "2021-04-03",
    "permalink": "beer/juice-shack-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "I&#39;ve been thinking about this &quot;Tropical Milkshake IPA&quot; all day, trying to work out what I thought about it. It certainly reminded me of a milkshake, and had a juicy taste to it but did I enjoy it? I&#39;m still not sure. I drank the whole can, I didn&#39;t turn my nose up at all but I didn&#39;t crave for more once I&#39;d finished. A bit like the &quot;Cream Soda Pilsner&quot; I had the other week, it seems like a novelty. I&#39;m glad I experienced it, but I wouldn&#39;t do it again anytime soon."
}
---