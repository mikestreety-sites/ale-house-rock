---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1439556442",
  "title": "Choos",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-12-07",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 997,
  "permalink": "beer/choos-beak/",
  "review": "This 8% DIPA hit me hard. Maybe because it was fairy drinkable, I found the aftertaste to have a bit of a tang, but on the whole this was a well-rounded beer"
}
---
