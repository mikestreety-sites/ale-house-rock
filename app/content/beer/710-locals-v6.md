---json
{
    "title": "Locals V6",
    "number": "710",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1198774827",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-09-04",
    "permalink": "beer/locals-v6-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "This was my least favourite of the series so far. Seemed to be missing the fullness I&#39;ve come to expect from the Locals beers. Still nice, but not up there with the rest."
}
---