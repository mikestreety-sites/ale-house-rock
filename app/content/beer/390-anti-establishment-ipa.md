---json
{
    "title": "Anti Establishment IPA",
    "number": "390",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CIOSaXMJTsh/",
    "date": "2020-11-30",
    "permalink": "beer/anti-establishment-ipa-aldi/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "Well, this one caught me by surprise. The catalyst for the Ald IPA I reviewed a few weeks ago, I was not expecting great things from this, based on the fact I am neither a fan of Pink IPA nor their IPA love-child. However, this was darn tasty. Crisp, flavoursome and a cut above the others. This is want Punk should taste like and I will definitely be hunting more of these down"
}
---