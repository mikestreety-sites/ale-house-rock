---json
{
    "title": "Pale Ale",
    "number": "537",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CRlEOT6l8lK/",
    "date": "2021-07-21",
    "permalink": "beer/pale-ale-sierra-nevada/",
    "breweries": [
        "brewery/sierra-nevada-brewing-co/"
    ],
    "tags": [],
    "review": "I&#39;ve seen this beer a lot, but never had it as I often thought it was going to be a light flavourless drink. I was wrong, light crisp and easy-drinking. Perfect for a summers evening. Not gallons of flavour, but you might not be looking for that when you pick a drink like this"
}
---
