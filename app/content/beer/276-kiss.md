---json
{
    "title": "Kiss",
    "number": "276",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B8FSVnJJyNq/",
    "date": "2020-02-02",
    "permalink": "beer/kiss-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Wasn&#39;t too impressed with this one. Nice flavor to begin with but an odd aftertaste and a smell I couldn&#39;t quite fathom. Not sure I&#39;ll be picking another one of these up when I head back to the shop"
}
---