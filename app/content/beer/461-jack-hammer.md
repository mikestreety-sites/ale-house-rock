---json
{
    "title": "Jack Hammer",
    "number": "461",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CNI67U1p7Mo/",
    "date": "2021-04-01",
    "permalink": "beer/jack-hammer-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "Another day, another Brewdog. This beer is described on the can as a &quot;ruthless IPA&quot; and, while I wouldn&#39;t go so far as to call it ruthless, it was certainly more abrasive than the other IPAs from @brewdogofficial. A welcome change from the sea of pale ales in the Lock Down Survival Kit, this 7.2% beer was one that got better the more you drank it. I can almost say I _really_ enjoyed it by the time I got to the last sip."
}
---