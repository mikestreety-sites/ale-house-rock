---json
{
    "title": "Word",
    "number": "750",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1232456007",
    "serving": "Can",
    "date": "2022-12-23",
    "permalink": "beer/word-laine-brew-co/",
    "breweries": [
        "brewery/laine-brew-co/"
    ],
    "tags": [
        "lager"
    ],
    "review": "A nice crisp lager with a good hoppy taste. An enjoyable beer"
}
---