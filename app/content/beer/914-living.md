---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1371529381",
  "title": "Living",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-12",
  "style": "IPA - American",
  "abv": "5.6%",
  "breweries": [
    "brewery/elusive-brewing/",
    "brewery/burnt-mill-brewery/"
  ],
  "number": 914,
  "permalink": "beer/living-elusive-brewing-burnt-mill-brewery/",
  "review": "The tang of a West Coast IPA is a nice change of pace every now and then - the bitterness was a perfect accompaniment to a podcast recording evening. Crisp, refreshing and very drinkable."
}
---

