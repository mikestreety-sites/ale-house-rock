---json
{
    "title": "Crunch",
    "number": "250",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/B4V1AI4FxnZ/",
    "date": "2019-11-01",
    "permalink": "beer/crunch-hammerton/",
    "breweries": [
        "brewery/hammerton/"
    ],
    "tags": [
        "stout",
        "milkstout"
    ],
    "review": "Well this was an experience I would rather forget. A peanut butter milk stout (apparently the best in the shop). Was excited about this as I love peanut butter &amp; milk and I have a fondness of stouts. It was not good, but took me the whole glass (yes, I did finish it) to find out. It&#39;s a flavour I can&#39;t put into words. I&#39;m glad I tried it (I think?), It hasn&#39;t put me off trying other peanut butter milk stouts (I think?) but I definitely won&#39;t have this one again (I know that for sure)"
}
---