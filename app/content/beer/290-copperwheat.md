---json
{
    "title": "Copperwheat",
    "number": "290",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B9sKk24J2Xy/",
    "date": "2020-03-13",
    "permalink": "beer/copperwheat-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Yes, yet another Harvey&#39;s beer. They draw you in as they recycle the bottles, so you have to keep going back. Not that impressed with this one, had a weird tang and was too bitter for me. I&#39;ve definitely had better wheat beers."
}
---