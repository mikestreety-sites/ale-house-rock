---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1451102719",
  "title": "Benchmark IPA",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/beer-no-evil/",
  "date": "2025-01-19",
  "style": "IPA - English",
  "abv": "4.7%",
  "breweries": [
    "brewery/unbarred-brewery/"
  ],
  "number": 1008,
  "permalink": "beer/benchmark-ipa-unbarred-brewery/",
  "review": "This was pretty spot on to set a benchmark for IPAs. Had the crisp happiness followed by a smooth taste. Reminded me more of a classic \"bitter\" style IPA. Would definitely have this again."
}
---

