---json
{
    "title": "Peedie Porter",
    "number": "212",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BvmnlZulQoT/",
    "date": "2019-03-29",
    "permalink": "beer/peedie-porter-swannay-brewery/",
    "breweries": [
        "brewery/swannay-brewery/"
    ],
    "tags": [
        "porter",
        "stout"
    ],
    "review": "A classic sipping Porter to while the evening away"
}
---