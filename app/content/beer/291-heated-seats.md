---json
{
    "title": "Heated Seats",
    "number": "291",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B9w6A4PJbUo/",
    "date": "2020-03-15",
    "permalink": "beer/heated-seats-mikkeller/",
    "breweries": [
        "brewery/mikkeller/"
    ],
    "tags": [],
    "review": "This beer was a lot better than I anticipated. It had all the hallmarks of being a hipster-no-taste pale ale but this beer was full of flavour. The smell took me back to Copenhagen where @mikkellerbeer is rife. Lovely job"
}
---