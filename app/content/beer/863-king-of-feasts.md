---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1326015810",
  "title": "King of Feasts",
  "serving": "Can",
  "rating": 9.5,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-27",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 863,
  "permalink": "beer/king-of-feasts-overtone-brewing-co/",
  "review": "This was a fantastic pale ale and one of the best Overtone beers out there. It was light but still packed full of flavour"
}
---

