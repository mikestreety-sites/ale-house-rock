---json
{
    "title": "Brune",
    "number": "226",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/ByvHjknFvDd/",
    "date": "2019-06-15",
    "permalink": "beer/brune-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [],
    "review": "When I poured this last night, our guests had mixed feelings. It had the consistency (and ambience) of an iced coffee. Was smooth, but heavy - definitely a post-dinner drink. Not so sure about this one"
}
---