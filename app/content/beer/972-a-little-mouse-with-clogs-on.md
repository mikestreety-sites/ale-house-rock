---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1418092625",
  "title": "A Little Mouse With Clogs On",
  "serving": "Can",
  "rating": 9.5,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-09-16",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4.5%",
  "breweries": [
    "brewery/pretty-decent-beer-co/"
  ],
  "number": 972,
  "permalink": "beer/a-little-mouse-with-clogs-on-pretty-decent-beer-co/",
  "review": "This pale ale is spot on. Clean & crisp with a slight fruity taste and accompanied a takeaway perfectly. Not too thin that it tasted watery, but didn't knock on the doors of the heavier IPA family."
}
---

