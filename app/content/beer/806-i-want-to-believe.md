---json
{
    "title": "I Want To Believe",
    "number": "806",
    "rating": "9.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1278997026",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-05-28",
    "permalink": "beer/i-want-to-believe-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "What an absolute banging NEIPA. It&#39;s so good, I&#39;ve bought another one. Fruity, smooth and full of flavour. Top notch."
}
---