---json
{
    "title": "Everytime the Sun Comes Up",
    "number": "737",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1220329277",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-11-12",
    "permalink": "beer/everytime-the-sun-comes-up-floc-rivington-brewing-co/",
    "breweries": [
        "brewery/floc/",
        "brewery/rivington-brewing-co/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Soft, flavourful IPA that went down very easy on a Friday night. Another absolute banger of a beer. Top drawer."
}
---