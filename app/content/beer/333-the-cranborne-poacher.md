---json
{
    "title": "The Cranborne Poacher",
    "number": "333",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CApe_f4JGJ5/",
    "date": "2020-05-26",
    "permalink": "beer/the-cranborne-poacher-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [],
    "review": "Forgot to post this on Sunday. With my recent splurge on Craft beers (as those breweries tend to be the ones delivering in lockdown), I found myself down the beer aisle of my local supermarket yearning after a good old fashioned ale. Along with a couple of @hobgoblin_beer IPAs (Number 223, 9/10), I picked up this number from @badgerbeers. Reading the back of the bottle when I got home, it said it had notes of liquorice - something I&#39;m not keen on myself and made me slightly regret my purchase. When drinking it, you could taste the sweetness which left an odd taste in the mouth. I managed to finish it but I wouldn&#39;t buy it again."
}
---