---json
{
    "title": "Elvis AF",
    "number": "659",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/CeEuGfSKXoh/",
    "date": "2022-05-27",
    "permalink": "beer/elvis-af-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "Another disappointing alcohol-free beer from Brewdog. I just about finished it, but it wasn&#39;t easy."
}
---