---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1391845746",
  "title": "GIBBON",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-06-21",
  "style": "Pale Ale - American",
  "abv": "4.8%",
  "breweries": [
    "brewery/missing-link-brewing/"
  ],
  "number": 939,
  "permalink": "beer/gibbon-missing-link-brewing/",
  "review": "This was awesome. Crisp and light, still with plenty of flavour. Went very well with fajitas, and I was a bit sad when I realised I'd finished it."
}
---

