---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1295515405",
  "title": "Inner City Green",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/asda/",
  "date": "2023-07-19",
  "style": "IPA - New England / Hazy",
  "abv": "5.1%",
  "breweries": [
    "brewery/fourpure-brewing-company/"
  ],
  "number": 827,
  "permalink": "beer/inner-city-green-fourpure-brewing-co/",
  "review": "I have a soft spot for Fourpure as they got me into craft beer during the pandemic, so I had high hopes for this. Unfortunately, it didn't quite deliver. It was ok for a supermarket NEIPA, but it could be so much better."
}
---

