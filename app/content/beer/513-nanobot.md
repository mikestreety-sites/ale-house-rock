---json
{
    "title": "Nanobot",
    "number": "513",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CQOzcF_pgPy/",
    "date": "2021-06-17",
    "permalink": "beer/nanobot-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [],
    "review": "A 2.8% session ale which has as much taste as it does colour. It&#39;s a shame, Beavertown have made some exceptional beers, but this isn&#39;t one of them. Weak in taste and disappointing. I did manage to finish it though."
}
---