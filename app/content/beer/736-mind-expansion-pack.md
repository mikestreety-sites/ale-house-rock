---json
{
    "title": "Mind Expansion Pack",
    "number": "736",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1219203327",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-11-09",
    "permalink": "beer/mind-expansion-pack-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "This was crisp, flavourful pale ale that I could drink for days, and it was only 3.8%! Perfect Monday night beer (or any night, to be honest)"
}
---