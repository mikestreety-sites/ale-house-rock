---json
{
    "title": "Understanding Whole Systems",
    "number": "794",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1271263939",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2023-05-05",
    "permalink": "beer/understanding-whole-systems-pressure-drop/",
    "breweries": [
        "brewery/pressure-drop/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "A Pressure Drop NEIPA, can&#39;t go wrong really. Wasn&#39;t outstanding but a decent drink nonetheless."
}
---