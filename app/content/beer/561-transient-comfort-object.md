---json
{
    "title": "Transient Comfort Object",
    "number": "561",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CTH_Lk7q5kw/",
    "date": "2021-08-28",
    "permalink": "beer/transient-comfort-object-verdant-brewing-company/",
    "breweries": [
        "brewery/verdant-brewing-company/"
    ],
    "tags": [],
    "review": "This was a nice DIPA. Soft, fruity and smooth easy drinking. I would have it again but there was something missing - it didn&#39;t wow me."
}
---