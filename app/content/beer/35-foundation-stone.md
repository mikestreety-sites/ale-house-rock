---json
{
    "title": "Foundation stone",
    "number": "35",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BQlf1ZLlH7Y/",
    "date": "2017-02-16",
    "permalink": "beer/foundation-stone-lymestone-brewery/",
    "breweries": [
        "brewery/lymestone-brewery/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Probably my favourite of the Lymestone Brewery trio. Slightly citrusy and an all round nice beer!"
}
---