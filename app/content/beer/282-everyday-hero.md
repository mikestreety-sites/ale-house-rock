---json
{
    "title": "Everyday Hero",
    "number": "282",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/B8uZ6RHp5OB/",
    "date": "2020-02-18",
    "permalink": "beer/everyday-hero-amundsen-brewery/",
    "breweries": [
        "brewery/amundsen-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "As you can probably tell from the colour, this tasted like a very watered down beer. I can&#39;t even dislike the taste, as there wasn&#39;t much taste to dislike. Plain, thirst quenching beer with nothing notable about it at all. Like a magnolia wall."
}
---