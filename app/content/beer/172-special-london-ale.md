---json
{
    "title": "Young's Special London Ale",
    "number": "172",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/Bp-o8dLAw0l/",
    "date": "2018-11-09",
    "permalink": "beer/special-london-ale-youngs/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [],
    "review": "What a delightful beer. Light but complex. @youngsbeer have really knocked this one out the park"
}
---