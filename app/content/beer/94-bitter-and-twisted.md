---json
{
    "title": "Bitter and Twisted",
    "number": "94",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bd02_djl8la/",
    "date": "2018-01-11",
    "permalink": "beer/bitter-and-twisted-harviestoun-brewery/",
    "breweries": [
        "brewery/harviestoun-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A light crisp beer, nice citrus notes. Would have again"
}
---