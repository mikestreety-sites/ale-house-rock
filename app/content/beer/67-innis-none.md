---json
{
    "title": "Innis & None",
    "number": "67",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BZE8vk1lElZ/",
    "date": "2017-09-15",
    "permalink": "beer/innis-none-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [],
    "review": "There has been a plethora of non-alcoholic beers appearing recently, so thought I would give one a go from a brewery I&#39;ve always admired. A bit of an odd after taste, but just drinkable. Took me nearly 2 hours to finish... won&#39;t be buying this one again"
}
---