---json
{
    "title": "Sussex Haze",
    "number": "766",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1245684911",
    "serving": "Can",
    "date": "2023-02-05",
    "permalink": "beer/sussex-haze-360-brewing-company/",
    "breweries": [
        "brewery/360-degree-brewing-company/"
    ],
    "tags": [],
    "review": "A flavourful hazy DDH IPA. Very drinkable and could imagine sitting in a pub with a good few pints of this, putting the world to rights."
}
---