---json
{
    "title": "Brighton Stout",
    "number": "808",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1279314564",
    "serving": "Can",
    "date": "2023-05-29",
    "permalink": "beer/brighton-stout-brighton-bier/",
    "breweries": [
        "brewery/brighton-bier/"
    ],
    "tags": [
        "stout"
    ],
    "review": "A great dessert beer to have after a full-on roast lamb. Smooth, not too thick or.full-on and with subtle hints of coffee. In another time, I would have been disappointed with this, but it did the job it needed to do."
}
---