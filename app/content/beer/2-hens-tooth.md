---json
{
    "title": "Hen's Tooth",
    "number": "2",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BNhnjOygBSY/",
    "serving": "Bottle",
    "date": "2016-12-02",
    "permalink": "beer/hens-tooth-morland/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [
        "strongale"
    ],
    "review": "From the same people as old speckled hen, this bottle conditioned malty Ale is a perfect beer for a night in. Strong flavours, ideal for sipping."
}
---