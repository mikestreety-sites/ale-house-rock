---json
{
    "title": "Ambrata",
    "number": "182",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BrJNlXeAcbw/",
    "date": "2018-12-08",
    "permalink": "beer/ambrata-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [],
    "review": "Another Genoa Ale - very compatible in flavour with the previous one but 7%! Very enjoyable, shame it’s all the way in Italy!"
}
---