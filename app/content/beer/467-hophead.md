---json
{
    "title": "Hophead",
    "number": "467",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CNdYnw3JMXw/",
    "date": "2021-04-09",
    "permalink": "beer/hophead-dark-star/",
    "breweries": [
        "brewery/dark-star/"
    ],
    "tags": [],
    "review": "This is a beer that isn&#39;t amazing, but one I could drink all evening of a session. It&#39;s hoppy and dry, with a classic bitter beer taste. It&#39;s definitely a &quot;drink in the pub&quot; kind of beer as it tastes like a rowdy atmosphere and raised voices."
}
---