---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1425253815",
  "title": "Does It Make A Sound",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-10-13",
  "style": "IPA - New Zealand",
  "abv": "6%",
  "breweries": [
    "brewery/vibrant-forest-brewery/"
  ],
  "number": 981,
  "permalink": "beer/does-it-make-a-sound-vibrant-forest-brewery/",
  "review": "A simple IPA which was drinkable, at least. Nothing outstanding but enjoyable."
}
---
