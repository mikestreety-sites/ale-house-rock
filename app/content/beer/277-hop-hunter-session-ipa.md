---json
{
    "title": "Hop Hunter Session IPA",
    "number": "277",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B8FSmgMJ3tM/",
    "date": "2020-02-02",
    "permalink": "beer/hop-hunter-session-ipa-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [],
    "review": "I was pleasantly surprised by this one. Not holding out much hope, this Lidl own brand beer was impressive. Much more suited to a summer&#39;s day by the BBQ, easy drinking and refreshing. If they are still around in a few months I&#39;ll be stocking my fridge with these."
}
---