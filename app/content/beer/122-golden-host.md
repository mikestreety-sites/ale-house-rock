---json
{
    "title": "Golden Host",
    "number": "122",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Bkn2UduFWxO/",
    "date": "2018-06-29",
    "permalink": "beer/golden-host-jennings/",
    "breweries": [
        "brewery/jennings-brewery/"
    ],
    "tags": [],
    "review": "Although not the best beer, very drinkable."
}
---