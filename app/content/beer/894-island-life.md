---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1350682065",
  "title": "Island Life",
  "serving": "Bottle",
  "rating": 8,
  "date": "2024-01-23",
  "style": "IPA - Session",
  "abv": "3.7%",
  "breweries": [
    "brewery/orkney-brewery/"
  ],
  "number": 894,
  "permalink": "beer/island-life-orkney-brewery/",
  "review": "I was worried when I poured this that it might not have much flavour (based on the colour) but was proved wrong. A lovely, subtle, Session IPA that would feel right at home in a micro pub or ale house."
}
---

