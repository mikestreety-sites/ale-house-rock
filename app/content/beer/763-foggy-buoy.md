---json
{
    "title": "Foggy Buoy",
    "number": "763",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1243353083",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-01-28",
    "permalink": "beer/foggy-buoy-lost-pier-brewing/",
    "breweries": [
        "brewery/lost-pier-brewing/"
    ],
    "tags": [
        "tipa"
    ],
    "review": "Once you got over the initial hit of this 10% monster, it became dangerously drinkable. You soon forgot how impressive the ABV of this beer was and I could have easily sat and drunk a few of these"
}
---