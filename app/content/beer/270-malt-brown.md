---json
{
    "title": "Malt Brown",
    "number": "270",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B65fbCbpkpG/",
    "date": "2020-01-04",
    "permalink": "beer/malt-brown-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "A tasty full-bodied brown ale. Malty, bitter and definitely a sipper. Drank this after dinner (can&#39;t imagine it complimenting many meals - except maybe a roast) while playing cards. Wonderful."
}
---