---json
{
    "title": "Sweet Sussex",
    "number": "292",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B99v8PUJdhg/",
    "date": "2020-03-20",
    "permalink": "beer/sweet-sussex-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Among the chaos it&#39;s still great to be able to sit down with a beer, especially one as pleasant as this one.  A stout, but a nice, easy drink, lovely flavoured one. Not too heavy or laden with coffee notes, this beer was just right 🏅 8/10"
}
---