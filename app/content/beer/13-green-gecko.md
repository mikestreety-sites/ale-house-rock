---json
{
    "title": "Green Gecko",
    "number": "13",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BN5BaTVgPm5/",
    "serving": "Bottle",
    "purchased": "shop/lidl/",
    "date": "2016-12-11",
    "permalink": "beer/green-gecko-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A pleasant beer. Has an odd twang but could drink all evening."
}
---