---json
{
    "title": "Robbie's Red",
    "number": "293",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B-AMnEEpron/",
    "date": "2020-03-21",
    "permalink": "beer/robbies-red-adur-brewery/",
    "breweries": [
        "brewery/adur-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "This beer was a bit of a rollercoaster. First sip was good, then the beer tasted really watery for most of the pint. Trouble was, the last two sips tasted good as well! So &quot;ok&quot; on the whole."
}
---