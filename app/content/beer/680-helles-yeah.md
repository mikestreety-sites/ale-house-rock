---json
{
    "title": "Helles Yeah",
    "number": "680",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CfweENcKiHG/",
    "date": "2022-07-08",
    "permalink": "beer/helles-yeah-lervig/",
    "breweries": [
        "brewery/lervig-aktiebryggeri/"
    ],
    "tags": [],
    "review": "Just a classic Helles lager - nothing really more to say. Unimpressive but drinkable."
}
---