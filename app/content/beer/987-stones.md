---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1431041103",
  "title": "Stones",
  "serving": "Can",
  "rating": 10,
  "purchased": "shop/middle-farm-tea-rooms/",
  "date": "2024-11-03",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/beak-brewery/",
    "brewery/the-seed-a-living-beer-project/"
  ],
  "number": 987,
  "permalink": "beer/stones-beak-the-seed-a-living-beer-project/",
  "review": "This. Was. Incredible. Everything you could ask for from an IPA, it was flavourful, light, balanced but juicy. I had to stop myself from drinking it all in one go. It was so moreish I was disappointed I'd only bought one!"
}
---
