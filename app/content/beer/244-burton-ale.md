---json
{
    "title": "Burton Ale",
    "number": "244",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B3nFX0VlgLp/",
    "date": "2019-10-14",
    "permalink": "beer/burton-ale-saltdean-brewery/",
    "breweries": [
        "brewery/saltdean-brewery/"
    ],
    "tags": [],
    "review": "Finally managed to wrangle another beer from the Saltdean brewery guys. This rich, deep ale was lovely. A great range of flavours but it&#39;s definitely a sipping beer of an evening - wouldn&#39;t want too many of these (nor could I manage it). One while watching Elementary was perfect"
}
---