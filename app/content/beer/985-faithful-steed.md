---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1429868323",
  "title": "Faithful Steed",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-10-31",
  "style": "Pilsner - German",
  "abv": "4.5%",
  "breweries": [
    "brewery/vibrant-forest-brewery/"
  ],
  "number": 985,
  "permalink": "beer/faithful-steed-vibrant-forest-brewery/",
  "review": "For a pilsner this was dang tasty. Crisp and clean and beautifully morish. Was light too, could easily knock a few of these back."
}
---
