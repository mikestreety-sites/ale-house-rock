---json
{
    "title": "Fables",
    "number": "627",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CavWVThKklA/",
    "date": "2022-03-05",
    "permalink": "beer/fables-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "A standard pale ale. Not really up to Beak standards but still tasty."
}
---