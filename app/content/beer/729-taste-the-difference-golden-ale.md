---json
{
    "title": "Taste the Difference Golden Ale",
    "number": "729",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1214640518",
    "serving": "Bottle",
    "purchased": "shop/sainsburys/",
    "date": "2022-10-24",
    "permalink": "beer/taste-the-difference-golden-ale-badger-sainsburys/",
    "breweries": [
        "brewery/badger/",
        "brewery/sainsburys/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A drinkable ale - a bit darker than I was expecting for a Golden beer. Inoffensive and went well with a spagbol dinner"
}
---