---json
{
    "title": "Clockwork Tangerine",
    "number": "230",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/B0_q9A5F6Sm/",
    "date": "2019-08-10",
    "permalink": "beer/clockwork-tangerine-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Sorry for the radio silence, not had the opportunity recently. Kicked it off again with this average beer. Slight citrus notes with a whole lot of nope. Wouldn&#39;t have it again, but managed to finish the bottle. On a positive note (for those that are that way inclined) I learnt that all but a handful of @brewdogofficial beers are Vegan"
}
---