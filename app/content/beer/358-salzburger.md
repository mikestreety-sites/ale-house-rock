---json
{
    "title": "Salzburger",
    "number": "358",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CCttgjRpjS1/",
    "date": "2020-07-16",
    "permalink": "beer/salzburger-steigl/",
    "breweries": [
        "brewery/steigl/"
    ],
    "tags": [],
    "review": "A &quot;tasty&quot; lager. Good depth and flavour but wouldn&#39;t actively buy again!"
}
---