---json
{
    "title": "Hobgoblin Gold",
    "number": "33",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/BQV7wmNAE4z/",
    "serving": "Bottle",
    "date": "2017-02-10",
    "permalink": "beer/hobgoblin-gold-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "An unusually light one from @hobgoblin_beer but still so very tasty. Had to take this quick as you can see it&#39;s already going down well. Not hoppy but light to drink. Give me another!"
}
---