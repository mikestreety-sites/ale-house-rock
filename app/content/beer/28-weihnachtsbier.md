---json
{
    "title": "Schwaben Bräu WeihnachtsBier",
    "number": "28",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BOqBro9AoMT/",
    "serving": "Bottle",
    "date": "2016-12-30",
    "permalink": "beer/weihnachtsbier-schwaben-bauu/",
    "breweries": [
        "brewery/schwaben-bauu/"
    ],
    "tags": [
        "lager"
    ],
    "review": "More flavour than the colour lets on. Tasty and quaffable"
}
---