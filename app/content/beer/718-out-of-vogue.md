---json
{
    "title": "Out of Vogue",
    "number": "718",
    "rating": "6.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1203710389",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-09-20",
    "permalink": "beer/out-of-vogue-burning-sky/",
    "breweries": [
        "brewery/burning-sky/"
    ],
    "tags": [
        "westcoastpale"
    ],
    "review": "I was forced to have this after I dropped it on the garage floor. Hoppy and light, not my favourite beer but passable. Got better as I drink it"
}
---