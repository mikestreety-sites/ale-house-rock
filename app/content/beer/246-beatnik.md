---json
{
    "title": "Beatnik",
    "number": "246",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B35CAVtpVGZ/",
    "date": "2019-10-21",
    "permalink": "beer/beatnik-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "A hoppy brother to the previous beer, but not quite as tasty (although I&#39;m not a fan of hoppy beers anyway) but would drink again"
}
---