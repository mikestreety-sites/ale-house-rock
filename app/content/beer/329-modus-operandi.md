---json
{
    "title": "Modus Operandi",
    "number": "329",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/CATcXI0pp5I/",
    "date": "2020-05-17",
    "permalink": "beer/modus-operandi-the-wild-beer-company/",
    "breweries": [
        "brewery/the-wild-beer-company/"
    ],
    "tags": [],
    "review": "This was disappointing. Started off not realising it was bottle conditioned so ended up with a glass full of sediment (as you can see at the bottom). When taking a sip of this &quot;oak aged&quot; beer, I was assaulted with the abismal taste of red wine, and it stuck. If you are a fan of the red grape juice, I can imagine you rather liking this beer - but I can&#39;t stand the stuff so its a big no from me and the rest ended up down the drain as it was ruining my dinner. It does get an extra point as my wife, who has an adverse dislike for any beer, is contemplating drinking the second bottle I have of this. Sorry @wildbeerco"
}
---