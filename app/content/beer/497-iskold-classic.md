---json
{
    "title": "Iskold Classic",
    "number": "497",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CPVMgQXltnM/",
    "date": "2021-05-26",
    "permalink": "beer/iskold-classic-mikkeller/",
    "breweries": [
        "brewery/mikkeller/"
    ],
    "tags": [],
    "review": "This was not the colour I was expecting this &quot;lager&quot; to be. Decent taste and certainly drinkable, just nothing outstanding"
}
---