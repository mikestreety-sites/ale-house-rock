---json
{
    "title": "One Eye",
    "number": "724",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1211509718",
    "serving": "Can",
    "purchased": "shop/sainsburys/",
    "date": "2022-10-15",
    "permalink": "beer/one-eye-tooth-claw-brewing/",
    "breweries": [
        "brewery/tooth-claw-brewing/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A bit of an anticlimactic beer. The can wrote cheques the beer couldn&#39;t cash. Was drinkable, but won&#39;t be rushing to buy it again"
}
---