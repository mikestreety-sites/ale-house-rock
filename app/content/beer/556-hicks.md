---json
{
    "title": "Hicks",
    "number": "556",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CSucgCvKXHG/",
    "date": "2021-08-18",
    "permalink": "beer/hicks-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [],
    "review": "This was a proper good ale. Bottle conditioned, a strong bitter and full of flavour. Would definitely have one of these again."
}
---