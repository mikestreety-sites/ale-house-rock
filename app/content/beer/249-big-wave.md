---json
{
    "title": "Big Wave",
    "number": "249",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B4JtuGelMC1/",
    "date": "2019-10-28",
    "permalink": "beer/big-wave-kona-brewing-company/",
    "breweries": [
        "brewery/kona-brewing-company/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Light &amp; tasty golden ale. Could easily drink this for hours (although not at the price I had to pay for it). Very drinkable and refreshing without being too hoppy. Lovely"
}
---