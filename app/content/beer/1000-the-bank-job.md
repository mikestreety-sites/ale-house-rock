---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1443088402",
  "title": "The Bank Job",
  "serving": "Can",
  "rating": 9,
  "date": "2024-12-21",
  "style": "Pale Ale - Other",
  "abv": "5.2%",
  "breweries": [
    "brewery/baron-brewing/"
  ],
  "number": 1000,
  "permalink": "beer/the-bank-job-baron-brewing/",
  "review": "This is it, my 1000th beer. This one was bought for me in exchange for servicing my sister's bike. Claiming to be a blend of West and East Coast pale ales which I feel it does perfectly. It's got the first-hand freshness of a West coast with the rounded aftertaste of an East. What a way to celebrate the milestone."
}
---

