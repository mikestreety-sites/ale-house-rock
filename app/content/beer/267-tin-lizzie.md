---json
{
    "title": "Tin Lizzie",
    "number": "267",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B6twl2dpI2Y/",
    "date": "2019-12-30",
    "permalink": "beer/tin-lizzie-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "An excellent Harvey&#39;s beer. After the recent run of 5s, I was dubious about this one, but I was proved wrong. I can see why this was award winning - lovely taste and not too heavy"
}
---