---json
{
    "title": "Bonfire Boy",
    "number": "271",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B69GmKzJZqz/",
    "date": "2020-01-05",
    "permalink": "beer/bonfire-boy-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "The final bottle of this @harveysbrewery exploration. A wonderful Christmas present (all the Harvey&#39;s beers of late) and a pleasent one to end the sprint on. It seems overall the cans faired better than the bottles! This one was indeed smokey (as stated by the bottle) and well balanced. Not too heavy, but plenty of taste. A very good beer and a great way to end the Christmas break"
}
---