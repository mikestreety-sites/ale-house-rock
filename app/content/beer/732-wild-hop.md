---json
{
    "title": "Wild Hop",
    "number": "732",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1217228827",
    "serving": "Can",
    "purchased": "shop/adnams-cellar-and-kitchen-store/",
    "date": "2022-11-02",
    "permalink": "beer/wild-hop-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [
        "redale"
    ],
    "review": "This was a lovely, smooth amber beer with just the right amount of flavour. Not too hoppy and not at all bitter. At a smidge under 5%, I could have had a good few of these in a session"
}
---