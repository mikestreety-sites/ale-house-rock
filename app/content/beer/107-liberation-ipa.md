---json
{
    "title": "Liberation I.P.A.",
    "number": "107",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BhZuLCtl10M/",
    "date": "2018-04-10",
    "permalink": "beer/liberation-ipa-liberation-brewery/",
    "breweries": [
        "brewery/liberation-brewery/"
    ],
    "tags": [
        "ipa",
        "bitter"
    ],
    "review": "A big standard beer. Nothing to rave about, nothing to moan about"
}
---