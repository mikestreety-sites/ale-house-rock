---json
{
    "title": "Audit Ale",
    "number": "48",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BUUM0kulBxv/",
    "serving": "Bottle",
    "purchased": "shop/seven-cellars/",
    "date": "2017-05-20",
    "permalink": "beer/audit-ale-westerham/",
    "breweries": [
        "brewery/westerham-brewery/"
    ],
    "tags": [
        "strongale"
    ],
    "review": "Out of the 3 craft beers I bought, this was my favourite. Drank it out the bottle hence the shot of beer to show the colour. Went down far to quickly and easily for a beer that was 6.8%!"
}
---