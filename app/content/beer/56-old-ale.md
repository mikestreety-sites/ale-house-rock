---json
{
    "title": "Old Ale XXXX",
    "number": "56",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BWi11C8lub6/",
    "serving": "Bottle",
    "date": "2017-07-14",
    "permalink": "beer/old-ale-firebird-brewing-company/",
    "breweries": [
        "brewery/firebird-brewing-company/"
    ],
    "tags": [
        "oldale"
    ],
    "review": "Such an odd choice of beer after sitting in the sun for a couple of hours. Didn&#39;t realise it was a &quot;winter&quot; Ale (I thought the waitress could have warned me!). Tasty nonetheless and went down rather well actually!"
}
---