---json
{
    "title": "DIPA",
    "number": "485",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/COlXvg2p7H5/",
    "date": "2021-05-07",
    "permalink": "beer/dipa-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [],
    "review": "After the last few @unbarredbrewery beers I was excited about this DIPA offering. Unfortunately, it didn&#39;t live up to my expectations. I know this is an odd comment for an 8% beer, but you could taste too much of the alcohol. Strong and unforgiving - I think I&#39;ll leave this one on the shelf next time."
}
---