---json
{
    "title": "Double Hazy",
    "number": "460",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CM-nYIpJDNZ/",
    "date": "2021-03-28",
    "permalink": "beer/double-hazy-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "I&#39;ve finally completed the triple. Finished the Hazy series. The double hopped version of @brewdogofficial&#39;s Hazy Jane (and the little brother of Triple Hazy), this beer has the best of both worlds. The kick that comes with a DIPA without the overbearing of alcohol found in the triple. This beer got better the more I drank it, and the 7.4% was a curve ball that snuck up on me. Would highly recommend trying the original first then moving onto this. Killer beer."
}
---