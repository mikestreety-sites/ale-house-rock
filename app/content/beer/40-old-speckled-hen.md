---json
{
    "title": "Morland Old Speckled Hen",
    "number": "40",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BSmMfxLFTBE/",
    "serving": "Bottle",
    "date": "2017-04-07",
    "permalink": "beer/morland-old-speckled-hen-greene-king/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A good entry-level bitter which fulfils every need for beer. A good all-rounder and Cask Marque&#39;s beer of the week!"
}
---