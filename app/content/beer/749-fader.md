---json
{
    "title": "Fader",
    "number": "749",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1230869288",
    "serving": "Can",
    "date": "2022-12-17",
    "permalink": "beer/fader-laine-brew-co/",
    "breweries": [
        "brewery/laine-brew-co/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Had this in the pub the other week and I can tell you that it&#39;s a lot nicer from a can. A lovely IPA that was smooth and refreshing"
}
---