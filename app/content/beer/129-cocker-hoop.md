---json
{
    "title": "Cocker Hoop",
    "number": "129",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BlRAn0IF1QH/",
    "date": "2018-07-15",
    "permalink": "beer/cocker-hoop-jennings/",
    "breweries": [
        "brewery/jennings-brewery/"
    ],
    "tags": [],
    "review": "A crisp, clean, refreshing beer"
}
---