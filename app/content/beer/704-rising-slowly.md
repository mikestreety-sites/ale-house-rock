---json
{
    "title": "Rising Slowly",
    "number": "704",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1194295414",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-08-22",
    "permalink": "beer/rising-slowly-saint-mars-of-the-desert/",
    "breweries": [
        "brewery/saint-mars-of-the-desert/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "I was disappointed with this beer. I had heard a lot of good things about the brewer, but this didn&#39;t live up to the hype. It was dry and tasted a bit basic. The alcohol taste was a bit too obvious for my liking."
}
---