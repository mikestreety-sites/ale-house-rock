---json
{
    "title": "Arosa",
    "number": "788",
    "rating": "2.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1264581712",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-04-13",
    "permalink": "beer/arosa-track-brewing-company/",
    "breweries": [
        "brewery/track-brewing-company/"
    ],
    "tags": [
        "lager"
    ],
    "review": "Lagers from.craft beer places are normally ok, however, this one was not. Made me squirm with the first sip and left an odd aftertaste"
}
---