---json
{
    "title": "We All Float",
    "number": "764",
    "rating": "3",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1243994875",
    "serving": "Can",
    "date": "2023-01-29",
    "permalink": "beer/we-all-float-cloak-and-dagger/",
    "breweries": [
        "brewery/cloak-and-dagger/"
    ],
    "tags": [
        "stout"
    ],
    "review": "A very disappointing stout. Not much depth and a watery finish"
}
---