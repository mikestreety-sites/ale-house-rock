---json
{
    "title": "Black Ale",
    "number": "319",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B_1dIlOJQ1M/",
    "date": "2020-05-06",
    "permalink": "beer/black-ale-harviestoun-brewery/",
    "breweries": [
        "brewery/harviestoun-brewery/"
    ],
    "tags": [],
    "review": "Forgot to post this from the other night. This beer was perfect after cooking and eating a Sunday roast and sitting down in front of the TV. Not too heavy and filling but was a full-on coffee flavour that took a few sips to get into. One of the better stouts/porters (black ales?!) I&#39;ve had that hit the spot, and a perfect size too. 🏅7/10"
}
---