---json
{
    "title": "Jack Brand India Pale Lager",
    "number": "725",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1211820907",
    "serving": "Can",
    "purchased": "shop/adnams-cellar-and-kitchen-store/",
    "date": "2022-10-15",
    "permalink": "beer/jack-brand-india-pale-lager-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [
        "lager"
    ],
    "review": "It just tasted a bit like a slightly better lager to me"
}
---