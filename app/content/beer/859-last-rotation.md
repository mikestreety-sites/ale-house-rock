---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1322748317",
  "title": "Last Rotation",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-14",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 859,
  "permalink": "beer/last-rotation-overtone-brewing-co/",
  "review": "An above average DIPA, although very similar to the last few Overtone beers I've had. Would drink again."
}
---

