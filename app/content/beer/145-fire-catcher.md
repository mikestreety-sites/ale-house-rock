---json
{
    "title": "Fire Catcher",
    "number": "145",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BmrAzyolWy1/",
    "date": "2018-08-19",
    "permalink": "beer/fire-catcher-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "A tasty golden ale from a birthday box of beer, drunk out of a new jug just for the occasion"
}
---