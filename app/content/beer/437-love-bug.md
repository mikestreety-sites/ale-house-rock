---json
{
    "title": "Love Bug",
    "number": "437",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CLiEqMzlZft/",
    "date": "2021-02-20",
    "permalink": "beer/love-bug-cellar-head-only-with-love/",
    "breweries": [
        "brewery/cellar-head/",
        "brewery/only-with-love/"
    ],
    "tags": [],
    "review": "What a fantastic beer. Darker than I was expecting but full of flavour. Thoroughly enjoyed this and was disappointed when I finished. Don&#39;t often award half points but this gets one 👍. Great work @cellarheadbrew and @onlywith_love and shout out to @palate.bottleshop for stocking."
}
---