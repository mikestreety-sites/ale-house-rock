---json
{
    "title": "9th Overall, 2013",
    "number": "751",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1232459049",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-12-23",
    "permalink": "beer/9th-overall-2013-sureshot-brewing/",
    "breweries": [
        "brewery/sureshot-brewing/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "An IPA with a bit of an odd aftertaste. It got better as I drank it, but there was something there I wasn&#39;t quite sure of"
}
---