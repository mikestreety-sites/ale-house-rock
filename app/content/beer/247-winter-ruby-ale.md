---json
{
    "title": "Winter Ruby Ale",
    "number": "247",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B4GJKIlF7DM/",
    "date": "2019-10-26",
    "permalink": "beer/winter-ruby-ale-co-op/",
    "breweries": [
        "brewery/co-op/"
    ],
    "tags": [],
    "review": "An unremarkable beer that has taken me over a week to post as I wasn&#39;t sure what to put about it. Drinkable, but not memorable"
}
---