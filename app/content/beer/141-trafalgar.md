---json
{
    "title": "Trafalgar",
    "number": "141",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BmWflgqFQcH/",
    "date": "2018-08-11",
    "permalink": "beer/trafalgar-nelson-brewery/",
    "breweries": [
        "brewery/nelson-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A present from life long Ale House Rock fans @geoffst and @mj.street. From the Chatham Historic Dockyard, this bottle conditioned ale is a nice drink. A bit of an odd after taste but the after-after taste Improves"
}
---