---json
{
    "title": "FAITH IN FUTURES // SKGN // DDH IPA",
    "number": "758",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1238435030",
    "serving": "Can",
    "purchased": "shop/tesco/",
    "date": "2023-01-08",
    "permalink": "beer/faith-in-futures-skgn-ddh-ipa-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [
        "ddhipa"
    ],
    "review": "A good, tasty DDH IPA - pretty much what I&#39;ve come to expect from Northern Monk. Would happily buy again"
}
---