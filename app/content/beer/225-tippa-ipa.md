---json
{
    "title": "Tippa IPA",
    "number": "225",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/ByvF1IIFJdE/",
    "date": "2019-06-15",
    "permalink": "beer/tippa-ipa-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Popped over to Genoa to pick up a few beers from Maltus Faber, a fantastic Genovese brewery I was introduced to at Christmas. This one is no exception, great taste and a lovely flavour"
}
---