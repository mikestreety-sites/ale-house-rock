---json
{
    "title": "Hazy Pale Ale",
    "number": "685",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CgPut2uq9PN/",
    "date": "2022-07-20",
    "permalink": "beer/hazy-pale-ale-marks-and-spencer-vocation-brewery/",
    "breweries": [
        "brewery/marks-and-spencer/",
        "brewery/vocation-brewery/"
    ],
    "tags": [],
    "review": "I don&#39;t know what it is about big chains getting involved in brewing but it always makes the beers a bit worse. Not sure if this was a commission or if M&amp;S actually had some say, but this hazy pale was &quot;ok&quot; - drinkable, but not astounding. It kind of seems like a hazy pale for people who like the idea of hazy pales without the punchy, fruity flavours."
}
---