---json
{
    "title": "Wand",
    "number": "760",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1240200272",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-01-15",
    "permalink": "beer/wand-abyss-brewing/",
    "breweries": [
        "brewery/abyss-brewing/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "An absolute fine example of a DIPA. So tasty, thick and full-bodied."
}
---