---json
{
    "title": "Oats of Disobedience",
    "number": "345",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CBdPc7gpAPz/",
    "date": "2020-06-15",
    "permalink": "beer/oats-of-disobedience-people-like-us/",
    "breweries": [
        "brewery/people-like-us/"
    ],
    "tags": [],
    "review": "A creamy, oat stout from @peoplelikeusdk. Went well with a couple of squares of dark chocolate last night. I still don&#39;t think oat stouts are my thing but this was one of the better ones."
}
---