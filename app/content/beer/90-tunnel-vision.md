---json
{
    "title": "Tunnel Vision",
    "number": "90",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BdYhC8GFr3C/",
    "date": "2017-12-31",
    "permalink": "beer/tunnel-vision-steam-box-brewery/",
    "breweries": [
        "brewery/steam-box-brewery/"
    ],
    "tags": [],
    "review": "A simple, slightly watery ale, but alright."
}
---