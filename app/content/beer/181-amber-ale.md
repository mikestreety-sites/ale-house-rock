---json
{
    "title": "Amber Ale",
    "number": "181",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BrIpfKxAf3R/",
    "date": "2018-12-08",
    "permalink": "beer/amber-ale-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [],
    "review": "One of three beers all the way from Genoa. After putting up the Christmas tree, this little wonder (along with matching glass) was a perfect way to finish the day. It’s like a hipster craft ale but tasty"
}
---