---json
{
    "title": "Golden Goal",
    "number": "128",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BlGjHSZFdZQ/",
    "date": "2018-07-11",
    "permalink": "beer/golden-goal-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "just before kick-off I poured this little number and must have bought good luck! It’s half time and at least England are playing better than this tastes. It’s ok but a bit watery"
}
---