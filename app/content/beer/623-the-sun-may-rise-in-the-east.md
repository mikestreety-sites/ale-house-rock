---json
{
    "title": "The Sun May Rise in the East",
    "number": "623",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CaaU8ldKEV4/",
    "date": "2022-02-25",
    "permalink": "beer/the-sun-may-rise-in-the-east-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [],
    "review": "There are some beers that rally taste of the alcohol. This 8% DIPA tasted (and felt) a lot stronger than was written on the can. The underlying flavours were nice, but the alcoholic taste was a bit too overwhelming."
}
---