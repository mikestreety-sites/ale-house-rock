---json
{
    "title": "Bravaria",
    "number": "54",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BWdkPWGFnil/",
    "serving": "Bottle",
    "date": "2017-07-12",
    "permalink": "beer/bravaria-swinkels-family-brewers/",
    "breweries": [
        "brewery/swinkels-family-brewers/"
    ],
    "tags": [
        "wheatbeer"
    ],
    "review": "this non-alcoholic wheat beer (yes, 0%) was gifted to me by my father to try. It&#39;s odd. I&#39;ve been sipping it all night and can&#39;t quite figure it out. It&#39;s nice but not amazing"
}
---