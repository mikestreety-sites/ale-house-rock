---json
{
    "title": "Exit Strategy",
    "number": "588",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CW1RdXIq1kC/",
    "date": "2021-11-28",
    "permalink": "beer/exit-strategy-silver-rocket-brewing/",
    "breweries": [
        "brewery/silver-rocket-brewing/"
    ],
    "tags": [],
    "review": "A good-tasting NEIPA from local Hassocks brewery @silverrocketbrewing. Not quite as thick and fruity as I would expect from a NEIPA, but enjoyable nonetheless. Looking forward to having more of their beers"
}
---