---json
{
    "title": "Steady Rolling Man",
    "number": "251",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B4t6vMmF8lA/",
    "date": "2019-11-11",
    "permalink": "beer/steady-rolling-man-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Going through a bit of a hipster beer thing at the moment and this is no exception. Pouring it out, I thought @laurzstreet had picked up a cider for me instead as it is very apple-juicey in colour. I wasn&#39;t holding out much hope but I was very much proven wrong. An absolutely lovely beer, easy to drink and very moreish. This is definitely one of the best hipster pale ales I&#39;ve had"
}
---