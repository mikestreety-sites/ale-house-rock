---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1366036444",
  "title": "Nashoba",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-23",
  "style": "IPA - New England / Hazy",
  "abv": "6.2%",
  "breweries": [
    "brewery/arbor-ales/"
  ],
  "number": 910,
  "permalink": "beer/nashoba-arbor-ales/",
  "review": "A lovely thick NEIPA with a bonus point for being a full pint. Every time I have an Arbor ale I become more and more of a fan."
}
---

