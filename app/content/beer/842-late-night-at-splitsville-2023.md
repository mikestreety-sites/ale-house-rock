---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1309479558",
  "title": "Late Night At Splitsville (2023)",
  "serving": "Can",
  "rating": 8,
  "date": "2023-08-31",
  "style": "IPA - New England / Hazy",
  "abv": "6.8%",
  "breweries": [
    "brewery/phantom-brewing-co/"
  ],
  "number": 842,
  "permalink": "beer/late-night-at-splitsville-2023-phantom-brewing-co/",
  "review": "This was lush. Thick and flavourful and just the right amount of juicy-ness."
}
---

