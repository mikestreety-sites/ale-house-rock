---json
{
    "title": "Lucha Lucha",
    "number": "558",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CSxfhvAquUM/",
    "date": "2021-08-19",
    "permalink": "beer/lucha-lucha-little-monster-brewing-company/",
    "breweries": [
        "brewery/little-monster-brewing-company/"
    ],
    "tags": [],
    "review": "I&#39;ve seen a lot of good things about @littlemonsterbrew and this one did not disappoint. You could smell the fruity-ness the moment the can popped and you could taste it in this DIPA. Smooth, light and flavourful."
}
---