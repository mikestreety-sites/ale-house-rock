---json
{
    "title": "Kipling",
    "number": "351",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CCMF0BKpzGg/",
    "date": "2020-07-03",
    "permalink": "beer/kipling-thornbridge/",
    "breweries": [
        "brewery/thornbridge/"
    ],
    "tags": [],
    "review": "This was such a forgettable beer. Tasted nice, but was not desirable in anyway. I drank it because it was there, not because I wanted more"
}
---