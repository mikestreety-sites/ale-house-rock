---json
{
    "title": "Betty Stogs",
    "number": "79",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BbFTEXHFazr/",
    "date": "2017-11-04",
    "permalink": "beer/betty-stogs-skinners-brewery/",
    "breweries": [
        "brewery/skinners-brewery/"
    ],
    "tags": [],
    "review": "A malty beer which has slowed my drinking pace but still tasty"
}
---