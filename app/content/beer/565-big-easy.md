---json
{
    "title": "Big Easy",
    "number": "565",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CTrWEylqpH4/",
    "date": "2021-09-11",
    "permalink": "beer/big-easy-burning-sky/",
    "breweries": [
        "brewery/burning-sky/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "This DIPA from @burningskybeer was lush. It was light and flavourful and there was no indication of its 8% strength in its taste. Could imagine sitting in a pub with a few of these, blissfully unaware of its potency. Another fantastic beer, picked up from @thefuss.club"
}
---