---json
{
    "title": "Abbot Reserve",
    "number": "285",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B84uLyzppjt/",
    "date": "2020-02-22",
    "permalink": "beer/abbot-reserve-greene-king/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [],
    "review": "Remeniscent of  the &quot;Old Speckled Hen&quot;, Abbot Reserve is more refined, alcoholic and better tasting than the original Abbot Ale. For general flavour it is an 8, but I fear this 6.5% has gone straight to my head"
}
---