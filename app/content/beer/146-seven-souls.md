---json
{
    "title": "Seven Souls",
    "number": "146",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BmraEGTl6ZO/",
    "date": "2018-08-19",
    "permalink": "beer/seven-souls-sharps/",
    "breweries": [
        "brewery/sharps/"
    ],
    "tags": [
        "stout"
    ],
    "review": "Another birthday beer (there was a lot of birthday beer, and will be a lot of reviews heading this way). Dark, chocolatey but not to my taste. Slow sipper, and better than some but wouldn’t rush to have it again. A heavy drink"
}
---