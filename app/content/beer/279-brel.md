---json
{
    "title": "Brel",
    "number": "279",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B8W3wJSpCX2/",
    "date": "2020-02-09",
    "permalink": "beer/brel-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "A light, crisp Aldi Belgian style IPA. Lovely in flavour with a great aftertaste. Will look out for more of these"
}
---