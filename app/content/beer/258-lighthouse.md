---json
{
    "title": "Lighthouse",
    "number": "258",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B5dT0Wwlw02/",
    "date": "2019-11-29",
    "permalink": "beer/lighthouse-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Crisp, light golden ale. Lovely flavours, not ground breaking, but very pleasant indeed"
}
---