---json
{
    "title": "Pure UBU.",
    "number": "17",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BODVDblA6_l/",
    "serving": "Bottle",
    "date": "2016-12-15",
    "permalink": "beer/pure-ubu-purity-brewing-company/",
    "breweries": [
        "brewery/purity-brewing-company/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A brother of the Mad Goose on day 1. Tastier, more malty &amp; more full bodied. A good beer, would happily drink several."
}
---