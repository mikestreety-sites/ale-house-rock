---json
{
    "title": "Sakura Twilight",
    "number": "692",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CgsDD9YKhpy/",
    "date": "2022-07-31",
    "permalink": "beer/sakura-twilight-wander-beyond-brewing/",
    "breweries": [
        "brewery/wander-beyond-brewing/"
    ],
    "tags": [],
    "review": "What a disappointing beer. This 11% cherry chocolate imperial stout had an unenjoyable first sip which got marginally better, but ultimately ended up discarding the last third. If it had been 330ml, I probably would have finished it, but this was too much for me  There was a brief period where I thought it was &quot;alright&quot;"
}
---