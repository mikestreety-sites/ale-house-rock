---json
{
    "title": "Georgian Dragon",
    "number": "264",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B6llG8XJWIZ/",
    "date": "2019-12-27",
    "permalink": "beer/georgian-dragon-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "At the in-laws for Christmas dinner and this beer accompanied it perfectly. The right amount of bitter (although couldn&#39;t taste the fruitcake) and a lovely ruby beer taste. This might be the Christmas cheer &amp; food talking but was an excellent beer"
}
---