---json
{
    "title": "The Wicked Wyvern",
    "number": "660",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CeE7ORWqMxL/",
    "date": "2022-05-27",
    "permalink": "beer/the-wicked-wyvern-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [],
    "review": "This was a lovely light, crisp, bitter that could have been drunk very quickly. It was exceptionally morish and reminded me of the @badgerbeers brewery I fell in love with."
}
---