---json
{
    "title": "Strange Seas",
    "number": "379",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CHQnnTtpKpw/",
    "date": "2020-11-06",
    "permalink": "beer/strange-seas-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "This collaboration with Beavertown was plain. A sub-par IPA really - I expected a better beer from these two breweries. Like a combo DVD/Video player - It doesn&#39;t make it twice as good."
}
---