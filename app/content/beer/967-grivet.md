---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1389940987",
  "title": "GRIVET",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-06-15",
  "style": "Farmhouse Ale - Saison",
  "abv": "6%",
  "breweries": [
    "brewery/missing-link-brewing/"
  ],
  "number": 967,
  "permalink": "beer/grivet-missing-link-brewing/",
  "review": "For a Saison, this was surprisingly smooth. It was a clean, crisp beer which was a nice sipper of an evening."
}
---

