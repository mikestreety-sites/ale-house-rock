---json
{
    "title": "Psychedelic IPA",
    "number": "306",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B_FicJdpZjB/",
    "date": "2020-04-17",
    "permalink": "beer/psychedelic-ipa-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "What a wonderful and light beer. It might have been the sunshine and sitting in the garden that tainted the taste of this bad boy, but thoroughly enjoyed this pint - even when I came inside! One of my favourite of all the @loudshirtbeer beers I&#39;ve had to date"
}
---