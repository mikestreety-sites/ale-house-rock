---json
{
    "title": "Session IPA",
    "number": "496",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CPQesgOFEEx/",
    "date": "2021-05-24",
    "permalink": "beer/session-ipa-toast/",
    "breweries": [
        "brewery/toast-ale/"
    ],
    "tags": [],
    "review": "I don&#39;t normally go for &quot;Session IPAs&quot; as can be a bit bland. This ale from @toastale, however, was full of flavour and delightfully morish. It was a shame to see the end of it. Toast Ales are made from surplus bread and all their profits go to charity - so not only did this beer taste great, I felt morally better by drinking it. A little present from my mum"
}
---