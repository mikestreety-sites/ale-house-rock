---json
{
    "title": "Now We're Here",
    "number": "787",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1263828448",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-04-09",
    "permalink": "beer/now-were-here-other-half-brewing-co-track-brewing-company/",
    "breweries": [
        "brewery/other-half-brewing-co/",
        "brewery/track-brewing-company/"
    ],
    "tags": [],
    "review": "I&#39;ve had my eye on a Track Gold Top for a while now as they always get good reviews. Sitting down after a 2yo birthday party, this hit the spot. I wouldn&#39;t have it again, but it was thick and tasty and juicy and alcoholic and a whole evening of a drink"
}
---
