---json
{
    "title": "The Hop Foundry - Passion Fruit Pale Ale",
    "number": "795",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1272958713",
    "serving": "Can",
    "purchased": "shop/aldi/",
    "date": "2023-05-10",
    "permalink": "beer/the-hop-foundry-passion-fruit-pale-ale-aldi-seven-bro7hers-brewery/",
    "breweries": [
        "brewery/aldi/",
        "brewery/seven-bro7hers-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I can&#39;t work out if this is the same beer as the one brewed just by 7 brothers. Very fruity, enjoyable enough to drink again but wouldn&#39;t go out of my way to buy it"
}
---