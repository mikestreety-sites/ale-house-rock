---json
{
    "title": "Ghost Dance",
    "number": "221",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Bwxc16Xl6E7/",
    "date": "2019-04-27",
    "permalink": "beer/ghost-dance-redwell-brewing-company/",
    "breweries": [
        "brewery/redwell-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "went down without incident. Nothing to report really. Not horrible, not staggeringly tasty"
}
---