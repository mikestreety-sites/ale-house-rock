---json
{
    "title": "Anti-Establishment Hazy IPA",
    "number": "777",
    "rating": "8.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1254961008",
    "serving": "Can",
    "purchased": "shop/aldi/",
    "date": "2023-03-11",
    "permalink": "beer/anti-establishment-hazy-ipa-aldi/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "hazy"
    ],
    "review": "This was actually pretty darn good. For an Aldi beer, it was flavourful and easy-to-drink (and very reasonably priced too). Definitely picking a few more of these up next time I&#39;m in the shop"
}
---