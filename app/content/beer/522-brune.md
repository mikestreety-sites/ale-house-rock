---json
{
    "title": "Brune",
    "number": "522",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CQtuhepJANf/",
    "date": "2021-06-29",
    "permalink": "beer/brune-st-feuillien/",
    "breweries": [
        "brewery/st-feuillien/"
    ],
    "tags": [],
    "review": "A very tasty Brune beer. Drank half of this before realising it packed a punch at 8.5%. Despite that, it was very smooth drinking. Sponsored by @geoffst"
}
---