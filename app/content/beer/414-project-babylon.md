---json
{
    "title": "Project Babylon",
    "number": "414",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CKC1G7ZlyvL/",
    "date": "2021-01-14",
    "permalink": "beer/project-babylon-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "Last of the @gunbrewery Christmas beers from Mrs Ale House. Orange hints with a bog standard IPA. This is a Rod Stewart of the Gun Brewery range. Inoffensive but nothing spectacular"
}
---