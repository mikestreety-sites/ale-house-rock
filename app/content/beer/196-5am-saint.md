---json
{
    "title": "5am Saint",
    "number": "196",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BsgXM_tgtMr/",
    "date": "2019-01-11",
    "permalink": "beer/5am-saint-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "aliases": [
        "5am-saint-brewdog-197"
    ],
    "review": "At last! A @brewdogofficial beer with some good taste. Enjoyed this one and would consider picking it up for a “party beer”. Not one I would have at home again though."
}
---
