---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1288155582",
  "title": "Let's Go Somewhere There's Cheese",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sureshot-brewing/",
  "date": "2023-06-26",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 817,
  "permalink": "beer/lets-go-somewhere-theres-cheese-sureshot-brewing/",
  "review": "Man, Sureshot keep absolutely smashing it out of the park. Another banging beer which I couldn't stop drinking and just makes me want to move to Manchester. This somehow was thick and juicy while being able to drink this over and over again."
}
---
