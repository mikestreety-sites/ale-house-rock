---json
{
    "title": "The 1079 Project",
    "number": "115",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BkGQMRWlgrt/",
    "date": "2018-06-16",
    "permalink": "beer/the-1079-project-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [
        "pilsner"
    ],
    "review": "I bought a 4 pack from Aldi of this pilsner. Perfect with a BBQ or on a hot summers day - neither of which I have - so post DIY will have to do. Crisp and refreshing although not an ale"
}
---