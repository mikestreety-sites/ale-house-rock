---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1292898058",
  "title": "You Look Like Brothers",
  "serving": "Can",
  "rating": 7.5,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-07-11",
  "style": "IPA - New England / Hazy",
  "abv": "5.9%",
  "breweries": [
    "brewery/bestens-brewery/"
  ],
  "number": 823,
  "permalink": "beer/you-look-like-brothers-bestens-brewery/",
  "review": "A fairly standard NEIPA, tasty enough. This would sit very nicely in a micropub, chatting and putting the world to rights."
}
---
