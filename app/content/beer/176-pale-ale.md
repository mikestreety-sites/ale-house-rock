---json
{
    "title": "Pale Ale",
    "number": "176",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BqauYMwAp_G/",
    "date": "2018-11-20",
    "permalink": "beer/pale-ale-little-creatures/",
    "breweries": [
        "brewery/little-creatures/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": ""
}
---