---json
{
    "title": "Market Porter",
    "number": "353",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CCOluH3pwV1/",
    "date": "2020-07-04",
    "permalink": "beer/market-porter-thornbridge/",
    "breweries": [
        "brewery/thornbridge/"
    ],
    "tags": [
        "porter"
    ],
    "review": "One of the best porters I&#39;ve had. Strong coffee flavour which didn&#39;t ruin the taste. Easily sat and drank this one (normally have to slog my way through a porter) and was slightly disappointed when I finished it. Good work @thornbridge"
}
---