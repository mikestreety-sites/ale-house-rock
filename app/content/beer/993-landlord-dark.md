---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1436896420",
  "title": "Landlord Dark",
  "serving": "Bottle",
  "rating": 8,
  "date": "2024-11-28",
  "style": "Dark Ale",
  "abv": "4.3%",
  "breweries": [
    "brewery/timothy-taylors-brewery/"
  ],
  "number": 993,
  "permalink": "beer/landlord-dark-timothy-taylors-brewery/",
  "review": "A nice take on the classic landlord. Dark and smooth, it was tasty enough without being overly complex. Went down very well while preparing the Sunday roast."
}
---
