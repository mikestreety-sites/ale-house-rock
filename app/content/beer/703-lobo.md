---json
{
    "title": "Lobo",
    "number": "703",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1193991830",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-08-21",
    "permalink": "beer/lobo-cloak-and-dagger-hand-brew-co/",
    "breweries": [
        "brewery/cloak-and-dagger/",
        "brewery/hand-brew-co/"
    ],
    "tags": [
        "hazy"
    ],
    "review": "Had this in a pub and had to find it in a can. Just a juicy full-bodied hazy. A bit too strong for a proper session but could smash a few of these in an evening. A fantastic beer"
}
---