---json
{
    "title": "Eight",
    "number": "780",
    "rating": "3",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1259572980",
    "serving": "Can",
    "date": "2023-03-26",
    "permalink": "beer/eight-phantom-brewing-co/",
    "breweries": [
        "brewery/phantom-brewing-co/"
    ],
    "tags": [
        "lager"
    ],
    "review": "Not really a fan of this - a bit bland"
}
---