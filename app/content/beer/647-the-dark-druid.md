---json
{
    "title": "The Dark Druid",
    "number": "647",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CdMUChQqTqu/",
    "date": "2022-05-05",
    "permalink": "beer/the-dark-druid-the-white-hag/",
    "breweries": [
        "brewery/the-white-hag/"
    ],
    "tags": [],
    "review": "Apparently a Mexican hot chocolate pastry stout - whatever that is. This was, however, a nice smooth stout. Possibly one of the smoothest and pastry stouts I&#39;ve had without being overly sweet."
}
---