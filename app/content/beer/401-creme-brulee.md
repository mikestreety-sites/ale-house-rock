---json
{
    "title": "Crème Brûlée",
    "number": "401",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CJWEBovFEt6/",
    "date": "2020-12-28",
    "permalink": "beer/creme-brulee-dark-star/",
    "breweries": [
        "brewery/dark-star/"
    ],
    "tags": [],
    "review": "One of the great things about running a beer review Instagram is that people buy you new beers. I prefer a stout after dinner, and this was a perfect dessert. Had a sweet aftertaste which wasn&#39;t too sickly nor was it too heavy. The drinkability of this beer was very moreish and, although I don&#39;t often buy stouts, I will keep an eye out for this beauty. Good work @darkstarbrewco"
}
---