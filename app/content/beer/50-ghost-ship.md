---json
{
    "title": "Ghost Ship",
    "number": "50",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BUpVBfHl-VX/",
    "serving": "Bottle",
    "date": "2017-05-28",
    "permalink": "beer/ghost-ship-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "a light tasty refreshing beer. Perfect for sitting on a campsite with the wife and in-laws"
}
---