---json
{
    "title": "Tart",
    "number": "152",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/BnUFAgNlD8_/",
    "date": "2018-09-04",
    "permalink": "beer/tart-thornbridge/",
    "breweries": [
        "brewery/thornbridge/"
    ],
    "tags": [],
    "review": "Wow. This. Is. Tart. I was not expecting the sharp citrus, cider-like taste. It. Is. Foul. Two sips and I’m out. We’ve actually managed to find a beer my wife would drink though. And that’s because it’s not a beer at all. although I’ve had to pour it away it gets an extra point for someone in this house declaring it drinkable."
}
---