---json
{
    "title": "Porter",
    "number": "299",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B-iQKxHJD8M/",
    "date": "2020-04-03",
    "permalink": "beer/porter-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "I purposely left this @harveysbrewery beer for the big 300 as they seem to have dominated the tail-end of this century. This porter was pleasant, but not outstanding. Nice and light, for a porter and subtle flavours."
}
---