---json
{
    "title": "Two Hoots",
    "number": "60",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BYGAvxFF4pj/",
    "serving": "Bottle",
    "date": "2017-08-22",
    "permalink": "beer/two-hoots-joseph-holt/",
    "breweries": [
        "brewery/joseph-holt/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A light beer for the beautiful weather, going down far too quickly. After the Hall and Woodhouse brewery tour today it&#39;s only fitting to have a beer. Although the badger ones were not cold enough to drink"
}
---