---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1374508052",
  "title": "Gran Riserva Doppio Malto",
  "serving": "Bottle",
  "rating": 5,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-22",
  "style": "Bock - Hell / Maibock / Lentebock",
  "abv": "6.6%",
  "breweries": [
    "brewery/birra-peroni/"
  ],
  "number": 919,
  "permalink": "beer/gran-riserva-doppio-malto-birra-peroni/",
  "review": "This definitely tastes better in a restaurant than it does at home. Had this in a Prezzo a few weeks ago and it tasted great - it's definitely not a home sipper beer."
}
---

