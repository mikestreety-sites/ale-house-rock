---json
{
    "title": "Old Thumper",
    "number": "15",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BN-IuW7gBsv/",
    "serving": "Bottle",
    "date": "2016-12-13",
    "permalink": "beer/old-thumper-ringwood-brewery/",
    "breweries": [
        "brewery/ringwood-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Another classic from Ringwood. Darker and fuller than the Boon, but not quite as tasty."
}
---