---json
{
    "title": "Native Place",
    "number": "673",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Cetqqp8qfrw/",
    "date": "2022-06-12",
    "permalink": "beer/native-place-boxcar/",
    "breweries": [
        "brewery/boxcar/"
    ],
    "tags": [],
    "review": "Its hard to write about a &quot;middle of the road beer&quot;, especially when it&#39;s a pale. Had this the other day and while I was drinking it, all I could think is: &quot;That&#39;s a 6&quot;."
}
---