---json
{
    "title": "Easter Ale",
    "number": "653",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CdidoRSq7sZ/",
    "date": "2022-05-14",
    "permalink": "beer/easter-ale-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "It always takes me a while to get back into a pint of bitter, but at nearly 7%, this beer from @harveysbrewery packed a punch. Quite rich in flavour but light in drinking, not my favourite of theirs but still a good one"
}
---