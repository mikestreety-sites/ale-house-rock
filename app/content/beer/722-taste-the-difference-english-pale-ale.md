---json
{
    "title": "Taste the Difference English Pale Ale",
    "number": "722",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1208797943",
    "serving": "Bottle",
    "purchased": "shop/sainsburys/",
    "date": "2022-10-07",
    "permalink": "beer/taste-the-difference-english-pale-ale-sainsburys-shepherd-neame/",
    "breweries": [
        "brewery/sainsburys/",
        "brewery/shepherd-neame/"
    ],
    "tags": [],
    "review": "A bit of a bland, uninteresting ale"
}
---