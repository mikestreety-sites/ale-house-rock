---json
{
    "title": "Goliath",
    "number": "153",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BnZY9A-FePQ/",
    "date": "2018-09-06",
    "permalink": "beer/goliath-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "what a nicely balanced crisp beer. Very drinkable"
}
---