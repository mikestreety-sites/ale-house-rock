---json
{
    "title": "Eternal",
    "number": "363",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CDKRE3CpnRq/",
    "date": "2020-07-27",
    "permalink": "beer/eternal-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "Another absolute corker from the @northernmonk team. Smashing it out of the park. Once my fridge is bordering on empty again I&#39;ll order some more - with a matching glass too. Fantastic flavour and horrifically moreish. Shame it was my last one!"
}
---