---json
{
    "title": "Dream Sequence",
    "number": "339",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CBDCbY8pZIu/",
    "date": "2020-06-05",
    "permalink": "beer/dream-sequence-yeastie-boys/",
    "breweries": [
        "brewery/yeastie-boys/"
    ],
    "tags": [],
    "review": "Excellent brewery name @yeastieboysaus! This American brown ale had notes of chocolate and orange. It was very smooth and went down well. Definitely an evening sipper. Might consider picking a couple of these up if I see them as an evening long drink."
}
---