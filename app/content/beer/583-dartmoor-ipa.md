---json
{
    "title": "Dartmoor IPA",
    "number": "583",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CU0bdYCK2uh/",
    "date": "2021-10-09",
    "permalink": "beer/dartmoor-ipa-dartmoor-brewery/",
    "breweries": [
        "brewery/dartmoor-brewery/"
    ],
    "tags": [],
    "review": "A disappointing pale ale. It didn&#39;t quite have the light and bitter taste I&#39;ve come to expect from a bottled IPA brewed in these parts."
}
---