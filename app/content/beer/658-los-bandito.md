---json
{
    "title": "Los Bandito",
    "number": "658",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/Cd8oCtHq0BX/",
    "date": "2022-05-24",
    "permalink": "beer/los-bandito-play-brew-co/",
    "breweries": [
        "brewery/play-brew-co/"
    ],
    "tags": [],
    "review": "What an absolute banger of a beer to have straight off the back of a stag do. Thick, fruity and full-bodied without being a silly ABV. After a weekend of lagers and pale ales, this DDH Pale ale from @playbrewco hit the spot."
}
---