---json
{
    "title": "Red IPA",
    "number": "408",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CJy2a1DF3ll/",
    "date": "2021-01-08",
    "permalink": "beer/red-ipa-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "This red IPA was full bodied and had a lot more taste than the API before it. A good beer which is more suited at home, that for sure. Enjoyed while putting the world to rights over a video call."
}
---