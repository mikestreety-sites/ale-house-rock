---json
{
    "title": "The Governor",
    "number": "125",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BkyRBvFFyOq/",
    "date": "2018-07-03",
    "permalink": "beer/the-governor-marco-pierre-white/",
    "breweries": [
        "brewery/jw-lees-and-co/"
    ],
    "tags": [],
    "review": "A bit watery and tasteless"
}
---