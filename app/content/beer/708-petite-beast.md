---json
{
    "title": "Petite Beast",
    "number": "708",
    "rating": "5.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1197379177",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-09-01",
    "permalink": "beer/petite-beast-cloak-and-dagger/",
    "breweries": [
        "brewery/cloak-and-dagger/"
    ],
    "tags": [
        "tablebeer"
    ],
    "review": "A good colour and a nice, basic flavour but you could tell it was a low ABV. It tasted like I imagine all pale beers to taste - nice enough but just missing something"
}
---