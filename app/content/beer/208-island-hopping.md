---json
{
    "title": "Island Hopping",
    "number": "208",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BvAMVqYlrNs/",
    "date": "2019-03-14",
    "permalink": "beer/island-hopping-swannay-brewery/",
    "breweries": [
        "brewery/swannay-brewery/"
    ],
    "tags": [],
    "review": "Tasty session ale but would leave it there. A bit too hoppy for home sipping but would be wonderful and right at home in a small, independent drinking establishment"
}
---