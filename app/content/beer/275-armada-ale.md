---json
{
    "title": "Armada Ale",
    "number": "275",
    "rating": "10",
    "canonical": "https://www.instagram.com/p/B72u0j2JWr3/",
    "date": "2020-01-28",
    "permalink": "beer/armada-ale-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Now this is a beer that plenty of breweries can learn from. I&#39;ve always been a fan of Armada Ale from @harveysbrewery and drinking this reaffirmed why. Lovely crisp golden ale with a fantastic taste and a morish flavour. I only wished I could afford to drink this all the time!"
}
---