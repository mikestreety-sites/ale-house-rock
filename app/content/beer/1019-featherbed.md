---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1459982318",
  "title": "Featherbed",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/brewser-online-store/",
  "date": "2025-03-01",
  "style": "IPA - New England / Hazy",
  "abv": "4.7%",
  "breweries": [
    "brewery/buxton-brewery/"
  ],
  "number": 1019,
  "permalink": "beer/featherbed-buxton-brewery/",
  "review": "This is a tricky one because, as a beer, this hit all the spots. However, as a NEIPA, it missed the mark a little. It didn't have all the fluffiness, fullness and fruitiness you expect from a new England. But as an IPA it stands up bang full of flavour"
}
---

