---json
{
    "title": "Southwold",
    "number": "84",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BcNm0oSFRJ4/",
    "date": "2017-12-02",
    "permalink": "beer/southwold-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "In the pub, this Adnams beer had an odd aftertaste. Turns out it’s “distinctively hoppy”"
}
---