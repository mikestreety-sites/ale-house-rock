---json
{
    "title": "Abdominal Stoneman",
    "number": "27",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BOkjBXXgBf5/",
    "serving": "Bottle",
    "date": "2016-12-28",
    "permalink": "beer/abdominal-stoneman-lymestone-brewery/",
    "breweries": [
        "brewery/lymestone-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "The #aleadvent is over, but I&#39;m going to keep track of some of the beers I drink from time to time. This 7% (!) beer is a strong full bodied Ale. Small brewery from Staffordshire - a Christmas present! I&#39;m off to watch Trainspotting with it and relate to the &quot;hit&quot; they keep mentioning"
}
---