---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1318167699",
  "title": "Lost In Mosaic",
  "serving": "Can",
  "rating": 5.5,
  "purchased": "shop/lidl/",
  "date": "2023-09-29",
  "style": "IPA - American",
  "abv": "5%",
  "breweries": [
    "brewery/loch-lomond-brewery/"
  ],
  "number": 851,
  "permalink": "beer/lost-in-mosaic-loch-lomond-brewery/",
  "review": "This is going to sound odd, but I found this beer a bit \"wet\". It tasted like a good beer watered down, so the flavours weren't very strong. Not one for me, really."
}
---

