---json
{
    "title": "Raspberry Royale Pale Ale",
    "number": "689",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Cgh9Dl8K7YE/",
    "date": "2022-07-27",
    "permalink": "beer/raspberry-royale-pale-ale-marks-and-spencer-arbor/",
    "breweries": [
        "brewery/arbor-ales/",
        "brewery/marks-and-spencer/"
    ],
    "tags": [],
    "review": "Now this was an experience. A raspberry &quot;pudding&quot; ale from a mainstream supermarket. It didn&#39;t really taste like beer at all and is definitely one you would want after dinner. It was light, bordering on a sour-style, but it was sweet. I quite enjoyed it, I think, but it wouldn&#39;t be at the top of my list to rush out and buy again. My wife enjoyed it (and went back for a second swig when trying) and she doesn&#39;t even like beer. I really struggled to score this one!"
}
---