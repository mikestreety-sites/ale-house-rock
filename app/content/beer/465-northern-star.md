---json
{
    "title": "Northern Star",
    "number": "465",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CNTMSwxpKNA/",
    "date": "2021-04-05",
    "permalink": "beer/northern-star-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "This porter was slightly disappointing and quite forgettable. It was too sweet to be a classic chocolate and/or coffee porter of breweries old, but not sweet enough to be a dessert beer like the recent Brewdog one. Drinkable, but I won&#39;t be going back for another one."
}
---