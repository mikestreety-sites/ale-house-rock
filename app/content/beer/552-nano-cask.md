---json
{
    "title": "Nano Cask",
    "number": "552",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CSfXQAuK5bd/",
    "date": "2021-08-12",
    "permalink": "beer/nano-cask-moor-beer-company/",
    "breweries": [
        "brewery/moor-beer-company/"
    ],
    "tags": [],
    "review": "Another Flavourly beer, another sub-par drink. Lifeless and simple. Nothing to write home about but at least it was a beer"
}
---