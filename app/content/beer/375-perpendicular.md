---json
{
    "title": "Perpendicular",
    "number": "375",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CG3RQrZJshJ/",
    "date": "2020-10-27",
    "permalink": "beer/perpendicular-briarbank-brewery/",
    "breweries": [
        "brewery/briarbank-brewery/"
    ],
    "tags": [],
    "review": "A holiday away in Suffolk calls for a sample of the local beers. Drove to, and waited outside, the @briarbankbrewery bar to open - only to find on their website they are closed at the moment. With a heavy heart we headed back to the car. On the way, we stumbled upon @hopstersipswich which fortunately stocked the beer (plus a few others coming soon). This beer is a classic British bitter. Was lovely to taste this bottle conditioned golden goodness. Worth waiting for"
}
---