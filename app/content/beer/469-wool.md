---json
{
    "title": "Wool",
    "number": "469",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CNn1Aw9FD17/",
    "date": "2021-04-13",
    "permalink": "beer/wool-beak-brewery-northern-monk/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/northern-monk/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "When I saw this collaboration between @thebeakbrewery and @northcraftbeer announced, I was very excited. Two of my favourite recently discovered breweries teaming up to make one of my favourite recently discovered types of beer? It was a definite must-buy, and what a great beer it turned out to be. Picked this up from @palate.bottleshop a few weeks ago as I saving it for something special. It&#39;s a lovely beer, but I feel like the price of it impacted on my enjoyment a bit. Despite this, I did still very much enjoy it. Full, thick, slightly fruity and hella strong. It&#39;s one of those beers that I&#39;m glad I have tried it, but it won&#39;t be a regular in my basket."
}
---