---json
{
    "title": "Greenwich Black IPA",
    "number": "11",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BN2V3v4g6q2/",
    "serving": "Bottle",
    "purchased": "shop/marks-and-spencer/",
    "date": "2016-12-10",
    "permalink": "beer/greenwich-black-ipa-meantime-brewing-company-marks-and-spencer/",
    "breweries": [
        "brewery/marks-and-spencer/",
        "brewery/meantime-brewing-company/"
    ],
    "tags": [
        "blackipa"
    ],
    "review": "Unfortunately in a plastic glass 😟 but a tasty beer. It does indeed taste like IPA and is indeed black. Ok as a beer I suppose!"
}
---