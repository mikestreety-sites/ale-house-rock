---json
{
    "title": "Chuggernaut",
    "number": "383",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CHdndspFR0c/",
    "date": "2020-11-11",
    "permalink": "beer/chuggernaut-amundsen-brewery/",
    "breweries": [
        "brewery/amundsen-brewery/"
    ],
    "tags": [],
    "review": "Hazy session IPA cracked open to put a drop in the Toad in the Hole this evening. Nothing special, tasted like I should be in a trendybar with some &quot;bros&quot; talking loudly nearby"
}
---