---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1344093338",
  "title": "Export India Porter",
  "serving": "Bottle",
  "rating": 7,
  "purchased": "shop/beer-no-evil/",
  "date": "2023-12-28",
  "style": "Porter - Other",
  "abv": "6.5%",
  "breweries": [
    "brewery/the-kernel-brewery/"
  ],
  "number": 882,
  "permalink": "beer/export-india-porter-the-kernel-brewery/",
  "review": "This was everything I would from a porter. It was dark, chocolatey, coffee-y. It did taste a little burnt for my liking. I enjoyed it, but wasn't over the moon about it. I'm still on the fence about The Kernel."
}
---
