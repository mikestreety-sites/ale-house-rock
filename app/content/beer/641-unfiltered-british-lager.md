---json
{
    "title": "Unfiltered British Lager",
    "number": "641",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CcbHBaKKUMw/",
    "date": "2022-04-16",
    "permalink": "beer/unfiltered-british-lager-utopia-brewing/",
    "breweries": [
        "brewery/utopian-brewing-ltd/"
    ],
    "tags": [],
    "review": "This was a tasty lager (don&#39;t often say that). I enjoyed every sip of this.  It has the lightness of lager with the taste and body of an IPA."
}
---
