---json
{
    "title": "Model Citizen",
    "number": "453",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CMncPdaptD1/",
    "date": "2021-03-19",
    "permalink": "beer/model-citizen-brewdog-salt-beer-factory/",
    "breweries": [
        "brewery/brewdog/",
        "brewery/salt-beer-factory/"
    ],
    "tags": [],
    "review": "@brewdog Vs @saltbrewing - this tasty NEIPA went down well with my pizza. Slightly thicker than your average IPA which made it a slow sipper - which is just what I was after. Nice taste too"
}
---