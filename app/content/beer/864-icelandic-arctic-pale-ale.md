---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1326222019",
  "title": "Icelandic Arctic Pale Ale",
  "serving": "Can",
  "rating": 6,
  "date": "2023-10-27",
  "style": "Pale Ale - English",
  "abv": "5.6%",
  "breweries": [
    "brewery/einstok-olger/"
  ],
  "number": 864,
  "permalink": "beer/icelandic-arctic-pale-ale-einstok-olger/",
  "review": "This was nice enough. Certainly ticked the box for a post-gym Friday night tipple. Reminded me of a \"good\" Aldi ale, good enough but wouldn't rush out to buy more"
}
---

