---json
{
    "title": "Milson",
    "number": "816",
    "rating": "10",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1287863010",
    "serving": "Can",
    "purchased": "shop/sureshot-brewing/",
    "date": "2023-06-25",
    "permalink": "beer/milson-sureshot-brewing/",
    "breweries": [
        "brewery/sureshot-brewing/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "This was an incredible Pale Ale and should be what all Pale Ales strive to be. It tasted like a DIPA; it was thick and flavourful - I could drink it all evening (and went down very quickly). This is the best Pale Ale I&#39;ve had."
}
---