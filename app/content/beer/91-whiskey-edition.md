---json
{
    "title": "McEwan's Whiskey Edition",
    "number": "91",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BdYvwZMFY-3/",
    "date": "2018-01-01",
    "permalink": "beer/whiskey-edition-mcewans/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [],
    "review": "An odd combination of beer and whiskey- what a way to see in the new year. Strong, dark and whiskey undertones"
}
---