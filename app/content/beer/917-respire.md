---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1373150440",
  "title": "Respire",
  "serving": "Bottle",
  "rating": 8,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-18",
  "style": "IPA - Session",
  "abv": "4%",
  "breweries": [
    "brewery/black-sheep-brewery/"
  ],
  "number": 917,
  "permalink": "beer/respire-black-sheep/",
  "review": "A lovely light, fruity bitter. Reminiscent of a Golden Glory from old Badger days."
}
---
