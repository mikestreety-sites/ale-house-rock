---json
{
    "title": "Low Voltage",
    "number": "603",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CYkTyXCKVfo/",
    "date": "2022-01-10",
    "permalink": "beer/low-voltage-brixton-brewery/",
    "breweries": [
        "brewery/brixton-brewery/"
    ],
    "tags": [],
    "review": "I always struggle to write about these &quot;middle of the road&quot; session IPAs. Enough flavour to keep you drinking but nothing groundbreaking."
}
---