---json
{
    "title": "Medusa",
    "number": "100",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bf31vvslasu/",
    "date": "2018-03-03",
    "permalink": "beer/medusa-harpers-brewing-company/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "rubyale"
    ],
    "review": "What a fine beer for the century. An Aldi special, this ruby beer is perfect for a windy, rainy evening while watching Game of Thrones. My wife informs me Medusa had two sisters and was beautiful until she was cursed with ugliness for being a bitch. Who knew? 🤷‍♀️"
}
---