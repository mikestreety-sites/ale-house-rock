---json
{
    "title": "Double Hopped Citra IPA",
    "number": "252",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B41o_g8FSTX/",
    "date": "2019-11-14",
    "permalink": "beer/double-hopped-citra-ipa-oakham-ales/",
    "breweries": [
        "brewery/oakham-ales/"
    ],
    "tags": [],
    "review": "One of the longer named beers I&#39;ve had. From M&amp;S, this was a reasonably priced good beer. Taste was nice, went down well, but nothing that blew me away."
}
---