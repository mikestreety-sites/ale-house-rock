---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1360508904",
  "title": "Creeks",
  "serving": "Can",
  "rating": 7.5,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-03-02",
  "style": "IPA - American",
  "abv": "6.3%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 902,
  "permalink": "beer/creeks-beak/",
  "review": "I wasn't a fan of this at first. Quite \"dry\" and very happy. However, while eating a burger this beer came into its own. It became a crisp, light, refreshing tipple of joy."
}
---
