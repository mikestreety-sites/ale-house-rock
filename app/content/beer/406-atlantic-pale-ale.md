---json
{
    "title": "Atlantic Pale Ale",
    "number": "406",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CJp9j2RFthH/",
    "date": "2021-01-05",
    "permalink": "beer/atlantic-pale-ale-sharps/",
    "breweries": [
        "brewery/sharps/"
    ],
    "tags": [],
    "review": "I feel like Sharps are one of those bands that do plenty of excellent songs but are only known for their most popular hit (which in this case is Doombar). It&#39;s a shame as they produce some very tasty beers indeed, like this one. Crisp, clean and easy drinking."
}
---