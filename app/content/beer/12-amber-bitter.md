---json
{
    "title": "Amber Bitter",
    "number": "12",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BN2eq-HgR9I/",
    "serving": "Bottle",
    "purchased": "shop/aldi/",
    "date": "2016-12-10",
    "permalink": "beer/amber-bitter-bankss/",
    "breweries": [
        "brewery/bankss/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A good beer that is 89p from Aldi. Enough said."
}
---