---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1399183675",
  "title": "Barefoot In Morning Dew",
  "serving": "Can",
  "rating": 8.5,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-07-14",
  "style": "Lager - Helles",
  "abv": "4.3%",
  "breweries": [
    "brewery/modest-beer/"
  ],
  "number": 949,
  "permalink": "beer/barefoot-in-morning-dew-modest-beer/",
  "review": "This was an absolute stonking Helles. I'm not often a fan of the style, but this was crops & clean with plenty of flavour. Went down well with a curry before the England match."
}
---

