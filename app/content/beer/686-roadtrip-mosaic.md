---json
{
    "title": "Roadtrip: Mosaic",
    "number": "686",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CgSqcJ_KO-j/",
    "date": "2022-07-21",
    "permalink": "beer/roadtrip-mosaic-abyss-brewing/",
    "breweries": [
        "brewery/abyss-brewing/"
    ],
    "tags": [],
    "review": "I can&#39;t go to a &quot;meet the brewers&quot; evening at @beernoeviluk and not buy one their beers... This single hop IPA was one of the 5 beers served from @abyssbrewing the other night and was my favourite of the lot. Nice and soft but still had a nice flavour to it. Felt like it was just missing something on the aftertaste but was a lovely drink. Tasted just as good from the can as it did from the cask on the night."
}
---