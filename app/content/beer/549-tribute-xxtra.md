---json
{
    "title": "Tribute XXTRA",
    "number": "549",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CSRDPnOq8Tl/",
    "date": "2021-08-07",
    "permalink": "beer/tribute-xxtra-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [],
    "review": "This was a similar vibe to the other ale-like DIPAs I&#39;ve had. Easy to drink although the taste of alcohol was a bit too present for me (if that makes sense)."
}
---