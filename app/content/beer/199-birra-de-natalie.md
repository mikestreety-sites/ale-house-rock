---json
{
    "title": "Birra de Natalie",
    "number": "199",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/Bsv3cqZgC3r/",
    "date": "2019-01-17",
    "permalink": "beer/birra-de-natalie-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [],
    "review": "My 200th beer and what a beauty it was. Bought from Genoa for my parents (they were kind enough to share it with me) this sneaks up on you. The first step is sharp - the 8% cutting through, but the more sips you take, the smoother and tastier it gets; it’s fruity too. Lovely beer"
}
---