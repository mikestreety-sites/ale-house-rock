---json
{
    "title": "Tropic Like it's Hop",
    "number": "300",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B-nK5zEJCyu/",
    "date": "2020-04-05",
    "permalink": "beer/tropic-like-its-hop-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Another @innisandgunn_ fruit flavoured beer. Doesn&#39;t taste as good as its name. I much prefer the whiskey/rum barrelled beer they do"
}
---