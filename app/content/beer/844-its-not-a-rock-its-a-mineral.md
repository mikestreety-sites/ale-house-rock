---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1312048579",
  "title": "It's Not A Rock, It's A Mineral",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/sureshot-brewing/",
  "date": "2023-09-08",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 844,
  "permalink": "beer/its-not-a-rock-its-a-mineral-sureshot-brewing/",
  "review": "A fairly standard NEIPA. Kind of supermarket quality - I would happily have another one, but not my favourite of the Sureshot tipples"
}
---

