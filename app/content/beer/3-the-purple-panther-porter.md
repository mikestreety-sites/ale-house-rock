---json
{
    "title": "The Purple Panther Porter",
    "number": "3",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BNkTIEvA4zw/",
    "serving": "Bottle",
    "purchased": "shop/lidl/",
    "date": "2016-12-03",
    "permalink": "beer/the-purple-panther-porter-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "porter"
    ],
    "review": "A dark, heavy beer. Wouldn&#39;t want a second pint of this. Might struggle through the first. Tastes of coffee and other things. Hmm"
}
---