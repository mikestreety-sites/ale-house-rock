---json
{
    "title": "Red Rye Captain Pale Ale",
    "number": "255",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B5Qz7d5Fi7x/",
    "date": "2019-11-24",
    "permalink": "beer/red-rye-captain-pale-ale-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [],
    "review": "A watery taste to the start of this, but soon got into the rhythm of it - was disappointed when it finished! Nice little tipple from Lidl, would drink again"
}
---