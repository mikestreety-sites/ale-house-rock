---json
{
    "title": "Tegernsee Pils",
    "number": "687",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CgXlhIlKMy4/",
    "date": "2022-07-23",
    "permalink": "beer/tegernsee-pils-tegernseer/",
    "breweries": [
        "brewery/tegernseer/"
    ],
    "tags": [],
    "review": "This was an average, European tasting pilsner. Sorry I have nothing else to say on the matter!"
}
---