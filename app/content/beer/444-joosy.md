---json
{
    "title": "Joosy",
    "number": "444",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CMDbRLEl_tv/",
    "date": "2021-03-05",
    "permalink": "beer/joosy-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "This juicy IPA hit the spot for Friday game night. Light &amp; easy drinking. However, I felt like it was missing something. Took me most of the can to work out that it could have done with the DIPA kick at the end. It was talking the talk on the taste buds but failed to walk the walk in the aftertaste. A nice enough beer (tasty) but it wouldn&#39;t be at the top of my list to buy again."
}
---