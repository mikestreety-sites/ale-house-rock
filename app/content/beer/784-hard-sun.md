---json
{
    "title": "Hard Sun",
    "number": "784",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1262416029",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-04-06",
    "permalink": "beer/hard-sun-floc/",
    "breweries": [
        "brewery/floc/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A good, hipster IPA. Flavourful, thick and juicy. Perfect accompaniment to catching up.with friends on Zoom"
}
---