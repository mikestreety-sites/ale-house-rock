---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1412845095",
  "title": "The Greatest Love Story",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-08-29",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4.5%",
  "breweries": [
    "brewery/pretty-decent-beer-co/"
  ],
  "number": 960,
  "permalink": "beer/the-greatest-love-story-pretty-decent-beer-co/",
  "review": "I always feel gluten-free beers have something missing (obviously, it's gluten). It often leaves a \"dry\" aftertaste. Saying that, this was one of the better ones, but I wouldn't buy it out of choice."
}
---

