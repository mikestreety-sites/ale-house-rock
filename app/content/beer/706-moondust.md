---json
{
    "title": "Moondust",
    "number": "706",
    "rating": "9.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1195990700",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-08-27",
    "permalink": "beer/moondust-baron-brewing/",
    "breweries": [
        "brewery/baron-brewing/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "This was super lush and was disappointed when I finished this beer. It was very Deya-esque (hence the glass) - super smooth, juicy and so very easy to drink. Wish I had bought two!"
}
---