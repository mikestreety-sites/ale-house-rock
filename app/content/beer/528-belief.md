---json
{
    "title": "Belief",
    "number": "528",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CRCrB-ZF4Gq/",
    "date": "2021-07-07",
    "permalink": "beer/belief-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "Needed a bit of belief this evening with the football and it seems @thebeakbrewery brought it. Not my favourite of theirs but still a good beer - could taste the pineapple inspired hops. Fresh and crisp."
}
---