---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1336515860",
  "title": "Equinox Symmetry",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-12-03",
  "style": "IPA - New England / Hazy",
  "abv": "6%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 877,
  "permalink": "beer/equinox-symmetry-overtone-brewing-co/",
  "review": "I felt a little underwhelmed with this one, especially after the recent string of good ones from Overtone. The last of the brewery box I had, this IPA had most of the flavours you would expect, just slightly muted."
}
---

