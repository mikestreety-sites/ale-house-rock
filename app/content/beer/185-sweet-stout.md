---json
{
    "title": "Sweet Stout",
    "number": "185",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bra7qPNA254/",
    "date": "2018-12-15",
    "permalink": "beer/sweet-stout-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [
        "stout"
    ],
    "review": "my final beer from Genoa. A nice evening beer, complex stout flavours without being heavy. Definitely a slow sipper"
}
---