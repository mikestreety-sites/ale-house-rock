---json
{
    "title": "Laid Back IPA",
    "number": "405",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CJj2h5dlkU_/",
    "date": "2021-01-02",
    "permalink": "beer/laid-back-ipa-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "A non-exciting IPA to fill the shelves of a supermarket. Brewed especially for M&amp;S and it can stay that way, out of the mainstream. A filler beer really, drinkable(with some taste) but just about that."
}
---