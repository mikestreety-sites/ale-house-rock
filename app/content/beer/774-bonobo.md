---json
{
    "title": "BONOBO",
    "number": "774",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1252832609",
    "serving": "Can",
    "date": "2023-03-04",
    "permalink": "beer/bonobo-missing-link-brewing/",
    "breweries": [
        "brewery/missing-link-brewing/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "This kind of went down with indifference, really. Not super tasty but not horrible to drink."
}
---