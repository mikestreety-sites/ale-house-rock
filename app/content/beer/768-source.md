---json
{
    "title": "Source",
    "number": "768",
    "rating": "4.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1247611278",
    "serving": "Can",
    "date": "2023-02-12",
    "permalink": "beer/source-laine-brew-co/",
    "breweries": [
        "brewery/laine-brew-co/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A bit lacking in flavour and a disappointing end to the beers in my fridge. From memory, it&#39;s better on tap but this is the review of the can."
}
---