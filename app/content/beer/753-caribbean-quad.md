---json
{
    "title": "Caribbean Quad",
    "number": "753",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1235324134",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-12-30",
    "permalink": "beer/caribbean-quad-unbarred-brewery/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [
        "beligan"
    ],
    "review": "A dark, rum-laced beer which was great to sit and sip over an evening. Wouldn&#39;t rush out to buy this again, but enjoyed the experience"
}
---