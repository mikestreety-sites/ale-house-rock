---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1424535541",
  "title": "Pupa",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-10-11",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4.5%",
  "breweries": [
    "brewery/vibrant-forest-brewery/"
  ],
  "number": 979,
  "permalink": "beer/pupa-vibrant-forest-brewery/",
  "review": "A fairly average pale ale. Nice enough, but could have done with a bit more flavour for me."
}
---
