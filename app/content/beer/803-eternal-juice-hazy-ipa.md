---json
{
    "title": "ETERNAL JUICE // HAZY IPA",
    "number": "803",
    "rating": "9.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1277445942",
    "serving": "Can",
    "purchased": "shop/tesco/",
    "date": "2023-05-25",
    "permalink": "beer/eternal-juice-hazy-ipa-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [
        "hazy"
    ],
    "review": "For a supermarket beer, this was amazing. Thick, juicy and full of flavour. One of the best Northern Monk beers I&#39;ve had for a long time."
}
---