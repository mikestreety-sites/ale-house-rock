---json
{
    "title": "Sunburst",
    "number": "163",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Boj87SilfAC/",
    "date": "2018-10-05",
    "permalink": "beer/sunburst-dark-star/",
    "breweries": [
        "brewery/dark-star/"
    ],
    "tags": [],
    "review": "Lovely balanced beer from @darkstarbrewco. Perfect end to the week. Could sit and drink this all evening."
}
---