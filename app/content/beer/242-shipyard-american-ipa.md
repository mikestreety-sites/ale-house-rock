---json
{
    "title": "Shipyard American IPA",
    "number": "242",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B3C0qVqluTQ/",
    "date": "2019-09-30",
    "permalink": "beer/shipyard-american-ipa-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [],
    "review": "Classic IPA. Nothing to write home about but a stable classic. I would happily sit and drink this all evening for a session"
}
---