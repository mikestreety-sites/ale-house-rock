---json
{
    "title": "Hogstar",
    "number": "601",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CYUgLqXKJI2/",
    "date": "2022-01-04",
    "permalink": "beer/hogstar-hogs-back-brewery/",
    "breweries": [
        "brewery/hogs-back-brewery/"
    ],
    "tags": [],
    "review": "What an absolute banger of a lager! Very easy drinking with bags of flavour - would easily pick this over a lot of other beers in the supermarket."
}
---