---json
{
    "title": "Heavy Focus",
    "number": "782",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1261455243",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-04-02",
    "permalink": "beer/heavy-focus-track-brewing-company/",
    "breweries": [
        "brewery/track-brewing-company/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "I&#39;m not sure if it was because I&#39;d just got home from holiday and was finally sitting in silence or if this beer was genuinely good, but in that moment it was excellent. Thick, smooth and fulfilling."
}
---