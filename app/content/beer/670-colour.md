---json
{
    "title": "Colour",
    "number": "670",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Ceb7sPKqGnW/",
    "date": "2022-06-05",
    "permalink": "beer/colour-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "Another cracking beer from @thebeakbrewery and another lovely pale ale from @thefuss.club. This may seem &quot;obvious&quot; but it just seemed like a lighter version of their awesome IPAs"
}
---