---json
{
    "title": "Line of Aquarius",
    "number": "615",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CZ4uOaWKQUt/",
    "date": "2022-02-12",
    "permalink": "beer/line-of-aquarius-north-brewing-co/",
    "breweries": [
        "brewery/north-brewing-co/"
    ],
    "tags": [],
    "review": "Unfortunately, when I got to this @northbrewco can out, my beer fridge had decided to freeze its contents (hence the misshapen can). After leaving it to thaw for a couple of hours, the result was a lovely smooth session IPA, which had lots of flavour and a solid weight to it, despite being ~4%."
}
---