---json
{
    "title": "St. Pierre",
    "number": "475",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/COGQwlHlFSg/",
    "date": "2021-04-25",
    "permalink": "beer/st-pierre-st-pieters-abbey/",
    "breweries": [
        "brewery/st-pieters-abbey/"
    ],
    "tags": [],
    "review": "Easy drinking Belgian style beer from Aldi. Went very well shared with my dad and accompanying the Chinese takeaway on Friday evening. Not outstanding but a good sharing beer."
}
---