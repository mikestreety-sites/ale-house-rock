---json
{
    "title": "Stannary Pale",
    "number": "810",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1281228016",
    "serving": "Can",
    "date": "2023-06-04",
    "permalink": "beer/stannary-pale-stannary-brewing-company/",
    "breweries": [
        "brewery/stannary-brewing-company/"
    ],
    "tags": [],
    "review": "I wouldn&#39;t ever pick a Gluten Free beer, but got sent this in a beer &amp; book box. It was ok - a lot better than a Brewdog one I tried the other day. This one had flavour at least. Not for me though."
}
---