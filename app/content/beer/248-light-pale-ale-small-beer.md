---json
{
    "title": "Light Pale Ale (Small Beer)",
    "number": "248",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B4GQiS8lV5o/",
    "date": "2019-10-26",
    "permalink": "beer/light-pale-ale-small-beer-wiper-and-true/",
    "breweries": [
        "brewery/wiper-and-true/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Lovely light beer from @wiperandtrue brewery. Went down very quickly - smooth and tasty and at only 2.7% could have easily had quite a few!"
}
---