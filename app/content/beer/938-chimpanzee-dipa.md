---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1391843691",
  "title": "CHIMPANZEE DIPA",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-06-21",
  "style": "IPA - Imperial / Double",
  "abv": "8.2%",
  "breweries": [
    "brewery/missing-link-brewing/"
  ],
  "number": 938,
  "permalink": "beer/chimpanzee-dipa-missing-link-brewing/",
  "review": "A lovely, fluffy simple DIPA. Nothing too overpowering - spot on for father's day."
}
---

