---json
{
    "title": "Duvel",
    "number": "698",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/ChQWb__KyH9/",
    "date": "2022-08-14",
    "permalink": "beer/duvel-duvel-moortgat/",
    "breweries": [
        "brewery/duvel-moortgat/"
    ],
    "tags": [],
    "review": "A nice enough Belgian-style golden beer. Not incredible but certainly refreshing during this heatwave. Quite crisp, but lacking any significant flavour"
}
---