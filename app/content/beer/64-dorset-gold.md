---json
{
    "title": "Dorset Gold",
    "number": "64",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BYloIQEF5CA/",
    "date": "2017-09-03",
    "permalink": "beer/dorset-gold-palmers/",
    "breweries": [
        "brewery/palmers/"
    ],
    "tags": [],
    "review": "Forgot to post this from Thursday. Very tasty, easy drinking beer. Could easily have sipped it all night, pint after pint but unfortunately I only had one, which went down very easy."
}
---