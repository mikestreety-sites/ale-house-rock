---json
{
    "title": "How's the Serenity?",
    "number": "412",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CJ6Z_iIladI/",
    "date": "2021-01-11",
    "permalink": "beer/hows-the-serenity-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "This caught me off guard. The lightness and &quot;fruity&quot; description had me wary, but it was very crisp and refreshing and I felt like I had to savour every sip. One of my favourite @gunbrewery beers"
}
---