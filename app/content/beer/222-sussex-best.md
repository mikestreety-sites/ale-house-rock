---json
{
    "title": "Sussex Best",
    "number": "222",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BxBBB20FgB6/",
    "date": "2019-05-03",
    "permalink": "beer/sussex-best-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "A good old fashioned English bitter from @harveysbrewery. Can&#39;t go wrong with this pint"
}
---