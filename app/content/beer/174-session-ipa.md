---json
{
    "title": "Session IPA",
    "number": "174",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/BqGJZyrgnRI/",
    "date": "2018-11-12",
    "permalink": "beer/session-ipa-the-white-hag/",
    "breweries": [
        "brewery/the-white-hag/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Just No. The first split second hints that this beer might be palatable but after that, it descends into a whirlwind of disappointment and dishwasher water"
}
---