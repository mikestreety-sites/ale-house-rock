---json
{
    "title": "Longhorn IPA",
    "number": "532",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CRMz2f5FSSW/",
    "date": "2021-07-11",
    "permalink": "beer/longhorn-ipa-purity-brewing-company/",
    "breweries": [
        "brewery/purity-brewing-company/"
    ],
    "tags": [],
    "review": "Another cracking beer from @purityale. Tasty and full-bodied. A great accomniament to the England game. I&#39;ve not had many ales of late, so this was a refreshing change and a great ale to break from the craft casks. Lovely stuff."
}
---