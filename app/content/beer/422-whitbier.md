---json
{
    "title": "Whitbier",
    "number": "422",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CKXW7Q-Fgde/",
    "date": "2021-01-22",
    "permalink": "beer/whitbier-wolfox/",
    "breweries": [
        "brewery/wolfox/"
    ],
    "tags": [],
    "review": "While Wolfox do excellent coffee, this beer left a lot to be desired. A bit too clear for a wheat beer and had a zingy aftertaste. Not to my liking, but went down all the same."
}
---