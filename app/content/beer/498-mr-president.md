---json
{
    "title": "Mr. President",
    "number": "498",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CPbYO1TJ1DG/",
    "date": "2021-05-28",
    "permalink": "beer/mr-president-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "I was quite excited to see a small @brewdog DIPA as it seemed to tick all my boxes, however the taste did not live up to the hype. First sip felt promising, but it was swiftly followed by a flat flavour with a slightly chemical aftertaste. Such a shame"
}
---