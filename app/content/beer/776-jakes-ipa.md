---json
{
    "title": "Jake's IPA",
    "number": "776",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1254171449",
    "serving": "Can",
    "date": "2023-03-09",
    "permalink": "beer/jakes-ipa-jakes-drinks-at-balfour-winery/",
    "breweries": [
        "brewery/jakes-drinks-at-balfour-winery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "I don&#39;t know if the brewery name tainted it with a placebo effect, but this definitely felt like it had a wine ambience to the hops. A bit too happy for me, but smooth nonetheless."
}
---