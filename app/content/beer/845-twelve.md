---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1312317570",
  "title": "Twelve",
  "serving": "Can",
  "rating": 4,
  "date": "2023-09-09",
  "style": "Pale Ale - Other",
  "abv": "4.2%",
  "breweries": [
    "brewery/long-man-brewery/"
  ],
  "number": 845,
  "permalink": "beer/twelve-long-man-brewery/",
  "review": "This pale ale was ok, but had a bit of an odd aftertaste. It was refreshing because it was cold and at the end of a hot day, but I don't think I would buy this again (or drink it if offered)."
}
---

