---json
{
    "title": "The Golden Champion",
    "number": "51",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BUub4Ehlb_8/",
    "serving": "Bottle",
    "date": "2017-05-30",
    "permalink": "beer/golden-champion-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A crisp golden ale from my favourite brewers @badgerbeer. Beautiful and refreshing after a day walking round Brighton"
}
---