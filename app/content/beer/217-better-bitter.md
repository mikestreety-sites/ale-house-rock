---json
{
    "title": "Better Bitter",
    "number": "217",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/BwH4yG4FawX/",
    "date": "2019-04-11",
    "permalink": "beer/better-bitter-lyme-regis-brewery/",
    "breweries": [
        "brewery/lyme-regis-brewery/"
    ],
    "tags": [],
    "review": "If this is their better bitter I would hate to taste their normal one. Eggy smell and a bad aftertaste, this was not what I was hoping for from a small brewery. Disappointing, especially after that 10 last week"
}
---