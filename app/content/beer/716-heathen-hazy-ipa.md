---json
{
    "title": "HEATHEN // HAZY IPA",
    "number": "716",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1202973454",
    "serving": "Can",
    "purchased": "shop/sainsburys/",
    "date": "2022-09-17",
    "permalink": "beer/heathen-hazy-ipa-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [
        "hazyipa"
    ],
    "review": "Run of the mill stuff for Northern Monk, but not their finest. Had a bit too much of the alcohol taste for me, but still enjoyable as a beer"
}
---