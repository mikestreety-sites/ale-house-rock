---json
{
    "title": "Phaedra",
    "number": "717",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1202973943",
    "serving": "Can",
    "date": "2022-09-17",
    "permalink": "beer/phaedra-pomona-island-brew-co/",
    "breweries": [
        "brewery/pomona-island-brew-co/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "Lovely hazy pale, although it was more reminiscent of a NEIPA. Tasted better once it had warmed up a bit but still very drinkable"
}
---