---json
{
    "title": "Session IPA",
    "number": "546",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CSIJG2Fq4rl/",
    "date": "2021-08-03",
    "permalink": "beer/session-ipa-cloudwater/",
    "breweries": [
        "brewery/cloudwater/"
    ],
    "tags": [],
    "review": "A lovely refreshing juicy beer"
}
---