---json
{
    "title": "Grand AM",
    "number": "204",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BtMIdAzlGW_/",
    "date": "2019-01-28",
    "permalink": "beer/grand-am-bear-republic/",
    "breweries": [
        "brewery/bear-republic/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "unexpectedly bottle conditioned for an APA. A strong taste, sipped this over an evening despite being small"
}
---