---json
{
    "title": "Revival",
    "number": "360",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CC1h9XPpChj/",
    "date": "2020-07-19",
    "permalink": "beer/revival-moor-beer-company/",
    "breweries": [
        "brewery/moor-beer-company/"
    ],
    "tags": [],
    "review": "This beer from @drinkmoorbeer was in my latest @flavourlyhq box. There were 4 of them in the box and after 3, I&#39;m still not sure what to make of it. It tastes like you&#39;ve just eaten a sweet and had a swig of beer (sometimes), watery (sometimes) or like a fine craft beer (sometimes). I would drink again, but re-buyability is low."
}
---