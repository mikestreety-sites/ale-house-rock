---json
{
    "title": "Hop House Lager",
    "number": "116",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BkQwxjnFkP0/",
    "date": "2018-06-20",
    "permalink": "beer/hop-house-lager-open-gate-brewery/",
    "breweries": [
        "brewery/open-gate-brewery/"
    ],
    "tags": [
        "lager"
    ],
    "review": "From the makers of @guinness, this lager is very similar to the pilsner that came before. Not much flavour but great on a hot day next to the BBQ, or in the garden or while you’re doing DIY."
}
---