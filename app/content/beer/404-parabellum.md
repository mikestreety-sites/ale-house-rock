---json
{
    "title": "Parabellum",
    "number": "404",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CJgumz2lxeZ/",
    "date": "2021-01-01",
    "permalink": "beer/parabellum-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "A lovely stout for post zoom New Year festivities but pre-midnight celebrations. Went down. Ice and smoothly. Nothing too heavy after an evening of stuffing my face."
}
---