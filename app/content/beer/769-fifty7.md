---json
{
    "title": "Fifty7",
    "number": "769",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1248682338",
    "serving": "Can",
    "date": "2023-02-17",
    "permalink": "beer/fifty7-long-man-brewery/",
    "breweries": [
        "brewery/long-man-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "This was a very light, mid-flavoured pale ale. Wouldn&#39;t be one for drinking on its own but went great with some food"
}
---