---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1363625271",
  "title": "TRIPLE BOGEY",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-15",
  "style": "IPA - New England / Hazy",
  "abv": "6.2%",
  "breweries": [
    "brewery/play-brew-co/"
  ],
  "number": 907,
  "permalink": "beer/triple-bogey-play-brew-co/",
  "review": "This was ok as a NEIPA, enjoyable but nothing special"
}
---

