---json
{
    "title": "Far Fetched",
    "number": "409",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CJ1jcXHlo-p/",
    "date": "2021-01-09",
    "permalink": "beer/far-fetched-hairy-dog-brewery/",
    "breweries": [
        "brewery/hairy-dog-brewery/"
    ],
    "tags": [],
    "review": "An enjoyable IPA. Can&#39;t say much about it, good or bad, but I generally enjoyed it."
}
---