---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1308058376",
  "title": "Drill Deeper",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/the-craft-beer-cabin/",
  "date": "2023-08-26",
  "style": "IPA - Imperial / Double",
  "abv": "9%",
  "breweries": [
    "brewery/magnify-brewing-company/"
  ],
  "number": 841,
  "permalink": "beer/drill-deeper-magnify-brewing-company/",
  "review": "This was aggressive. I was drawn to this can as it was, unusually, a pint. It was slightly bitter but it wasn't unpleasant. You could really taste the alcohol on this, it took a while to get used to and is definitely a sipper."
}
---

