---json
{
    "title": "Sussex IPA",
    "number": "21",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BOOv-7GgSnQ/",
    "serving": "Bottle",
    "date": "2016-12-20",
    "permalink": "beer/sussex-ipa-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A hoppy number. Not really my cup of tea. Didn&#39;t finish it, but probably could have."
}
---