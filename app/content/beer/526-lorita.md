---json
{
    "title": "Lorita",
    "number": "526",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CQ6-JvqJh4H/",
    "date": "2021-07-04",
    "permalink": "beer/lorita-amundsen-brewery/",
    "breweries": [
        "brewery/amundsen-brewery/"
    ],
    "tags": [],
    "review": "Another Father&#39;s Day beer from the boy (purchased from M&amp;S). Smooth drinking but not enough flavour for my liking."
}
---