---json
{
    "title": "Cascade",
    "number": "72",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BaRzbKblWNQ/",
    "date": "2017-10-15",
    "permalink": "beer/cascade-castle-rock-brewery/",
    "breweries": [
        "brewery/castle-rock-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Another M&amp;S beer, this time a bit better. Sweet and fizzy, this 5% isn’t too bad. But too light for my liking but good nonetheless. Was going to give it 6 but it’s a bit too gassy."
}
---