---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1293498955",
  "title": "Big IPA Series #1 NZIPA",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-07-13",
  "style": "IPA - New Zealand",
  "abv": "7%",
  "breweries": [
    "brewery/cloak-and-dagger/"
  ],
  "number": 824,
  "permalink": "beer/big-ipa-series-1-nzipa-cloak-and-dagger/",
  "review": "This was a dry, sharp IPA. It was no surprise the notes on the side mentioned gooseberry. It was refreshing, and a change to have a dry IP it was certainly one I would have again and happily sit in a micropub drink many of these"
}
---
