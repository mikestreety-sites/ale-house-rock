---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1386168408",
  "title": "Go With the Flow",
  "serving": "Can",
  "rating": 4,
  "date": "2024-06-01",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4.5%",
  "breweries": [
    "brewery/full-circle-brew-co/",
    "brewery/allkin-brewing-company/"
  ],
  "number": 934,
  "permalink": "beer/go-with-the-flow-full-circle-brew-co-allkin-brewing-company/",
  "review": "A disappointing hazy pale. Very lifeless and plain. I drank it, but I can't say I really enjoyed it."
}
---

