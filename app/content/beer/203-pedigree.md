---json
{
    "title": "Pedigree",
    "number": "203",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BtDK3qRAQh7/",
    "date": "2019-01-25",
    "permalink": "beer/pedigree-marstons/",
    "breweries": [
        "brewery/marstons/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "I was sure I had reviewed this one, but a quick search proved otherwise. It was better than I remember it tasting and would actively buy one after this lovely pint. Thoroughly enjoyed it"
}
---