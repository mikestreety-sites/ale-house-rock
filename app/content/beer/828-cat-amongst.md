---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1296017939",
  "title": "Cat Amongst",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/asda/",
  "date": "2023-07-21",
  "style": "IPA - New England / Hazy",
  "abv": "5.1%",
  "breweries": [
    "brewery/tooth-claw-brewing/"
  ],
  "number": 828,
  "permalink": "beer/cat-amongst-tooth-claw-brewing/",
  "review": "This was very fruity and thick, but not my favourite. It's drinkable, as a beer, but I wouldn't rush out the buy this again any time soon. Great artwork though!"
}
---

