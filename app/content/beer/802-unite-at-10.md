---json
{
    "title": "Unite At 10",
    "number": "802",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1275654496",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-05-19",
    "permalink": "beer/unite-at-10-glasshouse-beer-co/",
    "breweries": [
        "brewery/glasshouse-beer-co/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I didn&#39;t care much for this beer. It was drinkable, but nothing about it wanted me to drink it again. It was &quot;ok&quot;."
}
---