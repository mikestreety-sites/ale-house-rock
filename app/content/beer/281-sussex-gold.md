---json
{
    "title": "Sussex Gold",
    "number": "281",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B8mx00-pRVr/",
    "date": "2020-02-15",
    "permalink": "beer/sussex-gold-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [],
    "review": "Had a pint of this out of the barrel in  @thewatchmakersarms the other night which has tainted my opinion somewhat. This beer is glorious. Better out the barrel than the bottle, but the bottled product is still supreme. Crisp, refreshing &amp; light (without being too Hoppy). In the pub, this was easily a 10, but the fizzyness of the bottle means it loses a point. Still a cracking good pint though"
}
---