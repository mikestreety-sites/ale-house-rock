---json
{
    "title": "Arrowaine",
    "number": "158",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Bn9UyCuhVK3/",
    "date": "2018-09-20",
    "permalink": "beer/arrowaine-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "This dark beer looked like a stout but tasted more like the black IPAs that have been cropping up. Chocolate undertones but a bit too hard nosed for my liking"
}
---