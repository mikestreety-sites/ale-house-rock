---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1311092262",
  "title": "Sappp",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/the-beak-brewery/",
  "date": "2023-09-04",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 843,
  "permalink": "beer/sappp-beak/",
  "review": "A nice thick DIPA, that went well with watching a film. It took a few sips to get used to as the alcohol taste was a bit of a pang in the first mouthful. It was perfect when it was fresh out of the fridge."
}
---
