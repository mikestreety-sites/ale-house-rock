---json
{
    "title": "'R Ale",
    "number": "604",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CYv70Ldq_-z/",
    "date": "2022-01-15",
    "permalink": "beer/r-ale-co-op/",
    "breweries": [
        "brewery/co-op/"
    ],
    "tags": [],
    "review": "Not sure what Coronation Street had to do with this ale buy I like it. This was a real good old fashioned bitter you could imagine drinking in a country pub somewhere. Went lovely with the 8 hour slow cooked beef I had last Sunday."
}
---