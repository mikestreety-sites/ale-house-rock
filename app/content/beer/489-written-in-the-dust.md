---json
{
    "title": "Written in the Dust",
    "number": "489",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CO6KX34lUEJ/",
    "date": "2021-05-15",
    "permalink": "beer/written-in-the-dust-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "Since my first drink of theirs, I&#39;ve always held @deyabrewery with high regard. Looking back, however, I realise I have only ever had one or their beers. However, my trust was not misguided as this was another good one 👌. It was a lush, thick creamy DIPA. Perfect for an evening in (I could imagine it being quite filling if you decided to have a second)."
}
---