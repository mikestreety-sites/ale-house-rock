---json
{
    "title": "Tribute",
    "number": "563",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CTLQ_FWqjTx/",
    "date": "2021-08-30",
    "permalink": "beer/tribute-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "I&#39;ve definitely had Tribute before, so it was a surprise I&#39;d not logged it. For some reason I had it in my head it was a ruby beer so was a surprise to find a light hoppy pour. Definitely better than the XXTRA one I had a few weeks back. This is such a classic bitter/ale beer. Spot on after a day in the garden (not working, but drinking beer...)"
}
---