---json
{
    "title": "Urgent Philosophy",
    "number": "674",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Ce15bkjKa4o/",
    "date": "2022-06-15",
    "permalink": "beer/urgent-philosophy-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "I thought Deya could only do (D)IPAs well, but seems their porters are pretty damn tasty too. Not.too sweet or sickly, easy to drink and not overwhelming. It was missing a bit of the weight/gravitas a porter not ally carries, but this was a pretty good one by all accounts."
}
---