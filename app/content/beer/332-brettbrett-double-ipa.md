---json
{
    "title": "BrettBrett Double IPA",
    "number": "332",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CAi913iJDwl/",
    "date": "2020-05-23",
    "permalink": "beer/brettbrett-double-ipa-the-wild-beer-company/",
    "breweries": [
        "brewery/the-wild-beer-company/"
    ],
    "tags": [],
    "review": "After the success of the last IPA I had from the @wildbeerco I was hoping this was going to be a tad better than the last few Wild Beers. Definitely stronger and with a bigger punch (another strong one at 8.4%), I manged to polish two bottles of this off this evening. Went well with the curry I had and was a decent pour (careful of the sediment at the bottom of the bottle). Solid taste."
}
---