---json
{
    "title": "Parody",
    "number": "516",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CQToFpcJuq-/",
    "date": "2021-06-19",
    "permalink": "beer/parody-firebird-brewing-company/",
    "breweries": [
        "brewery/firebird-brewing-company/"
    ],
    "tags": [],
    "review": "Went to Horsham the other day and popped into a shop to get a coffee - came out with some @firebirdbrewing beers (and a coffee). Nice and light IPA (despite the colour) and as enjoyable as the NEIPA before it. Tasty enough for the second half of the England game."
}
---