---json
{
    "title": "Monument Valley",
    "number": "356",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CCbjIJspJGR/",
    "date": "2020-07-09",
    "permalink": "beer/monument-valley-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [],
    "review": "First beer out of the @flavourlyhq box from this week is a @fourpure classic. A &quot;tangerine pale&quot;, I was hoping for a Juicebox style beer but, instead, was faced with a pale ale with an orangey aftertaste. Palettable, and I&#39;ll happily drink the other 4 but certainly not one of my favourite Fourpure beers."
}
---