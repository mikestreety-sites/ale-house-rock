---json
{
    "title": "Now That's What I Call Sureshot Vol.100",
    "number": "815",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1286060714",
    "serving": "Can",
    "purchased": "shop/sureshot-brewing/",
    "date": "2023-06-20",
    "permalink": "beer/now-thats-what-i-call-sureshot-vol100-sureshot-brewing/",
    "breweries": [
        "brewery/sureshot-brewing/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "It was father&#39;s day and I&#39;d spent the afternoon at a children&#39;s party, so I looked for the strongest beer I had. Fruity, full of flavour and hit the spot. Could taste the alcohol a little too much for my liking, but still a banger"
}
---