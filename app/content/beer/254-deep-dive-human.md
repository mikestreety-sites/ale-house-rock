---json
{
    "title": "Deep Dive Human",
    "number": "254",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B48QpkWlcRx/",
    "date": "2019-11-16",
    "permalink": "beer/deep-dive-human-cloudwater/",
    "breweries": [
        "brewery/cloudwater/"
    ],
    "tags": [],
    "review": "Another beer I wasn&#39;t holding out much hope for but dang it was good. Juicy, without too much fruit, a lovely balance of flavours and not too heavy in the stomach. Would easily have a couple more of these"
}
---