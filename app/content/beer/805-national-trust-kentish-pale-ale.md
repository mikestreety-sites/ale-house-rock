---json
{
    "title": "National Trust Kentish Pale Ale",
    "number": "805",
    "rating": "4",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1278456840",
    "serving": "Can",
    "purchased": "shop/nymans-national-trust-house-gardens-and-woodland/",
    "date": "2023-05-27",
    "permalink": "beer/national-trust-kentish-pale-ale-westerham-brewery-company-ltd/",
    "breweries": [
        "brewery/westerham-brewery/"
    ],
    "tags": [
        "pale"
    ],
    "review": "Exactly what you would expect from a National Trust Pale Ale. A bit disappointing and bland. I finished it, but I won&#39;t be back for another"
}
---