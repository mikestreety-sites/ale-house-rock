---json
{
    "title": "Double Punk",
    "number": "394",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CItnAA9lrmV/",
    "date": "2020-12-12",
    "permalink": "beer/double-punk-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "After seeing this double IPA on my feed, I felt like I had to try it. I&#39;ve been a big fan of DIPAs recently, but this is the worst one yet (however, is it still better than a normal Punk IPA). Over 8%, and I felt it, there was an unusual post sip-twang I want keen on. I would still definitely have another one of these if offered."
}
---