---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1308053092",
  "title": "Dunblobbin",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sureshot-brewing/",
  "date": "2023-08-26",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 840,
  "permalink": "beer/dunblobbin-sureshot-brewing/",
  "review": "Another corker from Sureshot and great can art too! A lovely, easy drinking IPA. I could sit and have a few of these in an evening!"
}
---

