---json
{
    "title": "Belgian Wheat Beer",
    "number": "73",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BacHrQ8Fg-l/",
    "date": "2017-10-19",
    "permalink": "beer/belgian-wheat-beer-marks-and-spencer/",
    "breweries": [
        "brewery/marks-and-spencer/"
    ],
    "tags": [],
    "review": "Been intrigued to try this one since @mj.street and @geoffst bought it for me (along with the other m&amp;s beers which have adorned my feed of late). Apparently brewed with orange peel and coriander, it seems to taste just like any other wheat beer. Maybe it has some slight citrus undertones...? Certainly makes me feel like I’m on the continent. It’s drinkable but not sure I’d rush to have it again."
}
---