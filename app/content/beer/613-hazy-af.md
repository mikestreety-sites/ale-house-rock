---json
{
    "title": "Hazy AF",
    "number": "613",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/CZ2b9avqyWM/",
    "date": "2022-02-11",
    "permalink": "beer/hazy-af-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "I had high hopes for this - an alcohol free version of thier Hazy Jane (which is one of my faves) but this was disappointing. Just a watered down nothingness. I finished it, but would never pick this over a soft drink."
}
---