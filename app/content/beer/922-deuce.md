---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1376102451",
  "title": "Deuce",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/abyss-brewing-tap/",
  "date": "2024-04-28",
  "style": "IPA - Imperial / Double",
  "abv": "8.4%",
  "breweries": [
    "brewery/abyss-brewing/"
  ],
  "number": 922,
  "permalink": "beer/deuce-abyss-brewing/",
  "review": "This wasn't soft and fluffy like I've come to expect a DIPA from Abyss to be. It was quite harsh and felt like it was 8.4% for the sake of it. It was enjoyable, just not outstanding"
}
---

