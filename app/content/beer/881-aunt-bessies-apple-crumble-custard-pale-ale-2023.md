---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1342326664",
  "title": "AUNT BESSIE'S // APPLE CRUMBLE & CUSTARD PALE ALE (2023)",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/asda/",
  "date": "2023-12-24",
  "style": "Pale Ale - Milkshake",
  "abv": "4.5%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 881,
  "permalink": "beer/aunt-bessies-apple-crumble-custard-pale-ale-2023-northern-monk/",
  "review": "This was... Interesting. It had some sweetness, but you could also taste the artificial-ness of it all. Definitely wouldn't buy one of these again, but wouldn't throw it away if I was given one."
}
---
