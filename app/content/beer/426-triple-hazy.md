---json
{
    "title": "Triple Hazy",
    "number": "426",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CKqS4ytl8yZ/",
    "date": "2021-01-30",
    "permalink": "beer/triple-hazy-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "The triple hopped version of the Hazy Jane I had a couple of weeks ago. This triple IPA packs an absolute punch at 9.5%. Certainly not one to be chugged but after a monotonous Friday, it was a great end. Felt lightheaded after a third but had a great taste with no chemical after-burn which you might expect from a beer of this ABV.  It&#39;s a great tasting beer but don&#39;t think I would queue them up very often - maybe once in a while as a treat. There is a Double Hazy, which might be a perfect combo."
}
---