---json
{
    "title": "Big Eagle",
    "number": "814",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1285670660",
    "serving": "Can",
    "purchased": "shop/asda/",
    "date": "2023-06-18",
    "permalink": "beer/big-eagle-brew-york/",
    "breweries": [
        "brewery/brew-york/"
    ],
    "tags": [
        "westcoast"
    ],
    "review": "This went by without incident. Very unnoticeable as a beer. Filled a hole but won&#39;t buy it again."
}
---