---json
{
    "title": "Denim",
    "number": "594",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CYALCyOKZOk/",
    "date": "2021-12-27",
    "permalink": "beer/denim-salt-beer-factory/",
    "breweries": [
        "brewery/salt-beer-factory/"
    ],
    "tags": [],
    "review": "I&#39;m on the hunt for 600 before the year is out, so sorry for the incoming barrage of beers. This one was nice enough, but nothing mind-blowing. It&#39;s a &quot;HDHC IPA&quot; - whatever that means 😆, but at 7.2% it packs a secret punch."
}
---