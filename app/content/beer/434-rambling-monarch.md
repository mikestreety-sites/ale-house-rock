---json
{
    "title": "Rambling Monarch",
    "number": "434",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CLfOfN3lFzH/",
    "date": "2021-02-19",
    "permalink": "beer/rambling-monarch-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "Had this the other day and it was the best one so far. The second one I had the following day was not so good, so basin g my review on the first. Fresh, bitter and a good ale."
}
---