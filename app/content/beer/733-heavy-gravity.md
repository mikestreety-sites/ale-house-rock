---json
{
    "title": "Heavy Gravity",
    "number": "733",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1218625973",
    "serving": "Can",
    "purchased": "shop/sainsburys/",
    "date": "2022-11-06",
    "permalink": "beer/heavy-gravity-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [],
    "review": "A good supermarket Hazy IPA. Plenty of good flavours but still missing that magic. Would lean towards this over most of the other hazys on the shelf though"
}
---