---json
{
    "title": "Magazine Cover",
    "number": "562",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CTIgTWCKHJD/",
    "date": "2021-08-28",
    "permalink": "beer/magazine-cover-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "This was a lovely change of pace. I could imagine ordering pint after pint after pint of this Session IPA from @deyabrewery while sitting in a pub (preferably a garden of sorts). Refreshing, light and goddam easy to drink - I actually made an audible disappointed noise when I picked up my glass and found it empty. It wasn&#39;t overly packed with flavour - which is perfect for a session, but that is why it misses out on a point."
}
---