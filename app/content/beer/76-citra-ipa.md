---json
{
    "title": "Citra IPA",
    "number": "76",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bar-8EcFvlq/",
    "date": "2017-10-25",
    "permalink": "beer/citra-ipa-oakham-ales/",
    "breweries": [
        "brewery/oakham-ales/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Nice, light, inoffensive beer. Went down easily with no scrunching of this face. Yes I know it’s Wednesday but I had to review some words I had written and needed to get through it. Would have again."
}
---