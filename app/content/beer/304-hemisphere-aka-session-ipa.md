---json
{
    "title": "Hemisphere (A.K.A. Session IPA)",
    "number": "304",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B_A4J2UpdSc/",
    "date": "2020-04-15",
    "permalink": "beer/hemisphere-aka-session-ipa-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [
        "sessionipa"
    ],
    "review": "Fresh delivery of @fourpure beer today so, naturally, I had to try one. This session IPA was good, but I&#39;ve had better. While eating my Mexican dinner this evening, it improved somewhat but as a &quot;drinking&quot; beer, it was ok"
}
---