---json
{
    "title": "Southwold Pale Ale",
    "number": "170",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bpxf_kDgkEV/",
    "date": "2018-11-04",
    "permalink": "beer/southwold-pale-ale-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I was immediately put off by this beer due to the near transparent colour. However, I was wrong. A smooth refreshing pale ale that went down swimmingly"
}
---