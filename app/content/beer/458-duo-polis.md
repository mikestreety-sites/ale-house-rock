---json
{
    "title": "Duo-polis",
    "number": "458",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CM5kxd1JOKb/",
    "date": "2021-03-26",
    "permalink": "beer/duo-polis-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "Maybe it was a mistake have this one straight after the Mallow quest, but this beer was disappointing. I was hoping for a thicker beer (as it was an oat cream) but turned out to be a fizzy IPA with not much flavour."
}
---