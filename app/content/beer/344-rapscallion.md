---json
{
    "title": "Rapscallion",
    "number": "344",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CBWZrFKJcHr/",
    "date": "2020-06-12",
    "permalink": "beer/rapscallion-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A @northernmonk beer Mrs Ale House picked up in Tesco for me the other day. Not one of their best but still better than a lot of other beers out there. Light and hoppy with a fizzy tingle at the back of the throat. Slightly fruity but nothing to write home about."
}
---