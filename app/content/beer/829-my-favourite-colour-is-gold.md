---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1298520251",
  "title": "My Favourite Colour Is Gold",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sureshot-brewing/",
  "date": "2023-07-28",
  "style": "IPA - American",
  "abv": "6.7%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 829,
  "permalink": "beer/my-favourite-colour-is-gold-sureshot-brewing/",
  "review": "I've been putting off drinking this as it means the end of my Sureshot journey for now (there will be more...). This was light, crisp and refreshing. It would suit a pub garden very nicely"
}
---

