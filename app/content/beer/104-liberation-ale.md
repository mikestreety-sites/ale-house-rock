---json
{
    "title": "Liberation Ale",
    "number": "104",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BhW_paxl2Ek/",
    "date": "2018-04-09",
    "permalink": "beer/liberation-ale-liberation-brewery/",
    "breweries": [
        "brewery/liberation-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Sharing this one with @geoffst on our Guernsey adventure. Light crisp beer that tangs the tongue. Makes your mouth a bit puckered. It gets a 7/10 from both of us."
}
---