---json
{
    "title": "Faked Alaska",
    "number": "387",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CHnG-xbFZeY/",
    "date": "2020-11-15",
    "permalink": "beer/faked-alaska-arbor/",
    "breweries": [
        "brewery/arbor-ales/"
    ],
    "tags": [],
    "review": "When we were in Suffolk, I asked the gentleman working in @hopstersipswich what his favourite beer was. This was it, a &quot;pudding IPA&quot;. It was an odd taste to begin with (and, unsurprisingly didn&#39;t go with my dinner) but once I got into it I started to really enjoy it. Wouldn&#39;t rush for it again, but was certainly as experience I&#39;m glad I had. Had a bit of yeast in the can, which I wasn&#39;t expecting"
}
---