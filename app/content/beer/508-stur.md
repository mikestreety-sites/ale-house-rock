---json
{
    "title": "Stur",
    "number": "508",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CP_xKODp1mS/",
    "date": "2021-06-11",
    "permalink": "beer/stur-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "Another DIPA from @thebeakbrewery and another absolute banger. A bit more of a hoppy/harsh DIPA, not as smooth and hazy as the other ones I&#39;ve had from them. The 8.5% snacks you in the face  with this one and leaves no prisoners. A perfect one-can-of-an-evening, kind of drink. Picked up at @palate.bottleshop - looks like I&#39;ll have to go back there soon too, to pick up more Beak beer!"
}
---