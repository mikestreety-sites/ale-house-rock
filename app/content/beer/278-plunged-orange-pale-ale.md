---json
{
    "title": "Plunged Orange Pale Ale",
    "number": "278",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/B8W3d47J4bO/",
    "date": "2020-02-09",
    "permalink": "beer/plunged-orange-pale-ale-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I try not to have prejudices with my beer tasting, but I couldn&#39;t help but approach this one with caution. With the first few sips I was proved right, however by the end the score had increased by a point. Still only"
}
---