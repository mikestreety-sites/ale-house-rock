---json
{
    "title": "What the Helles?!",
    "number": "807",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1279306345",
    "serving": "Can",
    "date": "2023-05-29",
    "permalink": "beer/what-the-helles-big-hug-brewing/",
    "breweries": [
        "brewery/big-hug-brewing/"
    ],
    "tags": [
        "helles"
    ],
    "review": "This was surprisingly smooth for a Helles lager and went down wonderfully as an afternoon toppled. It was enjoyed while I was cooking dinner on a warm and sunny May bank holiday"
}
---