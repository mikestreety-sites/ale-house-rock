---json
{
    "title": "Southdown Harvest",
    "number": "268",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B6z98QHJJoM/",
    "date": "2020-01-02",
    "permalink": "beer/southdown-harvest-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "A lovely beer that I didn&#39;t even think about drinking more of. Great taste and balance, but not ground breaking"
}
---