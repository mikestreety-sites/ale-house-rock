---json
{
    "title": "Pale Ale",
    "number": "557",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CSuc_FKK2Lq/",
    "date": "2021-08-18",
    "permalink": "beer/pale-ale-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "A collaboration between @deyabrewery and @bundobust, this citrusy beer was a pleasant birthday sip from @laurzstreet. My least favourite of the Deya beers but by no means a bad beer by any standard. Very fruity and easy drinking"
}
---