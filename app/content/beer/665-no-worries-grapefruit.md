---json
{
    "title": "No Worries - Grapefruit",
    "number": "665",
    "rating": "1",
    "canonical": "https://www.instagram.com/p/CeTCj1XKivr/",
    "date": "2022-06-02",
    "permalink": "beer/no-worries-grapefruit-lervig/",
    "breweries": [
        "brewery/lervig-aktiebryggeri/"
    ],
    "tags": [],
    "review": "This is possibly the worst alcohol-free beer I&#39;ve had. I had to pour half of it away as I kept thinking it would get better but it didn&#39;t."
}
---