---json
{
    "title": "Great Glacial Lake",
    "number": "382",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CHWPM2fln_L/",
    "date": "2020-11-08",
    "permalink": "beer/great-glacial-lake-burnt-mill-brewery/",
    "breweries": [
        "brewery/burnt-mill-brewery/"
    ],
    "tags": [],
    "review": "I&#39;ve yet to find a bad DIPA. Still making my way through the Suffolk beers, this beauty from the same brewery of the DIPA I had the other day (@burntmillbrewery) ticks every box. Might have to see if they deliver down south!"
}
---