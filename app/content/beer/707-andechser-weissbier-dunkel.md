---json
{
    "title": "Andechser Weissbier Dunkel",
    "number": "707",
    "rating": "3",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1196079433",
    "date": "2022-08-27",
    "permalink": "beer/andechser-weissbier-dunkel-klosterbrauerei-andechs/",
    "breweries": [
        "brewery/klosterbrauerei-andechs/"
    ],
    "tags": [
        "weissbier"
    ],
    "review": "I had two of these and, I&#39;m glad I did, as it turns out it&#39;s bottle.conditioned. The first one was full of yeast, but this second one didn&#39;t taste much better. Dull and bland with a malty taste."
}
---