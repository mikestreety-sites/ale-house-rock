---json
{
    "title": "Long story short",
    "number": "210",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/BvSOoyyFMMb/",
    "date": "2019-03-21",
    "permalink": "beer/long-story-short-lost-and-grounded/",
    "breweries": [
        "brewery/lost-and-grounded/"
    ],
    "tags": [],
    "review": "A &quot;table beer&quot; from @lostandgroundedbrewers which leaves a lot to be desired. Far too hoppy and tasteless for my liking. Managed to finish it, just, but with a little grimace after each sip"
}
---