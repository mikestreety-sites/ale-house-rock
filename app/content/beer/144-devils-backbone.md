---json
{
    "title": "Devils Backbone",
    "number": "144",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BmozhGUlYNY/",
    "date": "2018-08-18",
    "permalink": "beer/devils-backbone-devils-backbone-brewing-company/",
    "breweries": [
        "brewery/devils-backbone-brewing-company/"
    ],
    "tags": [],
    "review": "This standard IPA is has got a cool name (although missing an apostrophe 🤔) but an average tase. Could easily drink several of an evening, but not an exceptional beer"
}
---