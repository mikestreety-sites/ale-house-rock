---json
{
    "title": "Big Baller",
    "number": "506",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CP3_-AgFKgx/",
    "date": "2021-06-08",
    "permalink": "beer/big-baller-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "Popped into the @craftbeercabin at the weekend to grab a park beer and this caught my eye as a take-home-and-try kind of beer. It was only when I got outside did I realise this TIPA from @gipsyhillbrew is 11.3%! Was a bit disappointed in how it poured - it was very sediment-y and this photo was taken after a while of letting it settle and pouring into a couple of glasses to try and reduce. This beer, for me, was very much a &quot;try it&quot; beer. The flavours were there (along with a very strong punch) but I won&#39;t be buying one again - no doubt marred by the pour"
}
---