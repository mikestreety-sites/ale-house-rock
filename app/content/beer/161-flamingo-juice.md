---json
{
    "title": "Flamingo Juice",
    "number": "161",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BoPacTLFARV/",
    "date": "2018-09-27",
    "permalink": "beer/flamingo-juice-flavourly/",
    "breweries": [
        "brewery/flavourly/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I purposely signed up to a new beer service just to get some more beers and this was the first one out. I was instantly dubious based on the last tropical beer experience (see number 152 “Tart”). However I was pleasantly surprised and have drunk this rather quickly (might have something to do with my pedal falling off)"
}
---