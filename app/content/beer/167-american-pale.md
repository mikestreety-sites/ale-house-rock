---json
{
    "title": "American Pale",
    "number": "167",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BpVFnbQFrK2/",
    "date": "2018-10-24",
    "permalink": "beer/american-pale-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "quite a nice hipster beer. Could have a few of these although it’s missing some depth."
}
---