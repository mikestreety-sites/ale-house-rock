---json
{
    "title": "See Side APA",
    "number": "633",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CbYbZnJqnoA/",
    "date": "2022-03-21",
    "permalink": "beer/see-side-apa-bison-beer/",
    "breweries": [
        "brewery/bison-beer/"
    ],
    "tags": [],
    "review": "This light-colpured APA is incredibly tasty and moorish. It&#39;s a regular of mine when out drinking in Brighton, the home of @bisonbeer and tastes just as good out the fridge as it does out the tap"
}
---