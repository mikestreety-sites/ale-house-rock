---json
{
    "title": "Hello My Name Is Gale",
    "number": "664",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CeO4G4qq_Pp/",
    "date": "2022-05-31",
    "permalink": "beer/hello-my-name-is-gale-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "As a Brewdog investor (for better or worse) I got sent a case of 24 of these and this was the last one. I&#39;d been telling myself for weeks I&#39;d review it but never got round to it. Such a nice, easy IPA. At 6% it packed a little fruity punch, but I would easily pick this over most of the other Brewdog offerings. If you see one, pick it up and give it a taste."
}
---