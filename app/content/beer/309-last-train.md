---json
{
    "title": "Last Train",
    "number": "309",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B_KJlqzpdX-/",
    "date": "2020-04-19",
    "permalink": "beer/last-train-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [],
    "review": "Was sitting down to watch some live comedy, so this beer seemed ripe for the occasion. Although a stout, it wasn&#39;t too heavy or syrupy. Took a few sips to get into but was fully onboard by the time it was finished. Glad it was 330ml though and not a 500ml bottle. Perfect amount 👍"
}
---