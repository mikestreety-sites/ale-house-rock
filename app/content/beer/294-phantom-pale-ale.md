---json
{
    "title": "Phantom Pale Ale",
    "number": "294",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B-PuzM-pbvz/",
    "date": "2020-03-27",
    "permalink": "beer/phantom-pale-ale-freak-aleworks/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Surprisingly pleasent for an Aldi beer. Refreshing after a hard afternoon in the garden. Can&#39;t write much as this was from last weekened and I forgot to post!"
}
---