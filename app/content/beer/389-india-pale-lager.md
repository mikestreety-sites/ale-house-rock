---json
{
    "title": "10 4 Brewing - India Pale Lager",
    "number": "389",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CIL-q64Jfyc/",
    "date": "2020-11-29",
    "permalink": "beer/india-pale-lager-10-4-brewing/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "Well this is a disappointing beer. When you say lager, I think of this kind of taste. Bubbly, flavourless and a struggle to finish."
}
---