---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1331225353",
  "title": "Day Glo",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-11-14",
  "style": "IPA - American",
  "abv": "6%",
  "breweries": [
    "brewery/abyss-brewing/"
  ],
  "number": 871,
  "permalink": "beer/day-glo-abyss-brewing/",
  "review": "This was quite the excellent IPA. Light and juicy and very easy to drink. This went down very well and is quite possibly one of my favourite Abyss beers."
}
---

