---json
{
    "title": "Filungar",
    "number": "398",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CJOWtxPFnOx/",
    "date": "2020-12-25",
    "permalink": "beer/filungar-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "Merry Christmas! The first of many festive beers, no doubt and an &quot;off brand&quot; photo (sorry). Provided by Sister Ale House, Gipsy Hill are becoming one of my favourite &quot;curve ball&quot; breweries. Lovely beer (could have done with the whole can but shared with Pops Ale House). Crisp, flavourful and morish. Scrapes in at an 8 - might need to find some more to be sure"
}
---