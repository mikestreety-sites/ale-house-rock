---json
{
    "title": "Dead Pony Club",
    "number": "195",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BsTgqplg0AH/",
    "date": "2019-01-06",
    "permalink": "beer/dead-pony-club-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [
        "ipa",
        "paleale"
    ],
    "review": "If you want a Brewdog beer that tastes better than the Punk IPA (#195) then this is it. Although, it’s not good enough to gain any more points. It’s still a hipster lager parading itself as an IPA. I expected better from this big name brand"
}
---