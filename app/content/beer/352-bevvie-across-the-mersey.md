---json
{
    "title": "Bevvie Across the Mersey",
    "number": "352",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CCMaay9pG_t/",
    "date": "2020-07-03",
    "permalink": "beer/bevvie-across-the-mersey-glen-affric-brewery/",
    "breweries": [
        "brewery/glen-affric-brewery/"
    ],
    "tags": [],
    "review": "Such a long title for a sub-par beer. Every few sips I would think it was turning a corner, but I would just be disappointed by the follow-up. You could smell the citrus/pineapple, but that was the only enjoyable thing about it. Drinkable, but avoidable."
}
---