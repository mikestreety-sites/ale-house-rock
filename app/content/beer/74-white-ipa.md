---json
{
    "title": "White IPA",
    "number": "74",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BacXc2FFhnR/",
    "date": "2017-10-19",
    "permalink": "beer/white-ipa-marks-and-spencer/",
    "breweries": [
        "brewery/marks-and-spencer/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A much better beer. Perfect to wash down a Dominos pizza. Light, a little lacking in flavour but that’s to be expected for an IPA"
}
---