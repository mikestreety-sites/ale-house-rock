---json
{
    "title": "Brian",
    "number": "216",
    "rating": "10",
    "canonical": "https://www.instagram.com/p/Bv11zQVlwOg/",
    "date": "2019-04-04",
    "permalink": "beer/brian-dynamite-valley-brewing-company/",
    "breweries": [
        "brewery/dynamite-valley-brewing-company/"
    ],
    "tags": [],
    "review": "Oh wow. What an absolutely fantastic beer. So good! This bottle came wrapped in the bit of paper, which gave information and a beer token for a free half pint at the brewers shop. A small brewery but a incredibly tasty beer. One I&#39;m sure Brian would have thoroughly enjoyed"
}
---