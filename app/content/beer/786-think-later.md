---json
{
    "title": "Think Later",
    "number": "786",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1262767394",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-04-07",
    "permalink": "beer/think-later-track-brewing-company/",
    "breweries": [
        "brewery/track-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "An enjoyable, but slightly forgettable, Pale Ale It was tasty, but not outstanding"
}
---