---json
{
    "title": "Folded Mountains",
    "number": "135",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Bl0wBOmFefC/",
    "date": "2018-07-29",
    "permalink": "beer/folded-mountains-genesee-brewing-company/",
    "breweries": [
        "brewery/genesee-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Generic pale ale from @aldiuk. Would be good as a BBQ companion, although not as flavourful as some others"
}
---