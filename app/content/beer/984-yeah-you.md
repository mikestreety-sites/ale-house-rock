---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1427251212",
  "title": "Yeah, You?",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/padstow-tasting-rooms/",
  "date": "2024-10-20",
  "style": "IPA - New England / Hazy",
  "abv": "3.8%",
  "breweries": [
    "brewery/padstow-brewing-company/"
  ],
  "number": 984,
  "permalink": "beer/yeah-you-padstow-brewing-company/",
  "review": "A fairly basic hazy IPA and it was lacking a fair bit of flavour for my taste. A little bit of a shame as I've had sick good ones from them recently"
}
---
