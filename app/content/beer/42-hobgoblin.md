---json
{
    "title": "Hobgoblin",
    "number": "42",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BS9X-unl7bz/",
    "serving": "Bottle",
    "date": "2017-04-16",
    "permalink": "beer/hobgoblin-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [
        "brownale"
    ],
    "review": "Back home and this beer welcomed me with warm open arms. It takes a particular time and feeing to have a Hobgoblin but this is it. TV, crackers and a dark warming beer. Complex, heavy taste but one that&#39;s great"
}
---