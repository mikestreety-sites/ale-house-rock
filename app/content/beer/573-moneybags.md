---json
{
    "title": "Moneybags",
    "number": "573",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CUBLVHfqSWk/",
    "date": "2021-09-19",
    "permalink": "beer/moneybags-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "Another smasher of a beer from @gipsyhillbrew. After the last debacle with the TIPA, I was careful not to pour out the whole beer to avoid any sediment. Fruity in smell and taste, an excellent long evening tipple. One of the last of my birthday cans"
}
---