---json
{
    "title": "Deucebox (A.K.A. Double Citrus IPA)",
    "number": "331",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CAhd83Jp1qm/",
    "date": "2020-05-23",
    "permalink": "beer/deucebox-aka-double-citrus-ipa-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "If there is a silver lining to this lockdown, it&#39;s my discovery of  @fourpure and their excellent delivery service. Once again, they&#39;ve produced an absolutely cracking beer. Not quite as good as this one&#39;s little brother, Juicebox, but still an excellent drink. An 8.3% double IPA, this drink was a slow sipper last night but was a great way to end the week."
}
---