---json
{
    "title": "Easy Peeler (A.K.A. Citrus Session IPA)",
    "number": "311",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B_XydjApEaH/",
    "date": "2020-04-24",
    "permalink": "beer/easy-peeler-aka-citrus-session-ipa-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [],
    "review": "Another citrus IPA but not a scratch on the other one. This one was a bit too orangey for me"
}
---