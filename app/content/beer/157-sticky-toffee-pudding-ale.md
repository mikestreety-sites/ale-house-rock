---json
{
    "title": "Sticky Toffee Pudding Ale",
    "number": "157",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BnuVOkjllyi/",
    "date": "2018-09-14",
    "permalink": "beer/sticky-toffee-pudding-ale-eagle-brewery/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [],
    "review": "(Brewery previously was Wells) Certainly a post-dinner beer this one. Heavy, sweet and slow drinking, but full of taste. A one-off"
}
---