---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1393657319",
  "title": "TAMARIN",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-06-27",
  "style": "IPA - American",
  "abv": "5.2%",
  "breweries": [
    "brewery/missing-link-brewing/"
  ],
  "number": 941,
  "permalink": "beer/tamarin-missing-link-brewing/",
  "review": "I seem to like grapefruit IPAs until I start thinking about the grapefruit. This was nice enough, but nothing too spectacular."
}
---

