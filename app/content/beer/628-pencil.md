---json
{
    "title": "Pencil",
    "number": "628",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CavXSGvqZDC/",
    "date": "2022-03-05",
    "permalink": "beer/pencil-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "A smooth, coffee-y chocolate-y porter. Yet another un-astounding-but-tasty-enough Beak brewery beer. Not too sickly or sweet and I can imagine those that are big fans of porters would enjoy this."
}
---