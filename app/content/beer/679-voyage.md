---json
{
    "title": "Voyage",
    "number": "679",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Cft6_90KVq-/",
    "date": "2022-07-07",
    "permalink": "beer/voyage-boojum-and-snark/",
    "breweries": [
        "brewery/boojum-and-snark/"
    ],
    "tags": [],
    "review": "Picked up a couple of beers form @boojumandsnarkiw when we were over on the Isle of Wight a few weeks ago. This red IPA was lovely. A rich beer and a classic bitter"
}
---