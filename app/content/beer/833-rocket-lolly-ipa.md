---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1303871705",
  "title": "ROCKET LOLLY // IPA",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/sainsburys/",
  "date": "2023-08-12",
  "style": "IPA - Fruited",
  "abv": "4.7%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 833,
  "permalink": "beer/rocket-lolly-ipa-northern-monk/",
  "review": "This was such an odd beer. It was like an IPA, but was sweet and sticky. It was bordering on a sour, but wasn't sour. It confused everything in my beer related brain. It was an experience I'm not going to repeat"
}
---

