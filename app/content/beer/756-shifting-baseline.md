---json
{
    "title": "Shifting Baseline",
    "number": "756",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1237658765",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2023-01-06",
    "permalink": "beer/shifting-baseline-duration-brewing/",
    "breweries": [
        "brewery/duration-brewing/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I was neither disappointed nor impressed with this pale ale. It did a job rather well"
}
---