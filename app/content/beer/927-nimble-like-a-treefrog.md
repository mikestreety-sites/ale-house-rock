---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1382023843",
  "title": "Nimble Like A Treefrog",
  "serving": "Can",
  "rating": 4,
  "date": "2024-05-18",
  "style": "IPA - Cold",
  "abv": "6.2%",
  "breweries": [
    "brewery/full-circle-brew-co/",
    "brewery/fierce-beer/"
  ],
  "number": 927,
  "permalink": "beer/nimble-like-a-treefrog-full-circle-brew-co-fierce-beer/",
  "review": "I don't quite know what this \"cold IPA\" style is, but I don't think I like it. This beer was a bit flavourless. Almost like a Helles or other lager."
}
---
