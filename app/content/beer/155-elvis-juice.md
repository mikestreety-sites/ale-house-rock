---json
{
    "title": "Elvis Juice",
    "number": "155",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BnhRzu8APhc/",
    "date": "2018-09-09",
    "permalink": "beer/elvis-juice-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "After the last citrus beer, I was dubious about this one. Grapefruit is definitely available to taste. First sip was meh, at a 4, but the more I drank the more I liked it, to the point where I would give it a"
}
---