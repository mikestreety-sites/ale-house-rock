---json
{
    "title": "Wuff",
    "number": "612",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CZqvRouqtnY/",
    "date": "2022-02-07",
    "permalink": "beer/wuff-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "@thebeakbrewery have absolutely smashed it out the park with this one. A beautifully silky smooth IPA I could drink for hours. This is one of those beers you don&#39;t want to rush. Sipped it over a zoom call last night and it was near-perfect. Lovely to have a high scorer after a string of 7s"
}
---