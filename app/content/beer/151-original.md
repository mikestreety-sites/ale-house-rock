---json
{
    "title": "Original",
    "number": "151",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BnKEBT2lLJM/",
    "date": "2018-08-31",
    "permalink": "beer/original-butcombe-brewing-company/",
    "breweries": [
        "brewery/butcombe-brewing-company/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A classic bitter. Small glass for the groom to be tomorrow"
}
---