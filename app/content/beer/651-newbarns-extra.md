---json
{
    "title": "Newbarns Extra",
    "number": "651",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CdeT1sSqXSV/",
    "date": "2022-05-12",
    "permalink": "beer/newbarns-extra-newbarns-brewery/",
    "breweries": [
        "brewery/newbarns-brewery/"
    ],
    "tags": [],
    "review": "If this was the &quot;extra&quot; pilsner I don&#39;t think I want to try the normal one. This was exactly what you would expect from a stereotypical pilsner - fizzy with not much taste. Went well with a curry, I suppose."
}
---