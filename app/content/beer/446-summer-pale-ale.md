---json
{
    "title": "Summer Pale Ale",
    "number": "446",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CMIigHaF_Fp/",
    "date": "2021-03-07",
    "permalink": "beer/summer-pale-ale-co-op/",
    "breweries": [
        "brewery/co-op/"
    ],
    "tags": [],
    "review": "Yes, I know it isn&#39;t summer yet but in our local Co-op you have to queue past the beer so I picked this up on a whim. Light and easy drinking, would be good in a garden on a hot day."
}
---