---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1349405430",
  "title": "AUNT BESSIE'S // ROAST DINNER BROWN ALE (2023)",
  "serving": "Can",
  "rating": 4,
  "purchased": "shop/asda/",
  "date": "2024-01-18",
  "style": "Brown Ale - Other",
  "abv": "4.5%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 891,
  "permalink": "beer/aunt-bessies-roast-dinner-brown-ale-2023-northern-monk/",
  "review": "I was having a roast dinner so thought I would use it as an opportunity to see how this fared. It was the best of the collabs but still not great. It tasted like a cheap brown ale with a slight fake taste - couldn't really get much of the roast from it"
}
---

