---json
{
    "title": "Investor IPA",
    "number": "350",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CCHGcHaJrSo/",
    "date": "2020-07-01",
    "permalink": "beer/investor-ipa-sori-brewing/",
    "breweries": [
        "brewery/sori-brewing/"
    ],
    "tags": [],
    "review": "Another #latergram (need to get better at posting these when I drink them). Boring, classic IPA from my  @beer52hq box this week. Could drink a few of these and would definitely taste better if it was proper fridge cold."
}
---