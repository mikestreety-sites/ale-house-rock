---json
{
    "title": "Longboard",
    "number": "324",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CABfy2ppDZk/",
    "date": "2020-05-10",
    "permalink": "beer/longboard-kona-brewing-company/",
    "breweries": [
        "brewery/kona-brewing-company/"
    ],
    "tags": [],
    "review": "The carbonation on this beer is epic. That aside, this was the last of the 3 different @konabrewingco beers I had delivered the other week and I have mixed feelings about it. I ordered two and drank the first a week or so ago and at the time, I wasn&#39;t impressed. A bit meh. This one, on the other hand, was a bit better - it had more character and complimented my roast dinner quite well (surprisingly). Based on my findings, I will lean generously towards the latter experience with my rating. Still not as good as the &quot;Big Wave&quot; though"
}
---