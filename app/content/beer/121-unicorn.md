---json
{
    "title": "Unicorn",
    "number": "121",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BklSEYHlSfQ/",
    "date": "2018-06-28",
    "permalink": "beer/unicorn-robinsons-brewery/",
    "breweries": [
        "brewery/robinsons-brewery/"
    ],
    "tags": [],
    "review": "Had this the other day as a celebration. Can’t remember what it tasted like but can remember it was pretty good"
}
---