---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1306616179",
  "title": "Big Birdie",
  "serving": "Can",
  "rating": 6.5,
  "date": "2023-08-20",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/phantom-brewing-co/",
    "brewery/time-tide-brewing/"
  ],
  "number": 837,
  "permalink": "beer/big-birdie-phantom-brewing-co-time-tide-brewing/",
  "review": "Pretty big on taste, this DIPA. Has all the hallmarks of a good beer, but the alcohol taste just cut through a tad too much for my liking. Still drinkable and an excellent, post-gardening tipple."
}
---

