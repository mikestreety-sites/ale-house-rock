---json
{
    "title": "Lock Down",
    "number": "471",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CNxpghnl6MW/",
    "date": "2021-04-17",
    "permalink": "beer/lock-down-3-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "If you&#39;ve been following along with my Brewdog exploration, it will come of no surprise to you that I didn&#39;t particularly enjoy this &quot;Guava and Grapefruit Pilsner&quot;. Hardly any flavour at all - a weak lager with some fruit mixed in. I&#39;m also not a fan of Brewdog having 3 beers called Lock Down which are all different. 🤷‍♂️"
}
---