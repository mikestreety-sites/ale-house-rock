---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1451964725",
  "title": "Axe Edge",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/brewser-online-store/",
  "date": "2025-01-24",
  "style": "IPA - American",
  "abv": "6.8%",
  "breweries": [
    "brewery/buxton-brewery/"
  ],
  "number": 1010,
  "permalink": "beer/axe-edge-buxton-brewery/",
  "review": "This was a full-on, slap-in-the-face IPA. It tasted raw and happy and exactly what a craft IPA should be. You could taste every bit of the near-7% witj this one and it was great."
}
---

