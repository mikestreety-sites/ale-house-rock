---json
{
    "title": "Work Ethic",
    "number": "515",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CQRgCvbpHg8/",
    "date": "2021-06-18",
    "permalink": "beer/work-ethic-firebird-brewing-company/",
    "breweries": [
        "brewery/firebird-brewing-company/"
    ],
    "tags": [],
    "review": "Tidy little NEIPA from local brewery @firebirdbrewing. Great company for the first half of the England game. 5% but carries plenty of flavour"
}
---