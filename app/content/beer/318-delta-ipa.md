---json
{
    "title": "Delta IPA",
    "number": "318",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B_spYFzJHJ0/",
    "date": "2020-05-02",
    "permalink": "beer/delta-ipa-brussels-beer-project/",
    "breweries": [
        "brewery/brussels-beer-project/"
    ],
    "tags": [],
    "review": "This 6.5% Saison IPA was a fruity little number. It got a bit better as the glass went on, but I still wouldn&#39;t buy it again. I have certainly had better Saisons!"
}
---