---json
{
    "title": "Gold Bier",
    "number": "265",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/B6rHBgAJdTc/",
    "date": "2019-12-29",
    "permalink": "beer/gold-bier-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Hoppy with a tangy aftertaste. Would drink again if offered, but wouldn&#39;t select above other beers"
}
---