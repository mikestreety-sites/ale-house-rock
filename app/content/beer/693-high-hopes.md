---json
{
    "title": "High Hopes",
    "number": "693",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Cg2TBonKhwK/",
    "date": "2022-08-04",
    "permalink": "beer/high-hopes-burnt-mill-brewery/",
    "breweries": [
        "brewery/burnt-mill-brewery/"
    ],
    "tags": [],
    "review": "This was a lovely, clean, easy-going West Coast IPA. Would drink it again but wouldn&#39;t rush out to pick up any more."
}
---