---json
{
    "title": "Vienna Keller Lager",
    "number": "642",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CcbXoN6KB0E/",
    "date": "2022-04-16",
    "permalink": "beer/vienna-keller-lager-utopia-brewing/",
    "breweries": [
        "brewery/utopian-brewing-ltd/"
    ],
    "tags": [],
    "review": "After the enjoyment of the previous Utopia Brewing lager, I had high hopes for this - but ended up disappointed. It was a classic lager - bubbly and lacking in flavour. This went down quickly to get it over and done with."
}
---
