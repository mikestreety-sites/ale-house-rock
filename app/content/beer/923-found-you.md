---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1378195476",
  "title": "Found You",
  "serving": "Can",
  "rating": 9.5,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-05-05",
  "style": "IPA - Imperial / Double",
  "abv": "8%",
  "breweries": [
    "brewery/floc/",
    "brewery/two-flints-brewery/"
  ],
  "number": 923,
  "permalink": "beer/found-you-floc-two-flints-brewery/",
  "review": "Exactly what I would imagine of a DIPA from Floc. Full-bodied, thick, juicy, flavourful and a big punch at 8% which tasted more like a 5%. Would be so easy to drink too many of these"
}
---

