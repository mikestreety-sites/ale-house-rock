---json
{
    "title": "Bubba",
    "number": "789",
    "rating": "10",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1265936889",
    "serving": "Can",
    "purchased": "shop/beak-brewery-taproom/",
    "date": "2023-04-16",
    "permalink": "beer/bubba-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "This was incredible. I had to open another beer not long after starting this as I was drinking it too quickly and I wanted to appreciate it. A lovely 8% for a Sunday evening"
}
---