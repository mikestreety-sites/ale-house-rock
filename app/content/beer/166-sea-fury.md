---json
{
    "title": "Sea Fury",
    "number": "166",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BpNS2YelEZl/",
    "date": "2018-10-21",
    "permalink": "beer/sea-fury-sharps/",
    "breweries": [
        "brewery/sharps/"
    ],
    "tags": [],
    "review": "lovely amber ale from @sharpsbrewery. Great way to end a lovely weekend"
}
---