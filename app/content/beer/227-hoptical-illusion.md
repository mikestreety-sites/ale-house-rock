---json
{
    "title": "Hoptical Illusion",
    "number": "227",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/ByvdU3llYVC/",
    "date": "2019-06-15",
    "permalink": "beer/hoptical-illusion-sadlers-ales/",
    "breweries": [
        "brewery/sadlers-ales/"
    ],
    "tags": [],
    "review": "A lager, but a pleasant one. Would have gone perfectly with the BBQ we had planned (which the rain put a stop to). Light and hoppy, relatable to Hop House 13, but with a better name"
}
---