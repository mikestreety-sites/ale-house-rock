---json
{
    "title": "Gunpowder IPA",
    "number": "111",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BjQRX_rFH_0/",
    "date": "2018-05-26",
    "permalink": "beer/gunpowder-ipa-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Light, wonderfully balanced ale that easily went down and doesn’t have the whiskey undertones. A cracking drink from Innis and Gunn. Happily have plenty of these"
}
---