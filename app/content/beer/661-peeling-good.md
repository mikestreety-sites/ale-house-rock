---json
{
    "title": "Peeling Good",
    "number": "661",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CeHNdntKfEr/",
    "date": "2022-05-28",
    "permalink": "beer/peeling-good-the-hop-foundry/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "This is a classic Aldi beer in that it pours well, smells great and tastes like an anticlimax. I cracked this open and could smell the chocolate and orange straight away - as a big Terry&#39;s fan, I was looking forward to this. The taste, however, was artificial and not very orange (or chocolate)-like."
}
---