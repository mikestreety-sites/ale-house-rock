---json
{
    "title": "Ecstasy Stout",
    "number": "303",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B-5KMQxpzyJ/",
    "date": "2020-04-12",
    "permalink": "beer/ecstasy-stout-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [],
    "review": "Nice sunny Easter Sunday in the garden followed by a roast dinner called for a stout (more of a winter beer but it needed drinking!). 6.6% and you can taste it. Strong coffee/chocolate flavours as expected from a stout but lovely taste"
}
---