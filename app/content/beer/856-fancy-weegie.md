---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1320754441",
  "title": "Fancy Weegie",
  "serving": "Can",
  "rating": 7.5,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-07",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/overtone-brewing-co/",
    "brewery/fierce-beer/"
  ],
  "number": 856,
  "permalink": "beer/fancy-weegie-overtone-brewing-co-fierce-beer/",
  "review": "A nice beer. Nothing really much more to say about it really. I enjoyed it and would have it again."
}
---

