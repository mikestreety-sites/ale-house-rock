---json
{
    "title": "Bloodhound",
    "number": "418",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CKHsUiLlaXC/",
    "date": "2021-01-16",
    "permalink": "beer/bloodhound-hairy-dog-brewery/",
    "breweries": [
        "brewery/hairy-dog-brewery/"
    ],
    "tags": [],
    "review": "A massively tasteful red IPA. Packed with flavour and an incredibly smooth drink. Would definitely drink again"
}
---