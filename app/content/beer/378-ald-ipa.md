---json
{
    "title": "ALD IPA",
    "number": "378",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CHBmS2SJcnO/",
    "date": "2020-10-31",
    "permalink": "beer/ald-ipa-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "There is an excellent story behind this beer, you should go and look it up - I&#39;ve had my eye on it since it was released and I was very glad a long time friend of Ale House Rock had a can spare for me to try. It&#39;s a shame the taste didn&#39;t quite live up to the legend. I suppose it isn&#39;t that much different from Punk IPA, so if that&#39;s your jam then maybe you&#39;ll like it."
}
---