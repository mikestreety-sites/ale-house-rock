---json
{
    "title": "Cheeseburger In Paradise",
    "number": "783",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1261593471",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-04-02",
    "permalink": "beer/cheeseburger-in-paradise-sureshot-brewing/",
    "breweries": [
        "brewery/sureshot-brewing/"
    ],
    "tags": [
        "ddhpaleale"
    ],
    "review": "Sureshot are fast becoming a favourite brewery of mine. Sitting next to the likes of Beak and Deya, this DDH Pale Ale was juicy and tasty and unbelievable at 5.5%."
}
---