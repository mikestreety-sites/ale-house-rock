---json
{
    "title": "Nanny State",
    "number": "241",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B3AH3oAltmK/",
    "date": "2019-09-29",
    "permalink": "beer/nanny-state-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "Now this is a non-alcoholic beer I can get behind. No metallic after-taste and a reasonable flavour. A little bit watery but by far the best alcohol free beer I&#39;ve had. Would even choose this over some of the ales I&#39;ve had previously"
}
---