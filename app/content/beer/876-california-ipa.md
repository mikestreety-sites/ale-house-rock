---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1336142845",
  "title": "California IPA",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/asda/",
  "date": "2023-12-02",
  "style": "IPA - Session",
  "abv": "4.3%",
  "breweries": [
    "brewery/sierra-nevada-brewing-co/"
  ],
  "number": 876,
  "permalink": "beer/california-ipa-sierra-nevada-brewing-co/",
  "review": "A very average IPA, if you can call it that. Until I examined the can, I was sure I was drinking some sort of lager."
}
---

