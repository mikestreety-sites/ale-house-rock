---json
{
    "title": "Sunny Dayz",
    "number": "52",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BWBEhTaFb6x/",
    "serving": "Bottle",
    "purchased": "shop/aldi/",
    "date": "2017-07-01",
    "permalink": "beer/sunny-dayz-aldi-hogs-back-brewery/",
    "breweries": [
        "brewery/aldi/",
        "brewery/hogs-back-brewery/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A light hoppy beer that is nothing too serious and perfect chilled with a curry after a long day."
}
---