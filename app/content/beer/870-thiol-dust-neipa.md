---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1330189638",
  "title": "THIOL DUST NEIPA",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-11-11",
  "style": "IPA - New England / Hazy",
  "abv": "6.4%",
  "breweries": [
    "brewery/lost-pier-brewing/"
  ],
  "number": 870,
  "permalink": "beer/thiol-dust-neipa-lost-pier-brewing/",
  "review": "This was a lovely NEIPA. Juicy and flavoursome without being too thick. Was nice to have a flavourful beer which was less than 7%"
}
---

