---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1438699148",
  "title": "Steady Rolling Strata DIPA",
  "serving": "Can",
  "rating": 10,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-12-05",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/deya-brewing-company/"
  ],
  "number": 995,
  "permalink": "beer/steady-rolling-strata-dipa-deya-brewing-company/",
  "review": "This was an absolute banger. It had everything you would expect from a DIPA, fluffy fruity notes with an underlying pang to remind you you're drinking beer. I could drink this for days (although at 8% I doubt I would be functional). One of the best Deyas."
}
---

