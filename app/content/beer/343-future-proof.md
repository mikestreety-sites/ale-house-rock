---json
{
    "title": "Future Proof",
    "number": "343",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CBT2TPDJhqe/",
    "date": "2020-06-11",
    "permalink": "beer/future-proof-brewdog-modern-times-beer/",
    "breweries": [
        "brewery/brewdog/",
        "brewery/modern-times-beer/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "@brewdogofficial Vs @moderntimesbeer. I appreciate the brewery above isn&#39;t &quot;right&quot; but wanted to group the Brewdog Vs. beers. Now about the beer... It&#39;s great to have a big beer again. It&#39;s only a few more ml (110 I think) but it makes a big difference in the longevity of a beer. This IPA was nice, but expensive. There are hints of fruit, it was smooth, had a lovely flavour and I could imagine drinking it on the beach or relaxing in a garden. 🏅 7/10"
}
---