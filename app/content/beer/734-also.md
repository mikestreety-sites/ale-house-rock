---json
{
    "title": "Also",
    "number": "734",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1218627394",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-11-06",
    "permalink": "beer/also-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "These craft beers are more expensive than your average supermarket tipple but are so much better. It seems you often get what you pay for. Lovely light IPA but still had plenty of flavour"
}
---