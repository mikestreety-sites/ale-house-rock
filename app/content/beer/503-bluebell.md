---json
{
    "title": "Bluebell",
    "number": "503",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CPwW_5AFIXp/",
    "date": "2021-06-05",
    "permalink": "beer/bluebell-360-degree-brewing-company/",
    "breweries": [
        "brewery/360-degree-brewing-company/"
    ],
    "tags": [],
    "review": "A refreshing change from hipster (D/T)IPAs. A classic bitter from a Brighton-based micro brewery. A little watery but some great flavours hiding below the surface. Looking forward to trying some more from these brewers."
}
---