---json
{
    "title": "Ease Up I.P.A.",
    "number": "224",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bx-0ZK5FrIv/",
    "date": "2019-05-27",
    "permalink": "beer/ease-up-ipa-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "In my opinion, @adnams is up there with the best. They can seem to do no wrong with their beers. Always lovely tasting and this is no exception. Crisp, tasty and went down well"
}
---