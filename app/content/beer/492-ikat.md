---json
{
    "title": "Ikat",
    "number": "492",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CPJNSWHlxV7/",
    "date": "2021-05-21",
    "permalink": "beer/ikat-salt-beer-factory/",
    "breweries": [
        "brewery/salt-beer-factory/"
    ],
    "tags": [],
    "review": "My first foray into @saltbeerfactory (having heard plenty of good things). This very juicy DDH IPA was nice enough, but nothing I would buy again. Kind of left a dry taste in my mouth but pleasant nonetheless."
}
---