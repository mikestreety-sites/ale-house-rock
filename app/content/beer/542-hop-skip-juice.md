---json
{
    "title": "Hop, Skip & Juice",
    "number": "542",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CRw2ZEupBIq/",
    "date": "2021-07-25",
    "permalink": "beer/hop-skip-juice-vocation-brewery-manchester-marble-brewery/",
    "breweries": [
        "brewery/manchester-marble-brewery/",
        "brewery/vocation-brewery/"
    ],
    "tags": [],
    "review": "A series of beers by @vocationbrewery teaming up with other breweries. This Hazy IPA made with Manchester Marble Brewery was very enjoyable at lunchtime on a Sunday. Sweet, hoppy, juicy and a lovely haze. Perfect straight from the fridge."
}
---