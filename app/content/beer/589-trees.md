---json
{
    "title": "Trees",
    "number": "589",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CXHTGkLKq_f/",
    "date": "2021-12-05",
    "permalink": "beer/trees-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "This DIPA took me by surprise in both colour and taste. Before opening the can, I was expecting a classic @thebeakbrewery DIPA, hazy and fruity but this was neither. The taste was good, don&#39;t get me wrong. It reminded me of a classic bottled bitter and was a refreshing change from the more softer IPAs available from smaller craft breweries"
}
---