---json
{
    "title": "Lunar Lager",
    "number": "520",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CQepqsTpJyL/",
    "date": "2021-06-23",
    "permalink": "beer/lunar-lager-aldi/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "Cracked open a beer as England were playing last night. It&#39;s exactly as you would expect from a lager made by Aldi."
}
---