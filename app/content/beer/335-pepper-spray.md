---json
{
    "title": "Pepper Spray",
    "number": "335",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CAyau6rJgs0/",
    "date": "2020-05-29",
    "permalink": "beer/pepper-spray-people-like-us/",
    "breweries": [
        "brewery/people-like-us/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "An interesting concept from @mikkellerbeer and @peoplelikeusdk. An IPA brewed with black pepper. First few sips were nothing special but once I got into the groove, and took a breather, the black pepper started to really come alive. Not overwhelming, but enough for you to notice in the aftertaste. Was spot on as an accompaniment to a pepperoni pizza. 👌 It&#39;s a tough one to score as I often rate beers based on taste and if I would buy it again. I&#39;m glad I&#39;ve had this, it was a nice taste but I can&#39;t say I would reach for it if I saw it on a shelf (except maybe for someone else)"
}
---