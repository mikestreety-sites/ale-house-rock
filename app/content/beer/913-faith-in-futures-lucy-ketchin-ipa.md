---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1370813564",
  "title": "FAITH IN FUTURES // LUCY KETCHIN // IPA",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-09",
  "style": "IPA - American",
  "abv": "6.5%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 913,
  "permalink": "beer/faith-in-futures-lucy-ketchin-ipa-northern-monk/",
  "review": "This had a real unexpected kick to it, especially for 6.5%. I enjoyed it more than I expected to and would happily have one of these again."
}
---

