---json
{
    "title": "Spitfire",
    "number": "109",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BipZRBEFmUU/",
    "date": "2018-05-11",
    "permalink": "beer/spitfire-shepherd-neame/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [],
    "review": "A staple, british bitter"
}
---