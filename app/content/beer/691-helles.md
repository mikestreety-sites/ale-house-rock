---json
{
    "title": "Helles",
    "number": "691",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CgnRB6_qLNI/",
    "date": "2022-07-29",
    "permalink": "beer/helles-lost-and-grounded/",
    "breweries": [
        "brewery/lost-and-grounded/"
    ],
    "tags": [],
    "review": "Possibly one of the worst Helles beers I&#39;ve had. Fizzy but somehow managing to taste flat. There was something there, but it wasn&#39;t impressive by any means. The only thing this has going for it was that I managed to finish it."
}
---