---json
{
    "title": "Morland Old Crafty Hen",
    "number": "49",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BUZUavxllYf/",
    "serving": "Bottle",
    "date": "2017-05-22",
    "permalink": "beer/morland-old-crafty-hen-greene-king/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [
        "strongale"
    ],
    "review": "My 49th beer and a good one too. The big brother of the Old Speckled Hen. Dangerously strong and full of flavour"
}
---