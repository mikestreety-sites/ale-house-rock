---json
{
    "title": "200",
    "number": "65",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/BYv67TjlRxd/",
    "date": "2017-09-07",
    "permalink": "beer/200-palmers/",
    "breweries": [
        "brewery/palmers/"
    ],
    "tags": [],
    "review": "The last beer of my Dorset trip. This corker from @palmersbrewery is very tasty. At 5% it&#39;s bordering on dangerous but is going down well. Well worth a sip if you get chance!"
}
---