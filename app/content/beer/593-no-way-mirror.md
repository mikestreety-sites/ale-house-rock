---json
{
    "title": "No Way Mirror",
    "number": "593",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CX-7XyKKQMx/",
    "date": "2021-12-27",
    "permalink": "beer/no-way-mirror-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "A low ABV for @deyabrewery (just 4.5%) but not low in taste. Soft and smooth with plenty of fruits. A lovely start to Boxing Day beers. This seems like a great entry into Deya beers - not one of their most abusively flavourful beers, but certainly gives an idea of what they are about."
}
---