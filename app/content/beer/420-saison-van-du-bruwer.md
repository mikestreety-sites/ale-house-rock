---json
{
    "title": "Saison Van Du Bruwer",
    "number": "420",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CKKLn4RF2Ko/",
    "date": "2021-01-17",
    "permalink": "beer/saison-van-du-bruwer-de-la-senne/",
    "breweries": [
        "brewery/de-la-senne/"
    ],
    "tags": [],
    "review": "A subtle lemony summer drink which is going down quite well after a Sunday roast. Would be better on a warm evening in the garden, but in the lounge will suffice. A Saison, I feel, needs to be drunk from an &quot;elegant&quot; glass and this @maltusfaber one is one of my faves."
}
---