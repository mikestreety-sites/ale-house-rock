---json
{
    "title": "Albini",
    "number": "638",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CcQcDdLKtIy/",
    "date": "2022-04-12",
    "permalink": "beer/albini-phjala-fuerst-wiacek/",
    "breweries": [
        "brewery/fuerst-wiacek/",
        "brewery/phjala/"
    ],
    "tags": [],
    "review": "An unimpressive DIPA. Nice enough, especially at the start but was a bit bland by the end"
}
---