---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1400849037",
  "title": "Juicy & Exotic IPA (Daydreaming of Far Off Lands)",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-07-20",
  "style": "IPA - New England / Hazy",
  "abv": "5.5%",
  "breweries": [
    "brewery/modest-beer/"
  ],
  "number": 950,
  "permalink": "beer/juicy-exotic-ipa-daydreaming-of-far-off-lands-modest-beer/",
  "review": "I had this a week or so ago, so my memory is hazy. I enjoyed it, but it was a bit too yeasty for me"
}
---

