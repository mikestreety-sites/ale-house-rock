---json
{
    "title": "Shipping out of Boston",
    "number": "423",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CKZ7b0NFW5i/",
    "date": "2021-01-23",
    "permalink": "beer/shipping-out-of-boston-jacks-abbey/",
    "breweries": [
        "brewery/jacks-abbey/"
    ],
    "tags": [
        "lager"
    ],
    "review": "A lager that&#39;s this colour?! And a lager that tastes good?! Wonderfully light and lager-like with a decent flavour to boot. Went very well with the takeaway curry I had this evening, but don&#39;t think it I would have enjoyed it as much if I was drinking it solo. I do, however, wish all lagers tasted like this. Despite the good review, it is still a lager and still has lager qualities, so can&#39;t get max marks from me. If this was a lager review account instead though, then well, I&#39;d have bigger problems to worry about (like why I&#39;m running a lager review account 😉)."
}
---