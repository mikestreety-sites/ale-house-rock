---json
{
    "title": "Faith",
    "number": "348",
    "rating": "10",
    "canonical": "https://www.instagram.com/p/CBtd9fpJVka/",
    "date": "2020-06-21",
    "permalink": "beer/faith-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "Now this. This is the epitome of the craft beer. An absolutely amazing pale ale and one I would drink time and time again. This isn&#39;t my first one of these and it definitely won&#39;t be my last. Fruity, but not overbearing, refreshing and wonderfully balanced. One of the best beers I&#39;ve had to come out of a 330ml can. Bravo @northernmonk"
}
---