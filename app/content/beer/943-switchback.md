---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1397653695",
  "title": "Switchback",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-07-10",
  "style": "IPA - American",
  "abv": "6.4%",
  "breweries": [
    "brewery/loud-shirt-brewing-company/",
    "brewery/triple-point-brewing/"
  ],
  "number": 943,
  "permalink": "beer/switchback-loud-shirt-brewing-company-triple-point-brewing/",
  "review": "This had a nice flavour but was a bit thin for what I was expecting."
}
---

