---json
{
    "title": "Olympia",
    "number": "396",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CJExcaJlLI0/",
    "date": "2020-12-21",
    "permalink": "beer/olympia-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "Picked this up thinking it was an old faithful I&#39;d reviewed many moons ago, but a quick check of the index (link in bio) revealed this one was missing. I&#39;ve had many a pint of this and it never fails to tick the boxes. Not an outstanding beer, but I&#39;ve never had a bad pint. Cheers @harveysbrewery!"
}
---