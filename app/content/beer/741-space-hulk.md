---json
{
    "title": "Space Hulk",
    "number": "741",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1224427152",
    "serving": "Can",
    "purchased": "shop/tesco/",
    "date": "2022-11-26",
    "permalink": "beer/space-hulk-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "These big Beavertown beers are nice, but all seem very similar and blend into each other. It&#39;s a good enough supermarket beer"
}
---