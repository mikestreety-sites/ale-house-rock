---json
{
    "title": "Organic Ale",
    "number": "43",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BS_uTGMlT2l/",
    "serving": "Bottle",
    "date": "2017-04-17",
    "permalink": "beer/organic-ale-st-peters/",
    "breweries": [
        "brewery/st-peters/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "What better way to end this holy weekend with a beer from God himself. Had such a busy day hunting for eggs in my parents garden before being presented with this beer instead of chocolate (they look after my teeth and health my mum and dad do!). It&#39;s crisp, light and golden (not normally a colour I would go for) but good for sipping of an evening."
}
---