---json
{
    "title": "Bonnie and Blonde",
    "number": "599",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CYJswtbLwDP/",
    "date": "2021-12-31",
    "permalink": "beer/bonnie-and-blonde-loch-lomond-brewery/",
    "breweries": [
        "brewery/loch-lomond-brewery/"
    ],
    "tags": [],
    "review": "The big 600. I entered 2021 with a mere 405 reviews under my belt and I think this is my biggest year yet. This clean, crisp ale had a nice flavour to it and certainly would be one I would drink again."
}
---