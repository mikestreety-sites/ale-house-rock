---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1302665834",
  "title": "Something Good IPA",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/lidl/",
  "date": "2023-08-09",
  "style": "IPA - American",
  "abv": "5.4%",
  "breweries": [
    "brewery/fourpure-brewing-company/",
    "brewery/lidl/"
  ],
  "number": 830,
  "permalink": "beer/something-good-ipa-fourpure-brewing-co-lidl-gb/",
  "review": "I picked up a 4 pack of these on a whim while visiting Lidl and it was surprisingly good. Light, refreshing and hoppy. Perfect summer afternoon tipple (and would go very well with a BBQ)"
}
---

