---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1419140409",
  "title": "It's A Classic For A Reason",
  "serving": "Can",
  "rating": 10,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-09-21",
  "style": "IPA - American",
  "abv": "5.5%",
  "breweries": [
    "brewery/quantock-brewery/"
  ],
  "number": 973,
  "permalink": "beer/its-a-classic-for-a-reason-quantock-brewery/",
  "review": "This was beautiful. It was smooth and crisp and moreish. Will definitely be seeing if I can get some more of this somewhere. A quintessential IPA"
}
---

