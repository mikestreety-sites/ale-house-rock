---json
{
    "title": "Sunny Spells",
    "number": "635",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CbxambJqQuc/",
    "date": "2022-03-31",
    "permalink": "beer/sunny-spells-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "A classic DIPA from Deya, which is what they do best."
}
---