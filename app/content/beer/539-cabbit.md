---json
{
    "title": "Cabbit",
    "number": "539",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CRt7dIUpsnV/",
    "date": "2021-07-24",
    "permalink": "beer/cabbit-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "A double IPA I picked up from @palate.bottleshop (and waited for my @beakbrewery glass to drink). There was something not quite hitting the spot with this DIPA, but that is only because I am used to such a high calibre of Beak Brewery beers. Still a tasty tipple - even when they make the less-favourable beers they are still a decent drink 👌"
}
---