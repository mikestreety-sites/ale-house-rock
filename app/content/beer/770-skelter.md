---json
{
    "title": "Skelter",
    "number": "770",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1248683482",
    "serving": "Can",
    "purchased": "shop/unbarred-brewery/",
    "date": "2023-02-17",
    "permalink": "beer/skelter-unbarred-brewery/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Picked this up from the Unbarred brewery tap the other night when I was there. Not quite sure what a &quot;cold&quot; IPA is, but it was plenty tasty enough. Very light in colour but a cool can!"
}
---