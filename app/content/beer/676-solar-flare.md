---json
{
    "title": "Solar Flare",
    "number": "676",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/Ce9JpeTqfbg/",
    "date": "2022-06-18",
    "permalink": "beer/solar-flare-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [],
    "review": "A bit disappointed in the DIPA, to e honest. Didn&#39;t have much of the &quot;D&quot; and the &quot;IPA&quot; reminded me of an Aldi special."
}
---