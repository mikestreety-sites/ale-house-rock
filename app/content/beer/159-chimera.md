---json
{
    "title": "Chimera",
    "number": "159",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BoE2S5LFz1p/",
    "date": "2018-09-23",
    "permalink": "beer/chimera-drygate-brewing-company/",
    "breweries": [
        "brewery/drygate-brewing-company/"
    ],
    "tags": [],
    "review": "Wasn’t expecting the best from this and its a bit better than expected. Crushed can as I forgot to take the photo. 🤦‍♂️"
}
---