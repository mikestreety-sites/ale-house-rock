---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1347473706",
  "title": "Pale Ale - Big Ideas Series 10",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-01-08",
  "style": "Pale Ale - American",
  "abv": "5.6%",
  "breweries": [
    "brewery/simple-things-fermentations/"
  ],
  "number": 887,
  "permalink": "beer/pale-ale-big-ideas-series-10-simple-things-fermentations/",
  "review": "The only way I can think of describing this is by saying it's quite a \"hard\" pale ale. It was crisp, sharp and hoppy. Not my usual taste, but I enjoyed it nonetheless."
}
---

