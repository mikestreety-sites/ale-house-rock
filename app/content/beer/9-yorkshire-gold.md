---json
{
    "title": "Yorkshire Gold",
    "number": "9",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BNxQgUIA34t/",
    "serving": "Bottle",
    "purchased": "shop/marks-and-spencer/",
    "date": "2016-12-08",
    "permalink": "beer/yorkshire-gold-black-sheep-brewery-marks-and-spencer/",
    "breweries": [
        "brewery/black-sheep-brewery/",
        "brewery/marks-and-spencer/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A light, crisp beer that seems to be going down rather well. Not offensive but not the #bestbitter"
}
---