---json
{
    "title": "Beeding Mead",
    "number": "441",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CL0G0lTFJ4k/",
    "date": "2021-02-27",
    "permalink": "beer/beeding-mead-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "I feel like this review is more for Mead in general than specifically this one. I&#39;ve never had mead before so thought I would take the opportunity when I ordered my recent batch of beers from @riverside_brewery. It&#39;s very fizzy, very sweet and very light. Would happily have it as a prosecco/champagne/toast alternative drink at an occasion, but not one for drinking at home in front of the TV really. Reminded me of Appletise."
}
---