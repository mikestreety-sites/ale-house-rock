---json
{
    "title": "Happy Every After",
    "number": "288",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B9MOOggJcFO/",
    "date": "2020-03-01",
    "permalink": "beer/happy-every-after-adur-brewery/",
    "breweries": [
        "brewery/adur-brewery/"
    ],
    "tags": [],
    "review": "This IPA tasted like it belonged at a beer festival. Bottle conditioned, this flat beer went down well with great flavours, although I was expecting better based on the recent IPAs I&#39;ve had. Apparently this beer is a homage to Wills and Kate and also Harry and Meghan Sperkle 🤷‍♀️"
}
---