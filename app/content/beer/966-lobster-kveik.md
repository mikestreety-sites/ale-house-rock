---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1416520499",
  "title": "Lobster Kveik",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/padstow-tasting-rooms/",
  "date": "2024-09-11",
  "style": "Pale Ale - Other",
  "abv": "4.5%",
  "breweries": [
    "brewery/padstow-brewing-company/"
  ],
  "number": 966,
  "permalink": "beer/lobster-kveik-padstow-brewing-company/",
  "review": "A light hazy ale picked up while I was down in Cornwall a few weeks ago. Thinner than other hazys, but a nice balance of flavours and fullness makes this lovely tipple. Would be a great session beer."
}
---

