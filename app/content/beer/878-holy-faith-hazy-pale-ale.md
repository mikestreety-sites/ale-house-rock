---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1338238247",
  "title": "HOLY FAITH // HAZY PALE ALE",
  "serving": "Can",
  "rating": 3,
  "purchased": "shop/co-op-food/",
  "date": "2023-12-10",
  "style": "Non-Alcoholic Beer - Pale Ale",
  "abv": "0.5%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 878,
  "permalink": "beer/holy-faith-hazy-pale-ale-northern-monk/",
  "review": "I picked up this as I'm a fan of Northern Monk and was wondering how their 0.5% beer behaved. It's a bit \"nothing\". There wasn't a sense of beer there at all, and it reminded me of a weak squash. I won't be buying this again."
}
---

