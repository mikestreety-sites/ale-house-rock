---json
{
    "title": "Routine Bites Hard",
    "number": "569",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CT8KRnvqRsR/",
    "date": "2021-09-17",
    "permalink": "beer/routine-bites-hard-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "This is the kind of beer dreams are made of. Smooth, fruity and so very easy to drink. You forget that this tasty TIPA is packing a 10% punch - something you only get hit with when you try and do something. This beer from @deyabrewery scores a near-perfect 10. Everything ticks the box except session-ability. As easy as it would be to sit and drink this all night, I&#39;m not sure any of my internal organs could handle it."
}
---