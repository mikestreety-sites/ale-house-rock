---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1361491497",
  "title": "2nd Birthday DIPA",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-07",
  "style": "IPA - Imperial / Double",
  "abv": "8%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 903,
  "permalink": "beer/2nd-birthday-dipa-sureshot-brewing/",
  "review": "I did not realise Sureshot were only 2 years old - their beers are absolutely banging. This was a classic DIPA, it was pretty tasty - although it was missing a little bit of a punch."
}
---

