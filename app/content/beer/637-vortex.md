---json
{
    "title": "Vortex",
    "number": "637",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Cb9p1-LKPOp/",
    "date": "2022-04-05",
    "permalink": "beer/vortex-drop-project/",
    "breweries": [
        "brewery/drop-project/"
    ],
    "tags": [],
    "review": "A classic TIPA. Nothing groundbreaking but enjoyable."
}
---