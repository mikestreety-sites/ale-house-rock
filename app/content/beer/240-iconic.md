---json
{
    "title": "Iconic",
    "number": "240",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B29zcz9F2Cl/",
    "date": "2019-09-28",
    "permalink": "beer/iconic-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "This was a tasty little number from Aldi. Will definitely keep an eye out for this one as I would love to drink this again"
}
---