---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1352507291",
  "title": "Cone",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-01-31",
  "style": "IPA - American",
  "abv": "6%",
  "breweries": [
    "brewery/saint-mars-of-the-desert/"
  ],
  "number": 897,
  "permalink": "beer/cone-saint-mars-of-the-desert/",
  "review": "This very much reminded me of a traditional IPA - like the ones found in the bottles. It had a bit of husk to it, not like the craft-y, fruity IPAs. I brought back feelings of drinking in an old man pub. Unexpectedly good, this one."
}
---

