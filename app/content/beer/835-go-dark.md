---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1304768971",
  "title": "Go Dark",
  "serving": "Can",
  "rating": 5,
  "date": "2023-08-15",
  "style": "Stout - Milk / Sweet",
  "abv": "4.9%",
  "breweries": [
    "brewery/moorhouses-brewery/"
  ],
  "number": 835,
  "permalink": "beer/go-dark-moorhouses-brewery/",
  "review": "A stout which wasn't horrible to drink. It wasn't sickly, as some chocolate stouts can be, but it wasn't \"incredible\" either. Very \"average\""
}
---

