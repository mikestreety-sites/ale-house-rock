---json
{
    "title": "Golden Torch",
    "number": "391",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CIY9gpAJNHB/",
    "date": "2020-12-04",
    "permalink": "beer/golden-torch-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "A classic disappointing Aldi beer. Flavourless and bland."
}
---