---json
{
    "title": "Lost",
    "number": "449",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CMVih20Fgim/",
    "date": "2021-03-12",
    "permalink": "beer/lost-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "2 months ago Brewdog ran a campaign whereby they promised everyone in the UK could get free beer. A 4 pack of their Lost Lager finally rocked up yesterday, so I proper battle tested it this evening with 2 cans. Not really an evening sipping beer, but certainly did the job. One of the few lagers I would happily opt for over some of the other beers I&#39;ve reviewed. Light and crisp but with plenty of flavour."
}
---