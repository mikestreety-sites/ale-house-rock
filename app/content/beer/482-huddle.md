---json
{
    "title": "Huddle",
    "number": "482",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/COXHSRpFoXM/",
    "date": "2021-05-02",
    "permalink": "beer/huddle-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "A lovely IPA from @gipsyhillbrew, given to me by @laurzstreet. Super tasty and easy drinking. Gipsy Hill consistently do excellent drinks and this is no exception."
}
---