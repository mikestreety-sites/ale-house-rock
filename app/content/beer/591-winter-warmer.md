---json
{
    "title": "Winter Warmer",
    "number": "591",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CX4XhOoKGwB/",
    "date": "2021-12-24",
    "permalink": "beer/winter-warmer-eagle-brewery/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [],
    "review": ""
}
---