---json
{
    "title": "Desert Haze",
    "number": "334",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CAv2EQqp-Wg/",
    "date": "2020-05-28",
    "permalink": "beer/desert-haze-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "First beer out of the @beer52hq delivery was this lovely little number from @gipsyhillbrew. Light, fruity and a fantastic beer to accompany an end-of-the-day meeting. Would definitely pick this one up again if I saw it, although unfortunately it&#39;s a collab with the Beer52, so doubt it will be in any shops!"
}
---