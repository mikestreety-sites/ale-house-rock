---json
{
    "title": "Cannon Ball",
    "number": "688",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CgfTpmEq8ie/",
    "date": "2022-07-26",
    "permalink": "beer/cannon-ball-magic-rock-brewing/",
    "breweries": [
        "brewery/magic-rock-brewing/"
    ],
    "tags": [],
    "review": "This was a tasty, full-bodied IPA from a brewery often found in a supermarket. The 7.4% caught me off guard, but I would happily have one of these again. It&#39;s refreshing to have a hoppy IPA like this in a  330ml can."
}
---