---json
{
    "title": "Said & Done",
    "number": "367",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CEH61rTJQS4/",
    "date": "2020-08-20",
    "permalink": "beer/said-done-track/",
    "breweries": [
        "brewery/track-brewing-company/"
    ],
    "tags": [],
    "review": "I&#39;ve seen plenty of @trackbrewingco beers floating around my timeline so I was excited to finally give one a go. Light, fruity, smooth and easy drinking. Certainly looking forward to trying more of their beers."
}
---