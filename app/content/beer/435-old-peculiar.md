---json
{
    "title": "Old Peculiar",
    "number": "435",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CLgo-sOFnGX/",
    "date": "2021-02-20",
    "permalink": "beer/old-peculiar-theakston/",
    "breweries": [
        "brewery/theakston/"
    ],
    "tags": [],
    "review": "A lovely full bodied old ale which has a hint of sweetness. Was a perfect way of ending an evening and hit the spot as a post-dinner tipple. This was another beer that I&#39;ve had countless times but never reviewed!"
}
---