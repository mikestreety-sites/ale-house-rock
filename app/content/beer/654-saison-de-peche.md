---json
{
    "title": "Saison de Pêche",
    "number": "654",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CdnVmkmq1q0/",
    "date": "2022-05-16",
    "permalink": "beer/saison-de-peche-burning-sky/",
    "breweries": [
        "brewery/burning-sky/"
    ],
    "tags": [],
    "review": "I think, based on pound per ml, this is the most expensive.beer I&#39;ve ever bought. It was also the mod challenging to open as it had a cork and made me realise I don&#39;t have a bottle opener 😆 As for the actual beer, Saisons have a particular hard-to-describe taste anyway (is it hoppy?). This one was particularly fruity (as expected) and was enjoyable. It was a great beer to share with a friend, sipping it slowly and catching up. This would go great with a picnic or similar. It&#39;s certainly a &quot;special occasion&quot; beer."
}
---