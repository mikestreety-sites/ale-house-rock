---json
{
    "title": "Christmas Ale",
    "number": "88",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BdV2kXGl6JG/",
    "date": "2017-12-30",
    "permalink": "beer/christmas-ale-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [],
    "review": "This 7.5% power house is definitely a sipper. So far taken an hour to drink half of it. It’s a heavy Christmas day in a glass, but by golly it’s wonderful. A half pint is more than enough and you certainly wouldn’t want it on a night out!"
}
---