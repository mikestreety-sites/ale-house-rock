---json
{
    "title": "Mangoes on the run",
    "number": "297",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B-iBvgXpQIB/",
    "date": "2020-04-03",
    "permalink": "beer/mangoes-on-the-run-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "This was surprisingly better than I expected. I&#39;m not really a fan of fruit beers, but this one surprised me. I&#39;m not sure I&#39;d actively seek it out again, but I won&#39;t say no if offered at a friend&#39;s house (if I&#39;m ever allowed around a friend&#39;s house again)"
}
---