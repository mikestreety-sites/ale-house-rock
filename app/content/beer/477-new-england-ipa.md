---json
{
    "title": "10 4 Brewing - New England IPA",
    "number": "477",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/COKqjb4FIV4/",
    "date": "2021-04-27",
    "permalink": "beer/new-england-ipa-10-4-brewing/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "Bought this one from Aldi a couple of days ago. Super smooth drinking and, for the price is a very good NEIPA. It&#39;s never going to be as good as one of the small brewery New England IPAs, but you can&#39;t ignore that it costs a third of the price. Will happily drink this again (and will pick a few more up next time I&#39;m in store) There is certainly a &quot;mass produced&quot; aftertaste to it, but it&#39;s not enough to put me off. The best 10 4 Brewing (Aldi&#39;s own) beer I&#39;ve had"
}
---