---json
{
    "title": "NEIPA the Cosmic Turtle",
    "number": "709",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1198674446",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-09-04",
    "permalink": "beer/neipa-the-cosmic-turtle-lowtide-brewing-co/",
    "breweries": [
        "brewery/lowtide-brewing-co/"
    ],
    "tags": [
        "neipa",
        "alcoholfree"
    ],
    "review": "Picked this up when I saw it said NEIPA, but I didn&#39;t see it was an alcohol free beer until later. Not really any better or distinguishable from any other 0% beer. Had that tang that all beers of this type do. Unimpressive really."
}
---