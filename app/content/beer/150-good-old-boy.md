---json
{
    "title": "Good Old Boy",
    "number": "150",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/Bm6slYulyt8/",
    "date": "2018-08-25",
    "permalink": "beer/good-old-boy-west-berkshire-brewery/",
    "breweries": [
        "brewery/renegade-brewery/"
    ],
    "tags": [],
    "review": "When I started drinking, I used to have a pint of lager and my dad would show so much disappointment on his face. When I finally got into ales in my twenties, I would drink the light hoppy floral numbers. This beer was bought for my 30th birthday and it is hard to beat. Full bodied bitter with a lovely taste. I’ll be onto stouts next decade 😉 fantastic beer. More please!"
}
---