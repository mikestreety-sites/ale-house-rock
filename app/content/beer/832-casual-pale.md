---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1303369951",
  "title": "Casual Pale",
  "serving": "Can",
  "rating": 6.5,
  "purchased": "shop/lidl/",
  "date": "2023-08-11",
  "style": "Pale Ale - Other",
  "abv": "4%",
  "breweries": [
    "brewery/salt-beer-factory/"
  ],
  "number": 832,
  "permalink": "beer/casual-pale-salt/",
  "review": "Crisp and refreshing, this pale ale was pretty good for the price. Nothing to rave about but might pick another up if I'm in Lidl again."
}
---

