---json
{
    "title": "Never Dug Disco",
    "number": "656",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CdwUnHWKdwR/",
    "date": "2022-05-19",
    "permalink": "beer/never-dug-disco-cloak-and-dagger/",
    "breweries": [
        "brewery/cloak-and-dagger/"
    ],
    "tags": [],
    "review": "I&#39;ve heard a lot of good things about Cloak + Dagger but this NEIPA was disappointing. It was lacking the big punch of flavour I&#39;ve come to expect and had a bit of an odd aftertaste. I&#39;ll try a few more of their beers before I pass judgement but I won&#39;t be buying this again."
}
---