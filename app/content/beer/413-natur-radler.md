---json
{
    "title": "Natur Radler",
    "number": "413",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CJ65eLylKwE/",
    "date": "2021-01-11",
    "permalink": "beer/natur-radler-paulaner/",
    "breweries": [
        "brewery/paulaner/"
    ],
    "tags": [],
    "review": "As per Ale House lockdown rules, when a beer delivery turns up, one must be drunk that evening. As it&#39;s only a Monday, I picked this 2.5% Radler (shandy) from the lot. It&#39;s exactly as you would expect from looking at the can: a slightly alcoholic lemonade &amp; does not taste at all like a beer (so much so my beer-hating wife said &quot;it was nice&quot;) so I&#39;m tentative about its appearance on this feed. Nice thirst quenching Monday night drink"
}
---