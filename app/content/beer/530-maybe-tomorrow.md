---json
{
    "title": "Maybe Tomorrow",
    "number": "530",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CRMfJWJphcN/",
    "date": "2021-07-11",
    "permalink": "beer/maybe-tomorrow-good-things-brewing-company-northern-monk/",
    "breweries": [
        "brewery/good-things-brewing-company/",
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "I&#39;ve heard plenty of good things about @goodthingsbrewingco, so when I saw they&#39;d made my favourite type of beer, with one of my favourite breweries (@northernmonk), sold at one of my favourite shops (@palate.bottleshop) I had to pick one of these up - it did not disappoint. Plenty of depth of flavour with this one - you can taste the 8%. It&#39;s certainly a special beer - not one for a drinking session or a regular tipple, but went down very nicely last night while playing a game."
}
---