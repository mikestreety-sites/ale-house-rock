---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1307225893",
  "title": "Koh Lab",
  "serving": "Can",
  "rating": 7,
  "date": "2023-08-23",
  "style": "IPA - New England / Hazy",
  "abv": "5.8%",
  "breweries": [
    "brewery/silver-rocket-brewing/"
  ],
  "number": 838,
  "permalink": "beer/koh-lab-silver-rocket-brewing/",
  "review": "A pretty good NEIPA, but nothing outstanding."
}
---

