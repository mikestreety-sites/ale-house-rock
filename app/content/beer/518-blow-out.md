---json
{
    "title": "Blow Out",
    "number": "518",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CQWlFYTJWVe/",
    "date": "2021-06-20",
    "permalink": "beer/blow-out-mikkeller/",
    "breweries": [
        "brewery/mikkeller/"
    ],
    "tags": [],
    "review": "Happy Father&#39;s Day all! My wife took my 2 1/2 year old into a shop and got him to pick out some beers for me. This is the first one and a beaut. Light and easy drinking, one of the best @mikkellerbeer beers I&#39;ve had."
}
---