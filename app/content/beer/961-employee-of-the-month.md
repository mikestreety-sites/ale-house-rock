---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1413467697",
  "title": "Employee of the Month",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-08-31",
  "style": "Stout - Milk / Sweet",
  "abv": "5%",
  "breweries": [
    "brewery/pretty-decent-beer-co/"
  ],
  "number": 961,
  "permalink": "beer/employee-of-the-month-pretty-decent-beer-co/",
  "review": "A pretty smooth stout with hints of coffee. Not too thick so it wasn't too heavy to drink. Hot the spot."
}
---

