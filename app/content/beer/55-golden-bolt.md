---json
{
    "title": "Golden Bolt",
    "number": "55",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BWiGRT5FcHJ/",
    "serving": "Bottle",
    "date": "2017-07-14",
    "permalink": "beer/golden-bolt-steam-box-brewery/",
    "breweries": [
        "brewery/steam-box-brewery/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A light golden crisp Ale. Nothing to shout about, not disappointing though!"
}
---