---json
{
    "title": "Victory Ale",
    "number": "179",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/Bq-I5lKAdvg/",
    "date": "2018-12-04",
    "permalink": "beer/victory-ale-batemans/",
    "breweries": [
        "brewery/batemans/"
    ],
    "tags": [],
    "review": "Lovely taste when you first take a sip but then an appalling after-taste bombards your mouth. Managed to finish it but I do not know how."
}
---