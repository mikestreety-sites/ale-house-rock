---json
{
    "title": "Sussex Golden Ale",
    "number": "26",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BOcKdZDgBTV/",
    "date": "2016-12-25",
    "permalink": "beer/sussex-golden-ale-hepworth-co-brewers-marks-and-spencer/",
    "breweries": [
        "brewery/hepworth-co-brewers/",
        "brewery/marks-and-spencer/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Light, tasty and perfect for a lunchtime, pre roast sip. Great last beer"
}
---