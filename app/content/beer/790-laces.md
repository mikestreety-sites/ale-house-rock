---json
{
    "title": "Laces",
    "number": "790",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1267539938",
    "serving": "Can",
    "purchased": "shop/beak-brewery-taproom/",
    "date": "2023-04-22",
    "permalink": "beer/laces-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A quaffable IPA made by two very respectable breweries. Light and easy-drinking - it went down very well with a burger"
}
---