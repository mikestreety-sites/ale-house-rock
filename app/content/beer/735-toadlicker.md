---json
{
    "title": "Toadlicker",
    "number": "735",
    "rating": "10",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1218889482",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-11-07",
    "permalink": "beer/toadlicker-hand-brew-co/",
    "breweries": [
        "brewery/hand-brew-co/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "Wow, if licking toads tastes like this then I&#39;m a covert. A perfect pale ale - fruity without being like a juice drink, a twang of bitter hops, light and so very moreish. Spot on"
}
---