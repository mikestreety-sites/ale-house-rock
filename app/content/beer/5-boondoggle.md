---json
{
    "title": "Boondoggle",
    "number": "5",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/BNmtp1MAA33/",
    "serving": "Bottle",
    "date": "2016-12-04",
    "permalink": "beer/boondoggle-ringwood-brewery/",
    "breweries": [
        "brewery/ringwood-brewery/"
    ],
    "tags": [
        "blondeale"
    ],
    "review": "Tasty. A tasty tasty drinkable beer. Give me more"
}
---