---json
{
    "title": "Wild Wader",
    "number": "30",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BP0ktzQAOue/",
    "serving": "Bottle",
    "date": "2017-01-28",
    "permalink": "beer/wild-wader-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "I took a break from home drinking for (most of) January so sorry for the radio silence, although I ended up in the pub more often. This one has been in my cupboard for a month and I&#39;ve been waiting to crack it open. It&#39;s a tasty, tasty beer - hopping on the heels of the Fursty Ferret (the best bottled beer) from the same brewer. I would happily buy this again. Thoroughly recommended! Slightly citrusy with a mix of hops"
}
---