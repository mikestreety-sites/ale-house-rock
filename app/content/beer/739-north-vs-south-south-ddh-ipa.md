---json
{
    "title": "NORTH VS SOUTH // SOUTH DDH IPA",
    "number": "739",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1222453205",
    "serving": "Can",
    "purchased": "shop/tesco/",
    "date": "2022-11-19",
    "permalink": "beer/north-vs-south-south-ddh-ipa-brew-by-numbers-staggeringly-good-yonder-brewing-blending/",
    "breweries": [
        "brewery/brew-by-numbers/",
        "brewery/staggeringly-good/",
        "brewery/yonder-brewing-blending/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "Now this is something special. Bought from a supermarket but tasted like a micro-brew. Dangerous, as you couldn&#39;t really taste the 7%, nice and light with plenty of flavour. An easily accessible DDH IPA"
}
---