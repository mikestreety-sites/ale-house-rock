---json
{
    "title": "Costa Rican Coffee Extra Porter",
    "number": "323",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CABetL-pgr7/",
    "date": "2020-05-10",
    "permalink": "beer/costa-rican-coffee-extra-porter-buxton-brewery/",
    "breweries": [
        "brewery/buxton-brewery/"
    ],
    "tags": [
        "porter"
    ],
    "review": "For 7.4% I was expecting this beer to pack a punch - instead it floated like a butterfly and stung like a bee. In other words, you couldn&#39;t taste the high alcohol volume of this monster (although I had already had a few, so maybe it had less of an impact). Subtle flavours, but complimentary as a &quot;dessert beer&quot; to the ice cream I had late last night. Surprisingly tasty without being too heavy."
}
---