---json
{
    "title": "Yakima Red",
    "number": "147",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BmtshvUlauT/",
    "date": "2018-08-20",
    "permalink": "beer/yakima-red-meantime-brewing-company/",
    "breweries": [
        "brewery/meantime-brewing-company/"
    ],
    "tags": [],
    "review": "Keeping me company while playing a game of Pandemic. A classic craft ale taste. Drinkable"
}
---