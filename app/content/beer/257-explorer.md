---json
{
    "title": "Explorer",
    "number": "257",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B5V_kYgFvtE/",
    "date": "2019-11-26",
    "permalink": "beer/explorer-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "Lovely light crisp ale I picked up from @adnamsnorwich while on holiday this weekend. Had to stop myself from buying the whole shop! Would love to have this again, becoming a massive fan of @adnams brewery. Beer went down very easily"
}
---