---json
{
    "title": "Tangmere Tower",
    "number": "436",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CLhvWBXlwJO/",
    "date": "2021-02-20",
    "permalink": "beer/tangmere-tower-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "This was another one I had two of, both tasting wildly different. The first one was a 4 or 5, but I enjoyed this one a lot more. Made me feel like I was having my nachos in a country pub. A classic ale"
}
---