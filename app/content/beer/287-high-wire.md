---json
{
    "title": "High Wire",
    "number": "287",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/B9KjMcxpWJs/",
    "date": "2020-02-29",
    "permalink": "beer/high-wire-magic-rock-brewing/",
    "breweries": [
        "brewery/magic-rock-brewing/"
    ],
    "tags": [],
    "review": "This beer was as disappointing as the hipster design of the can suggests. Left a dry taste in my mouth (had to have a glass of water) and was very hoppy. I&#39;m sure Brewdog fans would enjoy this, but it&#39;s not for me. On a related note, if you ever want to search for a beer or brewery, I launched a website allowing you to do just that (link in bio)"
}
---