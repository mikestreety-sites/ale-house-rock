---json
{
    "title": "Eternal Sunshine",
    "number": "678",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CfUL-gSqd1O/",
    "date": "2022-06-27",
    "permalink": "beer/eternal-sunshine-northern-monk-fieldwork-brewing/",
    "breweries": [
        "brewery/fieldwork-brewing/",
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "This was my second attempt at reviewing this beer after the first one got spilt over the worktop - slipping out of my washing-up-gloved fingers. Northern Monk will always have a special place in my heart as they were effectively the gateway drink to craft beers. Without them, I don&#39;t think I would have discovered half the beers that I have. This was a nice, subtle DDH IPA. Lots of gentle flavours and perfect for someone looking for a first &quot;craft&quot; beer. For me, it was lacking a punch but was still enjoyable"
}
---