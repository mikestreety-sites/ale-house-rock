---json
{
    "title": "Endeavour",
    "number": "548",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CSNaay3qGU6/",
    "date": "2021-08-05",
    "permalink": "beer/endeavour-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [],
    "review": "Going back to my roots with a fridge full of bitter and this is a great way to kick it all off. A small batch brew from @st_austell_brewery. Fruity, flowery, summery. Would have suited an evening in a pub garden. Immediately hits you with drink-ability but there is something not quite hitting the mark. Still a good pint."
}
---