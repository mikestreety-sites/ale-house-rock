---json
{
    "title": "Salted Caramel Chocolate Donut",
    "number": "442",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CL1RtP-FLZq/",
    "date": "2021-02-28",
    "permalink": "beer/salted-caramel-chocolate-donut-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [
        "stout"
    ],
    "review": "This @unbarredbrewery beer grew on me. The first few sips came after the mead, so my mouth wasn&#39;t quite sure what to do with all that information, but as the drink went on it got better and better. Mrs Ale House provided me with a brownie and caramel ice cream halfway through this can which paired nicely - it&#39;s not often you find a beer that works well with dessert. A bit too much of a &quot;salt&quot; taste (I know it&#39;s salted caramel) for my liking but still pleasurable."
}
---