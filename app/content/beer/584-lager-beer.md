---json
{
    "title": "Lager Beer",
    "number": "584",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CVp_-rdqPL2/",
    "date": "2021-10-30",
    "permalink": "beer/lager-beer-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [],
    "review": "My first beer of October after a few weeks rest (and after today I&#39;ll be back off the wagon). A light, tasty beer from the brewer known for brewing  in whiskey barrels. Hoppy and flavoursome, would definitely have this again"
}
---