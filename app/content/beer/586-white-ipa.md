---json
{
    "title": "White IPA",
    "number": "586",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CVqkA4KKICj/",
    "date": "2021-10-30",
    "permalink": "beer/white-ipa-big-hug-brewing/",
    "breweries": [
        "brewery/big-hug-brewing/"
    ],
    "tags": [],
    "review": "A wheat beer bodied IPA, apparently. Kind of went down without a hassle but can&#39;t really remember it."
}
---