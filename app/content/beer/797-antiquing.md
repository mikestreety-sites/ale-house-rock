---json
{
    "title": "Antiquing?",
    "number": "797",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1273167926",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2023-05-11",
    "permalink": "beer/antiquing-gravity-well-brewing-co/",
    "breweries": [
        "brewery/gravity-well-brewing-co/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "As expected, a high quality, highly enjoyable NEIPA. I would happily drink (and buy) again, but would not hunt it down"
}
---