---json
{
    "title": "Stout About It",
    "number": "519",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CQeoUPGpxj_/",
    "date": "2021-06-23",
    "permalink": "beer/stout-about-it-the-hop-foundry/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "A slightly watery stout from the Aldi own-brand brewers. Taste was there, just a bit weak"
}
---