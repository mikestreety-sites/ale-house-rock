---json
{
    "title": "Fake Empire",
    "number": "452",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/CMk42P4pB-9/",
    "date": "2021-03-18",
    "permalink": "beer/fake-empire-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "This beer was as disappointing as I was expecting. I don&#39;t tend to buy &quot;sour&quot; beers but this came in the Lockdown kit from Brewdog, so I had to try it. First taste is watery, followed by the sour hit. After swallowing, you get a hint of a &quot;cool&quot; breath - much like an Airwaves chewing gum. It got better as it went on but I still couldn&#39;t finish it. This beer did, however, confirm that my non-drinking wife does find sour beers &quot;ok&quot;."
}
---