---json
{
    "title": "More Wheat",
    "number": "536",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CRgUYmtFe_R/",
    "date": "2021-07-19",
    "permalink": "beer/more-wheat-verdant-brewing-company-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/verdant-brewing-company/"
    ],
    "tags": [],
    "review": "The other side of the two IPAs brewed by @verdantbrew and @thebeakbrewery. Thought it would only be fair to taste them back to back. This one had the same smoothness and full flavour as the More Oats beer did, but I much preferred the taste of this one. I can&#39;t quite explain what it was or why. Interesting experiment to brew the two beers, but seems I am a wheat man after all - I&#39;m glad I got to taste the two next to each other."
}
---