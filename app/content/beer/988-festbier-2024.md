---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1432293765",
  "title": "Festbier 2024",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-11-09",
  "style": "Festbier",
  "abv": "5.6%",
  "breweries": [
    "brewery/utopian-brewing-ltd/"
  ],
  "number": 988,
  "permalink": "beer/festbier-2024-utopian-brewing-ltd/",
  "review": "A light, crisp, helles. It has a bit more taste which you don't expect from this style."
}
---
