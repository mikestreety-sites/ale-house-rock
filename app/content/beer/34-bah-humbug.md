---json
{
    "title": "Bah Humbug",
    "number": "34",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BQYfWZKglPR/",
    "serving": "Bottle",
    "date": "2017-02-11",
    "permalink": "beer/bah-humbug-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [
        "winterale"
    ],
    "review": "Yes, I know this is a Christmas beer. But due to the &quot;no home drinking&quot; January it&#39;s only just been opened. It tastes like Christmas (which I suppose is a good thing). It&#39;s a slow sipper but tasty"
}
---