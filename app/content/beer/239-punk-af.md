---json
{
    "title": "Punk AF",
    "number": "239",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/B22VQHMlJqW/",
    "date": "2019-09-25",
    "permalink": "beer/punk-af-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "A request from a die-hard fan, although I wish they hadn&#39;t asked. A non-alcoholic beer from the kings of mainstream hipster. Tastes like any other disappointing non-alcoholic beer out there"
}
---