---json
{
    "title": "OPEN AMBER",
    "number": "701",
    "rating": "7.5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1192751026",
    "date": "2022-08-18",
    "permalink": "beer/open-amber-birrificio-agricolo-baladin-baladin-indipendente-italian-farm-brewery/",
    "breweries": [
        "brewery/birrificio-agricolo-baladin-baladin-indipendente-italian-farm-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "A present from my sister from her recent trip to Italy. Crisp and light, with a twang - it tasted like a classic IPA. Not staggering, but tasty enough."
}
---