---json
{
    "title": "Hallucination Brune",
    "number": "184",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BrWHyYeASMy/",
    "date": "2018-12-13",
    "permalink": "beer/hallucination-brune-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [],
    "review": "I only found out about @loudshirtbeer today, so had to hunt down one of their beers. What a fantastic pint. Well flavoured without being overbearing. Looking forward to trying out some of their other ones!"
}
---