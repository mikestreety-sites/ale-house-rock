---json
{
    "title": "Midway",
    "number": "529",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CRI-j3QKA_P/",
    "date": "2021-07-10",
    "permalink": "beer/midway-goose-island/",
    "breweries": [
        "brewery/goose-island/"
    ],
    "tags": [],
    "review": "The other half to the £8 for 8 beers off Amazon. It&#39;s a similar beer to the IPA in different packaging. Easy drinking but nothing special."
}
---