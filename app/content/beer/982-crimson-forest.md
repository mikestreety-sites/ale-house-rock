---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1425622700",
  "title": "Crimson Forest",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-10-14",
  "style": "Porter - Other",
  "abv": "5.6%",
  "breweries": [
    "brewery/vibrant-forest-brewery/"
  ],
  "number": 982,
  "permalink": "beer/crimson-forest-vibrant-forest-brewery/",
  "review": "I was slightly disappointed with this porter. It was smooth and inoffensive but didn't really have much flavour. No chocolate or coffee notes.or anything distinctive."
}
---
