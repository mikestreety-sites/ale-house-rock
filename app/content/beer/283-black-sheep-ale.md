---json
{
    "title": "Black Sheep Ale",
    "number": "283",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B84jMEIpicC/",
    "date": "2020-02-22",
    "permalink": "beer/black-sheep-ale-black-sheep-brewery/",
    "breweries": [
        "brewery/black-sheep-brewery/"
    ],
    "tags": [],
    "review": "This was one of those beers I was sure I&#39;d reviewed already. A regular occurrence in my fridge and, once you&#39;ve had a sip, I&#39;m sure you would understand. Tasty, full-bodied and full of flavour. A definite &quot;buy again&quot;"
}
---