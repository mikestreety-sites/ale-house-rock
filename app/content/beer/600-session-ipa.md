---json
{
    "title": "Session IPA",
    "number": "600",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CYOEVTgK2tf/",
    "date": "2022-01-02",
    "permalink": "beer/session-ipa-hiver/",
    "breweries": [
        "brewery/hiver/"
    ],
    "tags": [],
    "review": "New year, new beer. This was darker than I expected with a lovely full-bodied flavour. Will definitely be keeping my eye out for this again - it&#39;s a shame it was only a.small can!"
}
---