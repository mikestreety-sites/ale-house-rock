---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1397653193",
  "title": "MARMOSET SESSION PALE",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-07-10",
  "style": "Pale Ale - New England / Hazy",
  "abv": "3.8%",
  "breweries": [
    "brewery/missing-link-brewing/"
  ],
  "number": 942,
  "permalink": "beer/marmoset-session-pale-missing-link-brewing/",
  "review": "Pretty tasty session."
}
---

