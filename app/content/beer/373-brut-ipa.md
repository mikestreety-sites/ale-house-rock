---json
{
    "title": "10 4 Brewing - Brut IPA",
    "number": "373",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CGNk5GIlncQ/",
    "date": "2020-10-11",
    "permalink": "beer/brut-ipa-10-4-brewing/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "An interesting beer from Aldi. Very much trying to jump onto the hipster beer bandwagon and they do a pretty good job at holding on. Good flavours and easy drinking, just not as morish as the big boys. Will be keeping my eye out if ever I find myself there again."
}
---