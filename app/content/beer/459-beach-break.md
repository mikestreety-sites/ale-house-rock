---json
{
    "title": "Beach Break",
    "number": "459",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CM8MCcHpLUF/",
    "date": "2021-03-27",
    "permalink": "beer/beach-break-harbour/",
    "breweries": [
        "brewery/harbour/"
    ],
    "tags": [],
    "review": "This was an odd one. I was expecting an old fashioned ale, but a pale ale with a lemon twist. Took me a few sips to get into it, but once I found the groove I was able to enjoy it. Definitely wouldn&#39;t have it again, but was palatable for the evening."
}
---