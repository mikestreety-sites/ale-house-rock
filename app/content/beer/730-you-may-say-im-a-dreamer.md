---json
{
    "title": "You May Say I’m A Dreamer",
    "number": "730",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1215980682",
    "serving": "Can",
    "purchased": "shop/beer-no-evil/",
    "date": "2022-10-29",
    "permalink": "beer/you-may-say-im-a-dreamer-arbor-ales/",
    "breweries": [
        "brewery/arbor-ales/"
    ],
    "tags": [
        "redale"
    ],
    "review": "I&#39;ve had a string of mediocre beers and was hoping this one would break the spell. Maybe I put too much pressure on this one, but it had too many hops and not enough juice for my liking. Still drinkable but not the juicy amber ale I was expecting"
}
---