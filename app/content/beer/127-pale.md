---json
{
    "title": "The Rev. James Pale",
    "number": "127",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BlD40FGFKuu/",
    "date": "2018-07-10",
    "permalink": "beer/pale-the-rev-james/",
    "breweries": [
        "brewery/brains/"
    ],
    "tags": [],
    "review": "Another one from The Rev James. Crisp and light but a bit watery"
}
---