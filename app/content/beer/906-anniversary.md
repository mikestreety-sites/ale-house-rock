---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1363515176",
  "title": "Anniversary",
  "serving": "Can",
  "rating": 10,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-15",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/phantom-brewing-co/"
  ],
  "number": 906,
  "permalink": "beer/anniversary-phantom-brewing-co/",
  "review": "Now this was a beer I would actively hunt down. It was incredible. Soft and easy and flavourful and everything you could ask for in an IPA. 6.5% is on the high side for a session, but it smashes out the flavours without being too high %"
}
---

