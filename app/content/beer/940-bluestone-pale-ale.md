---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1392226186",
  "title": "Bluestone Pale Ale",
  "serving": "Bottle",
  "rating": 8,
  "purchased": "shop/bluestone-national-park-resort/",
  "date": "2024-06-22",
  "style": "Pale Ale - English",
  "abv": "4.1%",
  "breweries": [
    "brewery/glamorgan-brewing-co/"
  ],
  "number": 940,
  "permalink": "beer/bluestone-pale-ale-glamorgan-brewing-co/",
  "review": "One of the smoothest pale ales I've had in a long time, picked up a few of these. Good, old, traditional ale"
}
---

