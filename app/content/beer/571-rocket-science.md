---json
{
    "title": "Rocket Science",
    "number": "571",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CT-MQlUKcj_/",
    "date": "2021-09-18",
    "permalink": "beer/rocket-science-magnify-brewing/",
    "breweries": [
        "brewery/magnify-brewing-company/"
    ],
    "tags": [],
    "review": "I can&#39;t say I&#39;ve had an &quot;imperial India Pale Ale&quot; before, but it seemed to have all the characteristics of a DIPA. Very pleasant indeed."
}
---
