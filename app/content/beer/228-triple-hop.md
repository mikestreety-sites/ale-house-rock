---json
{
    "title": "Triple Hop",
    "number": "228",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/By-3IHplWrj/",
    "date": "2019-06-21",
    "permalink": "beer/triple-hop-maltus-faber/",
    "breweries": [
        "brewery/maltus-faber/"
    ],
    "tags": [],
    "review": "Last of the Genovese beers. Very hoppy, but unlike the hoppy English beers, this one was still tasty. Not into the light hopped beers so much these days, but this one still had some lovely notes"
}
---