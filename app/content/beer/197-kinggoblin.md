---json
{
    "title": "KingGoblin",
    "number": "197",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Bsql3QRAICW/",
    "date": "2019-01-15",
    "permalink": "beer/kinggoblin-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "You can taste the 6.6% of this monster. Full of flavour but it’s an evening sipper"
}
---