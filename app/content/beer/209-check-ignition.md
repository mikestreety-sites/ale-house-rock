---json
{
    "title": "Check Ignition",
    "number": "209",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BvFYzcvlS5d/",
    "date": "2019-03-16",
    "permalink": "beer/check-ignition-lost-and-grounded/",
    "breweries": [
        "brewery/lost-and-grounded/"
    ],
    "tags": [
        "stout",
        "porter"
    ],
    "review": "A Porter which apparently has raspberry notes - although they were lacking. Finished the pint but was underwhelmed"
}
---