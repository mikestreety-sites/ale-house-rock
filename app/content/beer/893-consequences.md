---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1350377907",
  "title": "Consequences",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-01-21",
  "style": "IPA - New England / Hazy",
  "abv": "6.3%",
  "breweries": [
    "brewery/deya-brewing-company/",
    "brewery/the-queer-brewing-project/"
  ],
  "number": 893,
  "permalink": "beer/consequences-deya-brewing-company-the-queer-brewing-project/",
  "review": "This was Deya on top form. Juicy and full of flavour. It wasn't too thick but still had a lovely depth to it. I could sit and drink a good few of these - I think it was lucky I only had one in the fridge!"
}
---

