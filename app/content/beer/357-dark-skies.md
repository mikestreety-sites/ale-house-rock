---json
{
    "title": "Dark Skies",
    "number": "357",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CChEYwtJv4z/",
    "date": "2020-07-11",
    "permalink": "beer/dark-skies-drygate-brewing-company/",
    "breweries": [
        "brewery/drygate-brewing-company/"
    ],
    "tags": [],
    "review": "This is an odd one and not a usual stout from @drygate (via @flavourlyhq). There was no initial shiver you get from a full-on flavoured stout and it wasn&#39;t the usual heavy taste. Somewhere there was a hint of coffee, but nothing overpowering. A good gentle sipper, which could be mixed in with a session, rather than being a nightcap"
}
---