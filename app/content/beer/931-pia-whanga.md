---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1384396331",
  "title": "Pia Whanga",
  "serving": "Bottle",
  "rating": 7,
  "purchased": "shop/bluestone-national-park-resort/",
  "date": "2024-05-26",
  "style": "Pale Ale - New Zealand",
  "abv": "4.2%",
  "breweries": [
    "brewery/tenby-harbwr-brewery/"
  ],
  "number": 931,
  "permalink": "beer/pia-whanga-tenby-harbwr-brewery/",
  "review": "Picked this up when I was in Wales a couple of weeks ago. It was a smooth, tasty ale. I would happily have one again, but wouldn't actively seek one out."
}
---

