---json
{
    "title": "Kew Gardens Honey Ale",
    "number": "263",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B6HSRrJlfq6/",
    "date": "2019-12-16",
    "permalink": "beer/kew-gardens-honey-ale-twickenham-fine-ales/",
    "breweries": [
        "brewery/twickenham-fine-ales/"
    ],
    "tags": [],
    "review": "A lovely dark, sweet beer. Normally a stout like this would have coffee or chocolate notes, but this @kewgardens tipple was laced with honey flavour, making it a lighter stout to drink"
}
---