---json
{
    "title": "Offshore",
    "number": "229",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BzQ-_CQFZ6F/",
    "date": "2019-06-28",
    "permalink": "beer/offshore-sharps/",
    "breweries": [
        "brewery/sharps/"
    ],
    "tags": [],
    "review": "Light crisp pilsner - great for a hot day. Remanicent of the Hop House lager in its taste"
}
---