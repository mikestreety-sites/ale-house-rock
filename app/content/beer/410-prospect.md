---json
{
    "title": "Prospect",
    "number": "410",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CJ1vza-FZE4/",
    "date": "2021-01-09",
    "permalink": "beer/prospect-hepworth-co-brewers/",
    "breweries": [
        "brewery/hepworth-co-brewers/"
    ],
    "tags": [],
    "review": "It&#39;s good to have a pint of bitter again. Got a bit shaken up in a backpack walking home but was an excellent beer nonetheless. Hepworth are excellent local brewers - I need to hunt some more down"
}
---