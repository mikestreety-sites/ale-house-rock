---json
{
    "title": "Locals V2",
    "number": "500",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CPi0a1BlX6y/",
    "date": "2021-05-31",
    "permalink": "beer/locals-v2-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [
        "dipa",
        "ipa"
    ],
    "review": "This is it, the 500th beer. Who would have thought that a 24-beer Ale advent calendar would turn into this? @thebeakbrewery are a brewery I have only recently discovered. Hailing from not-too-far Lewes, they brew consistently high quality beers - I wanted to be this that took the spot."
}
---