---json
{
    "title": "Sayzon",
    "number": "243",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B3PzupBlWSj/",
    "date": "2019-10-05",
    "permalink": "beer/sayzon-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [
        "saizon"
    ],
    "review": "Wow what can I say about this tasty mass-produced Belgium-style beer? It&#39;s got all the characteristics of a true Belgian tipple, dangerously qwaffable while being higher ADV than your average ale. Lovely stuff"
}
---