---json
{
    "title": "Tally Ho!",
    "number": "550",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CSSfcYSKeou/",
    "date": "2021-08-07",
    "permalink": "beer/tally-ho-palmers/",
    "breweries": [
        "brewery/palmers/"
    ],
    "tags": [],
    "review": "A super smooth ruby ale. Kind of like a Hobgoblin Ruby but better. Had to out this down on more than one occasion to stop me drinking it so quickly."
}
---