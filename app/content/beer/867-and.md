---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1329296784",
  "title": "And",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-11-07",
  "style": "IPA - American",
  "abv": "6.5%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 867,
  "permalink": "beer/and-beak/",
  "review": "I find a lot of Beak beers taste very similar - fortunately it's a taste I enjoy but I struggle to differentiate between them and it feels slightly cheating logging each one as a different beer. Another great IPA which is drinkable with plenty of flavour"
}
---
