---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1423599302",
  "title": "Parachute Club",
  "serving": "Can",
  "rating": 10,
  "purchased": "shop/middle-farm-tea-rooms/",
  "date": "2024-10-06",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/360-degree-brewing-company/"
  ],
  "number": 978,
  "permalink": "beer/parachute-club-360-degree-brewing-company/",
  "review": "This was a perfect NEIPA with its only fault being that it was so damn drinkable. Soft and fruity, an absolute spot-on banger."
}
---
