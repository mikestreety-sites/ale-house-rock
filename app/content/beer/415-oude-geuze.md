---json
{
    "title": "Oude Geuze",
    "number": "415",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CKFIHSylELO/",
    "date": "2021-01-15",
    "permalink": "beer/oude-geuze-boon/",
    "breweries": [
        "brewery/boon/"
    ],
    "tags": [],
    "review": "Beer one of the zoom tasting evening with @laurzstreet. We both ordered the January mystery mix box from @beermerchants. Started with this one which has the most dubious description (to be served with Apple and Vinaigrette pudding?). This instantly took me back to a summer evening on Brighton beach. I wouldn&#39;t order it again, but not as bad as I was expecting. Very Belgian in taste, but not a premium flavour."
}
---