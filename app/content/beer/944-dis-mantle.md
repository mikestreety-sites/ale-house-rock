---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1397679778",
  "title": "Dis-mantle",
  "serving": "Bottle",
  "rating": 7.5,
  "purchased": "shop/bluestone-national-park-resort/",
  "date": "2024-07-10",
  "style": "Strong Ale - English",
  "abv": "5.8%",
  "breweries": [
    "brewery/mantle/"
  ],
  "number": 944,
  "permalink": "beer/dis-mantle-mantle/",
  "review": "A smooth dark bitter which you'd have to be in the right frame-of-mind for. Picked up when I was in Wales."
}
---

