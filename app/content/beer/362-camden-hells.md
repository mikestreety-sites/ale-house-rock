---json
{
    "title": "Camden Hells",
    "number": "362",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CDAA_wVJbsO/",
    "date": "2020-07-23",
    "permalink": "beer/camden-hells-camden-town-brewery/",
    "breweries": [
        "brewery/camden-town-brewery/"
    ],
    "tags": [],
    "review": "Not sure if this strictly belongs on an Ale review account, although this has now turned into a &quot;beer in a vessel&quot; kind of place. Rescued this from the desolate office the other week as it was due to go out of date soon. This lager was nice and I would happily drink again. Preferably in a garden or other outdoors place with it being chilled. Nice hoppy taste, easy sippin&#39; and went down well."
}
---