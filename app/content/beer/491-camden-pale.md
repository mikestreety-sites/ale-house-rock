---json
{
    "title": "Camden Pale",
    "number": "491",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CPEQZVHp3-_/",
    "date": "2021-05-19",
    "permalink": "beer/camden-pale-camden-town-brewery/",
    "breweries": [
        "brewery/camden-town-brewery/"
    ],
    "tags": [],
    "review": "A fairly plain, non-descript pale"
}
---