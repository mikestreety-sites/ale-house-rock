---json
{
    "title": "Let's Go",
    "number": "501",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CPl_RwOl3Yi/",
    "date": "2021-06-01",
    "permalink": "beer/lets-go-only-with-love/",
    "breweries": [
        "brewery/only-with-love/"
    ],
    "tags": [],
    "review": "This IPA had a great first few sips when it was straight out the fridge but as the beer went on I fell slightly out of love with it. Can imagine it would be better suited drunk quickly and ice cold - not slowly over a warm evening (sorry I didn&#39;t do it justice). Only With Love are a small local brewery that seem to be popping up more and more. Will keep an eye out for more of their beers as this seemed promising but didn&#39;t quite work for me."
}
---