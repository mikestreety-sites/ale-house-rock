---json
{
    "title": "Momentary Bliss",
    "number": "559",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CS5Ky1mqet9/",
    "date": "2021-08-22",
    "permalink": "beer/momentary-bliss-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "A nice enough DIPA which did give you momentary bliss as you took a sip, but I there was something in the aftertaste that wasn&#39;t quite right for me. Still a good beer from @deyabrewery though!"
}
---