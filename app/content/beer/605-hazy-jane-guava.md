---json
{
    "title": "Hazy Jane Guava",
    "number": "605",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CYv8rBaqsVf/",
    "date": "2022-01-15",
    "permalink": "beer/hazy-jane-guava-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "Yet another installment in the &quot;Hazy Jane&quot; saga. I wasn&#39;t holding out much hope for it, really but it turned out nicer than I expected. I can&#39;t say I noticed much difference in the taste, maybe a bit more fruitier but definitely a gimmick. I gave the original Hazy Jane an 8, but in hindsight (and having drunk more), it deserved a 7, which is what I&#39;m awarding this one."
}
---