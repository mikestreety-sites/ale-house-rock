---json
{
    "title": "Flowerpower Blonde",
    "number": "313",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B_c3OBWJa-U/",
    "date": "2020-04-26",
    "permalink": "beer/flowerpower-blonde-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [],
    "review": "My last of the @loudshirtbeer beers from the delivery a couple of weeks ago and what an absolutely awesome beer to finish on. Slightly floral (but not overpowering) and beautifully light. Perfect as a post-gardening pint 👌"
}
---