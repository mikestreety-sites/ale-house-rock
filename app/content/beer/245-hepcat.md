---json
{
    "title": "Hepcat",
    "number": "245",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B35BCgZpDyX/",
    "date": "2019-10-21",
    "permalink": "beer/hepcat-gipsy-hill/",
    "breweries": [
        "brewery/gipsy-hill/"
    ],
    "tags": [],
    "review": "From a brewery in crystal palace, this hipster session ale was very fruity and went down easily after a Chinese takeaway, but not one I would really lust after"
}
---