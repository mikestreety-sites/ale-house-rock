---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1324597161",
  "title": "Good Impacts",
  "serving": "Can",
  "rating": 5,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-21",
  "style": "Pale Ale - American",
  "abv": "5%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 861,
  "permalink": "beer/good-impacts-overtone-brewing-co/",
  "review": "Was unimpressed by this pale ale. It seemed to lack any decent flavour. However, it was refreshing and drinkable."
}
---

