---json
{
    "title": "Caesar Augustus",
    "number": "576",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CUQf5FRqWFg/",
    "date": "2021-09-25",
    "permalink": "beer/caesar-augustus-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "A &quot;lager IPA hybrid&quot;. Tastes exactly how you would expect. The worst of both worlds. Not much flavour so not very offensive. Maybe palatable on a hot summers day?"
}
---