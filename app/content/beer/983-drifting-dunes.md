---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1425918810",
  "title": "Drifting Dunes",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/padstow-tasting-rooms/",
  "date": "2024-10-16",
  "style": "IPA - New England / Hazy",
  "abv": "6%",
  "breweries": [
    "brewery/padstow-brewing-company/"
  ],
  "number": 983,
  "permalink": "beer/drifting-dunes-padstow-brewing-company/",
  "review": "This was excellent. I picked one up on a whim when I popped in while on holiday, but had to go back to buy 2 more - one to drink that evening and one to review. Nicely balanced, not too heavy but still plenty of flavour."
}
---
