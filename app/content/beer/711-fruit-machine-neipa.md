---json
{
    "title": "Fruit Machine NEIPA",
    "number": "711",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1198812544",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-09-04",
    "permalink": "beer/fruit-machine-neipa-lost-pier-brewing/",
    "breweries": [
        "brewery/lost-pier-brewing/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "This was like a fruity milkshake IPA. Packed full of fruit flavours, it was a perfect dessert beer. Not sure I would have it again but it was nice enough"
}
---