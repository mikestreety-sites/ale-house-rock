---json
{
    "title": "Farne Island",
    "number": "512",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CQJ9btYJj6e/",
    "date": "2021-06-15",
    "permalink": "beer/farne-island-hadrian-border-brewery/",
    "breweries": [
        "brewery/hadrian-border-brewery/"
    ],
    "tags": [],
    "review": "A much cleaner, crisper beer and a pleasure to drink. Tasted like a good old fashioned IPA-style beer, although nothing spectacular."
}
---