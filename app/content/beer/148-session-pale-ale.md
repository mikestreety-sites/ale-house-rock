---json
{
    "title": "Hyde & Wilde Session Pale Ale",
    "number": "148",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BmwYG84FlYs/",
    "date": "2018-08-21",
    "permalink": "beer/session-pale-ale-hyde-wilde/",
    "breweries": [
        "brewery/sainsburys/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A generic, hipster, tasteless, craft beer. No taste, no offensiveness."
}
---