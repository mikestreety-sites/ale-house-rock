---json
{
    "title": "Lupuoid",
    "number": "481",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/COTgYLiFcaY/",
    "date": "2021-04-30",
    "permalink": "beer/lupuoid-beavertown/",
    "breweries": [
        "brewery/beavertown/"
    ],
    "tags": [],
    "review": "A tasty IPA from @beavertownbeer picked up from Sainsbury&#39;s. Easy drinking with plenty of flavour - would happily add this to my rotation."
}
---