---json
{
    "title": "Whitstable Bay",
    "number": "25",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BOXeyq_A-OT/",
    "serving": "Bottle",
    "date": "2016-12-23",
    "permalink": "beer/whitstable-bay-the-faversham-steam-brewery/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A sweet, golden ale with flavour and character. Drinkable. Very drinkable."
}
---