---json
{
    "title": "Beeding Best",
    "number": "432",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CLKyyogF9ZA/",
    "date": "2021-02-11",
    "permalink": "beer/beeding-best-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "Second beer from @riverside_brewery and one that is a lot clearer, and comes with a good solid taste. I think it is taking my taste-buds a bit of time to adjust from the smooth hipster craft beers to the rugged ales. Ordering from Riverside is certainly a comforting taste excursion; taking me back to &quot;old man&quot; pubs with friends, sitting by open fires in the winter. Lovely flavours and easy to drink. A bit of a bitter tang after each sip."
}
---