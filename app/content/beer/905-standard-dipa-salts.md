---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1363096609",
  "title": "Standard DIPA Salts",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-13",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/deya-brewing-company/"
  ],
  "number": 905,
  "permalink": "beer/standard-dipa-salts-deya-brewing-company/",
  "review": "Deya have done it again. An incredible DIPA which was soft and fluffy and thick and tasty. It was very enjoyable but I wouldn't go out to hunt one down (hence not getting a 10). Still worth picking up if you see one though!"
}
---

