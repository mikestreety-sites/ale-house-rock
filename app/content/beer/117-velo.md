---json
{
    "title": "Velo",
    "number": "117",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BkVs3iLFViB/",
    "date": "2018-06-22",
    "permalink": "beer/velo-black-sheep-brewery/",
    "breweries": [
        "brewery/black-sheep-brewery/"
    ],
    "tags": [],
    "review": "A nice crisp beer from @blacksheepbrewery . Perfect after a long drive and a lovely greeting for holiday"
}
---