---json
{
    "title": "McEwan's Levy",
    "number": "447",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CMPFll0lEWp/",
    "date": "2021-03-10",
    "permalink": "beer/levy-mcewans/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [],
    "review": "Had this sitting in my fridge for a while as I was reluctant to drink it. I&#39;ve had it a few times before and always been unimpressed. It&#39;s trying to be a black IPA (or a &quot;light&quot; stout) but falls short of the mark. There are some coffee/chocolate notes there but they get overwhelmed by the watery taste."
}
---