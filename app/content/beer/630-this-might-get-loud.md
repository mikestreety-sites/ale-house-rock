---json
{
    "title": "This Might Get Loud",
    "number": "630",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Ca-wLxlKfsK/",
    "date": "2022-03-11",
    "permalink": "beer/this-might-get-loud-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [],
    "review": "Lovely, easy-drinking smooth IPA. I wouldn&#39;t expect anything less from @deyabrewery"
}
---