---json
{
    "title": "18.05 Sandy Kerr // North Sea Sessions",
    "number": "494",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CPMJGO4Jli2/",
    "date": "2021-05-22",
    "permalink": "beer/1805-sandy-kerr-north-sea-sessions-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "An IPA/Lager fushion (IPL), I wasn&#39;t hugely impressed with this Northern Monk beer brewed with Donzoko Brewing Company. It tasted like a plain hoppy lager. Not for me"
}
---