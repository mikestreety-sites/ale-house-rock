---json
{
    "title": "Session Pale",
    "number": "595",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CYCsesWBQ42/",
    "date": "2021-12-28",
    "permalink": "beer/session-pale-small-beer/",
    "breweries": [
        "brewery/small-beer/"
    ],
    "tags": [],
    "review": "A poor choice of mine to have after a fruity IPA as this is quite bland in comparison. After a few sips I got the real taste of it, and  I was slightly disappointed. Not much flavour there at all. At only 2.5%, it&#39;s perfect for when  you fancy a beer but want to ride light. I could imagine the beer being friends with a Chinese or Curry takeaway - but doesn&#39;t really stand strong on its own."
}
---