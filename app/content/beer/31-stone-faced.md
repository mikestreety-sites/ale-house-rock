---json
{
    "title": "Stone Faced",
    "number": "31",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BQTSnYbAUTx/",
    "serving": "Bottle",
    "date": "2017-02-09",
    "permalink": "beer/stone-faced-lymestone-brewery/",
    "breweries": [
        "brewery/lymestone-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "An &quot;in-your-face&quot; beer that taste stronger than its 4% label. A bit of an &quot;off&quot; aftertaste. I&#39;ll finish it though"
}
---