---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1414832408",
  "title": "Waddesdon Shepherd's Gold Ale",
  "serving": "Bottle",
  "rating": 9,
  "date": "2024-09-05",
  "style": "Blonde / Golden Ale - Other",
  "abv": "4.8%",
  "breweries": [
    "brewery/the-chiltern-brewery/"
  ],
  "number": 962,
  "permalink": "beer/waddesdon-shepherds-gold-ale-the-chiltern-brewery/",
  "review": "This was a lovely, full-bodied, smooth ale. It went down very nicely and was perfect with a roast."
}
---

