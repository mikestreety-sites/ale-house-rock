---json
{
    "title": "Chairs Missing",
    "number": "632",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CbLGV2zqn9p/",
    "date": "2022-03-16",
    "permalink": "beer/chairs-missing-north-brewing-co/",
    "breweries": [
        "brewery/north-brewing-co/"
    ],
    "tags": [],
    "review": "This 11% imperial west coast IPA was lovely. The alcohol taste was there, but was more like an 8 or 9%, rather than an 11%. Took me all evening to drink but I enjoyed every drop."
}
---