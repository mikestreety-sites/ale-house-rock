---json
{
    "title": "Boadicea",
    "number": "171",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bp7tvqUAcBU/",
    "date": "2018-11-08",
    "permalink": "beer/boadicea-meantime-brewing-company/",
    "breweries": [
        "brewery/meantime-brewing-company/"
    ],
    "tags": [],
    "review": "managed to open and pour this beer with one hand (long story) but a nice refreshing beer. Once again, surprised by the colour producing a tasty beer"
}
---