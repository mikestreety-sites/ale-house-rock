---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1364040592",
  "title": "THREADS 2",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-16",
  "style": "IPA - New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/beak-brewery/",
    "brewery/floc/"
  ],
  "number": 908,
  "permalink": "beer/threads-2-beak-floc/",
  "review": "FLOC has been a recent fave of mine, so to hear they made a beer with Beak made it a must-have. Unfortunately, they had sold out of Threads 1, which is designed to be mixed with this - but this was still a fantastic standalone beer. Soft and juicy"
}
---
