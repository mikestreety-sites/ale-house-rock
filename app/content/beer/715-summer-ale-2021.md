---json
{
    "title": "Summer Ale (2021)",
    "number": "715",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1201387410",
    "serving": "Bottle",
    "purchased": "shop/sainsburys/",
    "date": "2022-09-12",
    "permalink": "beer/summer-ale-2021-sainsburys-shepherd-neame/",
    "breweries": [
        "brewery/sainsburys/",
        "brewery/shepherd-neame/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A tasty crisp classic bitter. Lovely and moreish, this was a great Sunday evening tipple."
}
---