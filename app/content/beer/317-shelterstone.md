---json
{
    "title": "Shelterstone",
    "number": "317",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/B_p6aSTpSzy/",
    "date": "2020-05-01",
    "permalink": "beer/shelterstone-buxton-brewery/",
    "breweries": [
        "brewery/buxton-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "After a strong start out of the gates for Buxton the other day (from the @beer52hq delivery), I was looking forward to this one but was bitterly disappointed. Tangy, odd taste . Certainly not a beer I would go back to."
}
---