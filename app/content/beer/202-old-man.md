---json
{
    "title": "Old Man",
    "number": "202",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BtBqrTWAcSb/",
    "date": "2019-01-24",
    "permalink": "beer/old-man-long-man-brewery/",
    "breweries": [
        "brewery/long-man-brewery/"
    ],
    "tags": [
        "stout"
    ],
    "review": "This is how this beer makes you feel, like an old man. It makes you yearn for a log fire and a cardigan. Full of flavour but not overpowering like some stouts. A long drink, one to be had over an evening and a conversation about what is wrong with the world"
}
---