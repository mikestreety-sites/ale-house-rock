---json
{
    "title": "Dance Every Day Pale Ale",
    "number": "761",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1241271308",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-01-20",
    "permalink": "beer/dance-every-day-pale-ale-only-with-love/",
    "breweries": [
        "brewery/only-with-love/"
    ],
    "tags": [
        "hazy"
    ],
    "review": "Felt.like this was missing something with the flavour, suspect the 3.5% had something to do with it."
}
---