---json
{
    "title": "Blood Red Sky",
    "number": "234",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B16wpCFFEg8/",
    "date": "2019-09-02",
    "permalink": "beer/blood-red-sky-innis-gunn/",
    "breweries": [
        "brewery/innis-gunn/"
    ],
    "tags": [],
    "review": "Beer aged in rum barrels. Lovely deep taste, definitely a sipper at the end of the evening rather than a session ale. Complex flavours"
}
---