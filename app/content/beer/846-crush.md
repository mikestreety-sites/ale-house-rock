---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1314036452",
  "title": "Crush",
  "serving": "Can",
  "rating": 5,
  "date": "2023-09-15",
  "style": "IPA - Session",
  "abv": "4.2%",
  "breweries": [
    "brewery/drop-project/",
    "brewery/missing-link-brewing/"
  ],
  "number": 846,
  "permalink": "beer/crush-drop-project-missing-link-brewing/",
  "review": "I don't think a \"session NEIPA\" is a thing that needs to be. NEIPAs in their nature need to be thick, juicy and big - if you try and make that sessionable it just tastes like an average pale ale."
}
---

