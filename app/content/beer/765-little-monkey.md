---json
{
    "title": "Little Monkey",
    "number": "765",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1245222571",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-02-04",
    "permalink": "beer/little-monkey-missing-link-brewing-pzdk-brew/",
    "breweries": [
        "brewery/missing-link-brewing/",
        "brewery/pzdk-brew/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "This was a lovely, juicy NEIPA, but a bit too much of the alcohol taste for my liking. It was enjoyable, but tasted stronger than the 10% from the other day!"
}
---