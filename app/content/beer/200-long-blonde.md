---json
{
    "title": "Long Blonde",
    "number": "200",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BsyU0mMgiDD/",
    "date": "2019-01-18",
    "permalink": "beer/long-blonde-long-man-brewery/",
    "breweries": [
        "brewery/long-man-brewery/"
    ],
    "tags": [],
    "review": "I didn’t care much for this hoppy, light beer from Long Man Brewery. It was too hoppy for me"
}
---