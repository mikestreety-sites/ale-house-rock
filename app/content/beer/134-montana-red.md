---json
{
    "title": "Montana Red",
    "number": "134",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/Bl0r1HKF_c1/",
    "date": "2018-07-29",
    "permalink": "beer/montana-red-fullers/",
    "breweries": [
        "brewery/fullers/"
    ],
    "tags": [
        "redale"
    ],
    "review": "A red ale with a cheap aftertaste. From Waitrose, this by no means carries the premium label that is normally associated with the shop. I wouldn’t buy this again"
}
---