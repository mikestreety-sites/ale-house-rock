---json
{
    "title": "Scafell",
    "number": "425",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CKmxkk8F8BC/",
    "date": "2021-01-28",
    "permalink": "beer/scafell-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Home. A @northernmonk Beer in a Northern Monk class. Had to keep reminding myself not to chug this 6.5% IPA, but it was tricky as it was so damn tasty. Full bodied and smooth, was an absolute treat for a Thursday night. I&#39;m sure this will be sneaking into the online shopping basket again. The Monks have smashed another one out the park."
}
---