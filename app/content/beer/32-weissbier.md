---json
{
    "title": "Rheinbacher Weissbier",
    "number": "32",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BQTTr-RAj4V/",
    "serving": "Bottle",
    "purchased": "shop/aldi/",
    "date": "2017-02-09",
    "permalink": "beer/weissbier-rheinbacher/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "wheatbeer"
    ],
    "review": "I&#39;ve been meaning to post this for a week! Another German one, a lager of f sorts with plenty of taste. Went down very easily and quickly while I was fixing my coffee machine. Bottle conditioned (hence not the full pint) but would happily have it again - preferably on a summers day in the garden"
}
---