---json
{
    "title": "Tea Party",
    "number": "336",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CA2e5Lxp6k7/",
    "date": "2020-05-31",
    "permalink": "beer/tea-party-people-like-us/",
    "breweries": [
        "brewery/people-like-us/"
    ],
    "tags": [],
    "review": "Another beer with a twist from @peoplelikeusdk. This time a cream ale with green tea. Nothing outstanding about this beer - good flavours but as a non-green tea drinker I can&#39;t say I picked up on any vibes. Smooth to drink and went down well."
}
---