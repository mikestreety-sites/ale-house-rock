---json
{
    "title": "Divine Faith",
    "number": "480",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/COSDRHHFBPd/",
    "date": "2021-04-30",
    "permalink": "beer/divine-faith-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "I&#39;m a huge fan of the original Faith and have come to love DIPAs of late, so when I saw @northernmonk release a DIPA of Faith I had to order it (side note, very impressed with Northern Monk&#39;s delivery service once again). This beer had the colour and consistency of apple juice, which was quite confusing at first. It&#39;s a thick, full beer which is not to be drunk quickly and lasted all evening. Full of flavour and a great end to the day. It&#39;s not as good as the Faith, but still a very good DIPA"
}
---