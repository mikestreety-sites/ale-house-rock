---json
{
    "title": "Whitstable Bay Organic Ale",
    "number": "376",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CG-nCQMJs4w/",
    "date": "2020-10-30",
    "permalink": "beer/whitstable-bay-organic-ale-shepherd-neame/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [],
    "review": "Although the bottle says &quot;Faversham Steam Brewery&quot;, I&#39;ve tagged this under Shepherd Neame as they are known for the Whitstable Bay beers. Not as good as the original, a slight post-sip tang which I&#39;m not too sure of. Picked this one up in co-op pre holiday (not knowing I would buy so much beer!)"
}
---