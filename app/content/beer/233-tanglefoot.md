---json
{
    "title": "Tanglefoot",
    "number": "233",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B16wUsXl4fd/",
    "date": "2019-09-02",
    "permalink": "beer/tanglefoot-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [],
    "review": "Not much to say on this. Light, crisp, satisfactory. Hits the spot when it aims to be a &quot;good beer after a hard day&quot;. Would never crave one but would be tempted to buy more"
}
---