---json
{
    "title": "Hazed & Confused",
    "number": "755",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1236797834",
    "serving": "Can",
    "date": "2023-01-02",
    "permalink": "beer/hazed-confused-loud-shirt-brewing-co/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [],
    "review": "Not the best NEIPA I&#39;ve had, there was some flavour there but not as much as I would like. I much prefer their bottled bitters."
}
---