---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1384479633",
  "title": "Under Skies",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-05-26",
  "style": "IPA - New England / Hazy",
  "abv": "6.2%",
  "breweries": [
    "brewery/floc/"
  ],
  "number": 932,
  "permalink": "beer/under-skies-floc/",
  "review": "It seems Flo.c struggles to make a bad beer. Another incredible IPA from the Canterbury-based brewery. I can't help but buy a Floc. Beer when I see them. Full-bodied and fruity, this was spot-on"
}
---

