---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1401958540",
  "title": "Satellite",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/tesco/",
  "date": "2024-07-24",
  "style": "IPA - Session",
  "abv": "2.8%",
  "breweries": [
    "brewery/beavertown/"
  ],
  "number": 951,
  "permalink": "beer/satellite-beavertown/",
  "review": "I picked these up when they were doing the offer with Tesco and I've concluded it is the perfect lunchtime beer. Enough ABV to give it the beer taste, but not enough to ruin your afternoon. I wouldn't buy these, but wouldn't say no if offered midday."
}
---

