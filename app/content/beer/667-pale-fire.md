---json
{
    "title": "Pale Fire",
    "number": "667",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CeVvdaSqMvx/",
    "date": "2022-06-03",
    "permalink": "beer/pale-fire-pressure-drop/",
    "breweries": [
        "brewery/pressure-drop/"
    ],
    "tags": [],
    "review": "Now this was a fine pale ale. Light and refreshing, a nice gentle flavour and a perfect end to a bank holiday Thursday. This beer from @pressuredropbrw really came out of its shell when it had warmed up slightly - not quite fridge cold."
}
---