---json
{
    "title": "IPA",
    "number": "427",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CKueyopFlJF/",
    "date": "2021-01-31",
    "permalink": "beer/ipa-wolfox/",
    "breweries": [
        "brewery/wolfox/"
    ],
    "tags": [],
    "review": "I actually bought one of these when I picked up the Pale Ale of theirs back in December, but completely forgot to post it. Of course, it meant I had to buy another one so I could remember what it was like. Light, fruity, refreshing and very easily drinkable. Smashing work @wolfoxcoffee (also meant I could buy some coffee when ordering, too)"
}
---