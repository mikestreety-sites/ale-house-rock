---json
{
    "title": "Kingpin Rising",
    "number": "585",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/CVqPUFJq16h/",
    "date": "2021-10-30",
    "permalink": "beer/kingpin-rising-boss-brewing/",
    "breweries": [
        "brewery/boss-brewing/"
    ],
    "tags": [],
    "review": "A generic, Lidl &quot;Session IPA&quot;. Bland and disappointing"
}
---