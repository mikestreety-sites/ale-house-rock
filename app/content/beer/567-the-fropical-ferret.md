---json
{
    "title": "The Fropical Ferret",
    "number": "567",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CT0FdIyKks9/",
    "date": "2021-09-14",
    "permalink": "beer/the-fropical-ferret-badger/",
    "breweries": [
        "brewery/badger/"
    ],
    "tags": [],
    "review": "Lovely crisp, fruity, light bitter - a lighter alternative than the original that I would definitely have this again."
}
---