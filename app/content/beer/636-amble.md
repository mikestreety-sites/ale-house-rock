---json
{
    "title": "Amble",
    "number": "636",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Cb0m-NPKHdY/",
    "date": "2022-04-01",
    "permalink": "beer/amble-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "It&#39;s really hard to write a review for these lovely @thebeakbrewery IPAs they keep churning out. Fruity, soft and so easy to drink, I could smash a few of these in one sitting and have a wobbly walk home."
}
---