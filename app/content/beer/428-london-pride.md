---json
{
    "title": "London Pride",
    "number": "428",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CK4hTVKl_9X/",
    "date": "2021-02-04",
    "permalink": "beer/london-pride-fullers/",
    "breweries": [
        "brewery/fullers/"
    ],
    "tags": [],
    "review": "I realised two things this week. First, my feed has become very craft-beery. This is mainly because of the readily available craft tipples in lockdown - it seems they swiftly adapted to postal pints. This isn&#39;t very good for an &quot;Ale&quot; review Instagram. The second thing I&#39;ve realised was that I have never reviewed a lot of classic beers, like this one. Tasty, easy drinking, never disappointing"
}
---