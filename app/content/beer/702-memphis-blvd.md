---json
{
    "title": "Memphis Blvd",
    "number": "702",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1193148933",
    "purchased": "shop/aldi/",
    "date": "2022-08-19",
    "permalink": "beer/memphis-blvd-aldi/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "An obvious Elvis Juice rip-off, this actually tasted better than what it was impersonating. I think this was due to the lack of grapefruit, which is quite prominent in the original. It tasted like an &quot;ok&quot; Aldi IPA ."
}
---