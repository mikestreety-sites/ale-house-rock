---json
{
    "title": "BrewDog VS Mikkeller: Urban Fog",
    "number": "713",
    "rating": "8",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1199771956",
    "serving": "Can",
    "purchased": "shop/sainsburys/",
    "date": "2022-09-08",
    "permalink": "beer/brewdog-vs-mikkeller-urban-fog-brewdog-mikkeller/",
    "breweries": [
        "brewery/brewdog/",
        "brewery/mikkeller/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "I&#39;m a big fan of Mikkeller, so this was a Brewdog beer i had high hopes for and it didn&#39;t disappoint. Nice and juicy without being sweet, a weighty NEIPA which is accessible from the big shop."
}
---