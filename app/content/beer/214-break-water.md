---json
{
    "title": "Break Water",
    "number": "214",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BvubvU5lr8L/",
    "date": "2019-04-01",
    "permalink": "beer/break-water-red-rock-brewery/",
    "breweries": [
        "brewery/red-rock-brewery/"
    ],
    "tags": [],
    "review": "Not much of an improvement from the previous beer of theirs that I tried (see #213). Bit watery and not quite the &quot;full flavour&quot; as promised on the bottle. It&#39;s a shame I don&#39;t do half points otherwise it would be a 5.5."
}
---