---json
{
    "title": "Striding Edge",
    "number": "369",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CEkaLGcJWir/",
    "date": "2020-08-31",
    "permalink": "beer/striding-edge-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "My last of the @northernmonk beers from Mrs. Ale House. Didn&#39;t expect much from this 2.8% as the low ABV beers tend to lack some body. A tasty tipple, but nowhere near the absolute bangers they have otherwise produced. Takes a while to get into, but eventually becomes moreish."
}
---