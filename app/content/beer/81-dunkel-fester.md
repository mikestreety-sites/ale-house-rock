---json
{
    "title": "Dunkel Fester",
    "number": "81",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BbFn4lnlVfT/",
    "date": "2017-11-04",
    "permalink": "beer/dunkel-fester-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "A dark coffee beer perfect for board games and post-dinner slump"
}
---