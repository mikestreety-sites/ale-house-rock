---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1348298325",
  "title": "Frends",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-01-13",
  "style": "IPA - Imperial / Double",
  "abv": "8%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 889,
  "permalink": "beer/frends-beak/",
  "review": "This was not like a Beak DIPA I was used to. It was nice, but lacked the juicy-ness I would expect from the local brewery. It tasted more like a \"simple\" IPA. I enjoyed it nonetheless, just not my favourite."
}
---
