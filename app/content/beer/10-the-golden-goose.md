---json
{
    "title": "The Golden Goose",
    "number": "10",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BNz17CPAJVG/",
    "serving": "Bottle",
    "purchased": "shop/lidl/",
    "date": "2016-12-09",
    "permalink": "beer/the-golden-goose-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "A simple inoffensive beer. Nothing more really to say!"
}
---