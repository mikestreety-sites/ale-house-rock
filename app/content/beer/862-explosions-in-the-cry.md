---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1325046164",
  "title": "Explosions In the Cry",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-22",
  "style": "IPA - Triple New England / Hazy",
  "abv": "10%",
  "breweries": [
    "brewery/overtone-brewing-co/"
  ],
  "number": 862,
  "permalink": "beer/explosions-in-the-cry-overtone-brewing-co/",
  "review": "One of the best TIPAs I've ever had. It packed a punch but didn't seem like a gimmick. You could taste the alcohol, but it wasn't overpowering and it felt it \"needed\" to be 11% to get that flavour profile"
}
---

