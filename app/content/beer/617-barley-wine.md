---json
{
    "title": "Barley Wine",
    "number": "617",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CZ8waygKtQ0/",
    "date": "2022-02-14",
    "permalink": "beer/barley-wine-north-brewing-co-good-things-brewing-company/",
    "breweries": [
        "brewery/good-things-brewing-company/",
        "brewery/north-brewing-co/"
    ],
    "tags": [],
    "review": "I&#39;d never had barley wine before, so I was intrigued as to what it would be like. If I had to sum it up in one word, I think I would choose &quot;rich&quot;. If normal beer was a normal glass of squash, this tasted like drinking the stuff neat. It was thick with a good flavour and you wouldn&#39;t want more than a sip each time - which is good, as it&#39;s 10%. I would have it again, but I can&#39;t say I would buy it over a beer given the choice."
}
---