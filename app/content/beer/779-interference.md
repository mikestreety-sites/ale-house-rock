---json
{
    "title": "Interference",
    "number": "779",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1259087051",
    "serving": "Can",
    "purchased": "shop/the-fussclub/",
    "date": "2023-03-25",
    "permalink": "beer/interference-deya-brewing-company/",
    "breweries": [
        "brewery/deya-brewing-company/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "Hot diggity damn this was good. Deya are so very good at DIPAs and this was no exception. Smooth, punchy and so very easy to drink"
}
---