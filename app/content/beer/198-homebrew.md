---json
{
    "title": "Homebrew",
    "number": "198",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BsqqtHBAunj/",
    "date": "2019-01-15",
    "permalink": "beer/homebrew-street-brewing-company/",
    "breweries": [
        "brewery/street-brewing-company/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Got a brewing kit last Christmas and made this brew for my birthday in August. It made 40 pints! Gave a lot a way but found another 7 pints under the stairs the other day. This beer was OK but has an odd aftertaste. It gets an extra point for the good reviews from everyone else, but I think I’m going to stick to drinking other people’s beer"
}
---