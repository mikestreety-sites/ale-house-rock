---json
{
    "title": "Proper Job",
    "number": "430",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CLCrmKSlX3o/",
    "date": "2021-02-08",
    "permalink": "beer/proper-job-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [],
    "review": "Another absolute banger of a classic I&#39;ve not reviewed before. A crisp, golden ale and a great bitter. They&#39;ve done a proper job with this one."
}
---