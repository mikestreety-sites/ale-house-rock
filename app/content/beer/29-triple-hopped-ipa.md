---json
{
    "title": "Triple Hopped IPA",
    "number": "29",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BO7zUgygsmZ/",
    "serving": "Bottle",
    "date": "2017-01-06",
    "permalink": "beer/triple-hopped-ipa-eagle-brewery/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "I started drinking this before the picture was taken. It must be good"
}
---