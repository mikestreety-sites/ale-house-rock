---json
{
    "title": "Vital Spark",
    "number": "592",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CX8j4uiK6Tp/",
    "date": "2021-12-26",
    "permalink": "beer/vital-spark-fyne-ales/",
    "breweries": [
        "brewery/fyne-ales/"
    ],
    "tags": [],
    "review": "Dark and malty and a lovely end to Christmas day."
}
---