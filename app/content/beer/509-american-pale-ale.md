---json
{
    "title": "American Pale Ale",
    "number": "509",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CQCUX6KFYsi/",
    "date": "2021-06-12",
    "permalink": "beer/american-pale-ale-toast/",
    "breweries": [
        "brewery/toast-ale/"
    ],
    "tags": [],
    "review": "The final @toastale I got from my mum and would say it was my second favourite. Crisp, light and easy drinking. Nice way to finish off an afternoon of drinking in the pub and accompanied dinner wonderfully. Wouldn&#39;t go out of my way to hunt one of these down, but would pick one up if I was passing"
}
---