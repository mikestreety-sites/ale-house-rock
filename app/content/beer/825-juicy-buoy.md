---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1293701810",
  "title": "Juicy Buoy",
  "serving": "Can",
  "rating": 8.5,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-07-14",
  "style": "IPA - New England / Hazy",
  "abv": "4.8%",
  "breweries": [
    "brewery/lost-pier-brewing/"
  ],
  "number": 825,
  "permalink": "beer/juicy-buoy-lost-pier-brewing/",
  "review": "A tasty little number from Lost Pier. Very easy to drink and had plenty of flavours to keep me interested."
}
---
