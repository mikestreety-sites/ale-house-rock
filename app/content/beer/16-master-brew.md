---json
{
    "title": "Master Brew",
    "number": "16",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BOAlSRGgybs/",
    "date": "2016-12-14",
    "permalink": "beer/master-brew-shepherd-neame/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A classic beer if ever there was one. A bitter Ale."
}
---