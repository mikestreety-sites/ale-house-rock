---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1364434158",
  "title": "2nd Birthday TDH IPA",
  "serving": "Can",
  "rating": 7.5,
  "purchased": "shop/the-fussclub/",
  "date": "2024-03-17",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "6.5%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 909,
  "permalink": "beer/2nd-birthday-tdh-ipa-sureshot-brewing/",
  "review": "It would have been rude not to try this birthday beer. Not as good as the DIPA, but still a pretty tasty tipple"
}
---

