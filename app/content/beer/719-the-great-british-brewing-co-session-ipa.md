---json
{
    "title": "The Great British Brewing Co. Session IPA",
    "number": "719",
    "rating": "1",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1204430508",
    "serving": "Can",
    "purchased": "shop/aldi/",
    "date": "2022-09-23",
    "permalink": "beer/the-great-british-brewing-co-session-ipa-aldi/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [
        "sessionipa"
    ],
    "review": "Absolutely awful &amp; tasted like soap. The can says it&#39;s &quot;A go-to IPA for any occasion&quot; - the only way that is true if your occasion is using it to cool down your glass before you have a proper beer."
}
---