---json
{
    "title": "31.02 SMUG // THE AWAKENING",
    "number": "572",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CUBLF6tqyer/",
    "date": "2021-09-19",
    "permalink": "beer/3102-smug-the-awakening-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "A bit of a forgettable DIPA really. It was nice, but certainly nothing I would write home about (I appreciate the irony in that, becuase my wife reads these reviews, I am in fact, writing home about it). Can&#39;t really comment on the beer as the moment I finished it I forgot anything about it."
}
---