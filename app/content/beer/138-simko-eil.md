---json
{
    "title": "Simko Eil",
    "number": "138",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BmB1lMMFuk1/",
    "date": "2018-08-03",
    "permalink": "beer/simko-eil-llenaut/",
    "breweries": [
        "brewery/llenaut/"
    ],
    "tags": [],
    "review": "I got a craft beer box and this is the second one out. Much like the first (#137), tasteless and meh"
}
---