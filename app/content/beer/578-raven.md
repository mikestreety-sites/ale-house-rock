---json
{
    "title": "Raven",
    "number": "578",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CUlOQ5_Koc9/",
    "date": "2021-10-03",
    "permalink": "beer/raven-black-tor/",
    "breweries": [
        "brewery/black-tor/"
    ],
    "tags": [],
    "review": "Back down in Devon so had to pick a couple of these up. A classic best, but with a slightly odd post-sip tang I couldn&#39;t quite put my finger on"
}
---