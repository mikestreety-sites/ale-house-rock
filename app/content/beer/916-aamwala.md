---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1372338011",
  "title": "Aamwala",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2024-04-14",
  "style": "IPA - New England / Hazy",
  "abv": "6.3%",
  "breweries": [
    "brewery/loud-shirt-brewing-company/",
    "brewery/bini-brew-co/"
  ],
  "number": 916,
  "permalink": "beer/aamwala-loud-shirt-brewing-company-bini-brew-co/",
  "review": "Went for a \"meet the brewery\" evening at a local beer shop to hear from Loud Shirt about their story and try a few of their beers. This was, bar far, my favourite. Nice and thick and full with plenty of flavour."
}
---

