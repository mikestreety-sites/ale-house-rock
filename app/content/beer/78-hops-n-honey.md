---json
{
    "title": "Hops ‘n’ Honey",
    "number": "78",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BbFQCaclzX8/",
    "date": "2017-11-04",
    "permalink": "beer/hops-n-honey-skinners-brewery/",
    "breweries": [
        "brewery/skinners-brewery/"
    ],
    "tags": [],
    "review": "A light, easy drinkin’ ale"
}
---