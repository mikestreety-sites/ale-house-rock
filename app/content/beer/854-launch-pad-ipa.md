---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1319852975",
  "title": "Launch Pad IPA",
  "serving": "Can",
  "rating": 6,
  "date": "2023-10-05",
  "style": "IPA - Session",
  "abv": "4.4%",
  "breweries": [
    "brewery/silver-rocket-brewing/"
  ],
  "number": 854,
  "permalink": "beer/launch-pad-ipa-silver-rocket-brewing/",
  "review": "A little disappointed with this as it tasted more like a generic megastore IPA rather than something from a craft brewery. Was ok to drink, but nothing spectacular"
}
---

