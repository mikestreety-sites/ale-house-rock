---json
{
    "title": "Sneaky Steamer",
    "number": "433",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CLPbJtDlLk-/",
    "date": "2021-02-13",
    "permalink": "beer/sneaky-steamer-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "A bit too grapefruity for me, I finished the drink but wouldn&#39;t rush to have another one of these. Despite that, I can&#39;t say enough how much these beers belong in a rustic pub or at a beer festival and how much I&#39;m enjoying the comfort."
}
---