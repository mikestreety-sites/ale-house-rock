---json
{
    "title": "Schlenkerla Marzen",
    "number": "424",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CKhq6q6FLEQ/",
    "date": "2021-01-26",
    "permalink": "beer/schlenkerla-marzen-heller/",
    "breweries": [
        "brewery/heller/"
    ],
    "tags": [],
    "review": "The first bite (and yes it was a bite, not a sip), my mouth was assailed with the taste of smokey meat and a light porter. It was reminiscent of an Italian antipasti - some peppery/smoked thin slices of sausage served with a complex beer all within a glass. After a while, the novelty wore off and the last third was a drag to finish (would have been better as a 330ml). It also, oddly, made my tongue feel a bit numb. Definitely wouldn&#39;t have it again, gets a bonus point for unusualness."
}
---