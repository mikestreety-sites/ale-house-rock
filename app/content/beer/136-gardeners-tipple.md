---json
{
    "title": "Gardeners Tipple",
    "number": "136",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bl6CaPHlvv7/",
    "date": "2018-07-31",
    "permalink": "beer/gardeners-tipple-hogs-back-brewery/",
    "breweries": [
        "brewery/hogs-back-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "This was a lovely beer. Not sure it would be ideal for a post-garden work drink, but tasty regardless"
}
---