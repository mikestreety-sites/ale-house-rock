---json
{
    "title": "Rift Valley",
    "number": "327",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CAN1GxmJQm6/",
    "date": "2020-05-15",
    "permalink": "beer/rift-valley-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [],
    "review": "Despite being 2.8%, this micro pale packed a punch. There is no loss in taste with that low ABV and I&#39;d be happy to drink these all night. A colour that looks like apple juice but a flavour that is fresh."
}
---