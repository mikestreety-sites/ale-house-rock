---json
{
    "title": "Golden Ale",
    "number": "126",
    "rating": "10",
    "canonical": "https://www.instagram.com/p/BlD17EtFiQG/",
    "date": "2018-07-10",
    "permalink": "beer/golden-ale-saltdean-brewery/",
    "breweries": [
        "brewery/saltdean-brewery/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Confession, this isn’t the bottle nor the day I drank this, however I needed something to represent this beer as I forgot to take a photo but I’ve been thinking about it. A lot. Another one from the Saltdean Brewery (of “Drunk Monk” #101 and ESB #108 fame), I drank their Golden Ale at the weekend. Now, I don’t know if this was situational bias (it was sunny and I’d just cooked an amazing BBQ for some wonderful friends) but this beer hit the spot. It was beautiful, well balanced and horrifically tasty. I could have sat and drunk it all afternoon. At 4.5% too, I could have done. Easily one of the best beers I’ve ever had. On top of all that, I was told today that it was the last one - so it gets a bonus exclusivity point."
}
---