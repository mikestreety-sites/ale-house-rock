---json
{
    "title": "Pride of Dartmoor",
    "number": "215",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/BvzTDqlFhJp/",
    "date": "2019-04-03",
    "permalink": "beer/pride-of-dartmoor-black-tor/",
    "breweries": [
        "brewery/black-tor/"
    ],
    "tags": [],
    "review": "Finally, a good beer from this part of the world. Full of flavour and body and a fantastic way to end the day. Cracking pint from @black_tor_brewery"
}
---