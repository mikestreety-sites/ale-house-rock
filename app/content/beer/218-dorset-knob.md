---json
{
    "title": "Dorset Knob",
    "number": "218",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/BwPouFmlQFU/",
    "date": "2019-04-14",
    "permalink": "beer/dorset-knob-dorset-brewing-company/",
    "breweries": [
        "brewery/dorset-brewing-company/"
    ],
    "tags": [],
    "review": "Underwhelming as a beer. Drinkable, with a good(ish) taste but could have been so much better"
}
---