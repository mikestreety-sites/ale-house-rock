---json
{
    "title": "Sea Fog",
    "number": "720",
    "rating": "6",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1207795606",
    "serving": "Can",
    "purchased": "shop/adnams-cellar-and-kitchen-store/",
    "date": "2022-10-02",
    "permalink": "beer/sea-fog-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "This was a bit inconsequential as a beer. It washed over me like the weather it&#39;s named after. It was missing the fruitiness this style normally has bags of"
}
---