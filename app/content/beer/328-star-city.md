---json
{
    "title": "Star City",
    "number": "328",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CAOUcCWpNLM/",
    "date": "2020-05-15",
    "permalink": "beer/star-city-fourpure-brewing-company/",
    "breweries": [
        "brewery/fourpure-brewing-company/"
    ],
    "tags": [
        "ipa"
    ],
    "review": "Followed up the last beer with this absolute beast of a near-pint from @fourpure. Brewed in partnership with @mothership.beer, this double IPA is an absolute stunner. Wouldn&#39;t want too many for these, but was perfect as an evening wind-down and I&#39;m glad I&#39;ve got a second one in the fridge. It was full of hops with a nice deep flavour, Fourpure have done it again."
}
---