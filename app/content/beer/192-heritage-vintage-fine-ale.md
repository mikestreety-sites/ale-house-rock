---json
{
    "title": "Heritage Vintage Fine Ale",
    "number": "192",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/BsG4DDZABC4/",
    "date": "2019-01-01",
    "permalink": "beer/heritage-vintage-fine-ale-greene-king/",
    "breweries": [
        "brewery/greene-king/"
    ],
    "tags": [],
    "review": "A couple of these bottle conditioned beers from @greenekingbrewery appeared in Tesco the other day. A full pint (568ml) and tasty. Believe it would be called a complex flavour. Certainly a slow drinker kind of beer - especially at 6.5%! Would have again"
}
---