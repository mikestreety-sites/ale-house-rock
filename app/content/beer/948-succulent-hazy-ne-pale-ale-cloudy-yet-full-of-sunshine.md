---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1399026827",
  "title": "Succulent & Hazy NE Pale Ale (Cloudy Yet Full of Sunshine)",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-07-14",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4.2%",
  "breweries": [
    "brewery/modest-beer/"
  ],
  "number": 948,
  "permalink": "beer/succulent-hazy-ne-pale-ale-cloudy-yet-full-of-sunshine-modest-beer/",
  "review": "A nice enough beer. Not quite as \"succulent\" as I was expecting, but close enough. Certainly hazy!"
}
---

