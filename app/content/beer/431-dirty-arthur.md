---json
{
    "title": "Dirty Arthur",
    "number": "431",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CLFpZWSlitg/",
    "date": "2021-02-09",
    "permalink": "beer/dirty-arthur-riverside-brewery/",
    "breweries": [
        "brewery/riverside-brewery/"
    ],
    "tags": [],
    "review": "Beer delivery day means beer on a Tuesday. This one tastes exactly how you imagine a beer festival to taste. Slightly cloudy (putting that down to not having time to settle) but an enjoyable pint nonetheless. Taking a sip of this transports you to days of yonder, where you would choose an ale from a stained leaflet purely based on the name, stagger up to beer belly tubbed man and ask for some in your glass. Not quite suited to home drinking, but a good nostalgia trip. @riverside_brewery"
}
---