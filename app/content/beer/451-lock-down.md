---json
{
    "title": "Lock Down",
    "number": "451",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CMafaQUl_K_/",
    "date": "2021-03-14",
    "permalink": "beer/lock-down-1-brewdog/",
    "breweries": [
        "brewery/brewdog/"
    ],
    "tags": [],
    "review": "An interesting beer from @brewdogofficial. Thought I would go straight out the gates from the Lockdown Survival Kit with this Lock Down beer (their inconsistent spelling of Lockdown irks me). This &quot;cream soda pilsner&quot; is definitely a novelty drink. It&#39;s a &quot;I drank it for the experience&quot; kind of drink. I definitely wouldn&#39;t buy it again and I would probably say no if offered one. It tastes very much like cream soda, took me back to my childhood, with an alcoholic twist. Would recommend for the nostalgia but that&#39;s about it."
}
---