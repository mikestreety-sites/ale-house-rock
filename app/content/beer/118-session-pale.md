---json
{
    "title": "Session Pale",
    "number": "118",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/BkV0uR_FJZs/",
    "date": "2018-06-22",
    "permalink": "beer/session-pale-nick-staffords-hambleton-ales/",
    "breweries": [
        "brewery/nick-staffords-hambleton-ales/"
    ],
    "tags": [],
    "review": "Far too hoppy for me. Struggled through this one"
}
---