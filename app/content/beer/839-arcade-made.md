---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1307435659",
  "title": "Arcade Made",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/sainsburys/",
  "date": "2023-08-24",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/brewdog/"
  ],
  "number": 839,
  "permalink": "beer/arcade-made-brewdog/",
  "review": "A surprisingly good Brewdog beer. As thick and juicy as you would expect from a mass produced DDH IPA. Very drinkable and a nice evening drink."
}
---

