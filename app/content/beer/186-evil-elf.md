---json
{
    "title": "Evil Elf",
    "number": "186",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BrdSfCdgBh0/",
    "date": "2018-12-16",
    "permalink": "beer/evil-elf-rudgate/",
    "breweries": [
        "brewery/rudgate/"
    ],
    "tags": [],
    "review": "Was disappointed about this supposedly festive beer. Although noticed that it is “tropical” apparently. a bit better than the beer the other day. Not enough for an extra point"
}
---