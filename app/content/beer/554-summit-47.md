---json
{
    "title": "Summit 47",
    "number": "554",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CShweQYq_03/",
    "date": "2021-08-13",
    "permalink": "beer/summit-47-st-austell-brewery/",
    "breweries": [
        "brewery/st-austell-brewery/"
    ],
    "tags": [],
    "review": "Wasn&#39;t so keen on this Small Batch Brews beer from the big brewery. Tasted a bit cheap and had a post-sip pang that wasn&#39;t for me. I finished it, but can&#39;t say I&#39;d be picking up one of these again."
}
---