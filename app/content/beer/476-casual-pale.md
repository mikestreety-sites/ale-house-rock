---json
{
    "title": "Casual Pale",
    "number": "476",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/COGrN5olQm0/",
    "date": "2021-04-25",
    "permalink": "beer/casual-pale-unbarred/",
    "breweries": [
        "brewery/unbarred-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "I&#39;ve had a few @unbarredbrewery beers now, most of them being the &quot;novelty&quot; ones, so I picked this one up in @palate.bottleshop to see what a &quot;normal&quot; brew of theirs is like and, damn, it&#39;s smooth. Had this while preparing and eating the Sunday roast and it was spot on. Flavoursome and very easy drinking. Would easily have polished off a couple of pints of this."
}
---