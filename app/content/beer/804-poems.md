---json
{
    "title": "Poems",
    "number": "804",
    "rating": "9",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1277446293",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2023-05-25",
    "permalink": "beer/poems-beak-brewery-verdant-brewing-co/",
    "breweries": [
        "brewery/beak-brewery/",
        "brewery/verdant-brewing-company/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "If Beak and Verdant collaborate on anything, it&#39;s going to be awesome. Lush DIPA which hit everything on the head after a long drive back from holiday."
}
---