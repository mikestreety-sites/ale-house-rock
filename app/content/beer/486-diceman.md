---json
{
    "title": "Diceman",
    "number": "486",
    "rating": "3",
    "canonical": "https://www.instagram.com/p/COlkW2OFAmd/",
    "date": "2021-05-07",
    "permalink": "beer/diceman-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "How can a brewery which gets IPAs and, in fact, every other type of beer so right get a stout so wrong. I could just about taste the chocolate coffee notes that are so often found in a stout below the slightly watery flavour. Didn&#39;t really enjoy this beer at all, which is a shame really. I did manage to finish it though."
}
---