---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1417884666",
  "title": "How Be Astro",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-09-15",
  "style": "IPA - Imperial / Double",
  "abv": "7.5%",
  "breweries": [
    "brewery/quantock-brewery/"
  ],
  "number": 970,
  "permalink": "beer/how-be-astro-quantock-brewery/",
  "review": "What a wonderfully fluffy NEIPA. Juicy and full bodied, it was a bit full-faced with the actual taste of alcohol, but was pretty good except that."
}
---

