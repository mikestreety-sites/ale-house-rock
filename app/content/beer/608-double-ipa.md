---json
{
    "title": "10 4 Brewing - Double IPA",
    "number": "608",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CZZIm8HqdNg/",
    "date": "2022-01-31",
    "permalink": "beer/double-ipa-10-4-brewing/",
    "breweries": [
        "brewery/aldi/"
    ],
    "tags": [],
    "review": "This caught me a bit off guard as it didn&#39;t have any DIPA characteristics except for the 6.4% written on the can. It tasted like a &quot;good&quot; Aldi IPA, same style and mouthfeel as their other offerings but with a bit more flavour."
}
---