---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1317102648",
  "title": "Tall Man's Wetsuit",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/sureshot-brewing/",
  "date": "2023-09-24",
  "style": "IPA - Imperial / Double",
  "abv": "8%",
  "breweries": [
    "brewery/sureshot-brewing/"
  ],
  "number": 850,
  "permalink": "beer/tall-mans-wetsuit-sureshot-brewing/",
  "review": "This was good, but nowhere near as good as Sureshot's other beers, especially the smaller pale ale. I was bowled over by the DIPA and drank it uneventfully. Not to be turned away though"
}
---

