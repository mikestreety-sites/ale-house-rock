---json
{
    "title": "Courage Directors",
    "number": "20",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BONfj9oA_1V/",
    "serving": "Bottle",
    "date": "2016-12-19",
    "permalink": "beer/courage-directors-eagle-brewery/",
    "breweries": [
        "brewery/eagle-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Firstly I have a couple of apologies to get out the way! 1) sorry for being a day late (was busy coming second in a pub quiz yesterday) 2) sorry for the poor lighting in the photo - I&#39;m in the garage 😀 Anyway, on with the beer. It&#39;s a rock solid taste. Can&#39;t go wrong. Would drink for days!"
}
---