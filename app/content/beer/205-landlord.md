---json
{
    "title": "Landlord",
    "number": "205",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BttezCalmAJ/",
    "date": "2019-02-10",
    "permalink": "beer/landlord-timothy-taylor/",
    "breweries": [
        "brewery/timothy-taylors-brewery/"
    ],
    "tags": [],
    "review": "One of my dad&#39;s favourite beers, I believe, and a good one too. Timothy Taylor have done an excellent job with this one"
}
---
