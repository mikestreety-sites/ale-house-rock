---json
{
    "title": "American Pale Ale",
    "number": "201",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bs_UgcbAeZx/",
    "date": "2019-01-23",
    "permalink": "beer/american-pale-ale-long-man-brewery/",
    "breweries": [
        "brewery/long-man-brewery/"
    ],
    "tags": [],
    "review": "A slightly better Long Man beer, one I could have a few pints of, of an evening. If you’re coming round, I won’t say no to this accompanying you"
}
---