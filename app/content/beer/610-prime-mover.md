---json
{
    "title": "Prime Mover",
    "number": "610",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CZnLYTSqEan/",
    "date": "2022-02-05",
    "permalink": "beer/prime-mover-arundel-brewery/",
    "breweries": [
        "brewery/arundel-brewery/"
    ],
    "tags": [],
    "review": "Enjoyable pale ale which went lovely with a takeaway curry. Nothing groundbreaking but a good beer and a fantastic can design"
}
---