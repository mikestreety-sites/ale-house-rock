---json
{
    "title": "Golden Beer",
    "number": "470",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CNvbeJjpDWp/",
    "date": "2021-04-16",
    "permalink": "beer/golden-beer-bankss/",
    "breweries": [
        "brewery/bankss/"
    ],
    "tags": [],
    "review": "I&#39;ve seen the Amber bitter Banks&#39;s do in all the budget supermarkets and it&#39;s a great staple beer for 80p. This is the first time I&#39;ve seen the golden offering from the brewery. Simplistic in taste and I much prefer the amber one. This one tastes like a £1 beer."
}
---