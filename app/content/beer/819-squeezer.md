---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1290176376",
  "title": "Squeezer",
  "serving": "Can",
  "rating": 5,
  "date": "2023-07-02",
  "style": "Pale Ale - American",
  "abv": "4.5%",
  "breweries": [
    "brewery/by-the-horns-brewing-co/"
  ],
  "number": 819,
  "permalink": "beer/squeezer-by-the-horns-brewing-co/",
  "review": "What a very mediocre beer this is. Tasted better once it had warmed up slightly. It was a light IPA, but was missing complexity, depth and a confident flavour. A middle of the road, average beer."
}
---
