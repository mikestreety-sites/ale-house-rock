---json
{
    "title": "Gose to Hollywood",
    "number": "385",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/CHi-8YelUzt/",
    "date": "2020-11-13",
    "permalink": "beer/gose-to-hollywood-to-l/",
    "breweries": [
        "brewery/to-l/"
    ],
    "tags": [],
    "review": "Mrs. Ale House picked me this up in M&amp;S and what a disappointment. It reminds me of the &quot;Tart&quot; I had a couple of years ago. It tastes like a cheap orange Fanta knock-off. Had to go down the drain but, as with the tart, it gets a bonus point for Mrs. Ale House liking it"
}
---