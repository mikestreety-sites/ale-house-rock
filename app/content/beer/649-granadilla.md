---json
{
    "title": "Granadilla",
    "number": "649",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CdQnIk6KoRk/",
    "date": "2022-05-07",
    "permalink": "beer/granadilla-siren-craft-brew/",
    "breweries": [
        "brewery/siren-craft-brew/"
    ],
    "tags": [],
    "review": "This fruity pale ale had all the fruits but not body or weight. I appreciate it is meant to be a pale ale, but I would expect some sort of depth there in the flavours. It was nice enough, just not one I would go back to."
}
---