---json
{
    "title": "Winter Special",
    "number": "259",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/B5jAxZTlzNm/",
    "date": "2019-12-01",
    "permalink": "beer/winter-special-hatherwood-craft-beer-company/",
    "breweries": [
        "brewery/lidl/"
    ],
    "tags": [],
    "review": "A Lidl special and a cracking one at that! Wasn&#39;t holding out much hope for this one, but was a lovely little warmer after a night time stroll round @wakehurst_botanic_garden. Very nice pint"
}
---