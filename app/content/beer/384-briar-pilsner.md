---json
{
    "title": "Briar Pilsner",
    "number": "384",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CHiheinljFb/",
    "date": "2020-11-13",
    "permalink": "beer/briar-pilsner-briarbank-brewery/",
    "breweries": [
        "brewery/briarbank-brewery/"
    ],
    "tags": [],
    "review": "A lovely full bodied, crisp pilsner which went well with the homemade Mexican meal yesterday. Wished I&#39;d picked up a few more beers from @briarbankbrewery when I was in Suffolk."
}
---