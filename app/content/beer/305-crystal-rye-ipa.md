---json
{
    "title": "Crystal Rye IPA",
    "number": "305",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/B_BLorNpzKs/",
    "date": "2020-04-15",
    "permalink": "beer/crystal-rye-ipa-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": ""
}
---