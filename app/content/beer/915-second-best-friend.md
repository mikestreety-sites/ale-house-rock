---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1371877893",
  "title": "Second Best Friend",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/the-fussclub/",
  "date": "2024-04-13",
  "style": "IPA - New England / Hazy",
  "abv": "7%",
  "breweries": [
    "brewery/phantom-brewing-co/"
  ],
  "number": 915,
  "permalink": "beer/second-best-friend-phantom-brewing-co/",
  "review": "A lovely full-bodied fluffy IPA. At 7%, you'd expect this body and flavour from a DIPA, not quite sure how they can make it this thick without it being a double. Picked the can entirely based on the artwork, and it did not disappoint."
}
---

