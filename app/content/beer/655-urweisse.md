---json
{
    "title": "Urweisse",
    "number": "655",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CdteYtQqA4K/",
    "date": "2022-05-18",
    "permalink": "beer/urweisse-ayinger-brewery/",
    "breweries": [
        "brewery/ayinger-brewery/"
    ],
    "tags": [],
    "review": "This beer was a lot smoother (and darker) than I was expecting for a wheat beer. I got out the chunky tankard as it felt like that kind of European-style pint. It was missing the harsh wheat-beer sting which made it very easy to drink. An unexpected golden egg."
}
---