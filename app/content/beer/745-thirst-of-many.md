---json
{
    "title": "Thirst of Many",
    "number": "745",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1228936563",
    "serving": "Can",
    "purchased": "shop/palate-bottle-shop/",
    "date": "2022-12-11",
    "permalink": "beer/thirst-of-many-allkin-brewing/",
    "breweries": [
        "brewery/allkin-brewing-brewery/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A nice, drinkable pale ale but was missing a bit of depth for my liking. It was interesting hearing about their history."
}
---
