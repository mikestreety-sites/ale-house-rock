---json
{
    "title": "Huckaback",
    "number": "545",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CSAp2J0KaD-/",
    "date": "2021-08-01",
    "permalink": "beer/huckaback-salt-beer-factory/",
    "breweries": [
        "brewery/salt-beer-factory/"
    ],
    "tags": [],
    "review": "These supermarket NEIPAs really remind you that you can still get good beer in Tesco, but that you can also get great beer from smaller breweries. This was a mass-produced passable New England IPA. Not sure I would to buy it again and there are certainly better NEIPAs out there"
}
---