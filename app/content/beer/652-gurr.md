---json
{
    "title": "Gurr",
    "number": "652",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/Cdic6QkKuUd/",
    "date": "2022-05-14",
    "permalink": "beer/gurr-beak-brewery/",
    "breweries": [
        "brewery/beak-brewery/"
    ],
    "tags": [],
    "review": "Lovely smooth West Coast IPA from @thebeakbrewery. You could taste the 7% but it was not overpowering - very moorish!"
}
---