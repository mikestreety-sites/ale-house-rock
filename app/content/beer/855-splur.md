---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1319854421",
  "title": "SPLUR",
  "serving": "Can",
  "rating": 9,
  "purchased": "shop/overtone-brewing-co/",
  "date": "2023-10-05",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/overtone-brewing-co/",
    "brewery/pollys-brew-co/"
  ],
  "number": 855,
  "permalink": "beer/splur-overtone-brewing-co-pollys-brew-co/",
  "review": "(I started recycling the can before I remembered to take the photo!) This was a lush DIPA. Full-bodied and thick, it's definitely a slow sipper but was worthy of the 8% ABV it carries with it."
}
---

