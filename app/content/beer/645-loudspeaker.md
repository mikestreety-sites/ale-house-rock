---json
{
    "title": "Loudspeaker",
    "number": "645",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Ccsk575q6YB/",
    "date": "2022-04-23",
    "permalink": "beer/loudspeaker-lervig/",
    "breweries": [
        "brewery/lervig-aktiebryggeri/"
    ],
    "tags": [],
    "review": "There were a few Lervig beers in my recent @beermerchants mystery box and they have been a bit of a mixed bag. Fortunately, this session IPA was a good &#39;un."
}
---