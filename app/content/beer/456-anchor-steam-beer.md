---json
{
    "title": "Anchor Steam Beer",
    "number": "456",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CM29QSLpBlT/",
    "date": "2021-03-25",
    "permalink": "beer/anchor-steam-beer-anchor-brewing/",
    "breweries": [
        "brewery/anchor-brewing/"
    ],
    "tags": [],
    "review": "A brew from 1896 (I assume the recipe, not the beer), this random beer was picked up in Asda last week. Crisp and inoffensive, it was the kind of beer you would expect to find in a Gastro pub or wine bar, when they want some &quot;cool&quot; token beers in. It had some flavour, but nothing to shout about."
}
---