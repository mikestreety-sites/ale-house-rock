---json
{
    "title": "Session Pale",
    "number": "609",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CZfl3-8qw1J/",
    "date": "2022-02-02",
    "permalink": "beer/session-pale-north-brewing-co/",
    "breweries": [
        "brewery/north-brewing-co/"
    ],
    "tags": [],
    "review": "Nice, soft fruity little number. A lot better cold than room temperature (I left some out while I was putting the kids to bed). East drinking and a good start to the @northbrewco haul"
}
---