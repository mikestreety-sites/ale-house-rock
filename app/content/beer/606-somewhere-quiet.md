---json
{
    "title": "Somewhere Quiet?",
    "number": "606",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CZU-XEgKz7E/",
    "date": "2022-01-29",
    "permalink": "beer/somewhere-quiet-howling-hops/",
    "breweries": [
        "brewery/howling-hops/"
    ],
    "tags": [],
    "review": "Found myself driving past @thebrewhouseprojectarundel for the first time today. Stopped and picked up a few beers as I&#39;d not had any new ones in a while. A tasty enough Pale Ale - very drinkable but not a huge deal of substance underneath."
}
---