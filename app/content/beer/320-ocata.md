---json
{
    "title": "Ocata",
    "number": "320",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B_7APwTJsk4/",
    "date": "2020-05-08",
    "permalink": "beer/ocata-garage-beer-company/",
    "breweries": [
        "brewery/garage-beer-company/"
    ],
    "tags": [],
    "review": "Was good to get a craft beer in a big can for a change. This IPA was lovely and smooth and went down very well while having a video chat with friends I&#39;d normally find in a pub. It was missing &quot;something&quot; but I would certainly drink it again and could have drunk some more."
}
---