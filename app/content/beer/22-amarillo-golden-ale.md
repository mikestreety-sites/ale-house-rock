---json
{
    "title": "Amarillo Golden Ale",
    "number": "22",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BOP7dgzAqyJ/",
    "serving": "Bottle",
    "date": "2016-12-20",
    "permalink": "beer/amarillo-golden-ale-greenwich-brewing-company-marks-and-spencer/",
    "breweries": [
        "brewery/marks-and-spencer/",
        "brewery/meantime-brewing-company/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Hoppy. Alright I suppose. Weird aftertaste."
}
---