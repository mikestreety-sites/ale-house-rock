---json
{
    "title": "Goram",
    "number": "113",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BkDjvrlFuhX/",
    "date": "2018-06-15",
    "permalink": "beer/goram-butcombe-brewing-company/",
    "breweries": [
        "brewery/butcombe-brewing-company/"
    ],
    "tags": [],
    "review": "This light ale is lovely (despite the rude Brewery name @butcombe) and crisp. Refreshing and tasty"
}
---