---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1446099000",
  "title": "Delirium Tremens",
  "serving": "Bottle",
  "rating": 7,
  "purchased": "shop/tesco/",
  "date": "2024-12-29",
  "style": "Belgian Strong Golden Ale",
  "abv": "8.5%",
  "breweries": [
    "brewery/delirium-huyghe-brewery/"
  ],
  "number": 1002,
  "permalink": "beer/delirium-tremens-delirium-huyghe-brewery/",
  "review": "A nice smooth strong beer - not quite the fullness I would expect from a Belgian beer though it was still enjoyable."
}
---

