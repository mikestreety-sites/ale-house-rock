---json
{
    "title": "Shameless",
    "number": "663",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CeJ1XuhqQvp/",
    "date": "2022-05-29",
    "permalink": "beer/shameless-redwillow-brewery/",
    "breweries": [
        "brewery/redwillow-brewery/"
    ],
    "tags": [],
    "review": "A nice enough West Coast IPA. Took a few sips to get into it."
}
---