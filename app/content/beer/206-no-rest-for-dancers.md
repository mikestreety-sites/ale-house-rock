---json
{
    "title": "No rest for dancers",
    "number": "206",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BuzRLMIFxnk/",
    "date": "2019-03-09",
    "permalink": "beer/no-rest-for-dancers-lost-and-grounded/",
    "breweries": [
        "brewery/lost-and-grounded/"
    ],
    "tags": [
        "redale"
    ],
    "review": "Small brewery (@lostandgroundedbrewers)  with a big taste. Lovely red ale that tastes like a brewery. Full of depth and flavour"
}
---