---json
{
    "title": "Bohemian Gold",
    "number": "301",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/B-vIv9vJZfk/",
    "date": "2020-04-08",
    "permalink": "beer/bohemian-gold-loud-shirt-brewing-company/",
    "breweries": [
        "brewery/loud-shirt-brewing-company/"
    ],
    "tags": [],
    "review": "With a delivery fresh off the truck (we&#39;re in lockdown, but it seems beer delivery is deemed &quot;essential&quot;), it would have been rude to wait until the weekend to try at least one of the beers from @loudshirtbeer (I can confirm the driver lived up to the company name). Lovely, light beer with a good flavour. Something I can&#39;t quite put my finger on keeps it just shy of an 8, but still a lovely tipple"
}
---