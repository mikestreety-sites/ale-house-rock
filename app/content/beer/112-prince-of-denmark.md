---json
{
    "title": "Prince of Denmark",
    "number": "112",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BjnV4zdlugN/",
    "date": "2018-06-04",
    "permalink": "beer/prince-of-denmark-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [
        "stout"
    ],
    "review": "This 7.5% is a dark syrupy Christmas style ale. Heavy and certainly not a summer ale or one for a warm day. Coffee under/over tones. Fortunately sharing with @geoffst so only need drink half of this half pint."
}
---