---json
{
    "title": "Jester IPA",
    "number": "168",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/BploSqrAxpj/",
    "date": "2018-10-31",
    "permalink": "beer/jester-ipa-adnams/",
    "breweries": [
        "brewery/adnams/"
    ],
    "tags": [],
    "review": "Lovely smooth tasty beer brewed by @adnams for Marks and Spencer. Went down rather quick, so it must have been good!"
}
---