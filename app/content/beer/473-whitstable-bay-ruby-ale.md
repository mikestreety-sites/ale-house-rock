---json
{
    "title": "Whitstable Bay Ruby Ale",
    "number": "473",
    "rating": "4",
    "canonical": "https://www.instagram.com/p/CN9fxSxl8Pi/",
    "date": "2021-04-22",
    "permalink": "beer/whitstable-bay-ruby-ale-shepherd-neame/",
    "breweries": [
        "brewery/shepherd-neame/"
    ],
    "tags": [],
    "review": "Slightly disappointed with this as i thought I was a fan of the original (although never reviewed). It tasted a bit cheap and left an acidic taste in your mouth."
}
---