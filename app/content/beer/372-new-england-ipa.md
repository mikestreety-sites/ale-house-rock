---json
{
    "title": "New England IPA",
    "number": "372",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CF4k6QWluCx/",
    "date": "2020-10-03",
    "permalink": "beer/new-england-ipa-brewdog-cloudwater/",
    "breweries": [
        "brewery/brewdog/",
        "brewery/cloudwater/"
    ],
    "tags": [],
    "review": "Had this been 6 months ago, I wouldn&#39;t have been seen dead with a craft beer in my fridge. Lockdown, however, seems to have altered my perspective on such beer. This collaboration between @brewdog and @cloudwaterbrew is an absolute corker. Unexpected sediment at the end meant I couldn&#39;t quite get the last mouthful I was expecting, but good nonetheless. Perfect accompaniment to Tony Hawk&#39;s 1 &amp; 2 on PS4"
}
---