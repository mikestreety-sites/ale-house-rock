---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1417889332",
  "title": "Wills Neck",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-09-15",
  "style": "Blonde / Golden Ale - Other",
  "abv": "4.3%",
  "breweries": [
    "brewery/quantock-brewery/"
  ],
  "number": 971,
  "permalink": "beer/wills-neck-quantock-brewery/",
  "review": "This was a refreshing golden ale. Nice and crisp and light and reminded me of a Golden Hen or Hobgoblin Gold. Immediately quaffable"
}
---

