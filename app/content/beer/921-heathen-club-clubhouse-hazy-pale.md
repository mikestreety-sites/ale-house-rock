---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1376064260",
  "title": "HEATHEN CLUB // CLUBHOUSE HAZY PALE",
  "serving": "Can",
  "rating": 7.5,
  "purchased": "shop/sainsburys/",
  "date": "2024-04-28",
  "style": "Pale Ale - New England / Hazy",
  "abv": "4.6%",
  "breweries": [
    "brewery/northern-monk/"
  ],
  "number": 921,
  "permalink": "beer/heathen-club-clubhouse-hazy-pale-northern-monk/",
  "review": "Very reminiscent of \"Faith\", their flagship hazy ale. This was easy drinking and a good staple to have in the fridge. It's very on par with a Brewdog Hazy Jane"
}
---

