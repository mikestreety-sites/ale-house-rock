---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1351852203",
  "title": "Dragon Stout",
  "serving": "Bottle",
  "rating": 7,
  "date": "2024-01-27",
  "style": "Stout - Foreign / Export",
  "abv": "7.5%",
  "breweries": [
    "brewery/desnoes-geddes/"
  ],
  "number": 896,
  "permalink": "beer/dragon-stout-desnoes-geddes/",
  "review": "This stout was surprisingly smooth, with a taste that was pleasant without being too overwhelming. The 284ml was perfect, any more and it would have been a slog. Great for watching the traitors final."
}
---

