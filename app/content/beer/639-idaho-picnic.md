---json
{
    "title": "Idaho Picnic",
    "number": "639",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CcQyg2hqjwT/",
    "date": "2022-04-12",
    "permalink": "beer/idaho-picnic-lervig/",
    "breweries": [
        "brewery/lervig-aktiebryggeri/"
    ],
    "tags": [],
    "review": "A refreshing 5.8% double dry-hopped pale ale. A bit of a tang in the aftertaste, but it&#39;s not too off-putting. Would session this of an evening."
}
---