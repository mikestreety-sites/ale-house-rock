---json
{
    "title": "Honeycomb Milkshake",
    "number": "438",
    "rating": "8",
    "canonical": "https://www.instagram.com/p/CLkjU3hlfa6/",
    "date": "2021-02-21",
    "permalink": "beer/honeycomb-milkshake-unbarred-bison-beer/",
    "breweries": [
        "brewery/bison-beer/",
        "brewery/unbarred-brewery/"
    ],
    "tags": [],
    "review": "How the hell did they make this beer taste like a milkshake?! The first few sips threw me right into a &#39;shake flashback. Once my taste-buds had settled, this wonderfully easy-drinking beer ticked a lot of my boxes. Despite being great tasting, I wouldn&#39;t add it to my regular &quot;buy again&quot; list. It&#39;s a one-off beer, a birthday treat. Certainly not one for everyday. A great beer though 🐝"
}
---