---json
{
    "title": "Snark",
    "number": "705",
    "rating": "5",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1195061252",
    "serving": "Bottle",
    "purchased": "shop/boojum-and-snark/",
    "date": "2022-08-25",
    "permalink": "beer/snark-boojum-and-snark/",
    "breweries": [
        "brewery/boojum-and-snark/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "A very pale bitter and very classic in its bittery-beer taste. A bit too hoppy and light for me - I think in the days gone by I would have liked this, as it is a good entry beer for lager heads."
}
---