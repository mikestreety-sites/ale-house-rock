---json
{
    "title": "Passion Fruit Pale",
    "number": "728",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1214431113",
    "serving": "Can",
    "purchased": "shop/asda/",
    "date": "2022-10-23",
    "permalink": "beer/passion-fruit-pale-seven-bro7hers-brewery/",
    "breweries": [
        "brewery/seven-bro7hers-brewery/"
    ],
    "tags": [],
    "review": "It&#39;s really hard to review this as a beer, because it tasted more like a tropical squash than a beer. It was nice, but didn&#39;t really satisfy my beer craving."
}
---