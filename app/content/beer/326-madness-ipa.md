---json
{
    "title": "Madness IPA",
    "number": "326",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CALvkaLJvtZ/",
    "date": "2020-05-14",
    "permalink": "beer/madness-ipa-the-wild-beer-company/",
    "breweries": [
        "brewery/the-wild-beer-company/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "A much better beer from @wildbeerco than yesterday&#39;s one. Many hops with a classic bitter taste (and a 6% ABV), this beer went down rather well. A bit too hoppy for my taste, but I appreciated a good beer"
}
---