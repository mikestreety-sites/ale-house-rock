---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1437977720",
  "title": "Plenty Motueka",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/beer-no-evil/",
  "date": "2024-12-01",
  "style": "IPA - Imperial / Double New England / Hazy",
  "abv": "8%",
  "breweries": [
    "brewery/floc/"
  ],
  "number": 994,
  "permalink": "beer/plenty-motueka-floc/",
  "review": "This beer was tasty, but a bit too fruity for me. There wasn't the bite of an IPA, just the fluffy fruits. It just meant it was a little underwhelming and felt like I was drinking a tropical juice instead of a beer."
}
---

