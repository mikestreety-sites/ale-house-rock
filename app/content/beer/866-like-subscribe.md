---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1328347890",
  "title": "Like & Subscribe",
  "rating": 8,
  "purchased": "shop/palate-bottle-shop/",
  "date": "2023-11-04",
  "style": "IPA - New England / Hazy",
  "abv": "7.2%",
  "breweries": [
    "brewery/unbarred-brewery/"
  ],
  "number": 866,
  "permalink": "beer/like-subscribe-unbarred-brewery/",
  "review": "Described as a \"juice bomb\" on the can, the description was apt. Thick and juicy and full of flavour, it was a highly enjoyable experience."
}
---
