---json
{
    "title": "NORTH VS SOUTH // NORTH DDH IPA",
    "number": "740",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1224131363",
    "serving": "Can",
    "purchased": "shop/tesco/",
    "date": "2022-11-25",
    "permalink": "beer/north-vs-south-north-ddh-ipa-neon-raptor-brewing-co-northern-monk-overtone-brewing-co/",
    "breweries": [
        "brewery/neon-raptor-brewing-co/",
        "brewery/northern-monk/",
        "brewery/overtone-brewing-co/"
    ],
    "tags": [
        "dipa"
    ],
    "review": "The &quot;North&quot; to the previous &quot;South&quot; I had. Not as thick or fruity as its counterpart. Tasty, but you could taste the 7% a lot more too. A good, solid DDH IPA"
}
---