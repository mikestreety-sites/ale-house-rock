---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1442646315",
  "title": "Whamageddon",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/tesco/",
  "date": "2024-12-20",
  "style": "IPA - New England / Hazy",
  "abv": "7%",
  "breweries": [
    "brewery/vocation-brewery/"
  ],
  "number": 999,
  "permalink": "beer/whamageddon-vocation-brewery/",
  "review": "A supermarket DDH IPA which is actually pretty tasty. I couldn't skip this beer this festive period and I'm glad I didn't. It was soft with a beer kick at the end and was enjoyable - especially for the price!"
}
---

