---json
{
    "title": "Premium Best Bitter",
    "number": "120",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/Bkapsh6lLYm/",
    "date": "2018-06-24",
    "permalink": "beer/premium-best-bitter-slaters-handcrafted-ales/",
    "breweries": [
        "brewery/slaters-handcrafted-ales/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "Lovely standard beer. Does the job"
}
---