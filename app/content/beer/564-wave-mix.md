---json
{
    "title": "Wave Mix",
    "number": "564",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CTcksLQKgH8/",
    "date": "2021-09-05",
    "permalink": "beer/wave-mix-pollys-brew-co/",
    "breweries": [
        "brewery/pollys-brew-co/"
    ],
    "tags": [],
    "review": "I feel like my reviews of these Polly&#39;s beers are all the same; good flavours but too much of an alcohol taste for my liking. I feel like this Australian brewery brew their beers in a style not for me."
}
---
