---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1305375105",
  "title": "Paraaade",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/the-beak-brewery/",
  "date": "2023-08-17",
  "style": "IPA - Triple New England / Hazy",
  "abv": "10%",
  "breweries": [
    "brewery/beak-brewery/"
  ],
  "number": 836,
  "permalink": "beer/paraaade-beak/",
  "review": "I ended up getting one of the first cans of this TIPA out of the brewery. Straight away the 10% hits you in the face, but by the last sip you're mellow enough to not notice it. I think a DIPA of this would be spot on. It's a long evening sipper for sure."
}
---
