---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1417750031",
  "title": "Brewed Awakening",
  "serving": "Can",
  "rating": 7,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-09-15",
  "style": "Pale Ale - American",
  "abv": "3.4%",
  "breweries": [
    "brewery/quantock-brewery/"
  ],
  "number": 969,
  "permalink": "beer/brewed-awakening-quantock-brewery/",
  "review": "This was pretty good for a Pale Ale. It was light and very drinkable but still had some good flavours. Was slightly too \"watery\"/thin to really hit the mark but I could very (very) easily drink a few of these in quick succession"
}
---

