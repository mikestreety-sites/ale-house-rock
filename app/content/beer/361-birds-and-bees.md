---json
{
    "title": "Birds and Bees",
    "number": "361",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CC9brXGJtq5/",
    "date": "2020-07-22",
    "permalink": "beer/birds-and-bees-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "Another &quot;meh&quot; beer from Flavourly. However tempting, I must never buy from them again. It&#39;s just a box of bland beers."
}
---