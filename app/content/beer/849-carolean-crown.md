---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1317100618",
  "title": "Carolean Crown",
  "serving": "Bottle",
  "rating": 6,
  "date": "2023-09-24",
  "style": "Pale Ale - English",
  "abv": "3.7%",
  "breweries": [
    "brewery/harveys-brewery/"
  ],
  "number": 849,
  "permalink": "beer/carolean-crown-harveys-brewery/",
  "review": "Not the standard I've come to expect from Harvey's, this Pale Ale fell a little bit flat. I was excited to revisit my roots of an Ale reviewer, but this beer felt like a run-of-the-mill ale from a supermarket"
}
---

