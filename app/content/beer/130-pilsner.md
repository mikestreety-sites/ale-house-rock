---json
{
    "title": "Pilsner",
    "number": "130",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/BlXMAWuFsqZ/",
    "date": "2018-07-18",
    "permalink": "beer/pilsner-open-gate-brewery/",
    "breweries": [
        "brewery/open-gate-brewery/"
    ],
    "tags": [
        "pilsner"
    ],
    "review": "this beer is from the same brewery as Hop House and Guinness. I did pie some into a shot glass to show the colour but I then forgot to take a picture. Slightly watery in comparison to hop house and not as much flavour. Won’t be buying again"
}
---