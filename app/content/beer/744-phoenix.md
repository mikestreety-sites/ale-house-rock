---json
{
    "title": "Phoenix",
    "number": "744",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1226458138",
    "serving": "Can",
    "purchased": "shop/tesco/",
    "date": "2022-12-03",
    "permalink": "beer/phoenix-buxton-brewery/",
    "breweries": [
        "brewery/buxton-brewery/"
    ],
    "tags": [],
    "review": "Run of the mill, supermarket IPA. Tasty enough to pick one up again, but wouldn&#39;t go on a specific hunt for it"
}
---