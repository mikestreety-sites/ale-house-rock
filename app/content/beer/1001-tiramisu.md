---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1443357234",
  "title": "Tiramisu",
  "serving": "Can",
  "rating": 3,
  "date": "2024-12-22",
  "style": "Stout - Pastry",
  "abv": "6.5%",
  "breweries": [
    "brewery/yonder-brewing-blending/"
  ],
  "number": 1001,
  "permalink": "beer/tiramisu-yonder-brewing-blending/",
  "review": "This was much too sticky and sweet for me - the kind of beer that if it gets on your fingers they are sticky for the rest of the evening. I finished it and you could taste the chocolate, coffee and general tiramisu-ness, but I won't be buying this again."
}
---

