---json
{
    "title": "Pähklipurt",
    "number": "143",
    "rating": "2",
    "canonical": "https://www.instagram.com/p/BmjdMw7FKQY/",
    "date": "2018-08-16",
    "permalink": "beer/pahklipurt-llenaut/",
    "breweries": [
        "brewery/llenaut/"
    ],
    "tags": [
        "stout"
    ],
    "review": "The last one from the craft ale box I ordered and what a disappointment to end on. You can taste the coffee and chocolate in this stout but it tastes so disgusting. It’s rare I have to throw a beer away but this is one of them. I tried it several times over the evening but every time it left a horrible taste in my mouth. Down the drain"
}
---