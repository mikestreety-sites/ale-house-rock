---json
{
    "title": "Don't Mess with Yorkshire",
    "number": "484",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/COd6B4pJdxn/",
    "date": "2021-05-04",
    "permalink": "beer/dont-mess-with-yorkshire-northern-monk/",
    "breweries": [
        "brewery/northern-monk/"
    ],
    "tags": [],
    "review": "A roast dinner calls for a real ale, a bitter, and old man beer. This is exactly what accompanied my Bank Holiday Monday roast - @northernmonk&#39;s answer to the ale. It tasted like a good ale and certainly worked well with the roast potatoes. It had good body and colour and I would certainly drink it again."
}
---