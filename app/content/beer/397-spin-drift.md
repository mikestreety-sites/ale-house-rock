---json
{
    "title": "Spin Drift",
    "number": "397",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CJMoO1blT_k/",
    "date": "2020-12-24",
    "permalink": "beer/spin-drift-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "My first foray into @gunbrewery beers and a good one to start. Full of flavour and a nice light IPA to kick things off, this went down very well for Christmas Eve. Although as 7, this could very easily be an 8 if I was in the right mood or it was sunny and warm!"
}
---