---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1293881727",
  "title": "Glowing Embers",
  "serving": "Can",
  "rating": 4,
  "date": "2023-07-14",
  "style": "IPA - Red",
  "abv": "4.7%",
  "breweries": [
    "brewery/siren-craft-brew/"
  ],
  "number": 826,
  "permalink": "beer/glowing-embers-siren-craft-brew/",
  "review": "I've had this in my fridge for a while as I had a feeling I wasn't going to enjoy it that much. It was passable, I finished it but the last few mouthfuls were a slog. A bit watery and the flavour that was there tasted a bit burnt."
}
---

