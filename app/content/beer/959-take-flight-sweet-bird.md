---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1412634603",
  "title": "Take Flight, Sweet Bird",
  "serving": "Can",
  "rating": 8,
  "purchased": "shop/brewser-online-store/",
  "date": "2024-08-28",
  "style": "IPA - New England / Hazy",
  "abv": "5.8%",
  "breweries": [
    "brewery/pretty-decent-beer-co/"
  ],
  "number": 959,
  "permalink": "beer/take-flight-sweet-bird-pretty-decent-beer-co/",
  "review": "This was a lovely crisp NEIPA. It was soft and juicy with a crisp undertone to remind you it's still beer."
}
---

