---json
{
    "title": "The Shy Retirer",
    "number": "364",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CDmOf17pdZO/",
    "date": "2020-08-07",
    "permalink": "beer/the-shy-retirer-bad-co/",
    "breweries": [
        "brewery/bad-co/"
    ],
    "tags": [],
    "review": "Average micro pale at at 2.8%, it&#39;s a tidy beer for when you want nothing too strong. Bland taste but better than many other low alcohol beers."
}
---