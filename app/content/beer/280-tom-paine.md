---json
{
    "title": "Tom Paine",
    "number": "280",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/B8f6pnHp-vo/",
    "date": "2020-02-13",
    "permalink": "beer/tom-paine-harveys-brewery/",
    "breweries": [
        "brewery/harveys-brewery/"
    ],
    "tags": [
        "bitter"
    ],
    "review": "If I had to pick a beer that was the epitome of an English Bitter, this would probably be it. When non ale drinkers think of a pint of ale, and are asked to describe it, they would ultimately be detailing this. A straight-to-the-point beer, this pint had no frilly flavours, no fancy aftertaste but the result is an excellent drink. Good work @harveysbrewery"
}
---