---json
{
    "title": "Staring at the Stars",
    "number": "538",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CRqV198lDCt/",
    "date": "2021-07-23",
    "permalink": "beer/staring-at-the-stars-lost-pier-brewing/",
    "breweries": [
        "brewery/lost-pier-brewing/"
    ],
    "tags": [],
    "review": "An absolute cracking NEIPA from @lostpierbrewing. Smooth and easy drinking and an absolute winner straight out the fridge on a hot day after work. Had to stop myself from draining this from the glass. So very morish."
}
---