---json
{
    "title": "Chummy Buster",
    "number": "402",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CJW9WJol_OB/",
    "date": "2020-12-28",
    "permalink": "beer/chummy-buster-gun-brewery/",
    "breweries": [
        "brewery/gun-brewery/"
    ],
    "tags": [],
    "review": "This canned best bitter tasted like it should have come from a bottle. A fantastic full-bodied ale which went down exceedingly well with a stew. I was disappointed when I finished it and even more disappointed when I realised I didn&#39;t have anymore. @gunbrewery"
}
---