---json
{
    "title": "West Coast Blues",
    "number": "590",
    "rating": "9",
    "canonical": "https://www.instagram.com/p/CXUa1rhjfzn/",
    "date": "2021-12-10",
    "permalink": "beer/west-coast-blues-burning-sky/",
    "breweries": [
        "brewery/burning-sky/"
    ],
    "tags": [],
    "review": "A lovely soft, very drinkable, IPA which I could easily drink plenty of (although at 6.5% and £5 a pop, I probably shouldn&#39;t). @burningskybeer have smashed out, yet another, absolute corker of a beer"
}
---