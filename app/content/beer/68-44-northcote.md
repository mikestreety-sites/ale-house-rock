---json
{
    "title": "Gourmet Burger Kitchen Pale Ale",
    "number": "68",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BZ6luDvlJFL/",
    "date": "2017-10-06",
    "permalink": "beer/44-northcote-gourmet-burger-kitchen/",
    "breweries": [
        "brewery/laverstoke-park-farm/"
    ],
    "tags": [
        "paleale"
    ],
    "review": "GBK’s very own pale ale.  It’s sweet, refreshing, light. Good choice to go with a burger, even if it was on the pricey side."
}
---