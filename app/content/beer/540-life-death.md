---json
{
    "title": "Life & Death",
    "number": "540",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/CRuO0ESpl0T/",
    "date": "2021-07-24",
    "permalink": "beer/life-death-vocation-brewery/",
    "breweries": [
        "brewery/vocation-brewery/"
    ],
    "tags": [],
    "review": "A clean, crisp, easy-drinking IPA"
}
---