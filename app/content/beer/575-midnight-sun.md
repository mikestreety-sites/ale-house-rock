---json
{
    "title": "Midnight Sun",
    "number": "575",
    "rating": "6",
    "canonical": "https://www.instagram.com/p/CUQfY_Lq3TU/",
    "date": "2021-09-25",
    "permalink": "beer/midnight-sun-williams-brothers-brewing-company/",
    "breweries": [
        "brewery/williams-brothers-brewing-company/"
    ],
    "tags": [],
    "review": "A surprisingly enjoyable porter from this brewery. I wasn&#39;t holding out much hope, but this was light bodied yet still had the coffee undertones you&#39;d expect from this style. A perfect Introductory beer. I wouldn&#39;t buy it again, but would recommend to someone who only drinks light beers and wants to broaden their horizons"
}
---