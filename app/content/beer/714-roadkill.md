---json
{
    "title": "Roadkill",
    "number": "714",
    "rating": "7",
    "canonical": "https://untappd.com/user/mikestreety/checkin/1201066231",
    "serving": "Can",
    "purchased": "shop/sainsburys/",
    "date": "2022-09-11",
    "permalink": "beer/roadkill-mad-squirrel-brewery/",
    "breweries": [
        "brewery/mad-squirrel-brewery/"
    ],
    "tags": [
        "neipa"
    ],
    "review": "Just a bog-standard supermarket NEIPA. Not disgusting, enjoyable but not outstanding."
}
---