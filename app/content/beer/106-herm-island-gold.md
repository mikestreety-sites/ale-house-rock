---json
{
    "title": "Herm Island Gold",
    "number": "106",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BhZkFaYld_D/",
    "date": "2018-04-10",
    "permalink": "beer/herm-island-gold-liberation-brewery/",
    "breweries": [
        "brewery/liberation-brewery/"
    ],
    "tags": [],
    "review": "This is a smooth light beer. Would happily have a full pint next time!"
}
---