---json
{
    "title": "Sucker Punch IPA",
    "number": "220",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BwxDkVjFj5n/",
    "date": "2019-04-27",
    "permalink": "beer/sucker-punch-ipa-octopus-energy/",
    "breweries": [
        "brewery/octopus-energy/"
    ],
    "tags": [],
    "review": "Brewed by @octopus_energy of all people. Lovely light beer, perfect for a summer BBQ. Slightly lacking in a strong flavour though"
}
---