---json
{
    "title": "Oxford Gold",
    "number": "45",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/BTep4N2l2eX/",
    "serving": "Bottle",
    "date": "2017-04-29",
    "permalink": "beer/oxford-gold-brakspear/",
    "breweries": [
        "brewery/brakspear/"
    ],
    "tags": [
        "goldenale"
    ],
    "review": "Sorry about the Carling glass. I thoroughly enjoy Brakspear beers and this is no different. Light and full of goodness"
}
---