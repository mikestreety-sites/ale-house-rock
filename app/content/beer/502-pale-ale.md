---json
{
    "title": "Pale Ale",
    "number": "502",
    "rating": "5",
    "canonical": "https://www.instagram.com/p/CPrBamCpfJU/",
    "date": "2021-06-03",
    "permalink": "beer/pale-ale-toast/",
    "breweries": [
        "brewery/toast-ale/"
    ],
    "tags": [],
    "review": "Second beer from Toast and no where near as good as the session ale. Slight odd aftertaste and missing any depth in the flavour."
}
---