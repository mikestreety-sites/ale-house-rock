---
{
  "canonical": "https://untappd.com/user/mikestreety/checkin/1332703446",
  "title": "NZ Pale",
  "serving": "Can",
  "rating": 6,
  "purchased": "shop/beer-no-evil/",
  "date": "2023-11-19",
  "style": "Pale Ale - New Zealand",
  "abv": "4.8%",
  "breweries": [
    "brewery/simple-things-fermentations/"
  ],
  "number": 873,
  "permalink": "beer/nz-pale-simple-things-fermentations/",
  "review": "A nice enough beer, but nothing outstanding. I would drink it again but I can't say I would rush to buy it again. It was smooth, but not the punch of flavours I was expecting."
}
---

