---json
{
    "title": "One In, All In",
    "number": "696",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/ChItZJ6qwGn/",
    "date": "2022-08-11",
    "permalink": "beer/one-in-all-in-siren-craft-brew-lines-brew-co-the-white-hag-fyne-ales/",
    "breweries": [
        "brewery/fyne-ales/",
        "brewery/lines-brew-co/",
        "brewery/siren-craft-brew/",
        "brewery/the-white-hag/"
    ],
    "tags": [],
    "review": "I think this beer was already out of date when it turned up in my @beermerchants mystery box - even before I forgot it was at the back of my fridge. 5 months after the best before, this red ale was smooth and generally above average in taste and texture. Not astounding, but bordering on enjoyable."
}
---