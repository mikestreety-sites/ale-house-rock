---json
{
    "title": "Ripsaw",
    "number": "124",
    "rating": "7",
    "canonical": "https://www.instagram.com/p/Bkx8wT_FtGj/",
    "date": "2018-07-03",
    "permalink": "beer/ripsaw-wychwood-brewery/",
    "breweries": [
        "brewery/wychwood-brewery/"
    ],
    "tags": [],
    "review": "forgot to post this tasty number from the makers of Hobgoblin the other day. Lovely sipper while listening to football and watching @fiaworldrx."
}
---